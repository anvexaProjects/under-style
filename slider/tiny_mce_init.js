tinyMCE.init({
        mode:"textareas",
        theme:"advanced",
		height: "200",
		//width: "50%",
		language : "ru",
	inline_styles: true,
	convert_urls : false,
	relative_urls : false,
	remove_script_host : false,
	cleanup: true,
	extended_valid_elements:"div[*],p[*]",
	 
	plugins : "pagebreak, style, layer, table, save, advhr, advimage, advlink, emotions, iespell, inlinepopups, insertdatetime, preview, media, searchreplace, print, contextmenu, paste, directionality, fullscreen, noneditable, visualchars, nonbreaking, xhtmlxtras, template, wordcount, advlist, autosave",
	 
	// Theme options
	theme_advanced_buttons1 : "undo, redo, |, bold, italic, underline, |, formatselect, fontselect, fontsizeselect, forecolor, |, code, |",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true
});