<?php
include ("functions.php");
include ("tiny_mce_init.php");
$query_cat = getCat();
?>
<script type="text/javascript" src="../lib/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../slider/tiny_mce_init.js"></script>
<form class="" action="/slider/add_slider.php" method="post">
<h3>Добавить блок слайдера:</h3>
<p><label>Описание:</label></p>
<textarea name="description" cols="" rows=""></textarea>
<br />
<p><label>Ссылка:</label> <input class="ankor_input" name="ankor" type="text" /></p>
<br />
<?php
if($query_cat)
{
?>
<label>Категория:</label>
<select name="cat_id">
	<?php
    while ($cat = mysql_fetch_array($query_cat)) {
		?>
        <option value="<?php echo $cat['id'] ?>"><?php echo $cat['cat_name'] ?></option>
		<?php
	}
	mysql_free_result($query_cat);
}
?>
</select>
<p><input type=submit value="Добавить"></p>
</form>
<br />
<hr />

<h4>Загруженные фото:</h4>

<?php
$sliders = getTree();
//echo '<pre>'; print_r($sliders); echo '</pre>';
?>
<div class="sliders">

<?php foreach ($sliders as $slider) { ?>
<div class="slider">
<br />
	<h2><small>Категория:</small> <?php echo $slider['cat_name'] ?></h2>
    <br />
    <?php
	if($slider['sliders'] != '') {
		foreach ($slider['sliders'] as $slide) { ?>
        	
            <div class="slider_item">
                <h3>Слайдер#<?php echo $slide['id'] ?>:</h3>
                <form class="" action="/slider/delete.php" method="post" enctype="multipart/form-data">
                    <input type="submit" value="Удалить слайдер">
                    <input name="slider_id" type="hidden" value="<?php echo $slide['id'] ?>" />
                </form>
                <br />
                <div class="ankor">Ссылка: <a href="<?php echo $slide['ankor'] ?>"><?php echo $slide['ankor'] ?></a></div>
                <h3>Текст:</h3>
                <div class="description"><?php echo $slide['description'] ?></div>
                <form class="" action="/slider/upload_img.php" method="post" enctype="multipart/form-data">
                    <h3>Фото:</h3>
                    <?php if($slide['image'] != '') { ?>
                    <img style="width:384px; height:auto;" src="../slider/<?php echo $slide['image'] ?>" alt="" />
                    <?php } else { ?>
                    <p>(Рекомендуемые минимальные размеры загружаемого изображения фото - 675 x 330 px.)</p>
                    <p><input type="file" name="imgupload"></p>
                    <?php } ?>
                    <h3>Лого:</h3>
                    <?php if($slide['logo'] != '') { ?>
                    <img style="width:120px; height:auto;" src="../slider/<?php echo $slide['logo'] ?>" alt="" />
                    <?php } else { ?>
                    <p>(Рекомендуемые минимальные размеры загружаемого изображения логотипа - 285 x 135 px.)</p>
                    <p><input type="file" name="logoupload"></p>
                    <?php } ?>
                    <input name="slider_id" type="hidden" value="<?php echo $slide['id'] ?>" />
                    <?php if( empty($slide['image']) || empty($slide['logo']) ) { ?>
                    <p><input type="submit" value="Загрузить фото"></p>
                    <?php } ?>
                </form>
    			<br />
                <hr />
            </div>
            
			
        <?php
        } // foreach $slider['sliders']
	}
	?>
</div>
<?php
} // foreach $sliders
?>
</div>

