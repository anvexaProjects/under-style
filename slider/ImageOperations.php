<?php

class Resize {
	
	public $image_name;
	
	public function img_resize($name, $source, $width, $height, $path_directory, $new_name)
	{
		if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)|(gif)|(GIF)|(png)|(PNG)$/',$name))
		//проверка формата исходного изображения
		{	
				
			$target = $path_directory . $name;
			move_uploaded_file($source, $target);
			//загрузка оригинала в папку $path_directory
			
			if(preg_match('/[.](GIF)|(gif)$/', $name)) {
				$im = imagecreatefromgif($path_directory.$name) ;
				//если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
			}
			if(preg_match('/[.](PNG)|(png)$/', $name)) {
				$im = imagecreatefrompng($path_directory.$name) ;
				//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
			}	
			if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/', $name)) {
				$im = imagecreatefromjpeg($path_directory.$name); 
				//если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
			}
			
			//СОЗДАНИЕ ИЗОБРАЖЕНИЯ И ЕГО ПОСЛЕДУЮЩЕЕ СЖАТИЕ
			
			// создаём исходное изображение на основе 
			// исходного файла и определяем его размеры 
			$w_src = imagesx($im); //вычисляем ширину
			$h_src = imagesy($im); //вычисляем высоту изображения
			
			// создаём пустую квадратную картинку 
			// важно именно truecolor!, иначе будем иметь 8-битный результат 
			$dest = imagecreatetruecolor($width,$height); 
			
			
			// Определяем необходимость преобразования размера так чтоб вписывалась наименьшая сторона
			if( $width<$w_src || $height<$h_src )
				$ratio = max($width/$w_src,$height/$h_src);
			else
				$ratio=1;
			
			if($width/$w_src > $height/$h_src) { // срезать верх и низ
				$dx = 0 ;
				$dy = floor((($h_src - $height) * $ratio) / 2) ; // отступ сверху
			}
			else { // срезать справа и слева
				$dx = floor((($w_src - $width) * $ratio) / 2) ; // отступ слева
				$dy = 0 ;
			}
			// скока пикселов считывать с источника
			$wsrc = floor($width/$ratio) ;  // по ширине
			$hsrc = floor($height/$ratio) ; // по высоте
			
			imagecopyresampled($dest, $im, 0, 0, $dx, $dy, $width, $height, $wsrc, $hsrc);
			
			$date=time(); 
			//вычисляем время в настоящий момент.
			imagejpeg($dest, $path_directory.$new_name.".jpg");
			//сохраняем изображение формата jpg в нужную папку, именем будет текущее время. Сделано, чтобы у не было одинаковых имен.
			//почему именно jpg? Он занимает очень мало места + уничтожается анимирование gif изображения, которое отвлекает пользователя. Не очень приятно читать его комментарий, когда краем глаза замечаешь какое-то движение.
			$this->image_name = $path_directory.$new_name.".jpg";
			
			//заносим в переменную путь.
			$delfull = $path_directory.$name; 
			unlink ($delfull);
			//удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
		}
		else 
		{
			//в случае несоответствия формата, выдаем соответствующее сообщение
			exit ("Должен быть формат <strong>JPG,GIF или PNG</strong>"); //останавливаем выполнение сценариев
		}
		
		return true;
	}

}
?>
