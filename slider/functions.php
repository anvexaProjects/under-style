<?php
include ("bd.php");

function getCat()
{
	$sliders_cat = mysql_query("SELECT * FROM slider_cat");
	return $sliders_cat;
}

function getTree()
{
	$sql = "
		SELECT *
		FROM `slider_cat`
	";
	$query = mysql_query($sql) or die(mysql_error());
	$i = 0;
	while ($row = mysql_fetch_assoc($query)) {
		$sliders_cat[$i] = $row;
		$sliders_cat[$i]['sliders'] = getSliders($row['id']);
		$i++;
	}
	return $sliders_cat;
}

function getSliders($id)
{
	$sliders = array();
	$sql = "
		SELECT *
		FROM `slider`
		WHERE `cat_id` = $id
		ORDER BY `id` DESC
	";
	$query = mysql_query($sql) or die(mysql_error());
	$i = 0;
	while ($row = mysql_fetch_assoc($query)) {
		$sliders[$i] = $row;
		$sliders[$i]['image'] = getImg($row['id']);
		$sliders[$i]['logo'] = getLogo($row['id']);
		$i++;
	}
	return $sliders;
}

function getImg($id)
{
	$row = array();
	$sql = "
		SELECT image
		FROM `slider_img`
		WHERE `slider_id` = $id
	";
	$query = mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_row($query);
	return $row[0];
}

function getLogo($id)
{
	$row = array();
	$sql = "
		SELECT image
		FROM `slider_logo`
		WHERE `slider_id` = $id
	";
	$query = mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_row($query);
	return $row[0];
}

function getImages($slider_id)
{
	$images = array();
	$sql_img = "
		SELECT image
		FROM `slider_img`
		WHERE `slider_id` = $slider_id
	";
	$query_img = mysql_query($sql_img) or die(mysql_error());
	$img = mysql_fetch_row($query_img);
	$sql_logo = "
		SELECT image
		FROM `slider_logo`
		WHERE `slider_id` = $slider_id
	";
	$query_logo = mysql_query($sql_logo) or die(mysql_error());
	$logo = mysql_fetch_row($query_logo);
	$images['image'] = $img[0];
	$images['logo'] = $logo[0];
	return $images;
}

function get_translit($s)
	{
		$s = (string) $s; // преобразуем в строковое значение
		$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
		$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = str_replace(" ", "_", $s); // заменяем пробелы знаком минус
		return $s; // возвращаем результат
	}
	
function random_string($length, $chartypes) 
	{
		$chartypes_array=explode(",", $chartypes);
		// задаем строки символов. 
		//Здесь вы можете редактировать наборы символов при необходимости
		$lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
		$upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
		$numbers = '1234567890'; // numbers
		$special = '^@*+-+%()!?'; //special characters
		$chars = "";
		// определяем на основе полученных параметров, 
		//из чего будет сгенерирована наша строка.
		if (in_array('all', $chartypes_array)) {
			$chars = $lower . $upper. $numbers . $special;
		} else {
			if(in_array('lower', $chartypes_array))
				$chars = $lower;
			if(in_array('upper', $chartypes_array))
				$chars .= $upper;
			if(in_array('numbers', $chartypes_array))
				$chars .= $numbers;
			if(in_array('special', $chartypes_array))
				$chars .= $special;
		}
		// длина строки с символами
		$chars_length = strlen($chars) - 1;
		// создаем нашу строку,
		//извлекаем из строки $chars символ со случайным 
		//номером от 0 до длины самой строки
		$string = $chars{rand(0, $chars_length)};
		// генерируем нашу строку
		for ($i = 1; $i < $length; $i = strlen($string)) {
			// выбираем случайный элемент из строки с допустимыми символами
			$random = $chars{rand(0, $chars_length)};
			// убеждаемся в том, что два символа не будут идти подряд
			if ($random != $string{$i - 1}) $string .= $random;
		}
		// возвращаем результат
		return $string;
	}

?>
