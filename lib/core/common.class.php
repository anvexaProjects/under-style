<?php
/** ============================================================
 * Contains common function and Error handling API.
 * @package core
 * ============================================================ */
class Common
{
    /** Error messages.
     * @var array
     */
    public $aErrors = array();


    /** Default constructor (empty)
     * @return Common
     */
    public function __construct() { }

    /** Returns all errors generated by object
     * @return array errors
     */
    public function getErrors()
    {
        return $this->aErrors;
    }

    /** Adds an error to object error pool.
     * @param string  $sKey   key of error message
     * @param mixed   $aParm  paramteres of error message
     * @param boolean $bFatal true - fatal error, false (deafult) - recoverable error
     * @return boolean false
     */
    public function _addError($sKey, $mParam=array(), $bFatal=false)
    {
        if ($mParam && !is_array($mParam))
            $mParam = array($mParam);
		
        $sErr = vsprintf($sKey, $mParam);
        $this->aErrors[] = $sErr;
		
		
		if ($bFatal)
        {
            //see trigger_error
            trigger_error($sErr, E_USER_ERROR);
            // exit;
		    
			/* TODO: move to debug module
            $time = date("Y/m/d (d F, D) H:i:s O");
            $msg = sprintf('[%s] Error occured at %s. %s ', $time, $_SERVER['PHP_SELF'], substr($sErr, 0, 1024));
            Logger::LogItem(LOG_FILE_PATH, $msg);
			
            if (1 == IS_SEND_ERRORS)
                mail(SYS_ADMIN_MAIL, sprintf('Error occured at %s on %s ', $time, TITLE), $msg);
			*/
        }
        return false;
    }

}
?>
