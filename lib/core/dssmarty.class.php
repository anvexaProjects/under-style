<?php 

require_once '../core/settings.inc';
require_once '../lib/smarty/Smarty.class.php';

class DsSmarty extends Smarty
{
	
	public function __construct()
	{
		$this->template_dir = SMARTY_TEMPLATE_DIR;
		$this->compile_dir  = SMARTY_COMPILE_DIR;
		$this->cache_dir    = SMARTY_CACHE_DIR;
		$this->trusted_dir  = array(SMARTY_TRUSTED_DIR);		
		parent::Smarty();
	}
}

?>