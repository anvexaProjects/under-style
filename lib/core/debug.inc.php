<?php
/** ============================================================
 * File contains debugging functions
 * @package core
 * ============================================================ */
if (!empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && 'XMLHttpRequest' == $_SERVER["HTTP_X_REQUESTED_WITH"])
    define('IS_DEBUG', 0);
elseif (isset($_GET['debug']) && 'd' != $_GET['debug'])
    define('IS_DEBUG', 0);
elseif (isset($_GET['debug']) && 'd' == $_GET['debug'])
    define('IS_DEBUG', 1);
elseif (1 == IS_DEBUG_INIT)
    define('IS_DEBUG', 1);
else define('IS_DEBUG', 0);

//define('IS_DEBUG', 1);

// Formats output. Using for debuging
function d($mParam, $bExit=0)
{
    echo '<hr><pre>';
    print_r($mParam);
    echo '</pre><hr>';
    if ($bExit) exit;
}

// Sets custom error handler
function errorHandler($nErrNo, $sErrMsg, $sFulename, $nLinenum, $aVars)
{
  
    $bHide = false;
    $aTypes = array (
        1   =>  "Error",
        2   =>  "Warning",
        4   =>  "Parsing Error",
        8   =>  "Notice",
        16  =>  "Core Error",
        32  =>  "Core Warning",
        64  =>  "Compile Error",
        128 =>  "Compile Warning",
        256 =>  "User Error",
        512 =>  "User Warning",
        1024=>  "User Notice",
        8192=> "undefined"
    );

    $sErr = '<br><b>'.$aTypes[$nErrNo].'</b>:&nbsp;<span style="color:red">'.$sErrMsg.'.</span><br>';
    $aFiles = debug_backtrace();
    for ($i=1, $n=sizeof($aFiles); $i<$n; ++$i)
    {
        if (strpos($aFiles[$i]['file'], 'Smarty') and 8 == $nErrNo)
            $bHide=true;

        $sErr .= $i.'&nbsp;&nbsp;&nbsp;IN FILE&nbsp;&nbsp;&nbsp;"'.$aFiles[$i]['file'].'"&nbsp;&nbsp;&nbsp;ON LINE&nbsp;&nbsp;&nbsp;<b>'.$aFiles[$i]['line'].'</b>  <br>';
    }
    if (!$bHide)
    {
        if (1 == IS_DEBUG && array_key_exists($nErrNo, $aTypes))
            echo $sErr.'<br>';
			
		if (1 == $nErrNo || 4 == $nErrNo || 16 == $nErrNo || 256 == $nErrNo) {
//            Logger::logStackTrace($sErr);
        }
    }
}

// Shows sql history for current db connection
function showSql()
{
    $oDb = Database::get();
    $fSum = 0.0;
    $fLimit = 0.01*128;

    foreach($oDb->aHistory as $aLine){
        $sColor = sprintf('%02X', min(255, $fLimit/$aLine['time']));
	    $msg = sprintf('%.8f',$aLine['time']) .':	'. wordwrap($aLine['sql'],100,"\r\n			");
		Debug_HackerConsole_Main::out($msg, 'SQL', '#FF'.$sColor.$sColor); // , '#FF'.$sColor.$sColor
        $fSum += $aLine['time'];
    }
	Debug_HackerConsole_Main::out( 'Total: '.sprintf('%.8f',$fSum), 'SQL', '#00FF00');
}

function p($data)
{
    return d($data);
}

function debug($msg)
{
    // Use call_user_func_array() to save caller context.
    call_user_func(array('Debug_HackerConsole_Main', 'out'), $msg, 'Debug Message');
}

set_error_handler('errorHandler');
if (1 == IS_DEBUG)
{
	require_once 'debug_console.inc.php';
	new Debug_HackerConsole_Main(true);
	//Debug_HackerConsole_Main::out($_GET, "GET");
	//Debug_HackerConsole_Main::out($_POST, "POST");
}
?>