<?php
/** ============================================================
 * Base class for all classes working with db
 * @package core
 * ============================================================ */

require_once 'common.class.php';

/** DbItem -- parent for all classes thats works with entities in db.
 *
 */
class DbItem extends Common
{
    /** DB driver
     * @var Database
     */
    public $oDb;

    /** Table columns (alias=>real_name).
     * @var array
     */
    public $aFields  = array();

    /** Insert/update/load  data (alias=>value).
     * @var array
     */
    public $aData    = array();

    /** Table name
     * @var string
     */
    public $sTable   = '';

    /** Table alias
     * @var string
     */
    public $sAlias = '';

    /** Primary key name
     * @var string
     */
    public $sId      = '';

    /** "Name" field - used for getHash()
     * @var unknown_type
     */
    public $sNameField = '';

    /** Default Constructor.
     */
    public function __construct($sTable = '', $sTableId = '')
    {
        parent::__construct();
        $this->oDb = Database::get();
        if ($sTable)
        {
            $this->_initTable($sTable, $sTableId = '');
        }
    }


	protected function _initTable($sTableName, $sTableId, $sTableAlias='') 
    {
        $this->sTable = $sTableName;
        $this->sAlias = $sTableAlias;
        $this->sId    = $sTableId;
        if (! $this->sAlias)
            $this->sAlias = substr($this->sTable, 0, 3);

        $this->_loadFields();
    }

    private function _loadFields()
    {
        //load filed from database
        $this->aFields = array();
        $sSql = 'SHOW COLUMNS FROM '.$this->sTable;
        $aRows = $this->oDb->getRows($sSql);
        foreach($aRows as $aRow)
            $this->aFields[$aRow['Field']] = $this->sAlias.'.'.$aRow['Field'];
    }


    /** Removes from $this->aData incorrect keys (used $this->aFields by default)
     * @param $aField field for filtering, if omitted used $this->aFields
     * @return true
     */
    private function _filterData($aFields=array())
    {
        if (! $aFields)
            $aFields = array_keys($this->aFields);
        foreach($this->aData as $sKey=>$sVal)
        {
            if (! in_array($sKey, $aFields))
                unset($this->aData[$sKey]);
        }
    }

    /** Set data to $this->aData
     * @param array  $aData  new data
     */
    public function setData($aData)
    {
        assert(is_array($aData));
        foreach($aData as $k => $v)
        {
            $this->aData[$k] = $v;
        }
    }
    
    /** Get item ID
     * 
     * @return int item ID or null if no ID (in case when new item is not save yet)
     */
    public function getId()
    {
        return isset($this->aData[$this->sId]) ? $this->aData[$this->sId] : null;
    }

    /** Performs insert.
    * @return int new item ID or false on error
    */
    public function insert($bEscape=true)
    {
        $this->_filterData();
        $iId = $this->oDb->insert($this->sTable, $this->aData, $bEscape);
        if (!$iId)
            return $this->_addError(ERR_DBITEM_INSERT);

        return $iId;
    }

    /** Performs update of current item ( You should fill in $this->aData ...)
     * @param array $aFields fields to update: all other fields will be omitted
     *                       or empty array for no filtration
     * @param bool  $bEsacape true (default) - escape all data, false - do not escape
     * @return bool
     */
    public function update($aFields=array(), $bEscape=true)
    {

        if (isset($this->aData[$this->sId]))
            $nId = $this->aData[$this->sId];
        else
            return $this->_addError(ERR_DBITEM_NO_ID, true);

        $this->_filterData($aFields);
        $aData = $this->aData;
        //trucate fields set (for security purposes)
        unset($aData[$this->sId]);


        if ($this->oDb->update($this->sTable, $aData, $this->sId.' = '.intVal($nId), $bEscape))
            return true;
        else
            return $this->_addError(ERR_DBITEM_UPDATE);
    }

    /** Removes record by PK
     * @param mixed $nId PK value
     * @return mixed result of oDb->query
     */
    public function delete($mId)
    {
        if (!$mId)
            return true;

        if (is_array($mId))
            $mId = implode(', ', array_map('intVal', $mId));
        else
            $mId = intval($mId);
        $sSql = 'DELETE FROM `'.$this->sTable.'` WHERE '.$this->sId.' IN('.$mId.')';
        return $this->oDb->query($sSql);
    }

    /** Loads info from DB by PK
     * @param int $nId PK
     * @return boolean 1 - loaded, false - not found
     */
    public function load($nId)
    {
        $sSql = 'SELECT '.join(', ', $this->aFields).' FROM '.$this->sTable.' AS '.$this->sAlias.' WHERE '.$this->sId.'="'.$nId.'"';
        $this->aData = $this->oDb->getRow($sSql);
        return sizeof($this->aData);
    }

    /** Load info from DB by condition. If more than one rows
     * selected then only first loaded.
     *
     * @param string $sCondition
     * @return bool
     */
    public function loadByCondition($sCondition)
    {
        $sSql = 'SELECT '.join(', ', $this->aFields).' FROM `'.$this->sTable.'` AS '.$this->sAlias.' WHERE '.$sCondition;
        $this->aData = $this->oDb->getRow($sSql);
        return sizeof($this->aData);
    }

    /** Prepare 'where' statement for sql query
     * @todo optimize and move to Sql class
     * @param array $aCond conditions like array('name'=>'LIKE "zz%"', 'price'=>'<12')
     * @param array $aMap mapping like array('name'=>'p.product_name', 'price'=>'p2o.price')
     * @return string where ststement like 1 AND p.product_name LIKE "zz%" AND p2o.price < 12
     */
    protected function _parseCond($aCond, $aMap)
    {
        $aRes = array('1');
        foreach($aCond as $sKey => $sExp)
        {
            $aRes[] = $sExp;
        }

        $sCond = join(' AND ', $aRes);
        foreach($aMap as $k=>$v)
        {
            if (false === strpos($sCond, '{#'))
                break; //no more {#..} -- exit
            $sCond = str_replace('{#'.$k.'}', $v, $sCond);
        }
        return $sCond;
    }

    /** Prepare list of fields for select sql query
     * @todo comment !! optimize and move to Sql class
     * @param
     * @return
     */
    protected function _joinFields($aMap, $aFields=array())
    {
        $aRes = array();
        $aFields = $aFields ? $aFields : array_keys($aMap);
        foreach($aFields as $sAlias)
            $aRes[] = $aMap[$sAlias].' AS '.$sAlias;
        return join(', ',$aRes);
    }

    /** Get offset for given page (fix pagen number if needed)
     * @param int $iPage      page number
     * @param int $iPageSize  page size (rows per page)
     * @param int $iCnt       records number
     * @return int offset of LIMIT in SQL
     */
    protected function _getOffset($iPage, $iPageSize, $iCnt)
    {
        if ($iPageSize) //if get page -- fix current page and get offset
        {
            $iPages  = ceil($iCnt / $iPageSize);
            $iPage   = max(1, min($iPages, $iPage));
            return $iPageSize*($iPage-1);
        }

        return 0;
    }

    /** Select from DB list of records.
     * @param array $aCond      conditions like array('name'=>'LIKE "zz%"', 'price'=>'<12') (usually prepared by Filter class)
     * @param int   $iOffset    page number (may be corrected)
     * @param int   $iPageSize  page size (row per page)
     * @param string $sSort     'order by' statement (usually prepared by Sorder class)
     * @return array ($aRows, $iCnt)
     */
    public function getList($aCond=array(), $iPage=0, $iPageSize=0, $sSort='', $aFields=array())
    {
        $aMap = $this->aFields;

        $sCond = $this->_parseCond($aCond, $aMap);
        $sSql = 'SELECT COUNT(*) FROM `'.$this->sTable.'` AS '.$this->sAlias.' WHERE '.$sCond;
        $iCnt = $this->oDb->getField($sSql);
        $aRows = array();
        if ($iCnt)
        {
            $iOffset = $this->_getOffset($iPage, $iPageSize, $iCnt);
            $sSql = 'SELECT '.$this->_joinFields($aMap, $aFields).
                    ' FROM '.$this->sTable.' AS '.$this->sAlias.
                    ' WHERE '.$sCond.
                    ($sSort?(' ORDER BY '.$sSort):'').
                    ($iPageSize?(' LIMIT '.$iOffset.','.$iPageSize):'');
            $aRows = $this->oDb->getRows($sSql);
        }
        return array($aRows, $iCnt);
    }

    /** Select data from table as hash.
     * @param string $sValue name of columns with values
     * @param array  $aCond  condition array
     * @return array hash ($id=>$value)
     */
    public function getHash($sValue='name', $aCond=array())
    {
        $aMap = array();
        $sCond = $this->_parseCond($aCond, array());
        $sSql = 'SELECT '.$this->sId.','.$sValue.'  FROM '.$this->sTable.
                ($sCond?'  WHERE '.$sCond:'').
                '  ORDER BY '.$sValue;
        $aRows = $this->oDb->getRows($sSql);
        $aRes = array();
        foreach($aRows as $aRow)
            $aRes[$aRow[$this->sId]] = $aRow[$sValue];
        return $aRes;
    }
}
?>