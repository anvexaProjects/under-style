<?php
/** ============================================================
 *  This class performs sorting operations
 * @package core
 * ============================================================ */

class Sorter
{
    /** Field for current sorting
     * @var string
     */
    public $sSortField='';

    /** Order (up/down) of current sort
     * @var string
     */
    public $sSortOrder='down';

    /** Aliases of available for sorting fields
     * @var array
     */
    public $aFields = array();
	
	/** Order (up/down) for each sort field
     * @var array
     */
	public $aDefSort = array();

    /** Key in url for storing field for sorting
     * @var string
     */
    public $sUrlFieldKey = 'field';

    /** Key in url for storing order of sorting
     * @var string
     */
    public $sUrlDirKey = 'order';

    /** Name of Up image
     * @var string
     */
    public $sUpImg = 'up.gif';

    /** Name of Down image
     * @var string
     */
    public $sDownImg = 'down.gif';

    /** Name of empty (no sorting) image
     * @var string
     */
    public $sEmptyImg = 'none.gif';

    /** Creates sorting info
     * @param array $aFields fields for sorting e.g. array('id', date, 'name'). Not empty array.
     * @param array $aDefSort ('id'=>'desc', 'name'=>'asc'). Empty array = (first field=>desc)
     * @return object Sorter
     * @access public
     */
    public function __construct($aFields, $aDefSort=array())
    {
        if (!is_array($aDefSort))
            $aDefSort = array('' => 'down');
        list($this->sSortField, $this->sSortOrder) = each($aDefSort);
        $this->sSortField = in_array($this->sSortField, $aFields) ? $this->sSortField : current($aFields);
        $this->sSortOrder = ($this->sSortOrder == 'up' ? 'up' : 'down');
        $this->aFields    = $aFields;
        $this->aDefSort   = $aDefSort;
    }

    /** Init sorter with current sorting params
     * @param array $aCurrent field and direction for current sorting  e.g.  array('field'=>'name','order'=>'up')
     * @return void
     * @access public
     */
    public function init($aCurrent)
    {
        $sField = $aCurrent[$this->sUrlFieldKey];
        if (in_array($sField, $this->aFields))
            $this->sSortField = $sField;
        $this->sSortOrder = ($aCurrent[$this->sUrlDirKey] == 'up' ? 'up' : 'down');
        return true;
    }

    /** Returns sorting info: array of urls/images
     * @param object $oUrl page url
     * @return array sorting info
     * @access public
     */
    public function getSorting($oUrl)
    {
        // array url + img
        $aSortParam = array();

        foreach($this->aFields as $sField)
        {
            $oUrl->setParam($this->sUrlFieldKey, $sField);
            $oUrl->setParam($this->sUrlDirKey, 'up');
            $aSortParam[$sField]['url'] = $oUrl->getUrl();
            $aSortParam[$sField]['img'] = $this->sEmptyImg;
        }

        $oUrl->setParam($this->sUrlFieldKey, $this->sSortField);
        $oUrl->setParam($this->sUrlDirKey, ('up' == $this->sSortOrder ? 'down': 'up'));

        $aSortParam[$this->sSortField]['url'] = $oUrl->getUrl();
        $aSortParam[$this->sSortField]['img'] = ('down'== $this->sSortOrder ? $this->sDownImg : $this->sUpImg);
        
        $oUrl->setParam($this->sUrlDirKey, ('up' == $this->sSortOrder ? 'up': 'down'));
        return $aSortParam;
    }

    /** Prepares string for sql 'order by' statement
     * @return string like 'field_name DESC'  or ''
    */
    public function getOrder()
    {
        if ($this->sSortField)
            return $this->sSortField.' '.( $this->sSortOrder == 'up' ? 'ASC' : 'DESC');
        return '';
    }
	
    /** Prepares string for sql 'order by' statement
     * @return string like 'field_name DESC' for all fields or '' 
    */
    public function getOrderSql()
    {
		$sOut = '';
		
        if (is_array($this->aFields))
			foreach($this->aFields as $sField)
				$sOut .= ' '.$sField.' '.($this->aDefSort[$sField] == 'up' ? 'ASC' : 'DESC').', ';
		
		if ( strlen($sOut) > 2)
		 	$sOut = substr($sOut, 0, -2);  
		
        return $sOut;
    }
}
?>