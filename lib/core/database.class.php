<?php
/** ============================================================
 * Database abstraction layer.
 * @package core
 * @see common.class.php
 * ============================================================ */

require_once 'common.class.php';

class Database extends Common
{
    /** SQL-requests history
     * @var array
     */
    public $aHistory = array();


    /** Database handler
     * @var handler
     */
    private $hLink = 0;


    protected static $aInstances;


    /** Default constructor.
     * Do not use it direct. Use static get() method instead.
     * @return Database
     * @see get()
     */
    function __construct()
    {
        $aInstances = array();
        parent::__construct();
    }

    static function closeConnections()
    {
        // close all connections (for not permanent connections)
        foreach (self::$aInstances as $aOneInstance)
            @mysql_close($aOneInstance->hLink);
    }


    /** Returns an instance of Database class.
     * @param array $aConf optional array with database configuration (name, host, user, pass)
     * @return Database object (already connected to DB)
     * @access public
     * @static
     */
    static function get($aConf = array())
    {
        if (!$aConf)
            $aConf = array('host' => DBHOST, 'name' => MAINDB, 'user' => DBUSER, 'pass' => DBPASSWD);

        $sKey = serialize($aConf);

        if (!isset(self::$aInstances[$sKey])) {
            self::$aInstances[$sKey] = new Database();
            self::$aInstances[$sKey]->connect($aConf['host'], $aConf['name'], $aConf['user'], $aConf['pass']);
        }

        return self::$aInstances[$sKey];
    }


    /** Connects to database server and selects db.
     * @param string $sHost database host
     * @param string $sName database name
     * @param string $sUser user name
     * @param string $sPass user password
     * @return boolean
     * @access public
     */
    public function connect($sHost, $sName, $sUser, $sPass)
    {
        $this->hLink = mysql_connect($sHost, $sUser, $sPass);
//        $this->hLink = mysqli_connect($sHost, $sUser, $sPass);
        if (!$this->hLink)
            $this->_addError(CANT_CONNECT_MYSQL, array(), true);
        if (!mysql_select_db($sName, $this->hLink))
            $this->_addError(CANT_SELECT_DB, array(), true);

        mysql_query("SET NAMES 'utf8'");
        mysql_query("SET time_zone='+04:00';");
        return true;
    }

    /** Performs sql query with error reporting and logging.
     * @param  string $sSql query string
     * @return int query result handle
     * @access public
     */
    public function query($sSql)
    {
#if ($_SERVER['HTTP_X_REAL_IP'] == "176.109.39.8")
//        echo $sSql . "<BR>";
        if (1 == IS_DEBUG) //make timestap of query start
        {
            list($nUsec, $nSec) = explode(' ', microtime());
            $nStart = ((float)$nUsec + (float)$nSec);
        }

        $hRes = mysql_query($sSql, $this->hLink);

        if (1 == IS_DEBUG) //make timestapm of query end
        {
            list($nUsec, $nSec) = explode(' ', microtime());
            $nEnd = ((float)$nUsec + (float)$nSec);
            $this->aHistory[] = array('sql' => $sSql, 'time' => ($nEnd - $nStart));
        }

        if (!$hRes)
            $this->_addError(LOG_FRM_SQL, array($sSql, mysql_error($this->hLink)), true);

        return $hRes;
    }

    /** Returns row id from last executed query
     * @return int id of last INSERT operation
     */
    public function getLastID()
    {
        return mysql_insert_id($this->hLink);
    }

    /** Escapes string for LIKE expression ('%' => '\%', '_' => '\_')
     * @param mixed $mParam string or array of string need to be escaped
     * @return mixed escaped string or array of escaped strings
     */
    static function escapeLike($sParam)
    {
        return str_replace(array('%', '_'), array('\%', '\_'), Database::escape($sParam));
    }

    /** Prepares string to store in db (performs  addslashes() )
     * @param mixed $mParam string or array of string need to be escaped
     * @return mixed escaped string or array of escaped strings
     * @see unescape()
     */
    static function escape($mParam)
    {
        // test string: SQL '_"_\' &&  <a>test<a>
        if (is_array($mParam))
            return array_map(array('Database', 'escape'), $mParam);

        $mParam = str_replace('\'', '\\\'', $mParam);

        if (get_magic_quotes_gpc())
            $mParam = stripslashes($mParam);

        return Val::ToDb($mParam);
    }


    /** Prepares string to load from db
     * @param mixed $mParam string or array of string need to be escaped
     * @return mixed unescaped string or array of escaped strings
     * @see escape()
     */
    static function unescape($mParam)
    {
        if (is_array($mParam))
            return array_map(array('Database', 'unescape'), $mParam);

        return Val::FromDb($mParam);
    }


    /** Gets data returned by sql query
     * @param string $sSql select query
     * @param string $bUnescape true - method unescapes values, false - not apply unescape
     * @param string $bAssoc type of returned rows array
     * @return array selected rows (each row is array of specified type) or emprt array on error
     */
    public function getRows($sSql, $bUnescape = true, $bAssoc = true, $sKeyField = '')
    {
        $aRows = array();
        $bAssoc = ($bAssoc ? MYSQL_ASSOC : MYSQL_NUM);

        $hRes = $this->query($sSql);
        while ($aRow = mysql_fetch_array($hRes, $bAssoc)) {
            if ($bUnescape) $aRow = $this->unescape($aRow);

            if ($sKeyField && isset($aRow[$sKeyField]))
                $aRows[$aRow[$sKeyField]] = $aRow; //add with given key
            else
                $aRows[] = $aRow; //add with default keys
        }
        return $aRows; //empty array on error
    }

    /** Returns exactly one row as array. If there is number of rows
     * satisfying the condition then the first one will be returned.
     * @param string $sSql select query
     * @param string $bUnescape true - method unescapes values, false - not apply unescape
     * @return array exact one row (first if multiply row selected):
     *                 or false on error
     * @see getRows()
     */
    public function getRow($sSql, $bUnescape = true, $bAssoc = true)
    {
        $hRes = $this->query($sSql);
        $aRes = mysql_fetch_array($hRes, ($bAssoc ? MYSQL_ASSOC : MYSQL_NUM));
        if ($aRes && $bUnescape) $aRes = $this->unescape($aRes);
        return $aRes ? $aRes : array();
    }

    /** Makes hash array from two columns 'col1'=>'col2'
     * @param string $sSql sql query
     * @param string $bUnescape true - method unescapes values, false - not apply unescape
     * @return array hash array with keys from first column
     *               and values from second
     */
    public function getHash($sSql, $bUnescape = true)
    {
        $aRows = $this->getRows($sSql, $bUnescape, false);
        $aRes = array();
        foreach ($aRows as $sVal)
            $aRes[$sVal[0]] = $sVal[1];
        return $aRes; //empty array on error
    }

    /** Returns one field from a row
     * @param string $sSql SQL query
     * @param string $bUnescape true - method unescapes values, false - not apply unescape
     * @return mixed field value
     */
    public function getField($sSql, $bUnescape = true)
    {
        $sRes = '';
        $aRow = $this->getRow($sSql, $bUnescape, false);
        if ($aRow)
            $sRes = $aRow[0];
        return $sRes;
    }

    /** Returns number of rows returned by a given SQL query
     * Use this  function only if query 'SELECT COUNT(*) ...' is very complicated
     * @param string $sSql sql query
     * @return int rows count
     */
    public function countRows($sSql)
    {
        $hRes = $this->query($sSql);
        return mysql_num_rows($hRes);
    }

    /** Performs insert of one row. Accepts values to insert as an array:
     *    'column1' => 'value1'
     *    'column2' => 'value2'
     *    ...
     * @param string $sTable table name
     * @param array $aValues column and values to insert
     * @param boolean $bEscape true - method escapes values (with "), false - not escapes
     * @return int last ID (or 0 on error)
     */
    public function insert($sTable, $aValues, $bEscape = true)
    {

        $sCols = implode(',', array_keys($aValues));
        if ($bEscape) {
            $aValues = $this->escape($aValues);
            $sVals = '"' . implode('","', array_values($aValues)) . '"';
        } else
            $sVals = implode(',', array_values($aValues));

        $sSql = 'INSERT INTO `' . $sTable . '` ' .
            '        (' . $sCols . ')' .
            ' VALUES (' . $sVals . ')';
        if ($this->query($sSql))
            return $this->getLastID();

        return 0;
    }

    /** Performs update of rows.
     * @param string $sTable table name
     * @param array $aValues array of column=>new_value
     * @param string $sCond condition (without WHERE)
     * @param boolean $bEscape true - method escapes values (with "), false - not escapes
     * @return boolean true - update successfule, false - error
     */
    public function update($sTable, $aValues, $sCond, $bEscape = true)
    {
        if (!is_array($aValues))
            return false;

        $sSets = '';
        $total = count($aValues);
        $counter = 0;
        foreach ($aValues as $sCol => $sValue) {
            $counter++;
            if ($counter == $total) {
                if ($bEscape)
                    $sSets .= $sCol . '="' . $this->escape($sValue) . '" ';
                else
                    $sSets .= $sCol . '=' . $sValue . ' ';
            } else {
                if ($bEscape)
                    $sSets .= $sCol . '="' . $this->escape($sValue) . '", ';
                else
                    $sSets .= $sCol . '=' . $sValue . ', ';
            }
        }
        //$sSets[strlen($sSets)-1]=' '; //replace trailing ','
        $sSql = 'UPDATE `' . $sTable . '` SET ' . $sSets . ' WHERE ' . $sCond;
        //print_r($sSql);
        //die($sSql);
        return $this->query($sSql);
    }

    /** Format date as string for MySQL
     * @param int $timestamp datetime as timestamp (current time if omitted)
     * @return string fomratted datetime
     */
    static function date($timestamp = 0)
    {
        $timestamp = $timestamp ? $timestamp : time();
        return date('Y-m-d H:i:s', $timestamp);
    }

    /** Create temporary table
     * @param array $aFields fields (ex. array('field'=>'fname', 'type'=>'VARCHAR(255)', 'attribute'=>'NOT NULL'))
     * @param array $aAttr table attributes
     * @return string temporary table name
     */
    public function createTempTable($aFields, $aAttr = '')
    {
        $sTmpName = "`" . md5(uniqid(rand(), 1)) . "`";
        $sSql = 'CREATE TEMPORARY TABLE IF NOT EXISTS ' . $sTmpName . ' (';
        $sSep = '';
        for ($i = 0; $i < count($aFields); $i++) {
            $sSql .= $sSep . $aFields[$i]["field"] . " " . $aFields[$i]["type"] . " " . (isset($aFields[$i]["attribute"]) ? $aFields[$i]["attribute"] : '');
            $sSep = ', ';
        }
        if ($aAttr) $sSql .= $sSep . $aAttr;
        $sSql .= ');';
        $this->query($sSql);
        return $sTmpName;
    }

}

?>