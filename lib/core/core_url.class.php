<?php
/** ============================================================
 * URL wrapper class
 * @package core
 * ============================================================ */

class CoreUrl
{
    /** The scheme(protocol) of the url, e.g. <code>http://</code>
     * @var string
     */
    private $sProtocol='http';

    /** Allowed protocols
     * @var array
     */
    private $_aProtocols = array('http', 'https', 'ftp');

    /** The host of the url, e.g. <code>www.example.com</code>
     * @var string
     */
    private $sHost='';

    /** The dirs of the url, e.g. <code>dir1/dir2/</code>
     * @var string
     */
    private $sDirs='';

    /** The file of the url, e.g. <code>script.php</code>
     * @var string
     */
    private $sFile='';

    /** The parameters of the url (from 'PATH_INFO')
     * e.g <code>a_1/b_2</code>
     * @var array
     */
    public $aParams=array();

    /** The fragment of the url
     * e.g <code>top</code>
     * @var array
     */
    private $sFragment='';


    /** The internal URL represtation (cache)
     * e.g <code>http://www.example.com/dir1/dir2/script.php/a_1/b_2/#top</code>
     * @var string
     */
    private $sUrl='';

    /** Internal flag to check if the cache was changed
     * @var boolean
     */
    private $bChanged=true;

    /** SEO: source url regexp (unmodified url pattern)
     *       ex. '!^(.+)/visitor/index.php/part_([a-z_]+)/sect_([a-z_]+)(.+)$!'
     * @var string regexp
     * @see $sSeoTrg
     */
    private $sSeoSrc = '';

    /** SEO: target url (modified url)
     *       ex. '$1/$2/$3/index$4.html'
     * @var unknown_type
     * @see $sSeoSrc
     */
    private $sSeoTrg = '';



    /** Constructs a new Url object.
     * @param string $sUrl    the optional URL (a string) to base the Url on
     * @param string $sParams the optional URL (a string) with parameters only
     */
    public function __construct($sUrl = '', $sParams = '')
    {
        $this->makeFromCurrent();
    }

    /** Parses parameters in a <code>key1=value1&key2=value2&...</code> string
     * @param string $sParams the URL-encoded string with the parameters
     * @return void
     * @access private
     */
    private function _parseParams($sParams)
    {
        $this->aParams = array();
        if(!isset($_GET['global']['has_mod_rewrite']) || strpos($sParams, '&') !== false)
        {
            $aList = explode('&', trim($sParams,'& '));
            for ($i = 0, $n=count($aList); $i < $n; ++$i)
            {
                $aPair = explode('=', $aList[$i], 2);
                if (isset($aPair[1]) && $aPair[0] != 'path')
                    $this->aParams[$aPair[0]] = urldecode($aPair[1]);
            }
        }
        else
        {
            $aList = explode('/', str_replace('path=', '', $sParams));
            foreach($aList as $a)
            {
                if(strstr($a, '='))
                {
                    $exp = explode('=', $a);
                    $this->aParams[$exp[0]] = $exp[1];
                }
            }
        }
        $this->bChanged = true;
    }

    /** Parses url from the string
     * @param string $sUrl url to parse
     * @return void
     * @access private
     */
    private function _parseUrl($sUrl)
    {
        // parse_url generate warning if URL contains more than 1 ':'
        if ($n = strpos($sUrl, ':', 7))
            $sUrl = substr($sUrl, 0, $n);
        $aInfo = parse_url($sUrl);

		// overwrite path when modrewrite is on
		if(isset($_GET['global']['has_mod_rewrite']))
			//$aInfo['path']=  SITE_REL . '/' . $_GET['path'];
			//$aInfo['path'] = SITE_REL . '/' . str_replace('path=', '', $_SERVER['QUERY_STRING']);
			$aInfo['path'] =  $_SERVER['REQUEST_URI'];
        
        
        // clear previous state
        $this->setDirs('');
        $this->setFile('');
        $this->clearParams();
        $this->setFragment('');

        if (array_key_exists('scheme', $aInfo))
            $this->setProtocol($aInfo['scheme']);

        if (array_key_exists('host', $aInfo))
            $this->setHost($aInfo['host']);

        if (array_key_exists('path', $aInfo))
        {
            if (strpos($aInfo['path'], '.php'))
            {
                if (preg_match('!^(.*)/([^/]+\.[^/]+)(.*)$!', $aInfo['path'], $aM))
                {
                    $this->setDirs($aM[1]);
                    $this->setFile($aM[2]);
                    //$this->_parseParams($aM[3]);
                }
            }
            else
                $this->setDirs($aInfo['path']);
        }

        if (array_key_exists('fragment', $aInfo))
            $this->setFragment($aInfo['fragment']);
    }

    /** Set a new value to the Url
     * @param string $sUrl    the URL (a string) to base this Url on
     * @param string $sParams the optional string of parameters (URL-encoded)
     * @return void
     */
    public function setUrl($sUrl, $sParams = '')
    {
        // Reset the current URL
        $this->aParams = array();

        // Create the new URL
        $this->_parseParams($sParams);
        $this->_parseUrl($sUrl);
    }

    /** Set the Url to the URL of the current page;
     * @return void
     */
    public function makeFromCurrent()
    {
        $this->_parseUrl($_SERVER['PHP_SELF']);
		//echo $_SERVER['REQUEST_URI'];
		// QUERY_STRING PHP_SELF
        if (isset($_SERVER['QUERY_STRING']))
            $this->_parseParams($_SERVER['QUERY_STRING']);
        $this->setProtocol(isset($_SERVER['HTTPS']) ? 'https' : 'http');
        $this->setHost(SITE_ADR);
    }

    /** Retruns protocol
     * @return string scheme of the url
     */
    public function getProtocol()
    {
        return $this->sProtocol;
    }

    /** Set protocol.
     * @param string $sProtocol new protocol: 'https' or 'https'
     * @return void
     */
    public function setProtocol($sProtocol)
    {
        if (in_array($sProtocol, $this->_aProtocols))
        {
            $this->sProtocol = $sProtocol;
            $this->bChanged = true;
        }
    }

    /** Sets the host for the Url
     * @param string $sHost
     * @return void
     */
    public function setHost($sHost)
    {
        $this->sHost = trim($sHost,'/ ');
        $this->bChanged = true;
    }

    /** Returns host of the url
     * @return string host
     */
    public function getHost()
    {
        return $this->sHost;
    }

    /** Returns dirs of the url
     * @return strung dirs
     */
    public function getDirs()
    {
        return $this->sDirs;
    }

    /** Sets the dirs for the Url
     * @param string $sDirs
     * @return void
     */
    public function setDirs($sDirs)
    {
        $sDirs = trim($sDirs, '/ ');
        $this->sDirs = $sDirs;
        if ($sDirs)
            $this->sDirs .= '/';
        $this->bChanged = true;
    }

    /** Returns dirs of the url
     * @return string file name
     */
    public function getFile()
    {
        return $this->sFile;
    }

    /** Sets the file for the Url
     * @param string $sFile
     * @return void
     */
    public function setFile($sFile)
    {
        $this->sFile = trim($sFile, '/ ');
        $this->bChanged = true;
    }

    /** Updates the value of a parameter
     * @param string $sName  the name of the parameter to update
     * @param string $sValue the new value of the parameter or null if the parameter should be deleted
     * @return void
     */
    public function setParam($sName, $sValue = null)
    {
        if (null === $sValue)
            unset($this->aParams[$sName]);
        else
            $this->aParams[$sName] = $sValue;

        $this->bChanged = true;
    }

    /** Updates the value of  parameters
     * @param array $aParams parameters to update (name=>value)
     * @return void
     */
    public function setParams($aParams)
    {
        foreach($aParams as $sName=>$sValue)
            $this->setParam($sName,$sValue);
    }

    /** Returns all parameters
     * @return array all url parameters ($key=>$value)
     */
    public function getParams()
    {
        return $this->aParams;
    }

    /** Gets the value of the specified Url parameter
     * @param string $sName parameter name
     * @param string $sDefault default name
     * @return string
     */
    public function getParam($sName, $sDefault = '')
    {
        if (array_key_exists($sName, $this->aParams))
            return $this->aParams[$sName];
        return $sDefault;
    }


    /** Checks whether a specific parameter exists in this Url
     * @return boolean
     */
    public function hasParam($sName)
    {
        return isset($this->aParams[$sName]);
    }

    /** Clears all params or a single parameter
     * @param string $sName  the name of the parameter to clear.
     * @return void
     */
    public function clearParams($sName='')
    {
        if ($sName)
            unset($this->aParams[$sName]);
        else
            $this->aParams = array();

        $this->bChanged = true;
    }


    /** Returns a fragment of the url
     * @return string fragment
     */
    public function getFragment()
    {
        return $this->sFragment;
    }

    /** Sets the fragment of the Url
     * @param string $sFragment
     * @return void
     */
    public function setFragment($sFragment)
    {
        $this->sFragment = $sFragment;
        $this->bChanged = true;
    }

    /** Returns a string representation of the URL
     * @return string
     */
    public function getUrlOld()
    {
        $sUrl = $this->sUrl;

        if ($this->bChanged)
        {
            $sUrl = $this->getProtocol().'://'.$this->getHost().'/'.$this->getDirs().$this->getFile();
            $aParams = array();
            foreach ($this->aParams as $sName => $sValue)
            {
                if (!empty($sName)) {
                    $aParams[] = $sName . '_' . urlencode($sValue);
                }
            }
            if (sizeof($aParams))
                $sUrl .= '/' . join('/', $aParams);
            if ($this->getFragment())
                $sUrl .= '/#'.urlencode($this->getFragment());

            $this->sUrl = $sUrl;
            $this->bChanged = false;
        }

        //tweak url if seo options not empty
        if ($this->sSeoSrc && $this->sSeoTrg)
            $sUrl = preg_replace($this->sSeoSrc, $this->sSeoTrg, $sUrl);

        if (substr($sUrl, count($sUrl) - 1) != '/')
            $sUrl = $sUrl.'/';
        return $sUrl;
    }


    /** Returns a string representation of the URL
     * @return string
     */
    public function getUrl()
    {
        $sUrl = $this->sUrl;
        if ($this->bChanged)
        {
            $sUrl = '/'.$this->getDirs().$this->getFile();
            $aParams = array();
            foreach ($this->aParams as $sName => $sValue)
            {
                if (!empty($sName)) {
                    $aParams[] = $sName . '=' . urlencode($sValue);
                }
            }
            if (sizeof($aParams))
            {
                if(isset($_GET['global']['has_mod_rewrite']))    
                {
                    $sUrl = SITE_REL . '/' . $_GET['path'] . '/' . join('/', $aParams) . '/';
                }
                else
                {
                    $sUrl .= '?' . join('&', $aParams);
                }
            }
                
            if ($this->getFragment())
                $sUrl .= '/#'.urlencode($this->getFragment());

            $this->sUrl = $sUrl;
            $this->bChanged = false;
        }

        //tweak url if seo options not empty
        if ($this->sSeoSrc && $this->sSeoTrg)
            $sUrl = preg_replace($this->sSeoSrc, $this->sSeoTrg, $sUrl);
        return $sUrl;
    }


    /** Set parameters for SEO optimising url conversion.
     *  If no parameters given, then Url will not convert url to seo optimised.
     * @param $sSrc pattern for source url (raw url)
     * @param $sTrg target SEO-optimised url
     */
    public function setSeoParams($sSrc='', $sTrg='')
    {
        $this->sSeoSrc = $sSrc;
        $this->sSeoTrg = $sTrg;
    }
	
    public function getAction()
	{
		return 'default';
	}
	
}
?>