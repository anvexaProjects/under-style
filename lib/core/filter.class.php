<?php
/** Filter class
 *
 * @package core
 * @version $Id: Filter.class.php 185 2007-04-19 11:18:58Z stelmakh $
 * 
 *  -= HELP =-
 * Filter class should be configured with array $aConditions. The simple format of this array is following:
 *      $aConditions = array(
 *          '[condition1]' => array(
 *              '[condition_parameters]'
 *          ),
 *      )
 * 
 * Condition Parameters :
 *      'type': field type
 *      --------------------
 *      possible values: 'text', 'drop_down', 'date', 'date_range', 'range'
 *      mandatory: Yes
 *      default value: 'text'
 *      info:
 * 
 *      'condition': sql condition (predefined or SQL expression with placers)
 *      --------------------
 *      possible values: 'like', '=', '!=', '>', '<', '>=', '<=' 
 *      mandatory: No
 *      default value: '='
 *      info: takes into account for condition types: 'text', 'date' and 'drop_down'
 * 
 *      'def': default value
 *      --------------------
 *      possible values: any
 *      mandatory: No
 *      default value: 
 *      info: 
 *          - for 'text' condition type: any text
 *          - for 'date' condition type: timestamp
 *          - for 'date_range' condition type: integer number of date period from $aPeriods property
 *          - for 'drop_down' condition type: any value from drop-down list
 *          - for 'range' condition type: any value in format [from_value]-||-[to_value] (-||-  -- separator)
 * 
 *      'select': values for select field
 *      --------------------
 *      possible values: array of the following format:
 *          array(
 *              '[option1_value]' => [option1_name],
 *              '[option2_value]' => [option2_name],
 *              ........
 *              '[optionN_value]' => [optionN_name],
 *          )
 *      mandatory: Yes (for 'drop_down' condition type only)
 *      default value: array()
 *      info: takes into account for condition type 'drop_down'
 * 
 *      'is_date_period': is display predefined date ranges (yeterday, last month etc.)
 *      --------------------
 *      possible values: true, false
 *      mandatory: No
 *      default value: false 
 *      info: takes into account for condition type 'date_range'. Drop down list of date periods
 * 
 *      'is_from_to': is dsiplay 'from' and 'to' fields (for date range)
 *      --------------------
 *      possible values: true, false
 *      mandatory: No
 *      default value: false 
 *      info: takes into account for condition type 'date_range'. Input texts for 'From' and 'To' dates 
 * 
 *      'is_calendar': is display calendar
 *      --------------------
 *      possible values: true, false
 *      mandatory: No
 *      default value: false 
 *      info: takes into account for condition type 'date_range'. Calendar for 'From' and 'To' input elements
 * 
 *      'all_option': is dispaly 'empty' option in select field
 *      --------------------
 *      possible values: array of the following format:
 *          array(
 *              'name' => [name_value],
 *              'value' => [value_value]
 *          )
 *      mandatory: No
 *      default value: 
 *      info: takes into account for condition type 'drop_down'. Adds "ALL" option in the begining of 
 *            the drop-down list. Not participates in result query.
 * 
 *      'date_format': date format
 *      --------------------
 *      possible values: any valid date format
 *      mandatory: No
 *      default value: 'm/d/Y'
 *      info: takes into account for condition types 'date_range' and 'date'.
 * 
 *      'datetime_format': datetime format
 *      --------------------
 *      possible values: any valid date format
 *      mandatory: No
 *      default value: 'm/d/Y H:i:s'
 *      info: takes into account for condition type 'date_range'
 * 
 *      'visible': is filter element is visible on the page
 *      --------------------
 *      possible values: true, false
 *      mandatory: No
 *      default value: true
 *      info: if 'true' element is visible on the page, otherwise no. 'false' 
 *            value is usually used for defining filter elements that always uses one 
 *            predefined custom condition and not take parameters from the user.
 *            e.g.
 *              $aCond = array(
 *                  'status' => array(
 *                      'condition' => '{#status} != "deleted"',
 *                      'visible'   => false,
 *                   ),
 *              )
 *
 * -- END OF HELP --
 * 
 * @package module.Navigator
 * @subpackage utils
 * @author Martin
 */
global $FILTER_PERIODS;
$FILTER_PERIODS =
    array(
        array(
            'name' => 'Choose Range',
            'from' => '',
            'to'   => '',
        ),
        array(
            'name' => 'Today',
            'from' => '-0 day',
            'to'   => '-0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Yesterday',
            'from' => '-1 day',
            'to'   => '-1 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last 2 days',
            'from' => '-1 day',
            'to'   => '0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last 3 days',
            'from' => '-2 day',
            'to'   => '0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last week',
            'from' => '-6 day',
            'to'   => '0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last 2 week',
            'from' => '-13 day',
            'to'   => '0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last month',
            'from' => '-1 month',
            'to'   => '-0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last 3 month',
            'from' => '-3 month',
            'to'   => '-0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last 6 month',
            'from' => '-6 month',
            'to'   => '-0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'Last year',
            'from' => '-1 year',
            'to'   => '-0 day',
            'start_time' => '00:00:00',
            'end_time' => '23:59:59',
        ),
        array(
            'name' => 'All Periods',
            'from' => '-',
            'to'   => '-',
        ),         
    );

class Filter
{
    /** Filter Name
     *
     * @var string
     */
    public $sFilterName = "";

    /** Filter data
     *
     * @var array
     */
    public $aData = array();
    
    /** Associative array of conditions
     * $alias => $internal_language
     *
     * @var array
     */
    public $aConditions = array();
    
    /** Associative array of HTML inputs
     *
     * @var mixed
     */
    public $aHtml = array();

    /** Periods
     *
     * @var mixed
     */
    public $aPeriods;

    /** Errors
     *
     * @var array
     */
    public $aErrors;

    /** Class Constructor
     * @param string $sFilterName filter name (unique within the system)
     * @param array $aConditions configuration array of specific format
     * @param array $aClearUrlParams array of URL params that will be cleaned upon filter reset
     * @param array $aCustomData custom values for filter fields
     * @return Mod_Navigator_UtilFilter Filter
     */
    public function __construct($sFilterName, $aConditions, $aClearUrlParams = array(), $aCustomData = array())
    {
        global $FILTER_PERIODS;
		$oUrl              = new CoreUrl();
        $oReq              = new Request($oUrl);
        $this->sFilterName = $sFilterName;
        $this->aConditions = $aConditions;
        $this->aPeriods    = $FILTER_PERIODS;

        //validate conditions
        if (!$this->validateConditions())
        {
            trigger_error('Filter error', E_USER_ERROR);
            exit;
        }

        $this->aHtml       = array();
        $this->_preparePeriods();
        $sAction = $oReq->getAction();
        //$sUrlParam = str_replace('_','-', $this->sFilterName).'-active';
        $sUrlParam =$this->sFilterName.'_active';

        if ($sAction == $this->sFilterName.'_show')
        {
            $aData = $oReq->getArray($sFilterName);
            $this->set($aData);
            /*
            $aClearUrlParams[] = 'act';
            $sGet = Util::ConcatGetParams($aClearUrlParams, array($sUrlParam=>1));
            Request::forward($_SERVER['PHP_SELF'].'?'.$sGet); */
            $oUrl->setParam($sUrlParam, 1);
            $oReq->forward($oUrl->getUrl()); 
        }
        elseif (count($aCustomData))
        {
            $this->set($aCustomData);
        }

        if ($sAction == $this->sFilterName.'_reset')
        { 
            $this->reset($aClearUrlParams, $oReq, $oUrl);
        }
        
        if (!$oReq->getInt($sUrlParam))
        {
            $this->_resetToDefault();
        }
        
        $this->_initFilter();
        $this->_prepareHtml();
        if ($this->isActive())
        {
            $this->_prepareConditions();
            $this->_processConditions();
        }
        else
        {
            $this->aConditions = array();
        }
    }

    /** Prepare periods for future use
     */
    private function _preparePeriods()
    {
        foreach ($this->aPeriods as $key=>$value)
        {
            $this->aPeriods[$key]['from'] = str_replace('%dom%', date('d')-1, $this->aPeriods[$key]['from']);
            $this->aPeriods[$key]['to']   = str_replace('%dom%', date('d')-1, $this->aPeriods[$key]['to']);
        }
    }
    
    /** Is current filter active?
     *
     * @return bool true - filter active, false - filter inactive
     */
    public function isActive()
    {
        if (isset($_SESSION['filter'][$this->sFilterName]['__is_active__']))
        {
            return $_SESSION['filter'][$this->sFilterName]['__is_active__'];
        }
        else
        {
            return false;
        }
    }
    
    /** Initialize/load filter
     */
    private function _initFilter()
    {
        if (!isset($_SESSION['filter']))
            $_SESSION['filter'] = array();
            
        if (array_key_exists($this->sFilterName,$_SESSION['filter']))
        {
            $this->aData = $_SESSION['filter'][$this->sFilterName];
        }
        else
        {
            $this->_resetToDefault();
        }
    }
    
    /**  Resets a current filter and forwards to the cleaned URL
     * @param $aUrlParams array of URL params that will be cleaned upon filter reset
     */
    public function reset($aUrlParams=array(), $oReq, $oUrl)
    {
        $this->_resetToDefault();
        if (isset($_SESSION['filter'][$this->sFilterName])) 
            $_SESSION['filter'][$this->sFilterName] = array();
        
        $this->disableFilter();
        $oUrl->setParam($this->sFilterName.'_active', 0);        
        $oReq->forward($oUrl->getUrl()); 
    }
    
    
    /** Main functions that process conditions
     */
    private function _processConditions()
    {
        foreach ($this->aConditions as $key=>$value)
        {
            foreach ($this->aData as $key1=>$value1)
            {
                if (false !== strpos($value['sql'], '{$'.$key1.'}'))
                {
                    $value['sql'] = str_replace('LIKE "%{$'.$key1.'}','LIKE "%'.DataBase::escapeLike($value1), $value['sql']);
                    $value['sql'] = str_replace('{$'.$key1.'}',DataBase::escape($value1), $value['sql']);
                    $this->aConditions[$key]['sql'] = $value['sql'];
                }
            }
        }
    }
    
    /**  Set (turn on) filter.
     * 
     * @param array $aData filter data (usually from POST)
     */
    public function set($aData) 
    {
        $this->aData = $aData;
        $_SESSION['filter'][$this->sFilterName] = $this->aData;
        $this->activateFilter();
    }
    
    /** Set filter status to active
     */
    public function activateFilter()
    {
        $_SESSION['filter'][$this->sFilterName]['__is_active__'] = true;
    }
    
    /** Disables current filter (set it status to disabled)
     */
    public function disableFilter()
    {
        $_SESSION['filter'][$this->sFilterName]['__is_active__'] = false;
    }
    
    /** Resets filter to default data
     */
    private function _resetToDefault()
    {
        foreach ($this->aConditions as $key=>$value)
        {
            switch (strtolower($value['type']))
            {
                case 'date':
                    if (array_key_exists('def', $value)) // $value['def'] -- timestamp
                    {
                        $this->aData[$key] = date($value['date_format'], $value['def']);
                    }
                    else
                        $this->aData[$key] = '';
                    break;
                    
                case 'text':
                case 'drop_down':
                    if (array_key_exists('def', $value)) 
                    {
                        $this->aData[$key] = $value['def'];
                    }
                    else
                        $this->aData[$key] = '';
                    break;
                    
                case 'date_range':
                    $nDefId = -1;
                    if (isset($value['def']) and is_int($value['def']) and array_key_exists($value['def'], $this->aPeriods))
                    {
                        $nDefId = $value['def'];
                    }
                    if (!empty($value['is_date_period']))
                    {
                        $this->aData[$key.'_period'] = '';
                        if ($nDefId != -1)
                        {
                            $this->aData[$key.'_period'] = $this->getPeriodValue($nDefId, $value);
                        }
                    }
                    if (!empty($value['is_from_to']))
                    {
                        $this->aData[$key.'_from'] = '';
                        $this->aData[$key.'_to'] = '';
                        if ($nDefId != -1)
                        {
                            $sValue = $this->getPeriodValue($nDefId, $value);
                            $aPeriod = explode('##', $sValue);
                            $this->aData[$key.'_from'] = isset($aPeriod[1]) ? isset($aPeriod[1]) : '';
                            $this->aData[$key.'_to'] = isset($aPeriod[2]) ? isset($aPeriod[2]) : '';
                        }
                    }
                    break;
                    
                case 'range':
                    if (array_key_exists('def', $value)) 
                    {
                        $aDef = explode('-||-', $value['def'], 2);
                        if (isset($aDef[0]))
                            $this->aData[$key.'_from'] = $aDef[0];
                        else
                            $this->aData[$key.'_from'] = '';
                        if (isset($aDef[1]))
                            $this->aData[$key.'_to'] = $aDef[1];
                        else
                            $this->aData[$key.'_to'] = '';
                    }
                    else
                    {
                        $this->aData[$key.'_from'] = '';
                        $this->aData[$key.'_to'] = '';
                    }
                    break;
            }
        }
        $this->set($this->aData);
    }
    
    /** Prepares external conditions into internal language
     */
    private function _prepareConditions()
    {
        foreach ($this->aConditions as $key=>$value)
        {
            $sSql = '';
            switch (strtolower($value['type']))
            {
                case 'text':
                case 'drop_down':
                    switch (strtolower($value['condition']))
                    {
                        case 'like':
                            $sSql = '{#'.$key.'} LIKE "%{$'.$key.'}%"';
                            if (array_key_exists($key, $this->aData))
                            {
                                if (trim($this->aData[$key]) == "") $sSql = "";
                            }
                            break;
                        case '=':
                        case '>':
                        case '<':
                        case '<>':
                        case '>=':
                        case '<=':
                            $sSql = '{#'.$key.'} '.$value['condition'].' "{$'.$key.'}"';
                            if (array_key_exists($key, $this->aData))
                            {
                                if (trim($this->aData[$key]) == "") $sSql = "";
                            }
                            break;
                        default: //custom 
                            $sSql = $value['condition'];
                            if (array_key_exists($key, $this->aData) && $value['visible'])
                            {
                                if (trim($this->aData[$key]) == "") $sSql = "";
                            }
                            break;
                    }
                    if ($value['type'] == 'drop_down' && array_key_exists('all_option', $value))
                    {
                        if ($value['all_option']['value'] == $this->aData[$key])
                            $sSql = "";
                    }
                    break;
                    
                case 'date':
                    if (array_key_exists($key, $this->aData) && $this->aData[$key] != "")
                    {
                        switch (strtolower($value['condition']))
                        {
                            case '=':
                            case '>':
                            case '<':
                            case '<>':
                            case '>=':
                            case '<=':
                                $sSql = '({#'.$key.'} '.$value['condition'].' "'.date('Y-m-d', strtotime($this->aData[$key])).'") ';
                                break;
                        }
                    }
                    break;
                    
                case 'date_range':
                    $sFrom = '';
                    $sTo   = '';
                    if (!empty($value['is_date_period']))
                    {
                        if (array_key_exists($key.'_period', $this->aData) && $this->aData[$key.'_period'] !== '')
                        {
                            $aPeriod = explode('##', $this->aData[$key.'_period']);
                            if (isset($aPeriod[1]))
                                $sFrom = $aPeriod[1];
                            if (isset($aPeriod[2]))
                                $sTo = $aPeriod[2];
                        }
                        elseif (array_key_exists($key.'_period', $this->aData) && 
                                $this->aData[$key.'_period'] === '' && 
                                !empty($value['is_from_to']))
                        {
                            if (array_key_exists($key.'_from', $this->aData) && $this->aData[$key.'_from'] !== '')
                            {
                                $sFrom = strtotime($this->aData[$key.'_from']);
                                $sFrom = date('m/d/Y H:i:s', mktime(0, 0, 0, date('m', $sFrom), date('d', $sFrom), date('y', $sFrom)));
                            }
                            if (array_key_exists($key.'_to', $this->aData) && $this->aData[$key.'_to'] !== '')
                            {
                                $sTo = strtotime($this->aData[$key.'_to']);
                                $sTo = date('m/d/Y H:i:s', mktime(23, 59, 59, date('m', $sTo), date('d', $sTo), date('y', $sTo)));
                            }
                        }
                    }
                    elseif (empty($value['is_date_period']) && !empty($value['is_from_to']))
                    {
                        if (array_key_exists($key.'_from', $this->aData) && $this->aData[$key.'_from'] !== '')
                        {
                            $sFrom = strtotime($this->aData[$key.'_from']);
                            $sFrom = date('m/d/Y H:i:s', mktime(0, 0, 0, date('m', $sFrom), date('d', $sFrom), date('y', $sFrom)));
                        }
                        if (array_key_exists($key.'_to', $this->aData) && $this->aData[$key.'_to'] !== '')
                        {
                            $sTo = strtotime($this->aData[$key.'_to']);
                            $sTo = date('m/d/Y H:i:s', mktime(23, 59, 59, date('m', $sTo), date('d', $sTo), date('y', $sTo)));
                        }
                    }

                    $bFound = false;
                    if ($sFrom)
                    {
                        $sSql .= '({#'.$key.'} >= "'.date('Y-m-d H:i:s', strtotime($sFrom)).'") ';
                        $bFound = true;
                    }
                    if ($sTo)
                    {
                        if ($bFound) $sSql .= ' AND ';
                        $sSql .= '({#'.$key.'} <= "'.date('Y-m-d H:i:s', strtotime($sTo)).'") ';
                    }
                    break;
                    
                case 'range':
                    $bFound = false;
                    if (array_key_exists($key.'_from', $this->aData) && $this->aData[$key.'_from'] != "")
                    {
                        $sSql .= '({#'.$key.'} >= "'.$this->aData[$key.'_from'].'") ';
                        $bFound = true;
                    }
                    if (array_key_exists($key.'_to', $this->aData) & $this->aData[$key.'_to'] != "")
                    {
                        if ($bFound) $sSql .= ' AND ';
                        $sSql .= '({#'.$key.'} <= "'.$this->aData[$key.'_to'].'") ';
                    }
                    break;
                    
            }
            $this->aConditions[$key]['sql'] = $sSql;
        }
    }


    private function cyrillicToUsDate($input)
    {
  	   // convert 05.06.2007 to 06/05/2007
	   $arr  = explode('.', $input);
	   if(3 == count($arr))
	       return sprintf("%s/%s/%s", $arr[1], $arr[0], $arr[2]);
       else
     	   return $input;
    }     

    /** Prepares html inputs in $aHtml
     */
    private function _prepareHtml()
    {
        $this->aHtml = array();
        foreach ($this->aConditions as $key=>$value)
        {
            if (!$value['visible'])
                continue;
            switch (strtolower($value['type']))
            {
                case 'drop_down':
                    $this->aHtml[$key]['type'] = 'select';
                    $this->aHtml[$key]['params']['name'] = $this->sFilterName.'['.$key.']';
                    if (is_array($value['select']))
                    {
                        if (isset($value['all_option']))
                            $this->aHtml[$key]['select'][htmlspecialchars($value['all_option']['value'])] = htmlspecialchars($value['all_option']['name']);
                        foreach ($value['select'] as $keyS => $valueS)
                        {
                            settype($keyS,'string');
                            settype($this->aData[$key],'string');
                            $this->aHtml[$key]['select'][htmlspecialchars($keyS)] = htmlspecialchars($valueS);
                        }
                        $this->aHtml[$key]['value'] = isset($this->aData[$key]) ? $this->aData[$key] : '';
                    }
                    break;
                case 'text':
                case 'date':
                    $this->aHtml[$key]['type'] = 'text';
                    $this->aHtml[$key]['params']['name'] = $this->sFilterName.'['.$key.']';
                    $this->aHtml[$key]['value'] = isset($this->aData[$key]) ? $this->aData[$key] : '';
                    break;
                    
                case 'date_range':
                    $nDefId = -1;
                    if (isset($value['def']) and is_int($value['def']) and array_key_exists($value['def'], $this->aPeriods))
                    {
                        $nDefId = $value['def'];
                    }
                    if (!empty($value['is_date_period']))
                    {
                        $this->aHtml[$key.'_period']['type'] = 'select';
                        $this->aHtml[$key.'_period']['params']['name'] = $this->sFilterName.'['.$key.'_period]';
                        $this->aHtml[$key.'_period']['params']['id'] = 'fldFilter_'.$this->sFilterName.'_'.$key.'_period';
                        $this->aHtml[$key.'_period']['prefix'] = 'fldFilter_'.$this->sFilterName.'_'.$key;
                        $this->aHtml[$key.'_period']['params']['onchange']='updateDates(this.selectedIndex, \'fldFilter_'.$this->sFilterName.'_'.$key.'\', \''.$value['date_format'].'\')';
                        $this->aHtml[$key.'_period']['date_range'] = 1;
                        
                        if (!array_key_exists($key.'_period', $this->aData)) 
                            $this->aData[$key.'_period'] = '';
                        
                        $this->aHtml[$key.'_period']['select'] = array();
                        $aPeriod = explode('##', $this->aData[$key.'_period']);
                        $sSelected = '-';
                        $sDefault  = '-';
                        foreach ($this->aPeriods as $key1=>$value1)
                        {
                            $sValue = $this->getPeriodValue($key1, $value);
                            if ($this->aData[$key.'_period'] == '##')
                                $sSelected = '##';
                            elseif ($this->aData[$key.'_period'] == '')
                                $sSelected = '';
                            
                            $this->aHtml[$key.'_period']['select'][$sValue] = $value1['name'];
                            
                            if ($sSelected == '-' && isset($aPeriod[0]) && $aPeriod[0] == $key1)
                                $sSelected = $sValue;

                            if ($nDefId == $key1)
                                $sDefault = $sValue;
                        }
                        
                        if ($sSelected == '-' && $sDefault != '-')
                        {
                            $sSelected = $sDefault;
                        }
                        if ($sSelected != '-')
                        {
                            $this->aHtml[$key.'_period']['value'] = $sSelected;
                        }
                    }
                    
                    if (!empty($value['is_from_to']))
                    {
                        $this->aHtml[$key.'_from']['type'] = 'text';
                        $this->aHtml[$key.'_from']['params']['name'] = $this->sFilterName.'['.$key.'_from]';
                        $this->aHtml[$key.'_from']['params']['id'] = 'fldFilter_'.$this->sFilterName.'_'.$key.'_from';
                        $this->aHtml[$key.'_from']['value'] = isset($this->aData[$key.'_from']) ? htmlspecialchars($this->aData[$key.'_from']) : '';
                        $this->aHtml[$key.'_from']['disabled'] = 0;
                        $this->aHtml[$key.'_from']['date_range'] = 1;

                        
                        
                        $this->aHtml[$key.'_to']['type'] = 'text';
                        $this->aHtml[$key.'_to']['params']['name'] = $this->sFilterName.'['.$key.'_to]';
                        $this->aHtml[$key.'_to']['params']['id'] = 'fldFilter_'.$this->sFilterName.'_'.$key.'_to';
                        $this->aHtml[$key.'_to']['value'] = isset($this->aData[$key.'_to']) ? htmlspecialchars($this->aData[$key.'_to']) : '';
                        $this->aHtml[$key.'_to']['disabled'] = 0;
                        $this->aHtml[$key.'_to']['date_range'] = 1;
                        
                        if (!empty($value['is_calendar']))
                        {
                            $this->aHtml[$key.'_from']['is_calendar'] = 1;
                            $this->aHtml[$key.'_to']['is_calendar'] = 1;
                        }
                        
                        if (!empty($value['is_date_period']) && isset($this->aData[$key.'_period']))
                        {
                            if ($this->aData[$key.'_period'] == '##')
                            {
                                $this->aHtml[$key.'_from']['value'] = '';
                                $this->aHtml[$key.'_to']['value'] = '';
                            }
                            elseif ($this->aData[$key.'_period'] !== '')
                            {
                                $aPeriod = explode('##', $this->aData[$key.'_period']);
                                $sValue = '';
                                if (isset($aPeriod[0]))
                                    $sValue = $this->getPeriodValue($aPeriod[0], $value, 'date');
                                
                                $aPeriod = explode('##', $sValue);
                                $this->aHtml[$key.'_from']['value'] = isset($aPeriod[1]) ? $aPeriod[1] : '';
                                $this->aHtml[$key.'_to']['value'] = isset($aPeriod[2]) ? $aPeriod[2] : '';
                            }
                            
                            if ($this->aHtml[$key.'_period']['value'] != '')
                            {
                                $this->aHtml[$key.'_from']['disabled'] = 1;
                                $this->aHtml[$key.'_to']['disabled'] = 1;
                            }
                        }
                        else
                        {
                            if ($nDefId != -1 && '-' != $this->aPeriods[$nDefId]['from'] && '' != $this->aPeriods[$nDefId]['from'])
                            {
                                $sValue = $this->getPeriodValue($nDefId, $value, 'date');
                                $aPeriod = explode('##', $sValue);

                                $this->aHtml[$key.'_from']['value'] = isset($aPeriod[1]) ? $aPeriod[1] : '';
                                $this->aHtml[$key.'_to']['value'] = isset($aPeriod[2]) ? $aPeriod[2] : '';
                            }
                            elseif ($nDefId != -1 && ('-' == $this->aPeriods[$nDefId]['from'] || '' == $this->aPeriods[$nDefId]['from']))
                            {
                                $this->aHtml[$key.'_from']['value'] = '';
                                $this->aHtml[$key.'_to']['value'] = '';
                            }
                        }
                    }
                    break;
                    
                case 'range':
                    $this->aHtml[$key.'_from']['type'] = 'text';
                    $this->aHtml[$key.'_from']['params']['name'] = $this->sFilterName.'['.$key.'_from]';
                    $this->aHtml[$key.'_from']['value'] = isset($this->aData[$key.'_from']) ? htmlspecialchars($this->aData[$key.'_from']) : '';
                    
                    $this->aHtml[$key.'_to']['type'] = 'text';
                    $this->aHtml[$key.'_to']['params']['name'] = $this->sFilterName.'['.$key.'_to]';
                    $this->aHtml[$key.'_to']['value'] = isset($this->aData[$key.'_to']) ? htmlspecialchars($this->aData[$key.'_to']) : '';
                    break;
            }
        }
        $this->aHtml['_submit'] = $this->sFilterName.'_show';
        $this->aHtml['_reset']  = $this->sFilterName.'_reset';
    }
    
    /** Returns array of sql conditions
     * @return array
     */
    public function getSql()
    {
        $aRet = array();
        foreach ($this->aConditions as $key =>$value)
        {
            if ($value['sql'])
                $aRet[] = $value['sql'];
        }
        return $aRet;
    }
    
    /** Validates conditions passed into the filter.
     *  Sets some conditions to default values.
     * 
     * @return bool true - all ok, false - error(s) in condition
     */
    public function validateConditions()
    {
        $aTypes = array('text', 'drop_down', 'date', 'date_range', 'range');
        $aConditions  = array('like','>','<','=','>=','<=','<>');
        
        foreach ($this->aConditions as $key=>$value)
        {
            if (!is_array($value)) 
                $value = array('type'=>$value);
            if (!isset($value['type']))
                $value['type'] = 'text';
            if (!isset($value['condition']))
                $value['condition'] = '=';
            if (!isset($value['visible']))
                $value['visible'] = true;
                
            if (!in_array($value['type'], $aTypes))
            {
                trigger_error('Undefined type "'.$value['type'].'" for `'.$key.'` condition', E_USER_ERROR);
                exit;
            }
            if ('date' == $value['type'] && !in_array($value['condition'], $aConditions))
            {
                trigger_error('Undefined condition "'.$value['condition'].'" for `'.$key.'` condition', E_USER_ERROR);
                exit;
            }
            if (isset($value['all_option']))
            {
                if (!is_array($value['all_option']) || !isset($value['all_option']['name']) || !isset($value['all_option']['value']))
                {
                    trigger_error('Parameter `all_option` for `'.$key.'` condition is incorrect', E_USER_ERROR);
                    exit;
                }
            }
            if ($value['type'] == 'date' || $value['type'] == 'date_range')
            {
                if (!isset($value['date_format']))
                {
                    $value['date_format'] = 'm/d/Y';
                }
                if (!isset($value['datetime_format']))
                {
                    $value['datetime_format'] = 'm/d/Y H:i:s';
                }
            }
            $this->aConditions[$key] = $value;
        }
        return true;
    }
    
    /** Get value of date range for specific period number in folowing format:
     *      '[period_number]##[from_date]##[to_date]'
     * - To specific values: '##' and  ''
     * - [from_date] and [to_date] may be either as date or datetime (depending on parameter $sType)
     *
     * @param integer $nPeriod period number
     * @param array $aCondition one filter condition
     * @param string $sType type of returnable value. may be 'date' or 'datetime'(default)
     * @return string period encoded as string
     */
    public function getPeriodValue($nPeriod, $aCondition, $sType = 'datetime')
    {
        if ($sType != 'date')
            $sType = 'datetime';
            
        $sValue = '';
        if (!array_key_exists($nPeriod, $this->aPeriods))
            return $sValue;
            
        if ('-' == $this->aPeriods[$nPeriod]['from'])
        {
            $sValue='##'; //value to reset
        }
        elseif ('' != $this->aPeriods[$nPeriod]['from'])
        {
            $nFromDate = strtotime($this->aPeriods[$nPeriod]['from']);
            $nToDate = strtotime($this->aPeriods[$nPeriod]['to']);
            if (isset($this->aPeriods[$nPeriod]['start_time']))
            {
                if ($this->aPeriods[$nPeriod]['start_time'] != 'now')
                {
                    list($nH, $nM, $nS) = explode(':', $this->aPeriods[$nPeriod]['start_time']);
                    $nFromDate = mktime($nH, $nM, $nS, date('m', $nFromDate), date('d', $nFromDate), date('Y', $nFromDate));
                }
            }
            if (isset($this->aPeriods[$nPeriod]['end_time']))
            {
                if ($this->aPeriods[$nPeriod]['end_time'] != 'now')
                {
                    list($nH, $nM, $nS) = explode(':', $this->aPeriods[$nPeriod]['end_time']);
                    $nToDate = mktime($nH, $nM, $nS, date('m', $nToDate), date('d', $nToDate), date('Y', $nToDate));
                }
            }
            $sValue = $nPeriod.'##';
            $sFormat = ($sType == 'date') ? $aCondition['date_format'] : $aCondition['datetime_format'];
            
            $sValue .= date($sFormat, $nFromDate).'##'.date($sFormat, $nToDate);
        }
        return $sValue;
    }
}


?>