<?
/*
    Name:           eMail
    Description:    Simple sending eMail in text and HTML with CC, BCC and attachment
    Version:        1.1
    last modified:  2004-05-14
    last modified:  2007-03-28 (added get_mime_type method)

    Autor:          Daniel K�fer
*/

class eMail
{
    public $to = array();
    public $cc = array();
    public $bcc = array();
    public $attachment = array();
    public $boundary = "";
    public $header = "";
    public $subject = "";
    public $body = "";

    public function __construct($name,$mail)
    {
        $this->boundary = md5(uniqid(time()));
        $this->header .= "From: $name <$mail>\n";
    }

    public function to($mail)
    {
        $this->to[] = $mail;
    }

    public function cc($mail)
    {
        $this->cc[] = $mail;
    }

    public function bcc($mail)
    {
        $this->bcc[] = $mail;
    }

    public function attachment($file)
    {
        $this->attachment[] = $file;
    }

    public function subject($subject)
    {
        $this->subject = $subject;
    }

    public function text($text)
    {
        $this->body = "Content-Type: text/plain; charset=ISO-8859-1\n";
        $this->body .= "Content-Transfer-Encoding: 8bit\n\n";
        $this->body .= $text."\n";
    }

    public function html($html)
    {
        $this->body = "Content-Type: text/html; charset=ISO-8859-1\n";
        $this->body .= "Content-Transfer-Encoding: quoted-printable\n\n";
        $this->body .= "<html><body>\n".$html."\n</body></html>\n";
    }

    public function send()
    {
        // CC Empf�nger hinzuf�gen
        $max = count($this->cc);
        if($max>0)
        {
            $this->header .= "Cc: ".$this->cc[0];
            for($i=1;$i<$max;$i++)
            {
                $this->header .= ", ".$this->cc[$i];
            }
            $this->header .= "\n";
        }
        // BCC Empf�nger hinzuf�gen
        $max = count($this->bcc);
        if($max>0)
        {
            $this->header .= "Bcc: ".$this->bcc[0];
            for($i=1;$i<$max;$i++)
            {
                $this->header .= ", ".$this->bcc[$i];
            }
            $this->header .= "\n";
        }
        $this->header .= "MIME-Version: 1.0\n";
        $this->header .= "Content-Type: multipart/mixed; boundary=$this->boundary\n\n";
        $this->header .= "This is a multi-part message in MIME format\n";
        $this->header .= "--$this->boundary\n";
        $this->header .= $this->body;

        // Attachment hinzuf�gen
        $max = count($this->attachment);
        if($max>0)
        {
            for($i=0;$i<$max;$i++)
            {           
                $mime = $this->get_mime_type($this->attachment[$i]);
                $file = fread(fopen($this->attachment[$i], "r"), filesize($this->attachment[$i]));
                $this->header .= "--".$this->boundary."\n";
                $this->header .= "Content-Type: $mime; name=".$this->attachment[$i]."\n";
                $this->header .= "Content-Transfer-Encoding: base64\n";
                $this->header .= "Content-Disposition: attachment; filename=".$this->attachment[$i]."\n\n";
                $this->header .= chunk_split(base64_encode($file))."\n";
                $file = "";
            }
        }
        $this->header .= "--".$this->boundary."--\n\n";

        foreach($this->to as $mail)
        {
            mail($mail,$this->subject,"",$this->header);
        }
    }    
    
    public function get_mime_type($file)
    {
        static $MimeTypes = Array(
            '.gif'  => 'image/gif',
            '.jpg'  => 'image/jpeg',
            '.jpeg' => 'image/jpeg',
            '.jpe'  => 'image/jpeg',
            '.bmp'  => 'image/bmp',
            '.png'  => 'image/png',
            '.tif'  => 'image/tiff',
            '.tiff' => 'image/tiff',
            '.swf'  => 'application/x-shockwave-flash',
            '.doc'  => 'application/msword',
            '.xls'  => 'application/vnd.ms-excel',
            '.ppt'  => 'application/vnd.ms-powerpoint',
            '.pdf'  => 'application/pdf',
            '.ps'   => 'application/postscript',
            '.eps'  => 'application/postscript',
            '.rtf'  => 'application/rtf',
            '.bz2'  => 'application/x-bzip2',
            '.gz'   => 'application/x-gzip',
            '.tgz'  => 'application/x-gzip',
            '.tar'  => 'application/x-tar',
            '.zip'  => 'application/zip',
            '.rar'  => 'application/rar',
            '.js'   => 'text/javascript',
            '.html' => 'text/html',
            '.htm'  => 'text/html',
            '.txt'  => 'text/plain',
            '.css'  => 'text/css'
        );
        
        $att = StrrChr(StrToLower($file), ".");
        if(!IsSet($MimeTypes[$att]))
            return "application/octet-stream";
        else
            return $MimeTypes[$att];
    }
 
}
?>
