<?php
	
	// require 3rd party class
	require_once("../lib/3rdparty/xmlparser.class.php");

	/*
		Office 2003 XML spreadsheets parser
		
		Usage example:
				
	
			$mapping = array('ID' => array('id', 'numeric'), 'Card ID' => array('card', 'numeric'), 'PIN' => 'value');
			$parser = new XlsXml2Array('../../_work/excel_upload/pins.xml.xls');
			$table = $parser->ToHash($mapping, 1);

			echo '<pre>';
			if($parser->HasErrors())
			{
				print_r($parser->Errors);
			}
			else
			{
				print_r($table);
			}
			echo '</pre>';

	*/
	class XlsXml2Array
	{
		var $_parser;
		var $_targetEncoding;
		var $_sourceEncoding;

		var $Errors;
		
		/* 
			ctor
		*/
		function XlsXml2Array($fileName, $targetEncoding = "Windows-1251", $sourceEncoding = "UTF-8") 
		{
			$this->_parser = new SimpleXmlParser($fileName);
			$this->_targetEncoding = $targetEncoding;
			$this->_sourceEncoding = $sourceEncoding;
			$this->Errors = array();
		} 
		
		function HasErrors()
		{
			return count($this->Errors);
		}
		
		/*
			Parses xls sheet into array of hashes.
			$mapping - hash mapping between the headers in xls and mysql field names
			$headerOffset - zero-based index of row in xls which contains headers
		*/
		function ToHash($mapping = null, $headerOffset = 0)
		{
			$hash = array();
			$arr = $this->ToArray();
			$tj = 0;
			
			while(list($j, $row) = each($arr))
			{
				if($j <= $headerOffset)
				{
					continue;
				}
				
				$hash[$tj] = array();
				
				while(list($i, $cell) = each($row))
				{
					if(!isset($arr[$headerOffset][$i]))
					{
						continue;
					}
					
					$key = $arr[$headerOffset][$i];
					
					if(isset($mapping))
					{
						if(isset($mapping[$key]))
						{
							if(is_array($mapping[$key]))
							{
								$type = $mapping[$key][1];
								$typeValid = true;							
								$key = $mapping[$key][0];

								eval('$typeValid = is_'.$type.'($cell);');					
								
								if(!$typeValid)
								{
									$this->Errors[] = "Error: '$cell' is not a valid $type at row ".($j+1).", cell ".($i+1).".";
								}
							}
							else
							{
								$key = $mapping[$key];
							}
							$hash[$tj][$key] = $cell;
						}
					}
					else
					{					
						$hash[$tj][$key] = $cell;
					}
				}
				
				$tj += 1;
			}
			
			return $hash;
		}
		
		/*
			Parses xls sheet into 2d array.
		*/
		function ToArray()
		{
			$table = $this->_GetTable();
			
			return $this->_TableToArray($table);
		}
		
		function _GetTable()
		{
			$root = $this->_parser->getRoot();
			$workbook = $root->getChildrenTagsArray();
			$worksheet = $workbook['WORKSHEET']->getChildrenTagsArray();
			
			return $worksheet['TABLE'];
		}
		
		function _TableToArray(&$table)
		{
			$xml = array();
			$ti = 0;
			
			$this->Errors = array();
			
			for($i = 0; $i < $table->getChildrenCount(); $i++)
			{
				$row = $table->getChildren($i);
		
				if($row->tag != 'ROW')
				{
					continue;
				}
		
				if(isset($row->attrs['SS:INDEX']))
				{
					$ti = $row->attrs['SS:INDEX'] - 1;
				}

				$xml[$ti] = $this->_ParseRow($row);

				$ti += 1;
			}
			
			return $xml;
		}
		
		function _ParseRow(&$row)
		{
			$xml = array();
			$tj = 0;
			
			for($j = 0; $j < $row->getChildrenCount(); $j++)
			{
				$cell = $row->getChildren($j);
			
				if($cell->tag != 'CELL')
				{
					continue;
				}

				if(isset($cell->attrs['SS:INDEX']))
				{
					$tj = $cell->attrs['SS:INDEX'] - 1;
				}
				
				$data = $cell->getChildren(0);
				
				$xml[$tj] = mb_convert_encoding($data->value, $this->_targetEncoding, $this->_sourceEncoding);
				
				$tj += 1;
			}
			
			return $xml;
		}
	}
	
	class Array2XlsXml
	{
		var $table;
		var $headers;
		
		function Array2XlsXml($table, $headers = array())
		{
			$this->table = $table;
			$this->headers = $headers;
			$this->CorrectEncoding();
		}
		
		function CorrectEncoding()
		{
			while(list($j, $row) = each($this->table))
			{
				while(list($i, $val) = each($row))
				{
					$this->table[$j][$i] = mb_convert_encoding($val, 'UTF-8', 'Windows-1251');
				}
			}
			/*
			while(list($j, $row) = each($this->headers))
			{
				while(list($i, $val) = each($row))
				{
					$this->headers[$j][$i] = mb_convert_encoding($val, 'UTF-8', 'Windows-1251');
				}
			} */
		}
		
		function RenderTemplate($template)
		{
			$tpl =& new Smarty;
			$headers = (count($this->headers) > 0) ? $this->headers : array_keys($this->table[0]);
			
			
			$tpl->register_function('is_numeric', '');
			$tpl->assign('table', $this->table);    
			$tpl->assign('header', $headers);
			

			return $tpl->fetch($template);
		}
	}
?>