<?php
/*
	Script:
		Deathead's Timer Class              
	Version:
		1.0
	Editor:
		EditPlus V2.01b
	Description
		A small class used to track the amount of time spent inside a php script. The script comes complete with
		start, stop, pause, unpause, and rest functions to completely control the timer. You are also able to get a
		"running time" without stopping the timer. This class is very user-friendly and offer it's own error system.
*/

class timer
{
	# Variables
	public $strt;
	public $stp;
	public $paus;
	public $unpaus;
	public $errno;
	public $errmsg;
	public $final;

	// Constructor
	public function timer($auto_start=0)
	{
		$this->strt = 0;
		$this->stp = 0;
		$this->paus = array();
		$this->unpaus = array();
		if($auto_start)
		{
			$this->start();
		}
		return 1;
	}

	public function start()
	{
		if(!$this->strt)
		{
			$mtime1 = microtime();
			$mtime1 = explode(" ",$mtime1);
			$mtime1 = $mtime1[1] + $mtime1[0];
			$this->strt = $mtime1;
			return 1;
		}
		else
		{
			$this->errno="01";
			$this->errmsg="Timer has already been started.";
			return 0;
		}
	}

	public function stop()
	{
		if(!$this->strt || $this->stp)
		{
			if($this->stp)
			{
				$this->errno="02";
				$this->errmsg="Timer has already been stopped.";
			}
			else
			{
				$this->errno="03";
				$this->errmsg="Timer has not been started.";
			}
			return 0;
		}
		else
		{
			$mtime1 = microtime();
			$mtime1 = explode(" ",$mtime1);
			$mtime1 = $mtime1[1] + $mtime1[0];
			$this->stp = $mtime1;
			$this->final = $this->stp - $this->strt;
			if($i=count($this->paus))
			{
				$subtract =0;
				while(list($k,$v)=each($this->paus))
				{
					if(isset($this->unpaus[$k]))
					{
						$subtract += $this->unpaus[$k]-$v;
					}
					else
					{
						$subtract += $this->stp - $v;
					}
				}
				reset($this->paus);
				reset($this->unpaus);
				$this->final-= $subtract;
			}
			return 1;
		}
	}

	public function pause()
	{
		if(count($this->paus)!=count($this->unpaus))
		{
			$this->errno="04";
			$this->errmsg="Timer is already paused.";
			return 0;
		}
		elseif(!$this->strt)
		{
			$this->errno="03";
			$this->errmsg="Timer has not been started.";
			return 0;
		}
		elseif($this->stp)
		{
			$this->errno="02";
			$this->errmsg="Timer has already been stopped.";
			return 0;
		}
		else
		{
			$mtime1 = microtime();
			$mtime1 = explode(" ",$mtime1);
			$mtime1 = $mtime1[1] + $mtime1[0];
			$this->paus[] = $mtime1;
			return 1;
		}
	}

	public function unpause()
	{
		if(count($this->paus)==count($this->unpaus))
		{
			$this->errno="05";
			$this->errmsg="Timer is not unpaused.";
			return 0;
		}
		elseif(!$this->strt)
		{
			$this->errno="03";
			$this->errmsg="Timer has not been started.";
			return 0;
		}
		elseif($this->stp)
		{
			$this->errno="02";
			$this->errmsg="Timer has already been stopped.";
			return 0;
		}
		else
		{
			$mtime1 = microtime();
			$mtime1 = explode(" ",$mtime1);
			$mtime1 = $mtime1[1] + $mtime1[0];
			$this->unpaus[] = $mtime1;
			return 1;
		}
	}

	public function get()
	{
		if(!$this->strt)
		{
			$this->errno="03";
			$this->errmsg="Timer has not been started.";
			return -1;
		}
		if(!$this->stp)
		{
			$temp = 0;
			$mtime1 = microtime();
			$mtime1 = explode(" ",$mtime1);
			$mtime1 = $mtime1[1] + $mtime1[0];
			$temp_p = $mtime1;
			$temp = $temp_p - $this->strt;
			if(count($this->paus))
			{
				$subtract =0;
				while(list($k,$v)=each($this->paus))
				{
					if(isset($this->unpaus[$k]))
					{
						$subtract += $this->unpaus[$k]-$v;
					}
					else
					{
						$subtract += $temp_p - $v;
					}
				}
				reset($this->paus);
				reset($this->unpaus);
				$temp -= $subtract;
			}
			return $temp;
		}
		else
		{
			return $this->final;
		}
	}

	public function reset()
	{
		$this->strt = 0;
		$this->stp = 0;
		$this->paus = array();
		$this->unpaus = array();
		return 1;
	}
}

?>