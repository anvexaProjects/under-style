<?php
	
	// require 3rd party class
	require_once '../lib/3rdparty/xlsparser/excel.php';
	require_once '../lib/3rdparty/xlsparser/cp1251.inc'; 

	
	class XlsBin2Array
	{
		var $_parser;
		var $Errors;
		var $cp1251;
		
		/* 
			ctor
		*/
		function XlsBin2Array($excel_file) 
		{
			$this->Errors = array();
			$this->cp1251 = Get1251Chars();
			$this->cp1251 = array_flip($this->cp1251);
			
			$fh = @fopen ($excel_file,'rb');
			if( !$fh ) $this->Errors[] = ERR_XLS_BIN_NO_FILE; 
			if( filesize($excel_file)==0 )  $this->Errors[] = ERR_XLS_BIN_NO_FILE;
		
			$fc = fread( $fh, filesize($excel_file) );
			@fclose($fh);
			if( strlen($fc) < filesize($excel_file) )
				 $this->Errors[] = ERR_XLS_BIN_CANT_READ_FILE;
			
			
			// �������� excel �����
			
			$this->_parser = new ExcelFileParser ();
			$res = $this->_parser->ParseFromString($fc);
			
			switch ($res) {
				case 0: break;
				case 1: $this->Errors[] = ERR_XLS_BIN_CANT_OPEN;
				case 2: $this->Errors[] = ERR_XLS_BIN_TOO_SMALL;
				case 3: $this->Errors[] = ERR_XLS_BIN_READ_HEADER;
				case 4: $this->Errors[] = ERR_XLS_BIN_READ_FILE;
				case 5: $this->Errors[] = ERR_XLS_BIN_NO_EXCEL;
				case 6: $this->Errors[] = ERR_XLS_BIN_CORRUPTED;
				case 7: $this->Errors[] = ERR_XLS_BIN_NO_DATA_FOUND;
				case 8: $this->Errors[] = ERR_XLS_BIN_NOT_SUPPORTED;
		
				default:
					 $this->Errors[] = ERR_XLS_BIN_UNKNOWN;
			}
			
		} 
		
		function HasErrors()
		{
			return count($this->Errors);
		}
		
		/*
			Parses xls sheet into array of hashes.
			$mapping - hash mapping between the headers in xls and mysql field names
			$headerOffset - zero-based index of row in xls which contains headers
		*/
		function ToHash($mapping = null, $headerOffset = 0)
		{
			$hash = array();
			$arr = $this->ToArray();
			$tj = 0;
			
			while(list($j, $row) = each($arr))
			{
				if($j <= $headerOffset)
				{
					continue;
				}
				
				$hash[$tj] = array();
				
				while(list($i, $cell) = each($row))
				{
					if(!isset($arr[$headerOffset][$i]))
					{
						continue;
					}
					
					$key = $arr[$headerOffset][$i];
					
					if(isset($mapping))
					{
						if(isset($mapping[$key]))
						{
							if(is_array($mapping[$key]))
							{
								$type = $mapping[$key][1];
								$typeValid = true;							
								$key = $mapping[$key][0];

								eval('$typeValid = is_'.$type.'($cell);');					
								
								if(!$typeValid)
								{
									$this->Errors[] = "Error: '$cell' is not a valid $type at row ".($j+1).", cell ".($i+1).".";
								}
							}
							else
							{
								$key = $mapping[$key];
							}
							$hash[$tj][$key] = $cell;
						}
					}
					else
					{					
						$hash[$tj][$key] = $cell;
					}
				}
				
				$tj += 1;
			}
			
			return $hash;
		}
		
		/*
			Parses xls sheet into 2d array.
		*/
		function ToArray()
		{
			// ���������� �������� �����
			
			$ws_number = count($this->_parser->worksheet['name']);
			if( $ws_number < 1 ) $this->Errors[] = ERR_XLS_BIN_NO_WORKSHEET;
			
			$ws_number = 1; // ���������, ����� ���������� ������ ������ ������� ����
			
			for ($ws_n = 0; $ws_n < $ws_number; $ws_n++) {
				
				$ws = $this->_parser->worksheet['data'][$ws_n]; // ��������� ������ �� �������� �����
				
				$max_row = $ws['max_row'];
				$max_col = $ws['max_col'];
				
				if ( $max_row > 0 && $max_col > 0 )
					return $this->getTable ($ws); // ��������� ��������� � ������ �������� �����
				else $this->Errors[] =  ERR_XLS_BIN_EMPTY_WORKSHEET;
			}

			return ERROR;
		}
		


	   function getTable($ws)
	   {
		  $data = $ws['cell'];
		  $aTable = array();
		
		  foreach( $data as $i => $row ) 
		  {
				$tRow = array();

				for ( $j = 0; $j <= $ws['max_col']; $j++ ) 
				{
					$tRow[$j] = $this->getItem( $row[$j] );
				}
		
			$aTable[$i] = $tRow;
			$i++;
		  }
		
			return $aTable;
		}



		function getItem($data)
		{	
		
		  switch( $data['type'] )
		  {
			  // ������
			  case 0:
				$ind = $data['data'];
				if( $this->_parser->sst[unicode][$ind] )
				  return $this->uc2html($this->_parser->sst['data'][$ind]);
				else
				  return $this->_parser->sst['data'][$ind];
			
			  // �����
			  case 1:
				return (integer) $data['data'];
			
				// ������������ �����
			  case 2:
				return (float) $data['data'];
				
			  case 3:
				return gmdate("m-d-Y",$this->_parser->xls2tstamp($data[data]));
			
			  default:
				return '';
		  }
		}

		
		
function uc2html($str) {
	$ret = '';
	for( $i=0; $i<strlen($str)/2; $i++ ) {
		$charcode = ord($str[$i*2])+256*ord($str[$i*2+1]);
		$ret .= '&#'.$charcode;
	}
	return $ret;
}


/*		
		function uc2html($str)
		{
			  $ret = '';
			
			  for( $i=0; $i<strlen($str)/2; $i++ )
			  {
				   $charcode = ord($str[$i*2]) + 256*ord($str[$i*2+1]);
				   $ret .=  chr($this->cp1251[$charcode]);
			  }
			  return $ret;
		}
*/
	}
	
?>