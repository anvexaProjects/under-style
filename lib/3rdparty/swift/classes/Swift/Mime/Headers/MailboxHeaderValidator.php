<?php

class Swift_Mime_Headers_MailboxHeaderValidator extends Swift_Mime_Headers_MailboxHeader
{
  /**
   * Creates a new MailboxHeader with $name.
   * @param string $name of Header
   * @param Swift_Mime_HeaderEncoder $encoder
   */
  public function __construct()
  {
    $this->initializeGrammar();
  }
  
  public function isEmail($address)
  {
    return preg_match('/^' . $this->getGrammar('addr-spec') . '$/D', $address);
  }
  
}