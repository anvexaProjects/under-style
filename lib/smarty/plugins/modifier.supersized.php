<?php
function smarty_modifier_supersized($images)
{
    $images = unserialize($images);
    $rs = array();
    foreach ($images as $k => $im)
    {
        $rs[$k]['image'] = $_GET['global']['root'] . '/upload/photo/'.$im['image_name'];
        $rs[$k]['title'] = $im['image_desc'];
        $rs[$k]['url'] = '';
    }
    return json_encode($rs);
}

/* vim: set expandtab: */

?>
