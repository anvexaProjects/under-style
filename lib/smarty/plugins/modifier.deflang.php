<?php

function smarty_modifier_deflang($lang, $def = 'ru')
{
	if ($def == $lang) return '';
	return "_$lang";
}

/* vim: set expandtab: */

?>
