<?php

function smarty_modifier_get_books_params($set, $param)
{
	if (empty($set) || empty($param)) return array();
	preg_match_all("/$param=(\d+)/", $set, $out);
	if (empty($out[1])) return array();
    return implode(';', $out[1]);
}


?>
