<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_modifier_format_price($price)
{	
	if(strlen($price) <=3)  return $price;
	$trig = 0;
	$string = '';
	$price = (string)$price;

	for($i = strlen($price)-1; $i >= 0;$i--){
		if($trig == 3){
		   		$string .= " ";
           		$trig = 1;
        	}else{
           		$trig++;
		}
	 	$string .= $price[$i];		
	}

	return strrev($string);
}

?>
