<?php


function smarty_modifier_subdomain($site_rel, $path){
    require '../templates/config/enum.inc';
    $subdomains = $enum['subdomains'];

    $subdomain_name = substr($path, 0, strpos($path, '/'));
    $subdomain_name = $subdomain_name ? $subdomain_name : $path;
    foreach ($subdomains as $alias => $domain){
        if ($subdomain_name == $alias){
            $site_rel = 'http://'.$domain.$site_rel;
            return $site_rel;
        }
    }
    return 'http://'.DOMAIN_0.$site_rel;
}
?>