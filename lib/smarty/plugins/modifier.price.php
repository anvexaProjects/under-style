<?php

function smarty_modifier_price($string)
{
    $exp = explode('.', $string);
    if (!isset($exp[1])) return $exp[0].',-';
    if (isset($exp[1]) && (int)$exp[1] == 0) return $exp[0].',-';
    return str_replace('.', ',', $string); 
}


?>
