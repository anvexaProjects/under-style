<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     symbol<br>
 * Purpose:  returns symbol by index
 * @param string
 * @param index
 * @return string
 */
function smarty_modifier_symbol($string, $index)
{
    return $string{$index - 1};
}

?>
