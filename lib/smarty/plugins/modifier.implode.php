<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty cat modifier plugin
 *
 * Type:     modifier<br>
 * Name:     implode<br>
 * Date:     Nov 4, 2008
 * Purpose:  implode an array
 * Input:    string/array to implode
 * @version 1.0
 * @param array
 * @param string
 * @return string
 */
function smarty_modifier_implode($array, $separator = ', ')
{
    if (!empty($array) && is_array($array)) return implode($separator, $array);
    return false;
}

/* vim: set expandtab: */

?>
