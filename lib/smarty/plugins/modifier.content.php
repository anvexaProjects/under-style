<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     content<br>
 * Purpose:  convert [$root] lexem to real value
 * @param string
 * @return string
 */
function smarty_modifier_content($string)
{
  $lexem = '[$root]';
  return  str_replace(array($lexem, 'href="/admin/', "href='/admin/"), array($_GET['global']['root'], 'href="', "href='"), $string);
}

?>
