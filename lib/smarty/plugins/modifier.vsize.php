<?php

function smarty_modifier_vsize($string, $w, $h)
{
    $string = preg_replace('/width="(\d+)"/', 'width="'.$w.'"', $string);
    $string = preg_replace('/height="(\d+)"/', 'height="'.$h.'"', $string);
	
	$string = str_replace("<embed", '<param name="wmode" value="opaque"></param><embed wmode="opaque"', $string);
    return $string;
}

/* vim: set expandtab: */

?>
