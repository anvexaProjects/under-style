<?php

function smarty_modifier_filetype($string)
{
    $ext = array(
    	'word'  => array('doc', 'docx'),
    	'excel' => array('xls', 'xlsx', 'csv'),
    	'pdf'   => array('pdf'),
    );
    $file_ext = pathinfo($string, PATHINFO_EXTENSION);
    foreach ($ext as $app => $exts) {
        if(in_array($file_ext, $exts)) return $app;
    }
    return 'other';
}


?>
