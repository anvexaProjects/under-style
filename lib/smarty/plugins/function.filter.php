<?php
/**
 * @package Smarty
 * @subpackage plugins.custom
 */
/** Create filter block 
 * 
 * @see Filter
 */
function smarty_function_filter($params, &$smarty)
{
    $aData = $params['data'];
    unset($params['data']);

    $aParams = array_merge($aData['params'], $params);
    $sOut = '';
    /*
    if ($aData['type'] == 'select' && isset($aData['date_range']) && $aData['date_range'])
    {
        $sOut .= '<script language="JavaScript" type="text/javascript" src="design/js/date.js"></script>
        	     <script language="JavaScript" type="text/javascript" src="design/js/filter.js"></script>';
    }
    if ($aData['type'] == 'text' && !empty($aData['is_calendar']) && isset($aParams['id']))
    {
        $cs = &new CalendarState();
        if ($cs->exist)
        {	
            $sOut .= '<script type="text/javascript" src="' . SITE_REL .'/templates/js/calendar/calendar.js"></script>
                      <script type="text/javascript" src="' . SITE_REL .'/templates/js/calendar/calendar-en.js"></script>
                      <script type="text/javascript" src="' . SITE_REL .'/templates/js/calendar/calend.js"></script>
                      <link rel="stylesheet" type="text/css" media="all" href="'. SITE_REL .'/templates/css/calendar.css" />';
        }
    }*/

    switch ($aData['type'])
    {
        case 'text':
            $sOut .= '<input type="text"';
            $aParams['value'] = $aData['value'];
            foreach ($aParams as $sParam => $sValue)
            {
                $sOut .= ' '.$sParam.'="'.$sValue.'"';
            }
            $sOut .= ' />';

            /*
            if (!empty($aData['is_calendar']) && isset($aParams['id']))
            {
                $sOut .= "<input type='reset' value='' onclick=\"return showCalendar('". $aParams['id'] ."', '".DATE_FORMAT."');\" class='calend_filter' id='".$aParams['id']."_button'/>";
            }
            */
            if (isset($aData['date_range']) && $aData['date_range'])
            {
                /*$sOut .= '<script language="JavaScript">document.getElementById("'.$aParams['id'].'").disabled='.$aData['disabled'].';';
                if (!empty($aData['is_calendar']))
                {
                    $sOut .= ' document.getElementById("'.$aParams['id'].'_button").disabled='.$aData['disabled'].';';
                }
                $sOut .= '</script>';
				*/
                //$sOut .= '<script language="JavaScript">$(function({$("#'.$aParams['id'].'").datepicker({dateFormat: "dd.mm.yy"});});)</script>'; 
                $sOut .= '<script language="JavaScript">$(function(){$("#'.$aParams['id'].'").datepicker({dateFormat: "dd.mm.yy"});});</script>';
            }
            break;
            
        case 'select':
            $sOut .= '<select';

            foreach ($aParams as $sParam => $sValue)
            {
                $sOut .= ' '.$sParam.'="'.$sValue.'"';
            }
            $sOut .= '>';
            foreach ($aData['select'] as $sKey => $sValue)
            {
            	$sOut .= '<option value="'.$sKey.'"';
            	$sOut .= (strval($sKey) === strval($aData['value'])) ? ' selected="selected"' : '';
            	$sOut .= '>'.$sValue.'</option>';
            }
            $sOut .= '</select>';
            break;
    }
    return $sOut;
}

class CalendarState
{
    var $exist;

    function CalendarState()
    {
        $this->exist = false;
        static $sSrc = null;
        if ($sSrc == null)
        {
            // this code run once (static field)
            $sSrc = true;
            $this->exist = true;
       }
    }
}
?>