<?php
function smarty_modifier_http($string)
{
    $prefix = "http://";
    if (substr($string, 0, strlen($prefix)) != $prefix) return $prefix.$string;
    else return $string;
}

?>
