<?php                                
/*
  Description: Basedate labels module
  Autor:       Oleg Koshkin
  Data:        23-08-2009
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2009 Oleg Koshkin
*/

class BaseDateLabels
{
	public  $month, $lmonth, $smonth;

	public function __construct()
	{
		$this->month = array();
		$this->lmonth = array();
		$this->smonth = array();
	}
	
	
	// 20 января 2007
	public function getMiddleMonth($num)
	{
		return 	$this->month[(int)$num];
	}
    
	// январь 2007
	public function getNativeMonth($num)
	{
		return $this->lmonth[(int)$num];
	}

	// янв
	public function getShortMonth($num)
	{	
		return $this->smonth[(int)$num];
	}


	// January 2007
	public function getBigMonth($num)
	{ 	
		$month = $this->lmonth[(int)$num];
	    return strtoupper( substr($month, 0, 1)) . substr($month, 1, 90);
	}


	public function getMonthList($nStartYear = '2007')
	{	
		$list = array();
		$curr_year = strftime("%Y");
		$curr_month = strftime("%m");

		
		// start from january 2007
		for($year = $nStartYear; $year <= $curr_year; $year++)
			for($month = 1; $month <= 12; $month++)
				if (($year != $curr_year) || (($year == $curr_year) && ($month <= $curr_month)))
				{
					$key = $year . '_' . $month;
					$list[$key] = $this->getBigMonth($month)  . ' ' . $year;
				}
				
		return 	$list;
	}
	
}
	
?>