<?php                                
/*
  Description: En date labels module
  Autor:       Oleg Koshkin
  Data:        23-08-2009
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2009 Oleg Koshkin
*/

require_once 'basedatelabels.class.php';

class DateLabelsEn extends BaseDateLabels
{
	public function __construct()
	{
		$month = array();
		$month[0] = '';
		$month[1] = 'januаry';
		$month[2] = 'february';
		$month[3] = 'march';
		$month[4] = 'april';
		$month[5] = 'may';
		$month[6] = 'june';
		$month[7] = 'july';
		$month[8] = 'august';
		$month[9] = 'september';
		$month[10] = 'october';
		$month[11] = 'november';
		$month[12] = 'december';   	
		$this->month = $month;
		
		$this->lmonth = $month;
		
		$smonth = array();
		$smonth[0] = '';
		$smonth[1] = 'jan';
		$smonth[2] = 'feb';
		$smonth[3] = 'mar';
		$smonth[4] = 'apr';
		$smonth[5] = 'may';
		$smonth[6] = 'jun';
		$smonth[7] = 'jul';
		$smonth[8] = 'aug';
		$smonth[9] = 'sep';
		$smonth[10] = 'oct';
		$smonth[11] = 'nov';
		$smonth[12] = 'dec';   
		$this->smonth = $smonth;
	}
	
}
	
?>