<?php              
/*
  Description: Validator Module
  Autor:       Oleg Koshkin
  Data:        15-05-2007
  Version:     1.73

  mailto:      WizardF@narod.ru
  Сopyright:   (C) 2005 Oleg Koshkin
  
  hystory:
  1.64 function IsEmpty() added
  1.64 function unescapehtmlchars() added
  1.65 constant RX_URL_SYM added
  1.66 function to_cms() and from_cms() added
  1.68 function from_cms() fix
  1.69 constant RX_FLOAT added
  1.70 unction IsEmptyContent() added 
  1.71 refactored to use classes  
  1.72 fixed IsNumber function 
  1.73 RX_LOGIN and RX_NAME added
  
*/

define('RX_STR', '/^[ёЁa-zA-Zа-яА-Я0-9 ._()*@!%|\~`";:?,-\/]+$/');
define('RX_ALIAS', '/^[a-zA-Z0-9_-]+$/');
define('RX_EMPTY', '.*');
define('RX_URL_SYM', '/^[a-zA-Z0-9-]+$/');
define('RX_URL_FORMAT', "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i");
define('RX_FLOAT', '/^[0-9\.]+$/');

define('RX_LOGIN', '/^[a-zA-Z0-9]+$/');
define('RX_NAME', '/^[ёЁa-zA-Zа-яА-Я0-9 ._-]+$/');



class Val
{
        static function IsText($param)
        { return !eregi('[^ёЁa-zA-Zа-яА-Я0-9_ \-]',$param); }
        
        static function IsTextEx($param)
        { return !eregi(RX_STR, $param); }
        
        static function IsNumber($param)
        { 
            if (Val::IsEmpty($param)) 
                return false;
            else
                return !eregi('[^0-9]',$param); 
        }
        
        static function IsStr($param)
        { return !eregi('[^ёЁa-zA-Zа-яА-Я]',$param); }
        
        static function IsEmail($param)
        { return preg_match('%[-\.\w]+@[-\w]+(?:\.[-\w]+)+%', $param); }
        
        static function IsEmpty($param)
        { return (strlen(trim($param)) == 0); }
        
        static function IsEmptyContent($param)
        {
            $from = array( '&amp;','&quot;','&#039;','&lt;','&gt;', '&nbsp;');
            $to = array( '', '','','','',''); 
        
            $res = strip_tags($param);
            $res = str_replace($from, $to, $res);
            return Val::IsEmpty($res);
        }
        
        
        static function IsContainInjection($param)
        {
            // TODO: add non secure items to array
            $not_allowed = array('javascript','<?','?>','http://'); 
            foreach ($not_allowed as $el)
            {    
                // три знака равенства
                // если подстрока не найдена, возвращает FALSE
                if ( !(strpos($param, $el) === false) ) return true;
            }
            
            // no special phrases found
            return false;
        }
        
        
        
        static function IsSimple($param)
        { 
            $pass_error = array();
            
            if (strlen($param) < 9 ) $pass_error['len'] = 'Длина паgоля должна быть больше 8 символов';
            if ( is_array($pass_error) && (count($pass_error) > 0) ) return $pass_error;
            else return 0;
        }
        
        static function UnescapeHtmlChars($string)
        {
            $from = array( '&amp;','&quot;','&#039;','&lt;','&gt;');
            $to = array( '&', '"','\'','<','>'); 
            $string = str_replace($from, $to, $string);
            return $string;      
        }
        
        
        static function FromDb($string)
        {
            return  Val::UnescapeHtmlChars(trim(stripslashes($string)));
        }
        
        //  Чистка строки
        static function ToDb($string)
        {
            $string = trim($string);                           // delete spaces at the beginning and the end
            $string = htmlspecialchars($string, ENT_QUOTES); // <TAG>  -> &lt;TAG&gt;
            $string = addslashes($string);                    
            return  $string;                 
        }
        
        // to avoid SQL Enjection
        static function ToDbEx($string)
        {
            $from = array( 'ALTER', 'CREATE', 'DROP', 'SELECT', 'UPDATE', 'DELETE',
                         'UNION', 'LIMIT', 'INSERT', 'ASC', 'DESC', 'LIKE', 'FROM');
        
            $to = array( "'ALTER'", "'CREATE'", "'DROP'", "'SELECT'", "'UPDATE'", "'DELETE'",
                         "'UNION'", "'LIMIT'", "'INSERT'", "'ASC'", "'DESC'", "'LIKE'", "'FROM'");
            
            $string = str_replace($from, $to, $string);
            return Val::ToDb($string);             
        }
        
        static function ToCms($string)
        {
            return str_replace('\'', '&#039;', $string);
        }
        
        static function FromCms($string)
        {
            $string = str_replace('&#039;', '\'', $string);
            //return htmlentities($string);
            return $string;
        }
}

        // standalone function to use Val::FromDb in array_walk function
        function WalkFromDb(&$value, $key)
        {
            $value = Val::FromDb($value);
        }
        
        // standalone function to use Val::ToDb in array_walk function
        function WalkToDb(&$value, $key)
        {
            $value = Val::ToDb($value);
        }

?>
