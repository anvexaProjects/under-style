<?php
/*
  Description: URL builder
  Autor:       Oleg Koshkin
  Data:        07-06-2007
  Version:     1.1

  mailto:      WizardF@narod.ru
  �opyright:   (C) 2007 Oleg Koshkin

  hystory:
  1.0 initial version
  1.1 migrate to path navigation instead of alias

*/
require_once '../lib/infrastruct/validator.class.php';

// because $_GET array is global, there is no reasonable to implement singleton
// TODO: add light mode for admin
class URL
{
	// private variables, do not call it directly (PHP4 does not allow to create private fields)
	private $alias, $lan, $pdir, $tdir, $url_prefix, $root, $module;

	//----------- init logic  ----------

	function __construct()
	{
	}


	public function Init($root)
	{
		$this->ProcessQuery();

		$this->root = $root;
	    //$this->tdir = ($this->lan == 'en') ? 'ru_master' : 'master';
	    //$this->pdir = ($this->lan == 'en') ? 'ru' : 'pub';
	    $this->tdir = 'master';
	    $this->pdir = '';
		$this->url_prefix = $this->root . '/' .  $this->lan ;

		$this->Globalize();

		//print_r($_GET);
		//die();
	}

	public function ProcessQuery()
    {
        require '../templates/config/enum.inc';
        // get parameters
        $params = explode('/', $_GET['path']);
        $pcnt = count($params);


        //parse page
        $this->lan = (2 == strlen($params[0]) && array_key_exists($params[0], $enum['langs'])) ? $params[0] : DEFAULT_LANG;
		$sClrPath = '';

        //parse parameters and save into $_GET array
        for($i=0; $i<$pcnt; $i++)
        {
            if (strpos($params[$i], '='))
            {
                $val = explode('=', $params[$i]);
                $_GET[$val[0]] = $val[1];
            }
            else
            {
                $this->alias = $params[$i];
                $sClrPath .= $params[$i] . '/';
	        }
        }

        if (strlen($_GET['path']) == 0) $_GET['path'] = 'index';
        if ( strlen($sClrPath) > 1) $sClrPath = substr($sClrPath, 0, -1);
        $_GET['path'] = $sClrPath; // clear path from GET params
    }

	public function Globalize()
	{
		$_GET['global']['alias'] = $this->alias;
		$_GET['global']['lan'] = $this->lan;
		$_GET['global']['pdir'] = $this->pdir;
		$_GET['global']['tdir'] = $this->tdir;
		$_GET['global']['root'] = $this->root;
		$_GET['global']['module'] = $this->module;
		$_GET['global']['path'] = $_GET['path'];
	}

	// ------------------------
	static function GetLan()
	{
		return $_GET['global']['lan'];
	}

	static function GetPDir()
	{
		return $_GET['global']['pdir'];
	}

	static function GetTDir()
	{
		return $_GET['global']['tdir'];
	}

	static function GetRoot()
	{
		return $_GET['global']['root'];
	}

	public static function GetModule()
	{
		return $_GET['global']['module'];
	}

	public static function GetLevel()
	{
		return $_GET['global']['level'];
	}

	static function GetUrlPrefix()
	{
		return $_GET['global']['root'] . '/' . $_GET['global']['lan'] ;
	}

	static function GetPath()
	{
		return $_GET['path'];
	}

}
?>
