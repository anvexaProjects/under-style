<?
/*
  Description: Global functions
  Autor:       Oleg Koshkin
  Data:        12-07-2010
  Version:     1.41

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
  
  hystory:
  1.1 mail notification added
  1.2 added functions: GetList(), array2hash() 
  1.3 added functions: DeleteItem() 
  1.31 added file name in error notification
  1.32 added function: DeleteItemEx() 
  1.33 added function: ConcatGetParams() 
  1.34 refactored function: LogEvent
  1.35 added function: UploadFile() 
  1.36 added function: LogItem() 
  1.37 all SQL methods moved to separated class
  1.4  refactored to use new named convension
  1.41 writeItem() method added
*/
  
class Logger
{
  static function logEvent($eventFormat, $_argList)
  {
     if(!is_array($_argList))
        $argList = array($_argList);
     else
        $argList = $_argList;
        
     $fileName = LOG_FILE_PATH;  
     $log = "";
        
     if(is_readable($fileName))
    	 $log = join("", file($fileName));
            
     $time = date("Y/m/d (d F, D) H:i:s O");
     $text = sprintf($eventFormat, $time, mysql_error(), preg_replace("/\t+/i", " ", str_replace("\n", "", $argList[0])), $_SERVER['PHP_SELF'].  " page '". $_GET['global']['path'] . "' "); 		
     if($fout = fopen($fileName, "a+"))
     {
     	fwrite($fout, $text);
        fclose($fout);
        unset($fout);
     } 

     if (1 == IS_SEND_ERRORS)
	  	mail(SYS_ADMIN_MAIL, sprintf('Error occured at %s on %s ', $time, TITLE), $text);
  }

  static function logItem($fileName, $item)
  {
       if($fout = fopen($fileName, "a+"))
	   {
	     	fwrite($fout, $item . "\r\n");
	        fclose($fout);
        	unset($fout);
       } 
  }
  
  static function writeItem($fileName, $item)
  {
       if($fout = fopen($fileName, "w"))
	   {
	     	fwrite($fout, $item . "\r\n");
	        fclose($fout);
        	unset($fout);
       } 
  }
  
  
  static function logStackTrace($sErr)
  {
  		$time = date("Y/m/d (d F, D) H:i:s O");
		
		$text = sprintf(LOG_FRM_GENERAL, $time,
							str_replace(array('&nbsp;', '<br>','<b>','</b>'), array(' ', "\r\n",'_','_'), $sErr),
							$_SERVER['PHP_SELF'], 
							$_GET['global']['path']).
							"Query string: ".$_SERVER['QUERY_STRING'];
							
		Logger::logItem(LOG_FILE_PATH, $text);

	    if (1 == IS_SEND_ERRORS && Logger::filterSysEmail($text))
		  	mail(SYS_ADMIN_MAIL, sprintf('Error occured at %s on %s (%s) ', $time, TITLE, $_SERVER['SERVER_NAME']), $text);
  }
  

  static function filterSysEmail($msg) {
        if (strpos($msg, 'Duplicate entry')) return false;
        return true;
  }  
  
  
}

?>