<?php
/**
 * Created by JetBrains PhpStorm.
 * User: bratchuk.andrew
 * Date: 25.12.11
 * Manages included files - _strings.inc & _labels.inc
 * Ver. 1.1
 */

class ResourceGroup
{
    private $language = DEFAULT_LANG;
    private $resource_type = 'strings';
    private $fileName;
    private $resource_path;
    private $keyvalueArray;
    private $NewLine;

    public function __construct($keyvalueArray, $language = DEFAULT_LANG, $resource_type = 'strings')
    {
        $this->language = $language;
        $this->resource_type = $resource_type;
        $this->resource_path = SITE_ABS.DIRECTORY_SEPARATOR.'templates_c/res';

        $this->filePathName = $this->resource_path.DIRECTORY_SEPARATOR.$language.'_'.$resource_type.'.inc';

        $this->keyvalueArray = $keyvalueArray;

//symbol 'end of line' for win & unix are different - do universal
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            //$this->NewLine = "\r\n"; -- adding bytes 0D0D0A but not 0D0A
            $this->NewLine = "\n";
        } else {
            $this->NewLine = "\n";
        }

    }

//checks if res file exist
//
    public function Check()
    {
         if(!file_exists($this->filePathName))
             return false;
        else return true;
    }

//deletes if res file exist
//
    public function Delete()
    {
         if(!file_exists($this->filePathName))
             return true;   //file not exists anyway
         else
         {
            return unlink($this->filePathName);
         }
    }

// loads and includes resfile if res file exist
// ele attampts to create res file and then include
//
    public function Load()
    {
         if(!$this->Check())
         {
             $res = $this->Build();
             if(!$res) return false;
             else $this->DoInclude();
         }

         else
         {
            $this->DoInclude();
         }
    }

//includes file
//
    private function DoInclude()
    {
        include_once($this->filePathName);
    }

//
//
    private function PrepareContent()
    {
        $stringContent = '';
        $newline = $this->NewLine;

        if (count($this->keyvalueArray)==0) $stringContent = 'no data';
        else
        {
            // var_dump($this->keyvalueArray);
            $stringContent = "$newline//=== {$this->resource_type} ===//";
            $stringContent .= "$newline// Language: {$this->language}";
            $stringContent .= "$newline// Generated: ".date("r");
            $stringContent .= "$newline//===       ===//";
            $stringContent .= $newline.$newline;

            foreach ($this->keyvalueArray as $resource)
                $stringContent .= "define('".$resource['resKey']."', '".
                                  str_replace('\'', '\\\'', $resource['resVal'])
                                  ."');".$newline ;

            $stringContent .= $newline.$newline;
        }

        return $stringContent;
    }

//builds file
//
    public function Build()
    {
        $fHandle = null;

        if ( !file_exists($this->filePathName) )
        {
            $isfolder = false;
            if( !file_exists($this->resource_path) )
                $isfolder = mkdir($this->resource_path, 0777);
            else $isfolder = true;

            if(!$isfolder)
                return false;

        }
        else
        {
// silently deleting old file
            unlink($this->filePathName);
        }

        $fHandle = fopen($this->filePathName, "wt");
        //$fHandle = fopen($this->filePathName, "w");

        if($fHandle == false)
            return false;       //creating file failed

        if (fwrite($fHandle, "<?php") === FALSE)
        {
            fclose($fHandle);
            return false;
        }

        if (fwrite($fHandle, $this->PrepareContent()) === FALSE)
        {
            fclose($fHandle);
            return false;
        }

        if (fwrite($fHandle, "?>") === FALSE)
        {
            fclose($fHandle);
            return false;
        }

        fclose($fHandle);
        return true;
    }

}

?>
