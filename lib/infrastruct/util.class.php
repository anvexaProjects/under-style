<?
/*
  Description: Util functions
  Autor:       Oleg Koshkin
  Data:        06-06-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin

  1.1 ConcatGetParams modifications, ConcatFriendlyGetParams addad
  1.2 Add Array2String
  1.3 Add String2Array
*/


class Util
{
	 //	Concat all GET parameters into string for using in links.
	 // $exclude_arr specifies what parameters should be ignored
	 static function ConcatGetParams($exclude_arr = array())
	 {
	 	$out_str = '';
		$keys = array_keys(@$_GET);
		$keys = array_unique($keys);

		foreach($keys as $one_key)
		{
			if (!in_array($one_key, $exclude_arr))
				$out_str .= $one_key . '=' . $_GET[$one_key] . '&';
		}

		return  substr($out_str, 0, strlen($out_str)-1);
	 }


	 static function ConcatFriendlyGetParams($exclude_arr = array())
	 {
	 	$gets = Util::ConcatGetParams($exclude_arr);
		$gets = str_replace('&', '/', $gets);
		return $gets;
	 }

	 static function Array2String($arr)
	 {
		$str = '';

		foreach($arr as $item)
			 $str .= $item . ',';

		if ( strlen($str) > 1)
		 	$str = substr($str, 0, -1);

		return $str;
	 }

	 static function String2Array($str)
	 {
	 	return explode(',', $str);
	 }

	 static function UploadFile($inputFileName, $uploaddir, $file_name, $multi = false, $index = 0)
	 {


            $inputFile                = $_FILES[$inputFileName];
            $multi ? $inputFile_tmp   = $_FILES[$inputFileName]['tmp_name'][$index] : $inputFile_tmp = $inputFile['tmp_name'];
            $multi ? $inputFile_error = $_FILES[$inputFileName]['error'][$index]    : $inputFile_error = $inputFile['error'];



	       $is_ok = 'false';  $err = '';

		   if( isset($inputFile) && !Val::IsEmpty($inputFile_tmp) )
		   {
			   if(!is_dir($uploaddir)) mkdir($uploaddir);

			   $uploadfile = $uploaddir . '/' .  $file_name;
				if ( strlen($file_name) )
				{

					if (@move_uploaded_file($inputFile_tmp, $uploadfile))
					{
						$is_ok = 'true';
						@chmod ("$uploadfile", 0644);
					}
					else
					{
						$err = ERR_UPLOAD_IMG;

						switch ($inputFile_error)
						{
							case UPLOAD_ERR_INI_SIZE:  $err .= ERR_UPLOAD_ERR_INI_SIZE; break;
							case UPLOAD_ERR_FORM_SIZE: $err .= ERR_UPLOAD_ERR_FORM_SIZE; break;
							case UPLOAD_ERR_PARTIAL:   $err .= ERR_UPLOAD_ERR_PARTIAL; break;
							case UPLOAD_ERR_NO_FILE:   $err .= ERR_UPLOAD_ERR_NO_FILE; break;
						}

						if (trim($err) != '') { echo UI::GetBlockError($err); return false; }
					}
				}
		   }
		}

		static function addWatermark($imageFile, $watermarkFile)
		{
			$watermark = imagecreatefrompng($watermarkFile);
			$watermark_width = imagesx($watermark);
			$watermark_height = imagesy($watermark);
			$image = imagecreatefromjpeg($imageFile);
			$size = getimagesize($imageFile);
			$dest_x = $size[0] - $watermark_width;
			$dest_y = $size[1] - $watermark_height;
			imagecopy($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
			imagejpeg($image, $imageFile);
			imagedestroy($image);
			imagedestroy($watermark);
		}


		static function getValueFromRange($range, $element) {
			foreach ($range as $k => $v)
			{
				if ($element >= $v[0] && $element <= $v[1])
				{
					return $k;
				}
			}
		}

		static function getFileSize($fileName, $autodetect = false, $range = 0, $round = null)
		{
		    $round = (null === $round) ? 3 : $round;
			if (!file_exists($fileName)) return '0 байт';
		    $size = filesize($fileName);
		    //$sizes = array('b', 'Kb', 'Mb', 'Gb');

		    if ($autodetect)
		    {
                if($size < pow(1024, 1)) return $size." байт";
                elseif($size < pow(1024, 2)) return round($size/pow(1024, 1), $round)." КБ";
                elseif($size < pow(1024, 3)) return round($size/pow(1024, 2), $round)." МБ";
                elseif($size < pow(1024, 4)) return round($size/pow(1024, 3), $round)." ГБ";
		    }
		    else
		    {
                if($range == 'b') return $size." байт";
                elseif($range == 'Kb') return round($size/pow(1024, 1), $round)." КБ";
                elseif($range == 'Mb') return round($size/pow(1024, 2), $round)." МБ";
                elseif($range == 'Gb') return round($size/pow(1024, 3), $round)." ГБ";
		    }
		}

		static function getNonExistentFilename($path)
		{
            if (!file_exists($path)) return $path;

            $ext = pathinfo($path, PATHINFO_EXTENSION);

            $i = 1;
            while(true == file_exists(str_replace('.'.$ext, '', $path).'_'.$i.'.'.$ext)) {
               $i++;
            }
            return str_replace('.'.$ext, '', $path).'_'.$i.'.'.$ext;
		}

		static function translit($s, $params = array())
		{
		    $map = array(
				'а' => 'a',
				'б' => 'b',
				'в' => 'v',
				'г' => 'g',
				'д' => 'd',
				'е' => 'e',
				'ё' => 'e',
				'ж' => 'g',
				'з' => 'z',
				'и' => 'i',
				'й' => 'i',
				'к' => 'k',
				'л' => 'l',
				'м' => 'm',
				'н' => 'n',
				'о' => 'o',
				'п' => 'p',
				'р' => 'r',
				'с' => 's',
				'т' => 't',
				'у' => 'y',
				'ф' => 'f',
				'х' => 'h',
				'ц' => 'ts',
				'ч' => 'ch',
				'ш' => 'sh',
				'щ' => 'sh',
				'ь' => '',
				'ы' => 'i',
				'ъ' => '',
				'э' => 'e',
				'ю' => 'u',
				'я' => 'ia',
				'А' => 'A',
				'Б' => 'B',
				'В' => 'V',
				'Г' => 'G',
				'Д' => 'D',
				'Е' => 'E',
				'Ё' => 'E',
				'Ж' => 'G',
				'З' => 'Z',
				'И' => 'I',
				'Й' => 'I',
				'К' => 'K',
				'Л' => 'L',
				'М' => 'M',
				'Н' => 'N',
				'О' => 'O',
				'П' => 'P',
				'Р' => 'R',
				'С' => 'S',
				'Т' => 'T',
				'У' => 'Y',
				'Ф' => 'F',
				'Х' => 'H',
				'Ц' => 'Ts',
				'Ч' => 'Ch',
				'Ш' => 'Sh',
				'Щ' => 'Sh',
				'Ь' => '',
				'Ы' => 'I',
				'Ъ' => '',
				'Э' => 'E',
				'Ю' => 'U',
				'Я' => 'Ia',
		    );
		    $s = str_replace(array_keys($map), array_values($map), $s);
		    if(isset($params['tolower']) && $params['tolower'] == true) $s = strtolower($s);
		    if(isset($params['symbols'])) {
		    	$s = preg_replace('/[^a-z0-9A-Z'.$params['symbols'].']/', '_', $s);
		    	$s = preg_replace('/_{2,}/','_', $s);
		    	$s = trim($s, '_');
		    }
		    return $s;
		}

}

?>