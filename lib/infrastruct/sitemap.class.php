<?php

/*
  Description: SiteMap
  Autor:       Oleg Koshkin
  Data:        10-25-2009
  Version:     1.3

  mailto:      WizardF@narod.ru
  Copyright:   (C) 2007 Oleg Koshkin

  history:
  1.0 initial version
  1.2 refactored to use updated DAL
  1.3 SQL optimized to avoid using the view

*/

class SiteMap
{
    public $struct;
    public $oDb;
    public $is404 = false;

    /**
     * Returns a singleton of SiteMap
     *
     * Usually, only one SiteMap object is needed, this is the reason
     * why it is recommended to use this method to get the validation object.
     *
     * @access    public
     * @static
     * @return    object    Reference to the SiteMap singleton
     */
    static function &singleton()
    {
        static $obj;
        if (!isset($obj)) {
            $obj = new SiteMap();
        }
        return $obj;
    }

    // end func singleton

    public function __construct()
    {
        $this->oDb = Database::get();
    }

    public function initStruct($criteria, $initializeByPath = true)
    {

        $fixForSale = false;

        $criteria = Val::ToDb($criteria);
        $joins = $fld = '';
        $struct = array();

        $path = $initializeByPath ? $criteria : $this->oDb->getField("SELECT path, CONCAT(struct_id, lang) AS art_id FROM art_content HAVING art_id = '$criteria'");

        $aPathParts = explode('/', $path);
        $total = count($aPathParts);
        $_SERVER['CLEAN_REQUEST_URI'] = $_SERVER['REQUEST_URI'];
        if (strpos($_SERVER['REQUEST_URI'], '/sale/') !== false && $_SERVER['REQUEST_URI'] != '/about/sale/') {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/sale/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['sale'] = true;
        }
        if (strpos($_SERVER['REQUEST_URI'], '/offers/') !== false) {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/offers/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['offers'] = true;
        }
        if (strpos($_SERVER['REQUEST_URI'], '/archives/') !== false) {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/archives/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['archives'] = true;
        }
        //die($total);
        $sPath = '';

        $sSql = "SELECT CONCAT(st.struct_id, c.lang) AS id, st.struct_id AS struct_id, c.modify,
					  content_id, control, ch_control, CONCAT(st.parent_id, c.lang) AS parent_id,
					  level, name, ord, path, spath, is_protected, mode, ch_mode
					  FROM art_struct AS st INNER JOIN art_content AS c ON st.struct_id = c.struct_id
					  WHERE is_deleted = 0 AND level = 0 ";

        $rstruct[] = $this->oDb->getRow($sSql);
        $sSql = '';


        if ($total > 0) {
            if (sizeof($rstruct) < 1) {

                //страница не найдена
                $this->notFound();
            } else {
                $sPath .= $aPathParts[0];

                // build SQL query

                $sSql .= "
                          SELECT CONCAT(st.struct_id, c.lang) AS id, st.struct_id AS struct_id, c.modify,
						  content_id, control, ch_control, IF(st.level = 1, st.parent_id, CONCAT(st.parent_id, c.lang) )  AS parent_id,
						  level, name, ord, path, spath, is_protected, mode, ch_mode, psevdonim
						  FROM art_struct AS st INNER JOIN art_content AS c ON st.struct_id = c.struct_id
						  WHERE is_deleted = 0 AND path = '$sPath'
						  ";

                #echo $sSql;

                /*	  die("SELECT CONCAT(st.struct_id, c.lang) AS id, st.struct_id AS struct_id,
                content_id, control, ch_control, IF(st.level = 1, st.parent_id, CONCAT(st.parent_id, c.lang) )  AS parent_id,
                level, name, ord, path, spath, is_protected, mode, ch_mode, psevdonim
                FROM art_struct AS st INNER JOIN art_content AS c ON st.struct_id = c.struct_id
                WHERE is_deleted = 0 AND path = '$sPath'
                ");*/

                $rstruct[] = $this->oDb->getRow($sSql);

                if ($sPath == "index") {
                    $sqqql = "SELECT CONCAT(st.struct_id, c.lang) AS id, st.struct_id AS struct_id, c.modify,
							  content_id, control, ch_control, IF(st.level = 1, st.parent_id, CONCAT(st.parent_id, c.lang) )  AS parent_id,
							  level, name, ord, path, spath, is_protected, mode, ch_mode, psevdonim
							  FROM art_struct AS st INNER JOIN art_content AS c ON st.struct_id = c.struct_id
							  WHERE is_deleted = 0 AND path = 'ru'
							  ";
                    $tempSSS = $this->oDb->getRow($sqqql);
#					 print_r($tempSSS);

                    foreach ($rstruct as $k => $v)
                        $rstruct[$k]['modify'] = $tempSSS['modify'];
                }


                $row = $this->oDb->getRow('SELECT * FROM art_content WHERE path ="' . $path . '"');
                $sSql = '';
                //	die();
                if ($total > 1 && empty($row)) {


                    // нужно было добавить роут для перехода на товарную группу
                    // попробуем внедрится в этот хаос, сделаем ветку проверки существования товарной группы

                    $group = $aPathParts[1] == 'sale' ? $aPathParts[2] : $aPathParts[1];

                    $sql = "SELECT * FROM category_psevdonims WHERE psevdonim = '$group' LIMIT 1";

                    $hasGroup = $this->oDb->getRow($sql);
                    //print_r($row);
                    //die();
//                    print_r($hasGroup);die();

                    if (sizeof($rstruct) < 2) {

                        //страница не найдена
                        $this->notFound();
                    } // вот и ветка для товарных групп

                    elseif (!empty($hasGroup)) {

                        $fixForSale = true;
//                        $rstruct[] = array();
                        $rstruct[] = array(
                            'id' => '',
                            'struct_id' => '',
                            'content_id' => '',
                            'control' => 'collection',
                            'ch_control' => 'collection',
                            'parent_id' => $rstruct[1]['struct_id'],
                            'level' => '2',
                            'name' => $row['name'],
                            'ord' => '111',
                            'path' => $row['psevdonim'],
                            'spath' => '',
                            'is_protected' => '0',
                            'mode' => '111',
                            'ch_mode' => '111',
                            'psevdonim' => $row['psevdonim'],
                            'group' => $hasGroup
                            );
//                        print_r($rstruct);
//                        die('groups');
                    } // build SQL query

                    else {

                        //не проверяется, есть ли такая категория у субдомена, надо проверить, пока как есть

                        if ($_GET['sale'] || $_GET['offers'] || $_GET['archives']) {
                            $sPath .= $aPathParts[2];
                            $aPathParts[1] = $aPathParts[2];
                            unset($aPathParts[2]);
                        } else {
                            $sPath .= $aPathParts[1];
                        }


                        $psevdonim = $aPathParts[1];

                        $sql = '
                            SELECT * FROM art_struct AS st
                            INNER JOIN art_content ON (st.struct_id = art_content.struct_id AND st.is_deleted = 0)
                            WHERE psevdonim = "' . $psevdonim . '" AND control="collection"
						';

                        $row = $this->oDb->getRow($sql);
                        $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                        if (strpos($_SERVER['REQUEST_URI'], '/place/')) {
                            $row = $this->oDb->getRow('SELECT * FROM place_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[3]) . '"');
                            if (!empty($row)) {
                                /*$rstruct[] = $row;
                                $rstruct[] = array('id' => '', 'struct_id' => '', 'content_id' => '', 'control' => 'collection', 'ch_control' => 'collection', 'parent_id' => $rstruct[2]['struct_id'], 'level' => '4', 'name' => $row['name'], 'ord' => '111', 'path' => $row['psevdonim'], 'spath' => '', 'is_protected' => '0', 'mode' => '111', 'ch_mode' => '111', 'psevdonim' => $row['psevdonim']);
                                $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                $rstruct[sizeof($rstruct) - 1]['level'] = 4; */
                            }
                        }
                        if (empty($row)) {
                            //страница не найдена
                            $this->notFound();
                        } else {
                            $rstruct[] = array('id' => '', 'struct_id' => '', 'content_id' => '', 'control' => 'collection', 'ch_control' => 'collection', 'parent_id' => $rstruct[1]['struct_id'], 'level' => '2', 'name' => $row['name'], 'ord' => '111', 'path' => $row['psevdonim'], 'spath' => '', 'is_protected' => '0', 'mode' => '111', 'ch_mode' => '111', 'psevdonim' => $row['psevdonim'],);

                            if ($total > 2) {

                                if (sizeof($rstruct) < 3) {
                                    //страница не найдена
                                    $this->notFound();
                                } // build SQL query

                                else {
                                    if ($_GET['sale'] || $_GET['offers'] || $_GET['archives']) {
                                        $sPath .= $aPathParts[3];
                                        $aPathParts[2] = $aPathParts[3];
                                        unset($aPathParts[3]);
                                    } else {
                                        $sPath .= $aPathParts[2];
                                    }

                                    $row = $this->oDb->getRow('SELECT * FROM collection_psevdonims WHERE psevdonim = "' . $aPathParts[2] . '"');
                                    /*  $row = $this->oDb->getRow('SELECT * FROM art_struct AS st INNER JOIN art_content ON (st.struct_id = art_content.struct_id AND st.is_deleted = 0)
                                                                 WHERE psevdonim = "' . $aPathParts[2] . '" AND control="collection"');*/

                                    //die('SELECT * FROM art_struct AS st

                                    //						   WHERE psevdonim = "'.$aPathParts[2].'" AND control="collection"');

                                    if (empty($row)) {
                                        if ($total == 4) {
                                            $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                                            $row = $this->oDb->getRow('SELECT * FROM category_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[4]) . '"');
                                            if (empty($row)) {
                                                $row = $this->oDb->getRow('SELECT * FROM place_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[4]) . '"');
                                            }
                                            if (empty($row)) {
                                                //страница не найдена
                                                $this->notFound();
                                            } else {
                                                $rstruct[] = $row;
                                                $rstruct[] = array('id' => '', 'struct_id' => '', 'content_id' => '', 'control' => 'collection', 'ch_control' => 'collection', 'parent_id' => $rstruct[2]['struct_id'], 'level' => '4', 'name' => $row['name'], 'ord' => '111', 'path' => $row['psevdonim'], 'spath' => '', 'is_protected' => '0', 'mode' => '111', 'ch_mode' => '111', 'psevdonim' => $row['psevdonim']);

                                                $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                                $rstruct[sizeof($rstruct) - 1]['level'] = 4;
                                            }
                                        } else {
                                            if (strpos($_SERVER['REQUEST_URI'], '/collection/')) {
                                                $rstruct[] = array();
                                                $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                                $rstruct[sizeof($rstruct) - 1]['level'] = 3;
                                                $rstruct[sizeof($rstruct) - 1]['ch_control'] = 'collection';
                                                $rstruct[sizeof($rstruct) - 1]['control'] = 'collection';
                                            } elseif (strpos($_SERVER['REQUEST_URI'], '/place/')) {
                                                $rstruct[] = array();
                                                $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                                $rstruct[sizeof($rstruct) - 1]['level'] = 3;
                                                $rstruct[sizeof($rstruct) - 1]['ch_control'] = 'collection';
                                                $rstruct[sizeof($rstruct) - 1]['control'] = 'collection';
                                            } else {
                                                $this->notFound();
                                            }
                                        }
                                    } else {
                                        $rstruct[] = $row;
                                        $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                        $rstruct[sizeof($rstruct) - 1]['level'] = 3;
                                        $rstruct[sizeof($rstruct) - 1]['ch_control'] = 'collection';
                                        $rstruct[sizeof($rstruct) - 1]['control'] = 'collection';

                                        if ($total > 3) {

                                            if (sizeof($rstruct) < 4) {

                                                //страница не найдена
                                                $this->notFound();
                                            } // build SQL query

                                            else {

                                                $sPath .= $aPathParts[3];

                                                $row = $this->oDb->getRow('SELECT * FROM collection_psevdonims WHERE psevdonim = "' . $aPathParts[3] . '"');
                                                if (empty($row)) {

                                                    //страница не найдена
                                                    $this->notFound();
                                                } else {

                                                    $rstruct[] = array('id' => '', 'struct_id' => '', 'content_id' => '', 'control' => 'collection', 'ch_control' => 'collection', 'parent_id' => $rstruct[2]['struct_id'], 'level' => '4', 'name' => $row['name'], 'ord' => '111', 'path' => $row['psevdonim'], 'spath' => '', 'is_protected' => '0', 'mode' => '111', 'ch_mode' => '111', 'psevdonim' => $row['psevdonim']);

                                                    //$rstruct[] = $row;
                                                    $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                                                    $rstruct[sizeof($rstruct) - 1]['level'] = 4;
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ((strpos($_SERVER['REQUEST_URI'], '/sale/') && $_SERVER['REQUEST_URI'] != '/about/sale/') || strpos($_SERVER['REQUEST_URI'], '/offers/') || strpos($_SERVER['REQUEST_URI'], '/archives/')) {
                        $rstruct[] = array();
                        $rstruct[sizeof($rstruct) - 1]['id'] = $rstruct[sizeof($rstruct) - 1]['struct_id'];
                        $rstruct[sizeof($rstruct) - 1]['level'] = 3;
                        $rstruct[sizeof($rstruct) - 1]['ch_control'] = 'collection';
                        $rstruct[sizeof($rstruct) - 1]['control'] = 'collection';
                    } else {
                        // build SQL query
                        for ($i = 1; $i < $total; $i++) {
                            $sPath .= '/';
                            if (1 != $i) {
                                $sSql .= ' UNION ';
                            }
                            $sPath .= $aPathParts[$i];

                            $sSql .= "SELECT CONCAT(st.struct_id, c.lang) AS id, c.modify, st.struct_id AS struct_id,
    								  content_id, control, ch_control, IF(st.level = 1, st.parent_id, CONCAT(st.parent_id, c.lang) )  AS parent_id,
    								  level, name, ord, path, spath, is_protected, mode, ch_mode
    								  FROM art_struct AS st INNER JOIN art_content AS c ON st.struct_id = c.struct_id
    								  WHERE is_deleted = 0 AND path = '$sPath'
    								  ";
                        }
                    }
                }
            }
        }
        //echo $sSql;exit;);

        $result = array();

        if($sSql) {
//            print_r($sSql);
//            die();
            $result = $this->oDb->getRows($sSql);
        }

        $rstruct = array_merge($rstruct, $result);
        //var_dump($rstruct);
        //die();

        $_GET['global']['level'] = count($rstruct) - 1;

        // 404 error handlers

        // empty rstruct
        if (!is_array($rstruct) || 0 == count($rstruct)) {

            $_GET['global']['struct'] = array();
            $_GET['global']['rstruct'] = array();
            $this->is404 = true;
            return;
        }

        // lang page site.com/en/
        if (sizeof($aPathParts) == 1 && $aPathParts[0] != "index" && sizeof($rstruct) != 2) {

            $_GET['global']['struct'] = array();
            $_GET['global']['rstruct'] = array();
            $this->is404 = true;
            return;
        }

        /*if (sizeof($rstruct) == 1 && sizeof($aPathParts) != 1)
        {
        $_GET['global']['struct'] = array();
        $_GET['global']['rstruct'] = array();
            $this->is404 = true;
        return;
        }*/

        $madCondition = sizeof($aPathParts) > 1 && sizeof($aPathParts) != (sizeof($rstruct) - 1);

        // inner pages
        if ($madCondition && !$fixForSale) {
//            print_r($rstruct);
//            die();
            //var_dump($rstruct);
            //die();
            //var_dump($rstruct);
            //die('test');
            $_GET['global']['struct'] = array();
            $_GET['global']['rstruct'] = array();
            $this->is404 = true;
            return;
        }

        /*$rstruct [] = array(
        'id'=>'495ru',
        'struct_id'=>'495',
        'content_id'=>'495',
        'control'=>'collection',
        'ch_control'=>'prod',
        'parent_id'=>'55',
        'level'=>'2',
        'name'=>'bath2',
        'ord'=>'20',
        'path'=>'sanitary/dath2',
        'spath'=>'',
        'is_protected'=>'',
        'mode'=>'111',
        'ch_mode'=>'111',
        );*/

        $struct = is_array($rstruct) ? array_reverse($rstruct) : array();

        // shift indexes for backward compatibility
        $total = count($rstruct);
        for ($i = $total; $i > 0; $i--) $struct[$i] = $struct[$i - 1];
        unset($struct[0]);

        $_GET['global']['struct'] = $struct;
        $_GET['global']['rstruct'] = $rstruct;

        //var_dump($struct);

        //	print_r($_GET);
        //	die();

        //print_r($_GET);exit;

    }

    static function getStruct()
    {
        return $_GET['global']['struct'];
    }

    static function getRStruct()
    {
        return $_GET['global']['rstruct'];
    }

    static function getCurTopCat()
    {
        $struct = $_GET['global']['struct'];
        return $struct[count($struct)];
    }

    public function getChilds($parent_path)
    {
        $parent_path = Val::ToDb($parent_path);
        $sSql = "SELECT a1.id, a1.control, a1.parent_id, a1.level, a1.name, a1.name2, a1.path, a1.spath, a1.ord
		FROM articles AS a1 INNER JOIN articles AS a2 ON a2.id = a1.parent_id
		WHERE a2.path = '$parent_path' AND a1.is_deleted = 0 AND a2.is_deleted = 0 ORDER BY ord, id ";

        $items = $this->oDb->getRows($sSql);

        return $items;
    }

    public function getTopCat()
    {
        $aPathParts = explode('/', $_GET['path']);
        return is_array($aPathParts) ? $aPathParts[0] : '';
    }

    function getTree2($cats, $products)
    {
        $tree = array();

        // rebuild flat structure into tree
        // 1 - look through Level 1
        foreach ($cats as $cat_lev_one) {

            // 2 - look throw Level 2
            $child_categories = array();
            foreach ($products as $cat_lev_two) {
                if ($cat_lev_one['id'] == $cat_lev_two['parent_id']) {

                    // append sub categories
                    array_push($child_categories, $cat_lev_two);
                }
            }

            // 2 - end of Level 2 cycle

            // append categories
            //if (count($child_categories) > 0)
            array_push($tree, array('name' => $cat_lev_one['name'], 'id' => $cat_lev_one['id'], 'items' => $child_categories, 'control' => $cat_lev_one['control'], 'child_count' => count($child_categories), 'img1' => $cat_lev_one['img1'], 'spath' => $cat_lev_one['spath'], 'path' => $cat_lev_one['path']));
        }

        return $tree;
    }

    public static function getTree3($cats, $products, $subprod)
    {
        $tree = array();

        // rebuild flat structure into tree
        // 1 - look through Level 1
        foreach ($cats as $cat_lev_one) {

            // 2 - look throw Level 2
            $child_categories = array();
            foreach ($products as $one_category) {
                if ($cat_lev_one['id'] == $one_category['parent_id']) {

                    // 3 - look throw Level 3
                    $child_subcat = array();
                    foreach ($subprod as $cat_lev_tree) {
                        if ($one_category['id'] == $cat_lev_tree['parent_id']) array_push($child_subcat, $cat_lev_tree);

                        // apply hard link
                        if (($cat_lev_tree['parent_id'] == 8) && ($one_category['id'] == 16)) array_push($child_subcat, $cat_lev_tree);
                    }

                    $one_category['child_count'] = count($child_subcat);
                    $one_category['items'] = $child_subcat;

                    // 3 - end of Level 3 cycle

                    // append sub categories
                    //array_push($child_categories, array('name'=>$one_category['name'], 'id'=>$one_category['id'], 'type'=>$one_category['type']));
                    array_push($child_categories, $one_category);
                }
            }

            // 2 - end of Level 2 cycle

            // append categories
            //if (count($child_categories) > 0)
            array_push($tree, array('name' => $cat_lev_one['name'], 'id' => $cat_lev_one['id'], 'path' => $cat_lev_one['path'], 'items' => $child_categories, 'child_count' => count($child_categories), 'control' => $cat_lev_one['control']));
        }

        // 1 - end of Level 1 cycle

        return $tree;
    }

    public function notFound()
    {

        $_GET['global']['struct'] = array();
        $_GET['global']['rstruct'] = array();
        $this->is404 = true;
        return;
    }
}

?>
