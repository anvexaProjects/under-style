<?php                                
/*
  Description: Date labels module
  Autor:       Oleg Koshkin
  Data:        06-06-2007
  Version:     1.3

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/

class Dates
{
	public  $month, $en_month;
	public  $lmonth;
	public  $smonth, $en_smonth;

	public function __construct()
	{
   		// month cyrilic labels
		$month = array();
		$month[0] = '';
		$month[1] = 'января';
		$month[2] = 'февраля';
		$month[3] = 'марта';
		$month[4] = 'апреля';
		$month[5] = 'мая';
		$month[6] = 'июня';
		$month[7] = 'июля';
		$month[8] = 'августа';
		$month[9] = 'сентября';
		$month[10] = 'октября';
		$month[11] = 'ноября';
		$month[12] = 'декабря';   	
		$this->month = $month;
		
		$en_month = array();
		$en_month[0] = '';
		$en_month[1] = 'januаry';
		$en_month[2] = 'february';
		$en_month[3] = 'march';
		$en_month[4] = 'april';
		$en_month[5] = 'may';
		$en_month[6] = 'june';
		$en_month[7] = 'july';
		$en_month[8] = 'august';
		$en_month[9] = 'september';
		$en_month[10] = 'october';
		$en_month[11] = 'november';
		$en_month[12] = 'december';   	
		$this->en_month = $en_month;
		
		$lmonth = array();
		$lmonth[0] = '';
		$lmonth[1] = 'январь';
		$lmonth[2] = 'февраль';
		$lmonth[3] = 'март';
		$lmonth[4] = 'апрель';
		$lmonth[5] = 'май';
		$lmonth[6] = 'июнь';
		$lmonth[7] = 'июль';
		$lmonth[8] = 'август';
		$lmonth[9] = 'сентябрь';
		$lmonth[10] = 'октябрь';
		$lmonth[11] = 'ноябрь';
		$lmonth[12] = 'декабрь';   
		$this->lmonth = $lmonth;
		
				
		$smonth = array();
		$smonth[0] = '';
		$smonth[1] = 'янв';
		$smonth[2] = 'фев';
		$smonth[3] = 'март';
		$smonth[4] = 'апр';
		$smonth[5] = 'май';
		$smonth[6] = 'июнь';
		$smonth[7] = 'июль';
		$smonth[8] = 'авг';
		$smonth[9] = 'сент';
		$smonth[10] = 'окт';
		$smonth[11] = 'нояб';
		$smonth[12] = 'дек';   
		$this->smonth = $smonth;
		
		$en_smonth = array();
		$en_smonth[0] = '';
		$en_smonth[1] = 'jan';
		$en_smonth[2] = 'feb';
		$en_smonth[3] = 'mar';
		$en_smonth[4] = 'apr';
		$en_smonth[5] = 'may';
		$en_smonth[6] = 'jun';
		$en_smonth[7] = 'jul';
		$en_smonth[8] = 'aug';
		$en_smonth[9] = 'sep';
		$en_smonth[10] = 'oct';
		$en_smonth[11] = 'nov';
		$en_smonth[12] = 'dec';   
		$this->en_smonth = $en_smonth;
	}
	
	
	// 20 января 2007
	public function GetMiddleMonth($num, $lan = 'ru')
	{
		if ('ru' == $lan)
			return 	$this->month[(int)$num];
		else
			return 	$this->en_month[(int)$num];
	}
    
	// январь 2007
	public function GetNativeMonth($num, $lan = 'ru')
	{
		if ('ru' == $lan)
			return $this->lmonth[(int)$num];
		else
			return $this->en_month[(int)$num];
	}

	// Январь 2007
	public function GetBigMonth($num, $lan = 'ru')
	{ 
		$month = ('ru'== $lan) ? $this->lmonth[(int)$num] : $this->en_month[(int)$num];
		if (function_exists("mb_strtoupper") && 'ru'== $lan) 
      mb_strtoupper( mb_substr($month, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($month, 1, 90, 'UTF-8');
    else
		 	strtoupper( substr($month, 0, 1)) . substr($month, 1, 90);
	}

	// янв
	public function GetShortMonth($num, $lan = 'ru')
	{	
		if ('ru' == $lan)
			return $this->smonth[(int)$num];
		else
			return $this->en_smonth[(int)$num];
	}


	public function GetMonthList($lan = 'ru')
	{	
		$list = array();
		$curr_year = strftime("%Y");
		$curr_month = strftime("%m");

		
		// start from january 2007
		for($year = 2007; $year <= $curr_year; $year++)
			for($month = 1; $month <= 12; $month++)
				if (($year != $curr_year) || (($year == $curr_year) && ($month <= $curr_month)))
				{
					$key = $year . '_' . $month;
					$list[$key] = $this->GetBigMonth($month, $lan)  . ' ' . $year;
				}
				
		return 	$list;
	}
	
}
	
?>