<?php                                
/*
  Description: Rus date labels module
  Autor:       Oleg Koshkin
  Data:        23-08-2009
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2009 Oleg Koshkin
*/

require_once 'basedatelabels.class.php';

class DateLabelsRu extends BaseDateLabels
{
	public function __construct()
	{
		$month = array();
		$month[0] = '';
		$month[1] = 'января';
		$month[2] = 'февраля';
		$month[3] = 'марта';
		$month[4] = 'апреля';
		$month[5] = 'мая';
		$month[6] = 'июня';
		$month[7] = 'июля';
		$month[8] = 'августа';
		$month[9] = 'сентября';
		$month[10] = 'октября';
		$month[11] = 'ноября';
		$month[12] = 'декабря';   	
		$this->month = $month;
		
		$lmonth = array();
		$lmonth[0] = '';
		$lmonth[1] = 'январь';
		$lmonth[2] = 'февраль';
		$lmonth[3] = 'март';
		$lmonth[4] = 'апрель';
		$lmonth[5] = 'май';
		$lmonth[6] = 'июнь';
		$lmonth[7] = 'июль';
		$lmonth[8] = 'август';
		$lmonth[9] = 'сентябрь';
		$lmonth[10] = 'октябрь';
		$lmonth[11] = 'ноябрь';
		$lmonth[12] = 'декабрь';   
		$this->lmonth = $lmonth;
		
				
		$smonth = array();
		$smonth[0] = '';
		$smonth[1] = 'янв';
		$smonth[2] = 'фев';
		$smonth[3] = 'март';
		$smonth[4] = 'апр';
		$smonth[5] = 'май';
		$smonth[6] = 'июнь';
		$smonth[7] = 'июль';
		$smonth[8] = 'авг';
		$smonth[9] = 'сент';
		$smonth[10] = 'окт';
		$smonth[11] = 'нояб';
		$smonth[12] = 'дек';   
		$this->smonth = $smonth;
	}
	

	// Январь 2007
	public function getBigMonth($num)
	{ 
		$month = $this->lmonth[(int)$num];
		if (function_exists("mb_strtoupper")) 
	      return  mb_strtoupper( mb_substr($month, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($month, 1, 90, 'UTF-8');
	    else
		  return strtoupper( substr($month, 0, 1)) . substr($month, 1, 90);
	}


}
	
?>