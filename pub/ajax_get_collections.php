<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgeni.zhuk
 * Date: 11.10.13
 * Time: 17:40
 * To change this template use File | Settings | File Templates.
 */
require_once '../component/libs.inc';

$oDb = Database::get();
$mans = $_POST['factories'];

$factory = $oDb->escape(implode("', '", $mans));
$sql = "SELECT DISTINCT sprop13 FROM art_struct WHERE sprop3 IN ('$factory') AND is_deleted = 0";
$array = $oDb->getRows($sql);
$arr = array();
foreach ($array as $i){
    if ($i["sprop13"]){
        $arr[] = $i["sprop13"];
    }
}
echo json_encode($arr);