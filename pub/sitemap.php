<?php
header("Content-Type: text/xml");
$_GET['debug'] = false;


require_once '../component/bl/lister/lister.class.php';
$params = array(
    'tpl_name' => "master/sitemap.xml.tpl",
    'plevel' => 0,
    'fields' => 'a1.path',
    'page_size' => 100000,
    'ord' => 'a1.path',
);

$lst = new Lister($params);
$lst->render();

?>