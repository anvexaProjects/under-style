<?php                                
       
   require_once '../core/settings.inc';
   require_once '../lib/smarty/Smarty.class.php';
   
   // $smarty = new Smarty;
//    $smarty->caching = true;
  //  $smarty->clear_all_cache();
   
    require_once '../lib/3rdparty/timer.class.php';

    $tm = new timer();
    $tm->start();

	removeFolderContent(SMARTY_COMPILE_DIR);
	removeFolderContent(SMARTY_CACHE_DIR);

    $tm->stop();
    echo 'Total Execution Time: '.$tm->get();


	function removeFolderContent($sDir)
	{
		$i=0;
		if ($rRootDir = @opendir($sDir))
		{
			while (false !== ($sFile = readdir($rRootDir))) 
			{                        
				  if( $sFile != '.svn' &&  $sFile != '.' &&  $sFile != '..')
				  {
					  echo $sFile.'<br>';
					  is_dir($sDir . '/' . $sFile) ? removeFolder($sDir . '/' . $sFile) : @unlink($sDir . '/' . $sFile);
					  $i++;
					  if ($i%100 == 0) flush();
				  }
			}      
			echo '<br><br>Cache was cleared<br>';
		}
		else 
		{
			echo 'Impossible to open directory /'.$rRootDir;
		}
		closedir($rRootDir); 
	}
    

?>
