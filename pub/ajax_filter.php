<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Evgeni Zhuk
 * Date: 12.06.13
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 */
require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';

if (isset($_REQUEST['rub'])){
    foreach($_REQUEST['rub'] as $value){
        $rub[] = htmlspecialchars(trim($value));
    }
}else $rub = '';

if (isset($_REQUEST['factory'])){
    foreach($_REQUEST['factory'] as $value){
        $factory[] = htmlspecialchars(trim($value));
    }
} else $factory = '';

if (isset($_REQUEST['collection'])){
    foreach($_REQUEST['collection'] as $value){
        $collection[] = htmlspecialchars(trim($value));
    }
}else $collection = '';

if (isset($_REQUEST['place'])){
    foreach($_REQUEST['place'] as $value){
        $place[] = htmlspecialchars(trim($value));
    }
}else $place = '';

if (isset($_REQUEST['catalog'])){
    $catalog = htmlspecialchars(trim($_REQUEST['catalog']));
}else $catalog = '';

$db = database::get();

$factory = $factory ? implode("', '" , $factory) : '';
$collection = $collection ? implode("', '", '', $collection) : '';
$place = $place ? implode("', '", $place) : '';
$rub = $rub ? implode("', '", $rub) : '';

$sql = "SELECT a1.*, as1.* FROM art_content AS a1
    INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
	WHERE ";
if ($factory){
    $sql .= "as1.sprop3 IN ('$factory')";
}
if ($rub && $factory){
    $sql .= "AND as1.sprop4 IN ('$rub')";
} elseif ($rub) {
    $sql .= "as1.sprop4 IN ('$rub')";
}
if (($place && $factory) ||($place && $rub)){
    $sql .= "AND as1.sprop6 IN ('$place')";
} elseif ($place){
    $sql .= "AND as1.sprop6 IN ('$place')";
}
$sql .= "AND as1.control='prod'";


//var_dump($sql);
$items = $db->getRows($sql);

$sql = "SELECT sprop4 FROM art_struct WHERE spruct_id='".$i['parent_id']."'";
$collection = $db->getRows($sql);
$collection = $collection[0]['sprop4'];
foreach($items as $k => $v)
{
    $sid = $v['struct_id'];
    $sql = "SELECT * FROM images WHERE art_id = '$sid' AND art_id != 0 GROUP BY image_name";
    $items[$k]['images'] = $db->getRows($sql);
}
//var_dump($items);
foreach ($items as $i):
    $sql = "SELECT name FROM art_content WHERE struct_id='".$i['parent_id']."'";
    $collection = $db->getRows($sql);
    $collection = $collection[0]['name'];
    $sql = "SELECT as1.img2 FROM art_content AS a1
    INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
	WHERE a1.name='".$i['sprop3']."' AND as1.control='company'";
    $brand = $db->getRows($sql);
    $brand_img = $brand[0]['img2'];
        ?>
<div class="prodItemWrapper vis">
    <div class="brand">
        <img src="/dc/under-style.ru/upload/article/<?php echo $brand_img ?>" />
    </div>
    <a href="<?php echo $_GET['global']['root'] ?>/<?php echo $i['path'] ?>/" class="prodItem">
		<div class="img">
			<img src="<?php echo $_GET['global']['root'] ?>/upload/article/<?php echo $i['images'][0]['image_name'] ?>"
                 width="165" height="102">
		</div>
        <?php if ($i['sprop7']): ?>
        <div class="flag_av"><?php echo S_AVAILABLE ?></div>
        <?php endif;
        if ($i['sprop8']): ?>
        <div class="flag"><?php echo S_NOT_AVAILABLE ?></div>
        <?php endif;
        if ($i['sprop9']): ?>
        <div class="flag_sp"><?php echo S_SPEC_OFFER ?></div>
        <?php endif; ?>
		<h3><?php echo $i['name']?></h3>
		<p>
                <?php echo S_MANUFACTURER ?>
			<b><?php echo $i['sprop3'] ?></b>
		</p>
		<p>
                <?php echo S_COUNTRY ?>
			<b><?php echo $i['sprop5'] ?></b>
		</p>
		<p>
            <?php echo S_COLLECTION ?>
			<b><?php echo $collection ?></b>
		</p>
        <p class="price_prod<?php if ($i['sprop9']){echo 'cancel';} ?>"><?php echo $i['sprop11'] ?></p>
        <?php if ($i['sprop9']): ?>
        <div class="price_prod"><span><?php echo $i['sprop12'] ?></span></div>
        <div class="counter"><?php
            $end_time=strtotime($i['end_time']);
            $rest_time = explode('.', ($end_time - time())/86400);
            $time = $rest_time[0];
            $time > 0 ? true : $time = 1;
            echo $time ." ".S_DAYS;
            ?>
            </div>
        <?php endif; ?>
	</a>
	<div class="prodShortInfo">
		<div class="img">
            <img src="<?php echo $_GET['global']['root'] ?>/upload/article_big/<?php echo $i['images'][0]['image_name'] ?>" width="380" height="232">
		</div>
		<div class="pSLeft">
            <div class="brand">
                <img src="/dc/under-style.ru/upload/article/<?php echo $brand_img ?>" />
            </div>
			<h3><?php echo $i['name'] ?></h3>
			<?php echo nl2br($i['description']) ?>
		</div>
        <div class="clear"></div>
</div>
</div>
<?php
endforeach;
