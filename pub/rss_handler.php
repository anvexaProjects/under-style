<?php
    require_once '../component/libs.inc';
    require_once '../component/bl/google_rssreader.class.php';

    $obj = new GoogleRssReader();
    $rs = $obj->process(GOOGLE_LOGIN, GOOGLE_PASS, GOOGLE_READER_LABEL);
	$smarty = new Smarty;
	$smarty->caching = true;
	$smarty->clear_all_cache();
	
    echo $rs;
?>
