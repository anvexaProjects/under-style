﻿<?php
session_start();
$item = $_GET['item'];

if (!isset($_SESSION['saved_items'])) $_SESSION['saved_items'] = array();


if (!in_array($item, $_SESSION['saved_items'])) array_unshift($_SESSION['saved_items'], $item);
if (count($_SESSION['saved_items']) > 100) array_pop($_SESSION['saved_items']);

?>