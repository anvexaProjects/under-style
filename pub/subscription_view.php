<?php

$_GET['debug'] = '';

require '../component/admin_libs.inc';
require_once '../component/bl/subscription/subscription_queue.class.php';

$obj_q = new SubscriptionQueue();

$hash = isset($_GET['u']) ? $_GET['u'] : '';
$queue_id = isset($_GET['m']) ? (int) $_GET['m'] : 0;

$is_email_viewed = isset($_GET['view_in_mailbox']) && $_GET['view_in_mailbox'] == 1;
$is_email_viewed_in_browser = isset($_GET['view_in_browser']) && $_GET['view_in_browser'] == 1;
$is_translated_email_viewed = isset($_GET['view_translated_in_browser']) && $_GET['view_translated_in_browser'] == 1;

$rs = $obj_q->getUserEmail($hash, $queue_id, $is_email_viewed, $is_email_viewed_in_browser, $is_translated_email_viewed);
if ($is_email_viewed === false)
{
    if (empty($rs)) echo S_OPENED_MAIL_NOT_FOUND;
    else echo($rs);
}

?>
