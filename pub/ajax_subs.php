<?php

$_GET['debug'] = 'no';
require_once '../component/libs.inc';
require_once '../component/bl/notification.class.php';

$msg = '';
$status = 0;
if (empty($_GET['email'])) {
	$msg = FLB_REQUIRED_EMAIL;
}
elseif (!Val::IsEmail($_REQUEST['email'])) {
	$msg = FLB_INVALID_EMAIL;
}
else {
	$db = Database::get();
	$email = $db->escape($_GET['email']);
	$is_exist = $db->getField("SELECT COUNT(*) FROM subscription_contacts WHERE email = '$email'");
	if ($is_exist != 0) {
		$msg = FLB_USER_ALREADY_REGISTERED;
	} 
	else {
		$data = array();
		$data['fname'] = $_GET['fname'];
		$data['lname'] = $_GET['lname'];
		$data['email'] = $_GET['email'];
		$data['created'] = date('Y-m-d H:i:s');
		$data['hash'] = md5(time().rand(1, 999));
		$data['is_active'] = 0;
		$id = $db->insert('subscription_contacts', $data);
		$data['id'] = $id;
		$status = 1;
		$msg = S_SUBSCRIBED_SUCCESSFULLY;
		
		$tpl = new Smarty;
		$tpl->assign('data', $data);
		$body = $tpl->fetch('notifications/subscription.tpl');
		Notification::SendBody($data['email'], S_SUBJ_SUBSCRIPTION, $body);
	}
}

echo json_encode(array('m' => $msg, 'status' => $status));

?>