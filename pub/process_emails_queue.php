<?php

require '../component/admin_libs.inc';
require_once '../component/bl/subscription/subscription_queue.class.php';
require_once '../component/bl/subscription/subscription_contact.class.php';
require_once '../component/bl/subscription/subscription_template.class.php';
require_once '../component/bl/subscription/subscription_mailing.class.php';


$obj = new SubscriptionQueue();

$queues = $obj->getNotCompletedQueues();
echo 'Queues count is '.sizeof($queues).'<br />';

foreach ($queues as $queue_id)
{
    echo "Start Processing queue with id $queue_id<br />";
    $obj->processQueue($queue_id);
    echo "End Processing  with id $queue_id<br />";
    flush();
}



?>