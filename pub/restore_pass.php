<?php
require_once '../component/libs.inc';
require_once '../component/bl/notification.class.php';

$db = Database::get();

$email = $db->escape($_GET['email']);
$user_id = $db->getField("SELECT id FROM users WHERE login = '$email'");
if (!$user_id) echo S_USER_NOT_FOUND;
else {
	$hash = md5(time().'_'.$user_id);
	$pass = getRandomPassword();
	$pass_md5 = md5(SALT.$pass.SALT);
	$db->query("UPDATE users SET restore_hash = '$hash', restore_pass = '$pass_md5' WHERE id = '$user_id'");
	
	$tpl = new Smarty;
	$tpl->assign('pass', $pass);
	$tpl->assign('hash', $hash);
	$msg_text = $tpl->fetch('notifications/restore_pass.tpl');
	$res = Notification::SendBody($email, LBL_SUBJ_RESTORE, $msg_text);
	echo S_RESTORE_EMAIL;	
}

function getRandomPassword() {
	return 'u'.rand(1000000, 9999999);
}

?>