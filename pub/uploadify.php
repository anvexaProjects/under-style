<?php

$_GET['debug'] = 'no';
require_once '../component/libs.inc';
$rs = array('e' => false, 'name' => '', 'mime');
if (!empty($_FILES)) {
    $fname = $_FILES['Filedata']['name'];
	if ($_FILES['Filedata']['size'] > 10*1024*1024) {
		echo 'File size must be less than 10Mb';
		$rs['e'] = true;
		
	} else {
		$fpath = SITE_ABS.'/upload/temp/';
		$fname = Util::translit($fname, array('tolower' => true, 'symbols' => '\.'));
		$fname = Util::getNonExistentFilename($fpath.'/'.$fname);
		$fname = basename($fname); 
		$e = Util::UploadFile('Filedata', $fpath, $fname);
		if (!empty($e)) $rs['e'] = true;
		$rs['name'] = $fname;
		$rs['mime'] = $_FILES['Filedata']['type'];
    }
	echo json_encode($rs);
	exit;
}
?>