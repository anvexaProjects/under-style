<?php

if($_SERVER['REQUEST_URI']=='/rss/news') {
	header('HTTP/1.0 200 OK');
	$_GET['path']='rss/news';
}
if($_SERVER['REQUEST_URI']=='/saved_items') {
	header('HTTP/1.0 200 OK');
	$_GET['path']='saved_items';
}

if( $_SERVER['REQUEST_URI']=='/partnership/catalogs/?sanitary=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/catalogs/?all=1' ||
    $_SERVER['REQUEST_URI']=='/partnership/catalogs/?furniture=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/catalogs/?till=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/catalogs/?light=1'
    ){
header("HTTP/1.1 301 Moved Permanently");
header("Location: http://www.under-style.ru/partnership/catalogs/");
exit();
}


if( $_SERVER['REQUEST_URI']=='/partnership/company_list/?sanitary=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/company_list/?all=1' ||
    $_SERVER['REQUEST_URI']=='/partnership/company_list/?furniture=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/company_list/?till=1' || 
    $_SERVER['REQUEST_URI']=='/partnership/company_list/?light=1'
    ){
header("HTTP/1.1 301 Moved Permanently");
header("Location: http://www.under-style.ru/partnership/company_list/");
exit();
}

// ЧПУ ---
  if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/d-url-rewriter.php')) {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/d-url-rewriter.php');
    durRun ();
  }
// --- ЧПУ
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

//error_reporting(0);
//ini_set('display_errors', 0);
//define('IS_DEBUG', 1);

//print_r(dirname(__FILE__));

session_start();
date_default_timezone_set('Europe/Moscow');
require_once '../component/libs.inc';
require_once '../lib/infrastruct/sitemap.class.php';
require_once '../lib/infrastruct/dates.class.php';
require '../templates/config/enum.inc';

function viewCaptcha() {
    $api_key = 'cw.1.1.20140108T201504Z.cec0a55f08b71251.eb7db4bedad12d456f8613fc33b3dd274bab79d1';
    $url_api = 'http://cleanweb-api.yandex.ru/1.0/';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_URL, $url_api . 'get-captcha?' . 'key=' . urlencode($api_key) . '&id=null&type=estd');
		$t=curl_exec($ch);
    curl_close($ch);
		if($t && !strpos($t,'<title>404</title>')) {
	    $response = new SimpleXMLElement($t);
  	  $captcha_url = $response->url;
    	$captcha_id = (string)$response->captcha;
	    $_SESSION['captcha'] = $captcha_id;
    }
    return "<img src=\"" . $captcha_url . "\">";
}

if ($_GET['path'] == "index.html")
    $_GET['path'] = "index";

if (defined('IS_DEBUG')) if (1 == IS_DEBUG) require_once '../component/fx/tm_in.inc.php';
if (URL::GetPath() == "rss/news") {
     // RSS новостей
    require_once '../component/rss/news.inc.php';
    exit;
}
if (URL::GetPath() == "rss/actions") {
     // RSS акций
    require_once '../component/rss/actions.inc.php';
    exit;
}

blockIe6();
ini_set("allow_call_time_pass_reference", "true");
$tpl = new Smarty;

//    ini_set("display_errors", "On");

// apply nessesaty template
$template = URL::GetTDir() . '/common1.tpl';
//$template = URL::GetTDir() . '/common-11.02.2016.tpl';

if ('login' == URL::GetPath()) UI::Redirect(URL::GetRoot() . '/pub/login.php');
if ('admin' == URL::GetPath()) UI::Redirect(URL::GetRoot() . '/admin/login.php');

// set 'has_mod_rewrite' flag
$struct = array();
$_GET['global']['has_mod_rewrite'] = '1';
$oSiteMap = SiteMap::singleton();
$oSiteMap->InitStruct(URL::GetPath());

/*if ($oSiteMap->is404) {
    $uri = preg_replace('#^/(.*)/?$#', '$1', $_SERVER['REQUEST_URI']);
    $uri = explode('/', $uri);
    $_SERVER['REQUEST_URI'] = '/' . $uri[0] . '/';
    $_SERVER['REDIRECT_URL'] = '/' . $uri[0] . '/';
    $_SERVER['REDIRECT_QUERY_STRING'] = $_SERVER['QUERY_STRING'] = $_SERVER['argv'][0] = 'path=' . $uri[0];
    unset($oSiteMap);
    $oSiteMap = SiteMap::singleton();
    $oSiteMap->InitStruct(URL::GetPath());
}*/
if ($oSiteMap->is404) {
    header("HTTP/1.0 404 Not Found");
    $_GET['path'] = '404';
    $_GET['global']['path'] = '404';
} else $struct = $oSiteMap->GetStruct();


if (isset($struct[1]) && $struct[1]['modify']) {
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= strtotime($struct[1]['modify'])){
		header('HTTP/1.1 304 Not Modified');
		die;
	}
	
	$LastModified_unix = strtotime($struct[1]['modify']); 
	$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
	
	/*
	echo "<!--".$struct[1]['modify']." - ".$LastModified;
	print_r($struct);
	echo "-->";
	*/
	
	$IfModifiedSince = false;
	if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
		$IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));  
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
		$IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
	if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
		header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
		exit;
	}
	header('Last-Modified: '. $LastModified);
}


$is_mod_exist = false;
$mod_inc = '';
if (isset($struct[1]) && $struct[1]['control']) {
    $mod_path = sprintf("%s/component/controls/%s.inc.php", SITE_ABS, $struct[1]['control']);
    $mod_inc = sprintf("controls/%s.inc.php", $struct[1]['control']);
    
//            echo $mod_inc;
    $is_mod_exist = (file_exists($mod_path));
}


if (isset($_GET['dh']) && !isset($_SESSION['discount_hash'])) $_SESSION['discount_hash'] = $_GET['dh'];

$date = new Dates();

$tpl->assign('langs', $enum['langs']);

$tpl->assign('page', URL::GetModule());
$tpl->assign('rstruct', $oSiteMap->GetRStruct());
$tpl->assign('is_mod_exist', $is_mod_exist);
$tpl->assign('mod_inc', $mod_inc);
$tpl->assign('site_addr', SITE_ADR);
$tpl->assign('is_mobile', isMobile());
$tpl->assign('is_debug', IS_DEBUG);

//$tpl->assign('captcha', viewCaptcha());


//var_dump($_GET);

#if ($_SERVER['HTTP_X_REAL_IP'] == "176.109.39.8") {
#print_r($_GET);
#exit;
#}
#die();
$tpl->caching = false;
//$tpl->clearAllCache();
$tpl->display($template);

Database::closeConnections();


if (defined('IS_DEBUG')) if (1 == IS_DEBUG) {
    require_once '../component/fx/tm_out.inc.php';
    @showSql();
    Debug_HackerConsole_Main::out($struct, "URL Struct");
}

function blockIe6() {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (stripos($user_agent, 'MSIE 6.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 7.0') === false) {
        if (!isset($_COOKIE["ie"])) {
            setcookie("ie", "yes", time() + 60 * 60 * 24 * 360);
            header("Location: " . SITE_REL . "/pub/ie6.php");
        }
    }
}

function isMobile() {
    $rx = "palm|palmos|palmsource|iphone|blackberry|nokia|phone|midp|mobi|pda|wap|java|nokia|hand|symbian|chtml|wml|ericsson|lg|audiovox|motorola|samsung|sanyo|sharp|telit|tsm|mobile|mini|windows ce|smartphone|240x320|320x320|mobileexplorer|j2me|sgh|portable|sprint|vodafone|docomo|kddi|softbank|pdxgw|j-phone|astel|minimo|plucker|netfront|xiino|mot-v|mot-e|portalmmm|sagem|sie-s|sie-m|android|ipod";
    $agent = $_SERVER['HTTP_USER_AGENT'];
    
    //$agent = 'windows ce/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.68 Safari/534.30';
    return (preg_match("/($rx)/i", $agent)) ? 1 : 0;
}
//    $files = get_included_files();
//foreach($files as $file) {
//echo "$file \n";
//}
?>
