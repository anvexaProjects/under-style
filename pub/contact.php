<?php

require_once '../component/libs.inc';
require_once '../component/bl/notification.class.php';

$e = '';
$regex = '/^((\"[^\"\f\n\r\t\v\b]+\")|([\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+(\.[\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+)*))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+))$/';
if (trim($_REQUEST['name']) == '') $e = sprintf(FLB_REQUIRED, S_NAME);
if (trim($_REQUEST['email']) == '') $e = sprintf(FLB_REQUIRED, S_EMAIL);
if (!preg_match($regex, $_REQUEST['email'])) $e = FLB_EMAIL_FORMAT;
if (trim($_REQUEST['message']) == '') $e = sprintf(FLB_REQUIRED, S_MESSAGE);
if (!empty($_REQUEST['f2']) || $_REQUEST['f1'] != CHECK_SPAM2) $e = FLB_ERROR_MESSAGE;

if (empty($e))
{
    $tpl = new Smarty();
    $tpl->assign('time', date('r'));
    $tpl->assign('site_addr', SITE_ADR);
    $msg_text = $tpl->fetch('notifications/feedback.tpl');

    $adr = explode(';', ADMIN_MAIL);
    foreach($adr as $a)
    {
        $res = Notification::SendBody($a, SUBJ_FEEDBACK, $msg_text);
        if (!$res)
        {
            $e = FLB_ERROR_MESSAGE;
            echo $e;
            exit;
        }
    }

    $tpl2 = new Smarty();
    $tpl2->assign('time', date('r'));
    $tpl2->assign('site_addr', SITE_ADR);
    $msg_text = $tpl2->fetch('notifications/feedback_copy.tpl');
    $res = Notification::SendBody($_REQUEST['email'], SUBJ_FEEDBACK_COPY, $msg_text);
    if (!$res)
    {
        $e = FLB_ERROR_MESSAGE;
        echo $e;
        exit;
    }
}
echo $e;

?>
