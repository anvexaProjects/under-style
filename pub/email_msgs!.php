
<!-- CREATED BY RIV, https://it39.ru/ -->

<style>
.mBorder
{
	border: 8px solid rgba(200,198,196,0.49);
	border-radius: 4px;
	left: 37%;
	moz-border-radius: 4px;
	position: absolute;
	top: 10%;
	webkit-border-radius: 4px;
}
.mButton
{
	background-color: #425d81;
	color: #f0eff1;
	cursor: pointer;
	font-size: 20px;
	height: 40px;
	margin-left: 45px;
	margin-top: 20px;
	padding-left: 70px;
	padding-top: 25px;
	width: 200px;
}
.mCatalogButton
{
	background-color: #425d81;
	border: 1px dotted #fff;
	color: #f0eff1;
	cursor: pointer;
	font-size: 20px;
	height: 30px;
	margin-bottom: 15px;
	margin-left: 15px;
	padding-top: 20px;
	width: 220px;
}
.mCatalogButton:active,.mSuccessButton:active,.mButton:active
{
	background-color: #364c68;
	color: #fff;
}

.mCatalogButton:hover,.mSuccessButton:hover,.mButton:hover
{
	background-color: #5689b8;
}

.mClose
{
	color: #8d8a8a;
	cursor: pointer;
	font-size: 16px;
	font-weight: 700;
	position: absolute;
	right: 5px;
	top: 5px;
	z-index: 9998;
}
.mConteiner
{
	background-color: #fff;
	border: 2px solid #a1a1a1;
	height: 480px;
	width: 355px;
	z-index:9999;
}
.mConteiner h1
{
	color: #8d8a8a;
	font-size: 20px;
	margin-bottom: 20px;
	margin-left: 20px;
	margin-top: 30px;
}
.mConteiner input
{
	border: 1px solid #a1a1a1;
	height: 35px;
	margin-bottom: 7px;
	margin-left: 45px;
	margin-top: 7px;
	padding-left: 10px;
	width: 268px;
}
.mSuccess
{
	background-color: #fff;
	color: #8d8a8a;
	display: none;
	font-size: 20px;
	height: 480px;
	position: absolute;
	width: 355px;
	z-index: 1000;
}
.mSuccess h2
{
	color: #8d8a8a;
	font-size: 20px;
	margin-bottom: 60px;
	margin-top: 70px;
}
.mSuccessButton
{
	background-color: #425d81;
	color: #f0eff1;
	cursor: pointer;
	font-size: 20px;
	height: 40px;
	margin-left: 45px;
	margin-top: 20px;
	padding-left: 120px;
	padding-top: 25px;
	width: 150px;
}
</style>


<?php 

if ($_POST['type'] == 'msg_opt')
{
	$header = "From: site@under-style.ru \r\n";
	$header .= "Content-type: text/html; charset=\"utf-8\"";
	if (isset ($_POST['question'])) {$subject = $_POST['question'];} else {$subject = "Сообщение с формы: 'Для оптовых покупателей'";}
	mail("opt@under-style.ru",
	$subject, 
	"<br /> <span style = 'margin-right:122px;'><b>Имя:</b></span> " . $_POST['name'] .
	"<br /> <span style = 'margin-right:92px;'><b>Телефон:</b></span> " . $_POST['phone'] .
	"<br /> <span style = 'margin-right:109px;'><b>E-mail:</b></span> " . $_POST['email'] .
	"<br /> <span style = 'margin-right:83px;'><b>Компания:</b></span> " . $_POST['company'] .
	"<br /> <span style = 'margin-right:118px;'><b>Сайт:</b></span> " . $_POST['site'] . 
	"<br /> <span style = 'margin-right:5px;'><b>Наименование товара:</b></span> " . $_POST['name_position'] . 
	"<br /> <span style = 'margin-right:46px;'><b>Сылка на товар:</b></span> " . $_POST['url'] .
	"<br /> <span style = 'margin-right:100px;'><b>Вопрос:</b></span> " . $_POST['question'] . 
	"<br /><br /><hr> 
	<span>&copy; 2015 under-style.ru - all rights reserved</span>
	<br /><span>Sent ". date ('F j, Y, D G:i:s T') ."</span>"	
	, $header);
}

//opt@under-style.ru

if ($_POST['type'] == 'msg_price')
{
	$header = "From: site@under-style.ru \r\n";
	$header .= "Content-type: text/html; charset=\"utf-8\"";
	if (isset ($_POST['question'])) {$subject = $_POST['question'];} else {$subject = "Сообщение с формы: 'Уточнить цену'";}
	mail("zapros@under-style.ru",
	$subject, 
	"<br /> <span style = 'margin-right:122px;'><b>Имя:</b></span> " . $_POST['name'] .
	"<br /> <span style = 'margin-right:92px;'><b>Телефон:</b></span> " . $_POST['phone'] .
	"<br /> <span style = 'margin-right:109px;'><b>E-mail:</b></span> " . $_POST['email'] .
	"<br /> <span style = 'margin-right:5px;'><b>Наименование товара:</b></span> " . $_POST['name_position'] . 
	"<br /> <span style = 'margin-right:46px;'><b>Сылка на товар:</b></span> " . $_POST['url'] .
	"<br /> <span style = 'margin-right:100px;'><b>Вопрос:</b></span> " . $_POST['question'] . 
	"<br /><br /><hr> 
	<span>&copy; 2015 under-style.ru - all rights reserved</span>
	<br /><span>Sent ". date ('F j, Y, D G:i:s T') ."</span>"	
	, $header);
}

//zapros@under-style.ru

?>

<div id="email_price_button" class="mCatalogButton">
    <center>УТОЧНИТЬ ЦЕНУ</center>
</div>
<div id="msg_price" class="fancybox-overlay fancybox-overlay-fixed" style="width: auto; height: auto; display: none;">
    <div class="mBorder">
        <div class="mConteiner" style="height:380px;">
            <div id="mClose_price" class="mClose">x</div>
            <div id="mSuccess_price" class="mSuccess" style="height:380px;">
                <center>
                    <h2>Сообщение отправлено</h2>
                </center>
                <div id="mSuccessButton_price" class="mSuccessButton">OK</div>
            </div>
            <center>
                <h1>УТОЧНИТЬ ЦЕНУ</h1></center>
            <form id="form_price" action="" method="post">
                <input id="name_price" type="text" name="name" value="" placeholder="Имя представителя*" maxlength="50" >
                <input id="phone_price" type="text" name="phone" value="" placeholder="Контактный телефон*" maxlength="50" >
                <input id="email_price" type="text" name="email" value="" placeholder="E-mail*" maxlength="50" >
				<input id="question_price" type="text" name="question" value="" placeholder="Вопрос" maxlength="150" >
                <input type="hidden" name="type" value="msg_price">
				<input id = "url_price" type="hidden" name="url" value="">
				<input id = "name_position_price" type="hidden" name="name_position" value="">
            </form>
			<input type="button" id="mButton_price" onclick="yaCounter23717377.reachGoal('get_pricing'); _gaq.push(['_trackEvent','Уточнить цену','get_pricing']); return true;" value="ОТПРАВИТЬ" class="mButton" style = "height: 50px; padding-top: 0px; margin-top: 20px;"/>
            <!-- <div id="mButton_price" class="mButton">ОТПРАВИТЬ</div> -->
        </div>
    </div>
</div>

<div id="email_opt_button" class="mCatalogButton">
    <center>ОПТОВИКУ</center>
</div>
<div id="msg_opt" class="fancybox-overlay fancybox-overlay-fixed" style="width: auto; height: auto; display: none;">
    <div class="mBorder">
        <div id = "msg_opt_conteiner" class="mConteiner">
            <div id="mClose_opt" class="mClose">x</div>
            <div id="mSuccess_opt" class="mSuccess">
                <center>
                    <h2>Сообщение отправлено</h2>
                </center>
                <div id="mSuccessButton_opt" class="mSuccessButton">OK</div>
            </div>
            <h1>ДЛЯ ОПТОВЫХ ПОКУПАТЕЛЕЙ</h1>
            <form id="form_opt" action="" method="post">
                <input id="name_opt" type="text" name="name" value="" placeholder="Имя представителя*" maxlength="50" >
                <input id="phone_opt" type="text" name="phone" value="" placeholder="Контактный телефон*" maxlength="50" >
                <input id="email_opt" type="text" name="email" value="" placeholder="E-mail*" maxlength="50" >
                <input id="company_opt" type="text" name="company" value="" placeholder="Название компании*" maxlength="50" >
                <input id="site_opt" type="text" name="site" value="" placeholder="Сайт компании" maxlength="50" >
				<input id="question_opt" type="text" name="question" value="" placeholder="Вопрос" maxlength="150" >
                <input type="hidden" name="type" value="msg_opt">
				<input id = "url_opt" type="hidden" name="url" value="">
				<input id = "name_position_opt" type="hidden" name="name_position" value="">
            </form>
            <input type="button" id="mButton_opt" onclick="yaCounter23717377.reachGoal('opt_price'); _gaq.push(['_trackEvent','Оптовику','opt_price']); return true;" value="ОТПРАВИТЬ" class="mButton" style = "height: 50px; padding-top: 0px; margin-top: 20px;"/>
            <!-- <div id="mButton_opt" class="mButton">ОТПРАВИТЬ</div>-->
        </div>
    </div>
</div>

<script>

$(document).ready(function() {

	$('#url_price').val(location.href);
	var name_position = $('.productInfo > h2').html();
	$('#name_position_price').val(name_position);

	$('#url_opt').val(location.href);
	var name_position = $('.productInfo > h2').html();
	$('#name_position_opt').val(name_position);

	 $('#msg_opt').click(function(event){
		if ($(event.target).attr('id') == 'msg_opt') {
			$('#msg_opt').hide();
		}
	 });

	  $('#msg_price').click(function(event){

		if ($(event.target).attr('id') == 'msg_price') {
			$('#msg_price').hide();
		}
	 });

    $('#mButton_opt').click(function() {
        var name = $('#name_opt').val();
        var phone = $('#phone_opt').val();
        var email = $('#email_opt').val();
        var company = $('#company_opt').val();
        if (name.length < 1) {
            $('#name_opt').css('border-color', 'red');
            return false;
        }
        if (phone.length < 1) {
            $('#phone_opt').css('border-color', 'red');
            return false;
        }
        if (email.length < 1) {
            $('#email_opt').css('border-color', 'red');
            return false;
        }
        if (!email.match(/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i)) {
            $('#email_opt').css('border-color', 'red');
            return false;
        }
        if (company.length < 1) {
            $('#company_opt').css('border-color', 'red');
            return false;
        }
        jQuery.ajax({
            url: "../../../../pub/email_msgs.php", //Адрес подгружаемой страницы
            type: "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: jQuery("#form_opt").serialize(),
            success: function() {
                $('#mSuccess_opt').show();
                //console.log(jQuery("#form_opt").serialize());
            },
            error: function() { //Если ошибка
                $('#mSuccess_opt').show();
                console.log('0');
            }
        });
    });
    $('#mClose_opt,#mSuccessButton_opt').click(function() {
        $('#msg_opt').hide();
    });
    $('#email_opt_button').click(function() {
        $('#msg_opt').show();
    });

    $('#mButton_price').click(function() {
        var name = $('#name_price').val();
        var phone = $('#phone_price').val();
        var email = $('#email_price').val();
        if (name.length < 1) {
            $('#name_price').css('border-color', 'red');
            return false;
        }
        if (phone.length < 1 && email.length < 1) {
            $('#phone_price').css('border-color', 'red');
            $('#email_price').css('border-color', 'red');
            return false;
        }
        if (email.length > 1) {
            if (!email.match(/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i)) {
                $('#email_price').css('border-color', 'red');
                return false;
            }
        }
        jQuery.ajax({
            url: "../../../../pub/email_msgs.php", //Адрес подгружаемой страницы
            type: "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: jQuery("#form_price").serialize(),
            success: function() {
                $('#mSuccess_price').show();
                //console.log(jQuery("#form_price").serialize());
            },
            error: function() { //Если ошибка
                $('#mSuccess_price').show();
                console.log('0');
            }
        });
    });
    $('#mClose_price,#mSuccessButton_price').click(function() {
        $('#msg_price').hide();
    });
    $('#email_price_button').click(function() {
        $('#msg_price').show();
    });


});

</script>


						<!--	{include_php file="content/pricing.inc.php"} -->
						<!--	{include_php file="content/spec_colling.inc.php"} -->


