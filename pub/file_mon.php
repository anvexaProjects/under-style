<?php

require_once '../component/libs.inc';

$files = array(
'index.php',
'pub/index.php',
'pub/index_db.php',
'admin/index.php'
);

$expr = array(
'iframe',
'globalmixgroup.cn',
'brandnameshoppin.cn',
'c5y.at:8080'
);

$message = '';

foreach ($files as $f)
{
	$file = SITE_ABS."/$f";
	if (file_exists($file) && is_file($file))
	{
		$content = file_get_contents($file);
		foreach($expr as $ex)
		{
			if (false !== stripos($content, $ex))
			{
				$message .= "'$ex' found in file $file\r\n";
			}	
		}
	}
}


echo 'Started...<br>';
if (!empty($message))
{
	$time = date("Y/m/d (d F, D) H:i:s O");
	$text = "[$time]\r\n".$message;
	mail(SYS_ADMIN_MAIL, sprintf('File monitor: error occured at %s on %s (%s) ', $time, TITLE, $_SERVER['SERVER_NAME']), $text);
	echo str_replace("\r\n", '<br>', $text).'<br>';
}
echo 'Finished.<br>';


?>
