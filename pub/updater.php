<?php

function rus2translit($string) {

    $converter = array(

        'а' => 'a',   'б' => 'b',   'в' => 'v',

        'г' => 'g',   'д' => 'd',   'е' => 'e',

        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

        'и' => 'i',   'й' => 'y',   'к' => 'k',

        'л' => 'l',   'м' => 'm',   'н' => 'n',

        'о' => 'o',   'п' => 'p',   'р' => 'r',

        'с' => 's',   'т' => 't',   'у' => 'u',

        'ф' => 'f',   'х' => 'h',   'ц' => 'c',

        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',

        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        

        'А' => 'A',   'Б' => 'B',   'В' => 'V',

        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

        'И' => 'I',   'Й' => 'Y',   'К' => 'K',

        'Л' => 'L',   'М' => 'M',   'Н' => 'N',

        'О' => 'O',   'П' => 'P',   'Р' => 'R',

        'С' => 'S',   'Т' => 'T',   'У' => 'U',

        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',

        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

    );

    return strtr($string, $converter);

}

function str2url($str) {

    // переводим в транслит

    $str = rus2translit($str);

    // в нижний регистр

    $str = strtolower($str);

    // заменям все ненужное нам на "-"

    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);

    // удаляем начальные и конечные '-'

    $str = trim($str, "-");

    return $str;

}

/*
$oDb = &Database::get();
$rows = $oDb->getRows('SELECT  art_struct.sprop13 as name , art_struct.parent_id
FROM    art_struct
WHERE   art_struct.control = "prod" GROUP BY art_struct.sprop13');


var_dump($rows);


*/

/*
$oDb = &Database::get();
$rows = $oDb->getRows('SELECT * FROM collection_psevdonims');

$values = array();
foreach($rows as $row)
{
	$oDb->update('collection_psevdonims', array('psevdonim'=>str2url($row['name'])), 'psevdonim_id = '.$row['psevdonim_id']);
}

die("test");

*/

    require_once '../component/libs.inc';
    require_once '../lib/infrastruct/sitemap.class.php';
    require_once '../lib/infrastruct/dates.class.php';
    require '../templates/config/enum.inc';


$oDb = &Database::get();

$oDb = &Database::get();

$rows = $oDb->getRows('SELECT * FROM art_struct WHERE `control`="collection" AND psevdonim="" AND `is_deleted`=0');

foreach($rows as $row)
{
    var_dump(array('psevdonim'=>str2url($row['0'])));
    $oDb->update('art_struct', array('psevdonim'=>str2url($row['sprop3'])), "`struct_id`='{$row['struct_id']}'");
}



/*****/


$rows = $oDb->getRows('SELECT * FROM art_struct WHERE `control`="prod" AND psevdonim="" AND `is_deleted`=0');

foreach($rows as $row)
{
    var_dump(array('psevdonim'=>str2url($row['sprop3'])));
    $oDb->update('art_struct', array('psevdonim'=>str2url($row['sprop3'])), "`struct_id`='{$row['struct_id']}'");
}


$rows = $oDb->getRows('SELECT a1.struct_id as struct_id, a1.sprop3 as sprop3, a2.psevdonim as psevdonim FROM `art_struct` as a1 JOIN `art_struct` a2  ON (a1.parent_id = a2.struct_id AND a1.is_deleted = 0 AND a2.is_deleted = 0 AND a1.psevdonim <> a2.psevdonim) WHERE a1.control = "prod"');

foreach($rows as $row)
{
    var_dump(array('psevdonim'=>$row['psevdonim']));
    $oDb->update('art_struct', array('psevdonim'=>$row['psevdonim']), "`struct_id`='{$row['struct_id']}'");
}





/*****/




$rows = $oDb->getRows('SELECT `sprop4`, `parent_id` FROM `art_struct` WHERE CONCAT(`parent_id`,`sprop4`)  NOT IN (SELECT CONCAT(`parent_id`,`name`) FROM `category_psevdonims`)   AND sprop4 != ""  AND `art_struct`.`is_deleted`=0 GROUP BY `parent_id`,`sprop4`');

foreach($rows as $row)
{
    if($row['sprop13'] == '')
    {
        $row['sprop13'] = 'Без категории';
    }
    $oDb->insert('category_psevdonims', array('name'=>$row['sprop4'], 'psevdonim'=>str2url($row['sprop4']), 'parent_id'=>$row['parent_id']));
}


$rows = $oDb->getRows('SELECT `sprop13`, `parent_id` FROM `art_struct` WHERE CONCAT(`parent_id`,`sprop13`) NOT IN (SELECT CONCAT(`parent_id`,`name`) FROM `collection_psevdonims`)  AND sprop13 != "" AND `is_deleted` = 0  GROUP BY `parent_id`,`sprop13`');

foreach($rows as $row)
{
    if($row['sprop13'] == '')
    {
        $row['sprop13'] = 'Без коллекции';
    }else{
	$oDb->insert('collection_psevdonims', array('name'=>$row['sprop13'], 'psevdonim'=>str2url($row['sprop13']), 'parent_id'=>$row['parent_id']));
    }
}

$rows = $oDb->getRows('SELECT `sprop6`, `parent_id` FROM `art_struct` WHERE  CONCAT(`parent_id`,`sprop6`) NOT IN (SELECT CONCAT(`parent_id`,`name`)  FROM `place_psevdonims`)  AND sprop6 != ""  AND `art_struct`.`is_deleted`=0 GROUP BY `parent_id`,`sprop6`');

foreach($rows as $row)
{
    if($row['sprop6'] == '') {
        continue;
    }
    $oDb->insert('place_psevdonims', array('name'=>$row['sprop6'], 'psevdonim'=>str2url($row['sprop6']), 'parent_id'=>$row['parent_id']));
}


die('ok');