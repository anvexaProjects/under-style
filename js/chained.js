var _filters = document.querySelector('#filters');
var productsFilter = document.querySelector('#products-filter');

var controls = {
    ctg: document.querySelector('#category'),
    ven: document.querySelector('#vendor'),
    grp: document.querySelector('#group'),
    col: document.querySelector('#col')
};


function loadXMLDoc() {

    var xmlhttp;

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                var response = xmlhttp.responseText;
                var parsed = JSON.parse(response);


                // arr.unshift("Выберите коллекцию");

                var cat = document.getElementById('col');

                var options = document.createDocumentFragment();

                for (var j = 0; j < parsed.length; j++) {
                    options.appendChild(new Option(parsed[j].col_title, parsed[j].col_name));
                }

                cat.appendChild(options);
                removeSelectOptionDuplicate();

            }

            else if (xmlhttp.status == 400) {
                console.log('There was an error 400')
            }
            else {
                console.log('something else other than 200 was returned')
            }
        }
    };

    var categoryNumber = document.getElementById('category').value;
    var vendorNumber = document.getElementById('vendor').value;
    var groupNumber = document.getElementById('group').value;

    xmlhttp.open("GET", "/picking_filter.php?category_filter=" + categoryNumber + "&vendor_filter=" + vendorNumber + "&group_filter=" + groupNumber, true);
    xmlhttp.send();
}

//QuerySelectors
var categoryFilter = document.querySelector('#category');
var vendorFilter = document.querySelector('#vendor');
var groupFilter = document.querySelector('#group');


//Functions

//Check Category & Vendor

function checkGroupVendorOption(categoryFilterOptionValue) {

    var categoryFilterOptionValue = categoryFilter.options[categoryFilter.selectedIndex].value;

    for (var i = 1; i < vendorFilter.options.length; i++) {

        var vendorFilterOptionsClass = vendorFilter.options[i].className;

        if (vendorFilterOptionsClass.indexOf(' ' + categoryFilterOptionValue + ' ') != -1) {
            vendorFilter.options[i].style.display = 'block';

        } else if (categoryFilter.options[categoryFilter.selectedIndex].value == '') {
            vendorFilter.options[i].style.display = 'block';

        } else {
            vendorFilter.options[i].style.display = 'none';
        }
    }
}


//Check Vendor & Category


function checkVendorOption(vendorFilterOptionValue) {

    var vendorFilterOptionValue = vendorFilter.options[vendorFilter.selectedIndex].value;

    for (var i = 1; i < categoryFilter.options.length; i++) {

        var categoryFilterOptions = categoryFilter.options[i].className;

        if (categoryFilterOptions.indexOf(vendorFilterOptionValue) != -1) {
            categoryFilter.options[i].style.display = 'block';
        } else {
            categoryFilter.options[i].style.display = 'none';
        }

    }
};


//Check Group & Vendor


function checkGroupOption(groupFilterOptionValue) {

    var groupFilterOptionValue = groupFilter.options[groupFilter.selectedIndex].value;

    for (var i = 1; i < vendorFilter.options.length; i++) {

        var vendorFilterOptions = vendorFilter.options[i].className;

        if (vendorFilterOptions.indexOf(groupFilterOptionValue) != -1) {
            vendorFilter.options[i].style.display = 'block';
        } else {
            vendorFilter.options[i].style.display = 'none';
        }

    }

}


//Check Vendor & Group


function checkGroupOptionReverse(vendorFilterOptionValue) {

    var vendorFilterOptionValue = vendorFilter.options[vendorFilter.selectedIndex].value;

    for (var i = 1; i < groupFilter.options.length; i++) {

        var groupFilterOptions = group.options[i].className;


        if (groupFilterOptions.indexOf(vendorFilterOptionValue) != -1) {
            groupFilter.options[i].style.display = 'block';
        } else {
            groupFilter.options[i].style.display = 'none';
        }
    }
}


//Check Group & Category


function checkGroupCategoryOption(categoryFilterOptionValue) {

    var categoryFilterOptionValue = categoryFilter.options[categoryFilter.selectedIndex].value;

    for (var i = 1; i < groupFilter.options.length; i++) {

        var groupFilterOptionsClass = groupFilter.options[i].className;

        if (groupFilterOptionsClass.indexOf(' ' + categoryFilterOptionValue + ' ') != -1) {
            groupFilter.options[i].style.display = 'block';

        } else if (categoryFilter.options[categoryFilter.selectedIndex].value == '') {
            groupFilter.options[i].style.display = 'block';

        } else {
            groupFilter.options[i].style.display = 'none';

        }
    }
}


//Check Category & Group


// function checkGroupCategoryOptionReverse(groupFilterOptionValueClassName){

//     var groupFilterOptionValueClassName = groupFilter.options[groupFilter.selectedIndex].className;

//     for(var i=1; i<groupFilter.options.length; i++){

//         var categoryFilterOptionValue = categoryFilter.options[i].value;

//         if(groupFilterOptionValueClassName.indexOf(categoryFilterOptionValue) != -1){
//             categoryFilter.options[i].style.display = 'block';
//         } else {
//             categoryFilter.options[i].style.display = 'none';
//         }

//     }
// };


//End Functions


function categoryChange() {
    var col = document.getElementById('col');
    Array.prototype.splice.call(col, 1);
    checkGroupVendorOption();
    checkGroupCategoryOption();
    loadXMLDoc();
}

//Check category & brand

controls.ctg.addEventListener('change', function (e) {
    categoryChange();
    productsFilter.submit();
});

//Check brand & category

function vendorChange() {
    var col = document.getElementById('col');
    Array.prototype.splice.call(col, 1);
    checkGroupOptionReverse();
    checkVendorOption();
    loadXMLDoc();
}

controls.ven.addEventListener('change', function (e) {
    vendorChange();
    productsFilter.submit();
});

//Check group 

function groupChange() {
    var col = document.getElementById('col');
    Array.prototype.splice.call(col, 1);
    checkVendorOption();
    checkGroupOptionReverse();
    if (vendorFilter.options[vendorFilter.selectedIndex].value == '') {
        checkGroupCategoryOption();
    }
    // checkGroupCategoryOptionReverse();
    checkGroupOption();

    loadXMLDoc();
}

controls.grp.addEventListener('change', function (e) {
    groupChange();
    productsFilter.submit();
});

controls.col.addEventListener('change', function (e) {
    productsFilter.submit();
});






