var _filters = document.querySelector('#filters');

var controls = {
    ctg: document.querySelector('#category'),
    ven: document.querySelector('#vendor'),
    grp: document.querySelector('#group'),
    col: document.querySelector('#col')
};

function filterChild(selected, child, dependencys) {
    for (var i = 0; i < child.options.length; i++) {
        var option = child.options[i];

        if (!option.className) {
            continue;
        }

        if (!isClassInOption(' ' + selected + ' ', option)) {
            option.style.display = 'none';
        } else {
            var count = 0;
            dependencys.forEach(function (item) {
                if (isClassInOption(' ' + item + ' ', option) || !item) {
                    count++;
                }
            });

            if (dependencys.length == count) {
                option.style.display = 'block';
            }
        }
    }
}

function filterParent(selected, parent, isNotHideUncompared) {
    var isNotHideUncompared = typeof isNotHideUncompared !== 'undefined' ? isNotHideUncompared : false;
    var parentId = parent.getAttribute('id');
    var selectedParent = parent.options[parent.selectedIndex];
    var template = "#" + parentId + " option[class~='" + selected + "']";
    var parentContain = document.querySelectorAll(template);

    if (!isClassInOption(' ' + selected + ' ', selectedParent)) {
        if (parentContain.length == 1) {
            parent.selectedIndex = parentContain[0].index;
        } else if (!parentContain.length) {
            parent.selectedIndex = 0;
        } else {
            parent.selectedIndex = 0;
        }
    }

    if (!isNotHideUncompared) {
        for (var i = 0; i < parent.options.length; i++) {
            var option = parent.options[i];

            if (!option.className) {
                continue;
            }

            //if (option.className.indexOf(selected) == -1) {
            if (!isClassInOption(' ' + selected + ' ', option)) {
                option.style.display = 'none';
            } else {
                option.style.display = 'block';
            }
        }
    }
}

function getSelValue(select) {
    return select.options[select.selectedIndex].value;
}

function isClassInOption(className, optionWhere) {
    return optionWhere.className.indexOf(className) != -1;
}

function ctgChained(value, controls) {
    filterChild(value, controls.ven, [getSelValue(controls.grp), getSelValue(controls.col)]);
    filterChild(value, controls.grp, [getSelValue(controls.ven), getSelValue(controls.col)]);
    //filterChild(value, controls.col, [getSelValue(controls.ven), getSelValue(controls.grp)]);
}

function venChained(value, controls) {
    if (value) {
        filterParent(value, controls.ctg);
    }
    filterChild(value, controls.grp, [getSelValue(controls.ven), getSelValue(controls.col)]);
    //filterChild(value, controls.col, [getSelValue(controls.ven), getSelValue(controls.grp)]);
}

function grpChained(value, controls) {
    if (value) {
        filterParent(value, controls.ctg);
        filterParent(value, controls.ven);
    }
    else {
        resetSelect(controls.col);
    }

    //filterChild(value, controls.col, [getSelValue(controls.ven), getSelValue(controls.ctg)]);
}

function colChained(value, controls) {
    if (value) {
        filterParent(value, controls.ctg);
        filterParent(value, controls.ven);
        filterParent(value, controls.grp);
    }
}

function resetSelect(control) {
    for (var i = 0; i < control.options.length; i++) {
        var option = control.options[i];
        option.style.display = 'block';
    }

    control.selectedIndex = 0;
}

function resetFilter() {
    resetSelect(controls.ctg);
    resetSelect(controls.ven);
    resetSelect(controls.grp);
    resetSelect(controls.col);
}

function initFilters() {
    controls.ctg.addEventListener('change', function (e) {
        ctgChained(e.target.value, controls);
    });

    controls.ven.addEventListener('change', function (e) {
        venChained(e.target.value, controls);
    });

    controls.grp.addEventListener('change', function (e) {
        grpChained(e.target.value, controls);
    });

    controls.col.addEventListener('change', function (e) {
        colChained(e.target.value, controls);
    });

    //var template = "#col option[value='" + "535" + "']";
//var option = document.querySelector(template);
//controls.col.selectedIndex = option.index;

    if (controls.ctg.selectedIndex) {
        ctgChained(controls.ctg.options[controls.ctg.selectedIndex].value, controls);
    }

    if (controls.ven.selectedIndex) {
        venChained(controls.ven.options[controls.ven.selectedIndex].value, controls);
    }

    if (controls.grp.selectedIndex) {
        grpChained(controls.grp.options[controls.grp.selectedIndex].value, controls);
    }

    if (controls.col.selectedIndex) {
        colChained(controls.col.options[controls.col.selectedIndex].value, controls);
    }
}

if(_filters) {
    initFilters();
}



