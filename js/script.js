$(document).ready(function () {
    _subscribe();

    // jQuery.scrollSpeed(100, 800);
    deferClickToNextFrom('#categories .thumbnail img');
    $('.collapse').collapse();

    //$("#vendor").chained("#category");

    //$("#group").chained("#vendor");

    //$("#group").chained("#vendor");

    //$("#col").chained("#category, #group, #vendor");

    //$("#col").chained("#group, #vendor");

    //$("#group").chained("#vendor");
    //$("#place").chained("#category");
    //$('input, select').styler();

    var container = $('#products');

    container.imagesLoaded(function () {
        container.masonry({
            itemSelector: '.post-box',
            columnWidth: '.post-box',
            transitionDuration: 0
        });
    });

    $('.lightbox').prettyPhoto({
        theme: 'light_rounded',
        overlay_gallery: false
    });

    callMeInit();

    productActions();

    imageAttrEdit();

    productsGroupsTextDecrease();

    if(controls.ctg.value) {
        categoryChange();
    }

    if(controls.ven.value) {
        vendorChange();
    }

    if(controls.grp.value) {
        groupChange();
    }

    console.log('its work');
});

function removeSelectOptionDuplicate() {
    $('select option').each(function() {
        $(this).prevAll('option[value="' + this.value + '"]').remove();
    });
}

function findCyrPhrase(str, cyrPhrase) {
    var cyrPhrase = typeof cyrPhrase != 'undefined' ? cyrPhrase : '';
    var cyrpos = str.search(/[А-яЁё]/);

    if(cyrpos != -1) {
        var cyrWordstart = str.substr(cyrpos);
        var cyrWordEnd = cyrWordstart.indexOf(' ');
        var cyrWord = cyrWordstart.substring(0, cyrWordEnd);
        cyrPhrase += cyrWord;
        str = str.substr(str.indexOf(cyrWord) + cyrWord.length);
        return findCyrPhrase(str, cyrPhrase + ' ');
    } else {
        return cyrPhrase;
    }
}

function productsGroupsTextDecrease() {
    var slides = document.querySelectorAll(".post-box .caption > a");

    if(!slides.length) {
        return false;
    }

    for(var i = 0; i < slides.length; i++) {
        var row = slides.item(i);
        var wrap = findCyrPhrase(row.innerHTML);
        //row.innerHTML = row.innerHTML.replace(wrap, '<span class="" style="font-size: 12px;">' + wrap + '</span>');
        row.innerHTML = row.innerHTML.replace(wrap, '<small>' + wrap + '</small>');
        //row.innerHTML = '<span style="font-size: 12px;">' + row.innerHTML + '</span>';
    }
}

function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

function _subscribe() {
    $("body").imagesLoaded(function () {
        $(".page-loader div").fadeOut();
        $(".page-loader").delay(200).fadeOut("slow");
    });

    $(document).on('change', '#category, #vendor, #col, #group, #place', function () {
        $('#category, #vendor, #col, #group, #place').trigger('refresh');
    });

    $('#feedback_button').on('click', function () {
        $('#feedback').submit();
    });

    $('#feedback').on('submit', function (e) {
        var emailinput = $("#feedback_email").val();
        e.preventDefault();
        if (!$("#feedback_name").val() == "" && !$("#feedback_phone").val() == "" && !$("#feedback_email").val() == ""
            && !$("#feedback_text").val() == "" && validateEmail(emailinput)) {
            $.ajax({
                url: "/about/contacts/feedback/",
                type: "POST",
                data: $(this).serialize(),
                success: function (data) {
                    $('#myModalSecond .alert-success').show();
                    $('#myModalSecond .modal-footer').hide();
                }
            });
        }
    });
}

function deferClickToNextFrom(selector) {
    $(selector).on('click', function (e) {
        $(this).next()[0].click();
    });
}

function check_form(form) {
    var errors = new Array();

    //if (form.category_filter.options[form.category_filter.selectedIndex].value == '') {
    //    errors.push('Выберите категорию товара!');
    //}

    //if (form.category_filter.options[form.category_filter.selectedIndex].value != '' && form.vendor_filter.options[form.vendor_filter.selectedIndex].value == '') {
    //    errors.push('Выберите бренд!');
    //}

    if (errors.length == 0) return true;
    alert(errors.join("\n"));
    return false;
}

function callMeInit() {
    $(function () {
        $('[data-toggle="modal"]').click(function () {
            // console.log($(".modal").find('form'));
            $('.modal form').each(function () {
                this.reset()
            });
            $('.alert').css("display", "none");
            $("#myModal .modal-footer").show();
            $("#myModalSecond .modal-footer").show();
            $("#callme_phone, #callme_name, #callme_text, #callme_time").css("border-bottom", "1px solid #000");
            $(".danger_sms, .danger_name, .danger_phone, .danger_time, .validemail").css("display", "none");
            $(".danger_feedback_name, .danger_feedback_phone, .danger_feedback_email, .danger_feedback_text").css("display", "none");
            $("#feedback_name, #feedback_phone, #feedback_email, #feedback_text").css("border-bottom", "1px solid #000");
        });

        $("#feedback_button").click(function () {
            var emailinput = $("#feedback_email").val();
            error = false;
            if ($("#feedback_name").val() == "") {
                $("#feedback_name").css("border-bottom", "1px solid #af4442");
                $('.danger_feedback_name').css("display", "block");
                error = true;
            } else {
                $("#feedback_name").css("border-bottom", "1px solid #000");
                $('.danger_feedback_name').css("display", "none");
            }
            if ($("#feedback_phone").val() == "" || $('#feedback_phone').val().length < 4 || $('#feedback_phone').val().length > 15) {
                $("#feedback_phone").css("border-bottom", "1px solid #af4442");
                $('.danger_feedback_phone').css("display", "block");
                error = true;
            } else {
                $("#feedback_phone").css("border-bottom", "1px solid #000");
                $('.danger_feedback_phone').css("display", "none");
            }
            if ($("#feedback_email").val() == "") {
                $("#feedback_email").css("border-bottom", "1px solid #af4442");
                $('.danger_feedback_email').css("display", "block");
                $('.validemail').hide();
                error = true;
            } else if (!validateEmail(emailinput)) {
                $('.validemail').text('Вы ввели неправильный email!').show();
                $('.danger_feedback_email').css("display", "none");
            } else {
                $("#feedback_email").css("border-bottom", "1px solid #000");
                $('.danger_feedback_email').css("display", "none");
                $('.validemail').hide();
            }
            if ($("#feedback_text").val() == "") {
                $("#feedback_text").css("border-bottom", "1px solid #af4442");
                $('.danger_feedback_text').css("display", "block");
                error = true;
            } else {
                $("#feedback_text").css("border-bottom", "1px solid #000");
                $('.danger_feedback_text').css("display", "none");
            }
        });


        $("#callme_button").click(function () {
            error = false;
            if ($("#callme_name").val() == "") {
                $("#callme_name").css("border-bottom", "1px solid #af4442");
                $('.danger_name').css("display", "block");
                error = true;
            } else {
                $("#callme_name").css("border-bottom", "1px solid #000");
                $('.danger_name').css("display", "none");
            }
            if ($('#callme_phone').val() == "" || $('#callme_phone').val().length < 4 || $('#callme_phone').val().length > 15) {
                $("#callme_phone").css("border-bottom", "1px solid #af4442");
                $('.danger_phone').css("display", "block");
                error = true;
            } else {
                $("#callme_phone").css("border-bottom", "1px solid #000");
                $('.danger_phone').css("display", "none");
            }
            if ($("#callme_text").val() == "") {
                $("#callme_text").css("border-bottom", "1px solid #af4442");
                $('.danger_sms').css("display", "block");
                error = true;
            } else {
                $("#callme_text").css("border-bottom", "1px solid #000");
                $('.danger_sms').css("display", "none");
            }
            if ($("#callme_time").val() === "0") {
                $("#callme_time").css("border-bottom", "1px solid #af4442");
                $('.danger_time').css("display", "block");
                error = true;
            } else {
                $("#callme_time").css("border-bottom", "1px solid #000");
                $('.danger_time').css("display", "none");
            }
            if (error == false) {
                $.post("/ajax.php", {
                        action: "callme",
                        name: $("#callme_name").val(),
                        phone: $("#callme_phone").val(),
                        time: $("#callme_time").val(),
                        text: $("#callme_text").val()
                    },
                    function (a) {
                        if (a.status == 'fail') {
                            $.fancybox(a.error);
                        } else {
                            //$("#callme_name").val("");
                            //$("#callme_phone").val("");
                            //$("#callme_time").val("08:00-09:00");
                            //$("#callme_text").val("");
                            //$.fancybox.close();

                            $("#myModal .alert-success").show();
                            $("#myModal .modal-footer").hide();

                            yaCounter23717377.reachGoal('callme');
                            _gaq.push(['_trackEvent', 'Обратный звонок', 'callme']);
                            return true;
                        }
                    }, "json");
            }
        });


        // $(".fancy").fancybox({
        //     afterClose: function () {
        //         $("#callme_name").val("");
        //         $("#callme_phone").val("");
        //         $("#callme_time").val("08:00-09:00");
        //         $("#callme_text").val("");
        //     }
        // });

    });
}

function productActions() {

    $('#url_price').val(location.href);
    var name_position = $('.productInfo > h2').html();
    $('#name_position_price').val(name_position);

    $('#url_opt').val(location.href);
    var name_position = $('.productInfo > h2').html();
    $('#name_position_opt').val(name_position);

    $('#msg_opt').click(function (event) {
        if ($(event.target).attr('id') == 'msg_opt') {
            $('#msg_opt').hide();
        }
    });

    $('#msg_price').click(function (event) {

        if ($(event.target).attr('id') == 'msg_price') {
            $('#msg_price').hide();
        }
    });

    $('#mButton_opt').click(function () {
        var name = $('#name_opt').val();
        var phone = $('#phone_opt').val();
        var email = $('#email_opt').val();
        var company = $('#company_opt').val();
        if (name.length < 1) {
            $('#name_opt').css('border-color', 'red');
            return false;
        }
        if (phone.length < 1) {
            $('#phone_opt').css('border-color', 'red');
            return false;
        }
        if (email.length < 1) {
            $('#email_opt').css('border-color', 'red');
            return false;
        }
        if (!email.match(/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i)) {
            $('#email_opt').css('border-color', 'red');
            return false;
        }
        if (company.length < 1) {
            $('#company_opt').css('border-color', 'red');
            return false;
        }
        jQuery.ajax({
            url: "../../../../pub/email_msgs.php", //Адрес подгружаемой страницы
            type: "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: jQuery("#form_opt").serialize(),
            success: function () {
                $('#mSuccess_opt').show();
                //console.log(jQuery("#form_opt").serialize());
            },
            error: function () { //Если ошибка
                $('#mSuccess_opt').show();
                console.log('0');
            }
        });
    });
    $('#mClose_opt,#mSuccessButton_opt').click(function () {
        $('#msg_opt').hide();
    });
    $('#email_opt_button').click(function () {
        $('#msg_opt').show();
    });

    $('#mButton_price').click(function () {
        var name = $('#name_price').val();
        var phone = $('#phone_price').val();
        var email = $('#email_price').val();
        if (name.length < 1) {
            $('#name_price').css('border-color', 'red');
            return false;
        }
        if (phone.length < 1 && email.length < 1) {
            $('#phone_price').css('border-color', 'red');
            $('#email_price').css('border-color', 'red');
            return false;
        }
        if (email.length > 1) {
            if (!email.match(/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i)) {
                $('#email_price').css('border-color', 'red');
                return false;
            }
        }
        jQuery.ajax({
            url: "../../../../pub/email_msgs.php", //Адрес подгружаемой страницы
            type: "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: jQuery("#form_price").serialize(),
            success: function () {
                $('#mSuccess_price').show();
                //console.log(jQuery("#form_price").serialize());
            },
            error: function () { //Если ошибка
                $('#mSuccess_price').show();
                console.log('0');
            }
        });
    });
    $('#mClose_price,#mSuccessButton_price').click(function () {
        $('#msg_price').hide();
    });
    $('#email_price_button').click(function () {
        $('#msg_price').show();
    });

}

function imageAttrEdit() {
    var images = document.querySelectorAll('img');

    for (var i in images) {
        images[i].title = images[i].title ? images[i].title : images[i].alt;
    }

    //console.log(images);
}