<?php
/**
 * Works with strings and labels resources
 * User: Abratchuk
 * Date: 10.01.12
 * v.1.3
 * 1.1: -
 * 1.2: all checkings and json-replies
 * 1.3: security update
 */

$_GET['debug'] = 'no';
require_once '../component/admin_libs.inc';

if( ! isset($_SERVER["HTTP_X_REQUESTED_WITH"])) die(ERR_LOGIN);

require_once '../component/admin/check_access.inc.php';

    require_once '../lib/smarty/Smarty.class.php';
    require_once '../component/bl/admin/resource_manager.class.php';

if('XMLHttpRequest' == $_SERVER["HTTP_X_REQUESTED_WITH"])
{
    $reply = '';

    switch ($_GET['operation'])
    {
        case 'addnew':

            $resman = new ResourceManager($_GET['l']); //lang
            $issuccess = $resman->add($_GET['id'], $_GET['content'], $_GET['m']);

            if ( ! $issuccess )
            {
                $reply = FLB_SERVERERROR.' : '.$resman->get_error();
            }
            else
            {
                $reply = $resman->get_last_id();
            }


            echo json_encode(array("result"=>$issuccess,"reply"=>$reply, "operation"=>$resman->get_last_operation() ));

            exit();
            break;

        case 'edit':
            //Update
            $resman = new ResourceManager($_GET['l']); //lang
            $issuccess = $resman->Update($_GET['id'], $_GET['content'], $_GET['m']); //id-resKey, content-resVal, m-mode

            if ( ! $issuccess )
            {
                $reply = FLB_SERVERERROR.' : '.$resman->get_error();
            }
            else
            {
                $reply = 'OK';
            }

            echo json_encode(array("result"=>$issuccess,"reply"=>$reply));

            exit();
            break;

        case 'delete':
            //delete
            $resman = new ResourceManager($_GET['l']); //lang
            $issuccess = $resman->Delete($_GET['id'], $_GET['m']); //id-resKey,  m-mode

            if ( ! $issuccess )
            {
                $reply = FLB_SERVERERROR.' : '.$resman->get_error();
            }
            else
                $reply = 'OK';

            echo json_encode(array("result"=>$issuccess,"reply"=>$reply));

            exit();
            break;

    }
}
    else
    {
        //nothing
    }
?>