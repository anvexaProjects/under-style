<?php                                
    
    require_once '../component/admin/check_access.inc.php';
	require_once '../component/admin_libs.inc';
	require '../templates/config/enum.inc';
    
    if (!isset($_GET['id'])) UI::Redirect($_SERVER['HTTP_REFERER']);
	$db = Database::get();
    $id = (int) $_GET['id'];
    $i = $db->getRow("SELECT * FROM articles WHERE struct_id = '$id' LIMIT 1");
    
    $cols = $db->getHash("SHOW COLUMNS FROM art_struct");
	$ins = array();
	// define article-specific fields
	foreach ($cols as $k => $v) {
		if ($k == 'ord') {
			$ins[$k] = $db->getField("SELECT MAX(ord)+10 FROM art_struct WHERE parent_id = '".$i['parent_id']."'");
		} elseif ($k == 'created') {
			$ins[$k] = date('Y-m-d H:i:s');
		} elseif ($k != 'struct_id') {
			$ins[$k] = $i[$k];
		}
	}
	$sid = $db->insert('art_struct', $ins);
	
	$cols = $db->getHash("SHOW COLUMNS FROM art_content");
	foreach ($enum['langs'] as $l => $v) {
		$i = $db->getRow("SELECT * FROM articles WHERE struct_id = '$id' AND lang = '$l'");
		$ins = array();
		foreach ($cols as $k => $v) {
			if ($k == 'struct_id') {
				$ins[$k] = $sid;
			} elseif ($k == 'lang') {
				$ins[$k] = $l;
			} elseif ($k == 'name') {
				$ins[$k] = $i['name'].'_'.date("y-m-d-His");
			} elseif ($k == 'path') {
				$exp = explode('/', $i[$k]);
				$path = '';
				//if (sizeof($exp) > 1) array_pop($exp); // remove the last alias from the path
				if (sizeof($enum['langs']) == 1) {
					if (sizeof($exp) == 1) $path = date("y-m-d-His");
					else {
                        $last_path = array_pop($exp);
                        $path = implode('/', $exp).'/'.$last_path.'_'.date("y-m-d-His");
                    }
				} else {
					array_shift($exp);
					array_pop($exp);
					if (sizeof($exp) == 1 || sizeof($exp) == 0) $path = $l.'/'.date("y-m-d-His");
					else $path = $l.'/'.implode('/', $exp).'/'.date("y-m-d-His");
				}
				$ins[$k] = trim($path, '/');
				$ins['spath'] = $ins['path'];
			} elseif ($k != 'content_id' && $k != 'spath') {
				$ins[$k] = $i[$k];
			}
		}
		$db->insert('art_content', $ins);
	}
	 UI::Redirect('index.php?bid='.$_GET['bid']);
