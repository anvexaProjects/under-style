<?php

header('Cache-Control: no-cache');
header('Expires: Mon, 7 Apr 2008 20:00:00 GMT');

require_once '../component/admin_libs.inc';
require_once '../lib/infrastruct/sitemap.class.php';

if ('XMLHttpRequest' != $_SERVER["HTTP_X_REQUESTED_WITH"])
    die(ERR_UNAUTHORIZED);

session_start();
$tpl = new Smarty();
$db = Database::get();
$html = '';
switch ($_REQUEST['action']) {
    case 'init':
    default:
        // Loads parent level of tree
        $rows = $db->getRows('
                SELECT parent.struct_id, parent.id, parent.name, parent.ord, COUNT(child.id) AS subitem_count
                FROM articles AS parent
                LEFT JOIN articles AS child ON parent.id = child.parent_id AND child.is_deleted = 0
                WHERE parent.is_deleted = 0 AND parent.level = 0 AND parent.is_local_hidden = 0
                GROUP BY parent.id ORDER BY parent.ord ASC, parent.id ASC
    		');
        $tpl->assign('tree', $rows);
        $tpl->display('admin/' . CMS . '/simpletree/tree_init.tpl');
        break;

    case 'list':
        // Loads specified branch
        $maxlevel_sql = '';
        /*if (!VIEW_LAST_LEVEL_SIMPLE_TREE) {
            $maxlevel_sql = 'AND level < '.$db->getField('SELECT MAX(level) FROM articles').' ';
        }*/
        $rows = $db->getRows('
                SELECT parent.struct_id, parent.id, parent.id, parent.name, parent.ord, COUNT(child.id) AS subitem_count
                FROM articles AS parent
                LEFT JOIN articles AS child ON parent.id = child.parent_id AND child.is_deleted = 0
                WHERE parent.parent_id = "' . $_REQUEST['id'] . '" AND parent.is_deleted = 0 ' . $maxlevel_sql . ' AND parent.is_local_hidden = 0
                GROUP BY parent.id ORDER BY parent.ord ASC, parent.id ASC
            ');

        $tpl->assign('tree', $rows);
        $tpl->display('admin/' . CMS . '/simpletree/tree_nodes.tpl');
        break;

    case 'table':
        // Loads specified branch in table view
        if ($_SESSION['role'] == SALERS_REDACTOR) {
            $_REQUEST['id'] = '67ru';
        }
        if ($_SESSION['role'] == DESIGNERS_REDACTOR) {
            $_REQUEST['id'] = '68ru';
        }

        $sql = '
                SELECT parent.struct_id, parent.id, parent.parent_id AS parent_id, parent.name, parent.ord, parent.control, parent.level, parent.path, COUNT(child.id) AS subitem_count, parent.mode, parent.ch_control, parent.is_hidden
                FROM articles AS parent
                LEFT JOIN articles AS child ON parent.id = child.parent_id AND child.is_deleted = 0
                WHERE parent.parent_id = "' . trim($_REQUEST['id']) . '" AND parent.is_deleted = 0 AND parent.is_local_hidden = 0
                GROUP BY parent.id ORDER BY parent.ord ASC, parent.id ASC
            ';

//            print_r($sql);

        $rows = $db->getRows($sql);

        if (isset($_SESSION['role'])) {
            switch ($_SESSION['role']) {
                case SALERS_REDACTOR:
                    $tpl->assign('saler_red', 1);
                    break;
                case DESIGNERS_REDACTOR:
                    $tpl->assign('des_red', 1);
                    break;
            }
        }
        $oSiteMap = SiteMap::singleton();
        $oSiteMap->initStruct($_REQUEST['id'], false);
        $struct = $_GET['global']['rstruct'];

//        echo '<pre>';
//        print_r($struct);
//        echo '</pre>';


        $tpl->assign('tree', $rows);
        $tpl->assign('struct', $struct);



        $template = 'admin/' . CMS . '/simpletree/tree_table.tpl';
        $tpl->display($template);
        break;

    case 'delete':
        // Removes article (within table view)
        $id = $_REQUEST['id'];
        $db->query("UPDATE articles SET is_deleted=1 WHERE id = '$id' OR parent_id = '$id'");
        $db->query("UPDATE articles SET `path`=CONCAT(`path`,'-deleted') WHERE id = '$id' OR parent_id = '$id'");
        deleteChildren($db, $id);
        //if (sizeof($children > 0)) $children = deleteChildren($children));
        break;

    case 'tree_path':
        $tree_ids = '';
        // Returns list of articles ids, started from top (implementd in order opens corresponding braches on tree load)
        if (isset($_REQUEST['tree_id'])) {
            $oSiteMap = SiteMap::singleton();
            $oSiteMap->initStruct($_REQUEST['tree_id'], false);
            $struct = $_GET['global']['rstruct'];

            $tree_ids = array();
            for ($i = 0; $i < sizeof($struct); $i++) {
                // skip 1st level
                if (isset($struct[$i - 1])) $tree_ids[] = $struct[$i]['ord'] . '_' . $struct[$i]['id'];
            }
        } elseif (MAX_LEVEL_SIMPLE_TREE >= 0) {
            $data = $db->getHash('SELECT parent.id, COUNT(child.id) AS subitem_count
                    FROM articles AS parent
                    LEFT JOIN articles AS child ON parent.id = child.parent_id AND child.is_deleted = 0
                    WHERE parent.is_deleted =0  AND parent.level <= ' . MAX_LEVEL_SIMPLE_TREE . ' AND child.is_deleted = 0
                    GROUP BY parent.id
                ');
            asort($data);
            $tree_ids = array_keys($data);
        }
        echo json_encode($tree_ids);
        break;

    case 'move':
        list($source_ord, $source_id) = explode('_', $_GET['source']);

        if (0 == $_GET['destination']) {
            // if level == 1
            $parent_id = 1;
            $init_level = $db->getField('SELECT level FROM articles WHERE is_deleted = 0 AND id = "' . $source_id . '"');
            $destination_level = 1;
            $destination_ord = $source_ord;
        } else {
            list($destination_ord, $destination_id) = explode('_', $_GET['destination']);
            $parent_id = $destination_id;
            $destination_level = $db->getField('SELECT level FROM articles WHERE is_deleted = 0 AND id = "' . $destination_id . '"') + 1;
            $init_level = $db->getField('SELECT level FROM articles WHERE is_deleted = 0 AND id = "' . $source_id . '"');
        }

        $parent_path = $db->getField('SELECT path FROM articles WHERE id = ' . $parent_id);
        $child_path = array_pop(explode('/', $db->getField('SELECT path FROM articles WHERE id = "' . $source_id . '"')));
        $path = trim($parent_path . '/' . $child_path, '/');

        $init_pos = $db->getField('SELECT COUNT(*) FROM articles WHERE is_deleted = 0 AND parent_id = "' . $parent_id . '" AND ord < ' . $destination_ord);
        $destination_pos = $_GET['pos'];
        if (($destination_pos - $init_pos) <= 0) // common elemnts count ++ if the selected element new position is > than initial position
        {
            if ($_GET['pos'] + 1 <= $init_pos) {
                $destination_pos -= 1;
            }
        }

        if (0 != $_GET['pos']) {
            $new_ord = $db->getField('SELECT ord FROM articles WHERE is_deleted = 0 AND parent_id = "' . $parent_id . '" ORDER BY ord LIMIT ' . $destination_pos . ', 1') + 1;
        } else $new_ord = 1;
        $db->query('UPDATE articles SET ord = "' . $new_ord . '", parent_id = "' . $parent_id . '", level = "' . $destination_level . '", path = "' . $path . '" WHERE id = "' . $source_id . '"');
        $db->query('UPDATE articles SET ord = 1 + ord WHERE ord >= "' . $new_ord . '" AND parent_id = "' . $parent_id . '" AND id != "' . $source_id . '"');

        if ($init_level != $destination_level) {
            updateLevel($db, array($source_id), $destination_id);
        }
        break;

    case 'change_order':
        if (ADD_NEW_ALGORITHM_RESORTING) {
            resorting($db, intval($_GET['id']), $_GET['order']);
        }
        $data = $db->getRow('SELECT parent_id, ord FROM art_struct WHERE is_deleted = 0 AND struct_id = "' . intval($_GET['id']) . '"');
        $sql_ord_s = ('up' == $_GET['order']) ? '<' : '>';
        $sql_ord = ('up' == $_GET['order']) ? 'DESC' : 'ASC';
        $new_data = $db->getRow('SELECT struct_id AS id, ord FROM art_struct WHERE is_deleted = 0 AND ord ' . $sql_ord_s . $data['ord'] . ' AND parent_id = "' . $data['parent_id'] . '" ORDER BY ord ' . $sql_ord);
        if (!empty($new_data)) {
            $db->query('UPDATE art_struct SET ord = ' . intval($data['ord']) . ' WHERE struct_id = "' . intval($new_data['id']) . '"');
            $db->query('UPDATE art_struct SET ord = ' . intval($new_data['ord']) . ' WHERE struct_id = "' . intval($_GET['id']) . '"');
        }
        echo $data['parent_id'] . ($data['parent_id'] != '1' ? preg_replace('/(\d+)/', '', $_GET['id']) : '');
        break;
}

// Updates levels and paths
function updateLevel($db, $id, $did)
{
    $level = $db->getField('SELECT level FROM articles WHERE is_deleted = 0 AND id = "' . $id[0] . '"');
    $path = $db->getField('SELECT path FROM articles WHERE is_deleted = 0 AND id = "' . $id[0] . '"');
    echo $path . "\r\n";
    $ids = $db->getHash('SELECT id, parent_id FROM articles WHERE is_deleted = 0 AND parent_id IN(' . implode(',', $id) . ')');
    $ids = array_keys($ids);
    if (!empty($ids)) {
        $db->query('
            	UPDATE articles
            		SET level = ' . ($level + 1) . ',
            		path = CONCAT("' . $path . '", "/", REVERSE(SUBSTRING(REVERSE(path), 1, INSTR(REVERSE(path), "/") - 1)))
            	WHERE id IN(' . implode(',', $ids) . ')');
        updateLevel($db, $ids, $ids);
    }
}

function deleteChildren($db, $parent_ids)
{
    $children = $db->getHash("SELECT id, NULL FROM articles WHERE parent_id IN ('$parent_ids')");
    $db->query("UPDATE articles SET is_deleted=1 WHERE id IN('$parent_ids') OR parent_id IN('$parent_ids')");
    $children = array_keys($children);
    if (sizeof($children) > 0) {
        $children = deleteChildren($db, implode(',', $children));
    }
}

function resorting($db, $id = 0, $order)
{
    $rows = $db->getRows('SELECT ord, struct_id FROM art_struct WHERE parent_id IN (SELECT parent_id FROM art_struct WHERE struct_id = "' . $id . '") ORDER BY ord ASC, struct_id ASC');
    $update = "";
    if ($rows) {
        $key_ord = ($order == "up") ? -1 : 1;
        foreach ($rows as $k => $v) {
            $db->query("UPDATE art_struct SET ord = '" . (10 * ($k + 1)) . "' WHERE struct_id = '" . $v['struct_id'] . "';");
        }
    }
}

?>
