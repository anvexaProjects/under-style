<?php

$_GET['debug'] = '';

require_once '../component/admin_libs.inc'; 
require_once '../component/admin/check_access.inc.php'; 
require_once '../component/bl/subscription/subscription_contact.class.php';

set_time_limit(360);
ini_set('memory_limit', '256M');

$obj = new SubscriptionContact();
list($items, $n) = $obj->getList();

$csv = "";
foreach ($items as $i) {
    $csv .= $i['email'].';'.$i['fname'].';'.$i['lname'].';'.$i['group_id'].';'.$i['group_name']."\n";
}

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Length: " . strlen($csv));
header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=subscription_contacts.csv");	
//header('Content-type: application/octet-stream');
echo $csv;
exit;


?>