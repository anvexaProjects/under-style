<?php

if('XMLHttpRequest' != $_SERVER["HTTP_X_REQUESTED_WITH"]) die(ERR_UNAUTHORIZED);

require_once '../component/admin/check_access.inc.php';
require_once '../component/admin_libs.inc';
require_once '../component/bl/entity/dictionary.class.php';

extract($_REQUEST);

$obj = new Dictionary();

if (isset($action))
{
    switch ($action) {
        case 'delete':
            if (!isset($id) || !$id) echo 0;
            else {
                $rs = $obj->delete((int) $id);
                echo ($rs ? 1 : 0);
            }
        break;

        case 'update':
            if (!isset($id) || !$id) echo 0;
            else {
                $obj->setData($_REQUEST);
                $obj->sId = 'id';
                $rs = $obj->update();
                echo ($rs ? 1 : 0);
            }
        break;

        case 'add':
            $obj->setData($_REQUEST);
            $rs = $obj->insert();
            echo ($rs ? 1 : 0);
        break;
    }
}



?>
