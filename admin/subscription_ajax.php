<?php

require_once '../component/admin_libs.inc'; 
require_once '../component/admin/check_access.inc.php'; 
require '../templates/config/enum.inc';
require_once '../component/bl/subscription/subscription_contact.class.php';
require_once '../component/bl/subscription/subscription_contact_group.class.php';
require_once '../component/bl/subscription/subscription_template.class.php';

$obj_c = new SubscriptionContact(); 
$obj_c->sId = 'id';
$obj_g = new SubscriptionContactGroup();
$obj_g->sId = 'id';
$obj_t = new SubscriptionTemplate();
$obj_t->sId = 'id';

$id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : 0;

$rs = array('e' => '', 'data' => array());

if (isset($_REQUEST['contact_update'])) $rs = updateContact($obj_c, $id, $enum['bool'], $obj_g->getHashIdName());
if (isset($_REQUEST['contact_delete'])) $rs = deleteContact($obj_c, $id, $enum['bool']);

if (isset($_REQUEST['tpl_delete'])) $rs = deleteTemplate($obj_t, $id);

if (isset($_REQUEST['contact_group_update'])) $rs = updateContactGroup($obj_g, $id);
if (isset($_REQUEST['contact_group_delete'])) $rs = deleteContactGroup($obj_g, $id);


echo json_encode($rs);

function deleteContact($obj, $id, $bool) {
    $obj->delete($id);   
    return array('e' => '', 'data' => array());
}

function deleteTemplate($obj, $id) {
	$data = array();
	$data['is_deleted'] = 1;
	$data['id'] = $id;
	$obj->setData($data);
    $obj->update();   
    return array('e' => '', 'data' => array());
}

function updateContact($obj, $id, $bool, $groups) {
    $errors = array();
    $data = array();
	$contact_id = $obj->getContactIdByMapId($id);
	
    if (!isset($_REQUEST['email']) || !Val::IsEmail($_REQUEST['email'])) $errors[] = FLB_INVALID_EMAIL;
	elseif ($obj->isContactInGroupExists($_REQUEST['email'], $_REQUEST['group']) && $_REQUEST['group'] != $obj->GetGroupByMapId($_REQUEST['group_map_id'])) $errors[] = sprintf(ERR_USER_GROUP_ALREADY_REGISTERED, $_REQUEST['email']);
    else { 
        $data['fname'] = $_REQUEST['fname'];
        $data['lname'] = $_REQUEST['lname'];
        $data['email'] = $_REQUEST['email'];
        $data['is_active'] = $_REQUEST['is_active'];
        $data['is_test'] = $_REQUEST['is_test'];    
		$data['id'] = $id;
//		$data['id'] = $obj->getContactIdByMapId($id);
        $obj->setData($data);
        $obj->update(); 
        unset($data['id']);
		$obj->updateContactGroup($_REQUEST['group_map_id'], $_REQUEST['group']);
		
		
    }
	$is_active = $data['is_active'];
    $data['is_active'] = $bool[$data['is_active']];
    $data['is_test'] = $bool[$data['is_test']];
	$data['group'] = $groups[$_REQUEST["group"]];
    return array('e' => implode("\n", $errors), 'data' => $data, 'is_active' => $is_active);
}

function updateContactGroup($obj, $id) {
    
    $errors = array();
    $data = array();

    if (!isset($_REQUEST['name']) || Val::IsEmpty($_REQUEST['name'])) $errors[] = sprintf(FLB_REQUIRED, S_NAME);
    else {
        $data['name'] = $_REQUEST['name'];
        $data['id'] = $id;
        $obj->setData($data);
        $obj->update(); 
        unset($data['id']);
    }
    return array('e' => implode("\n", $errors), 'data' => $data, 'is_active' => $is_active);
}

function deleteContactGroup($obj, $id) {
    $obj->delete($id);   
    return array('e' => '', 'data' => array());
}


?>