<?php
	header('Cache-Control: no-cache');
	header('Expires: Mon, 7 Apr 2008 20:00:00 GMT');

	require_once '../component/admin_libs.inc';
    require_once '../component/bl/admin/imager.class.php';

	if('XMLHttpRequest' != $_SERVER["HTTP_X_REQUESTED_WITH"])
      die(ERR_UNAUTHORIZED);

    $aid = $_REQUEST['art_id'];
    $obj = new Imager();
    $tpl = new Smarty();
	$art_config = ArtConfig::getUploadConfig($_REQUEST['control']);
	$cfg = $art_config[0];

	
    switch ($_REQUEST['action']) {
        case 'init':
            $rs = $obj->getList($aid);
            $tpl->assign('items', $rs);
            $tpl->assign('path', $cfg[0]['dir']);
            echo $tpl->fetch('admin/'.CMS.'/edit/tab_multiupload_images.tpl');
        break;

        case 'order':
            $image_ids = explode('|', $_REQUEST['ids']);
            $obj->reorderImages($image_ids);
        break;

        case 'dupdate':
            echo $obj->updateDescription($_REQUEST['id'], $_REQUEST['value_desc'], $_REQUEST['value_title']);
        break;
    
        case 'delete':
            $image_ids = explode('|', $_REQUEST['ids']);
            $obj->deleteImages($image_ids, $_REQUEST['control']);
        break;

    }
    
?>
