<?php
    require_once '../component/admin/check_access.inc.php';
	require_once '../component/admin_libs.inc';

	$oDb = Database::get();

    $IsNoError = true;

//  delete article
    if (isset($_GET['id']))
	  if(preg_match("/^((\d+)(\w+))/",$_GET['id']) && strlen($_GET['id'])<10)
			$IsNoError = RemoveChilds($_GET['id'], $oDb);

    if (isset($_POST['delete_art']) && isset($_POST['article_id']))
    {
        foreach (array_keys($_POST['article_id']) as $id) 
        {
            $IsNoError = RemoveChilds($id, $oDb);
            if (!$IsNoError) break;
        }
    }
    

    if ($IsNoError)
        if (isset($_GET['bur']))
			 UI::Redirect($_GET['bur']);
        elseif (strlen(@$_SERVER['HTTP_REFERER']) > 0)
               UI::Redirect($_SERVER['HTTP_REFERER']);
        else
             echo '<script>history.back();</script>';

	function RemoveChilds($parent_id, $oDb)
	{
		$sql_extra_cond = "(-1) ";
		$query_upd = "UPDATE articles SET is_deleted = 1 WHERE id = '$parent_id' AND '$parent_id' NOT IN $sql_extra_cond ";
		$query_select = "SELECT id FROM articles WHERE is_deleted = 0  AND parent_id = '$parent_id' ";

		 if ( !$oDb->query($query_upd) )
         {
			   ShowError();
			   return false;
         }
		 else
		 {
		 
		 	$Childs = $oDb->getRows($query_select);
			
			// check error
			if (ERROR == $Childs)
			{
				   ShowError();
				   return false;
			}
			else
			{
					//check child count
					if (count($Childs) > 0)
					{
						foreach ($Childs as $art)
							RemoveChilds($art['id'], $oDb);
					}
					else
					{
						 return true; // return back if no childs
					}
			}
		 }
		
		return true;
	}
	
	function ShowError()
	{
         $tpl = new Smarty;
         $tpl->assign('page', 'content');
         $tpl->assign('param', UI::GetBlockError(ERR_DEL_ARTICLE));
         $tpl->display('admin/acommon.tpl');
	}
	
	

?>