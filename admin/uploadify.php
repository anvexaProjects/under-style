<?php

require_once '../component/admin_libs.inc';
require_once '../component/bl/admin/imager.class.php';
require_once '../component/bl/imageresizer.class.php';
require '../templates/config/enum.inc';


$art_config = ArtConfig::getUploadConfig($_REQUEST['control']);
$cfg = $art_config[0];

if (!empty($_FILES)) {
    $fpath = SITE_ABS.$cfg[0]['dir'];
    $fname = $_FILES['Filedata']['name'];
   
    if ($cfg[0]['tr'] == false) {
        $fname = substr(md5(uniqid(true)), 0, 14) . '.' . pathinfo($fname, PATHINFO_EXTENSION);
    } else {
        $fname = Util::translit($fname, array('tolower' => true, 'symbols' => '\.'));
        $fname = Util::getNonExistentFilename($fpath.'/'.$fname);
        $fname = basename($fname); 
    }
    Util::UploadFile('Filedata', $fpath, $fname);

	$obj = new Imager();
    $data = array('art_id' => $_REQUEST['id'], 'image_name' => $fname, 'image_hash' => $_REQUEST['image_hash']);
    $obj->insert($data);
    //$obj->setData(array('art_id' => $_REQUEST['id'], 'image_name' => $fname));
    //$obj->insert();

    foreach ($cfg as $c) {
        if($c['m'] == 'ar') {
            ImageResizer::ResizeImgAsymetric(
                $fpath.'/'.$fname, SITE_ABS.$c['dir'].'/'.$fname,
                $c['w'], $c['h'],
                'auto', 0xFFFFFF, 90, true
            );
        }
        elseif($c['m'] == 'sr') {
            ImageResizer::ResizeImgSymetric(
                $fpath.'/'.$fname, SITE_ABS.$c['dir'].'/'.$fname,
                $c['w'], $c['h'],
                0xFFFFFF, 90, true
            );
        }
        elseif ($c['m'] == 'rc')
            ImageResizer::cutAndResizeImage(
                $fpath.'/'.$fname, SITE_ABS.$c['dir'].'/'.$fname,
                $c['w'], $c['h'],
                0xFFFFFF, 90, false
            );
    }
	// adding watermarks
	if (WATERMARK_ENABLE)
	{
		foreach ($cfg as $item)
		{
			$imagePath = SITE_ABS.$item['dir'].'/'.$fname;
			list($width, $height) = getimagesize($imagePath);
			$watermarkPath = SITE_ABS.sprintf(WATERMARK_PATH, Util::getValueFromRange($enum['watermarks'], $width));
			Util::addWatermark($imagePath, $watermarkPath);
		}
	}
    echo '1';
}
?>