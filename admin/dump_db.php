﻿<?php

/*
  Description: MySQL database dump
  Autor:       Oleg Koshkin
  Data:        14-07-2006
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2006 Oleg Koshkin
  
*/
   require_once '../core/settings.inc';
   require_once '../lib/zip_create.inc.php';

    error_reporting(E_ALL & ~E_WARNING);
    ini_set("display_errors", "On");
	set_time_limit(180);

	$is_allow_zip	= true; //разрешить архивирование
    $is_full_insert	= false; //полная вставка

    $sTable = (isset($_GET['table'])) ?  $_GET['table'].'_' : '';
    $itemName = 'data_'.$sTable.date("Y-m-d_H.i.s");
    $fileName = '../upload/sql/'.$itemName;

    $tableList = getTableList();
    $output = getHeader();


    //extract dump
    foreach  ($tableList as $table)
    {
        $output .= getStructCaption($table);
        $output .= getDefinition($table);
        $output .= getDataCaption($table);
        $output .= getData($table,  $is_full_insert);
        $output .= getFinalLine();
    }
    //encode
    //$output = iconv('windows-1251', 'utf-8', $output);
	
	//save dump to file
	if($fout = fopen($fileName . ($is_allow_zip ? '.zip' : '.sql'), "wb"))
	{
		if($is_allow_zip)
		{
			// arhive
			$createZip = new createZip;  
			$createZip -> addFile($output, "$itemName" . '.sql');  
			unset($output);
			fwrite($fout, $createZip -> getZippedfile());
		}	
		else
		{
			fwrite($fout, $output);
			unset($output);
		}
		
		fclose($fout);
		unset($fout);
		echo 'Database sucessfully backed up';
	} 
	else
	{
		echo 'Error ocuured while creating backup';	
	}
	

     
function connect_db()
{
    $db = mysql_connect(DBHOST, DBUSER, DBPASSWD) or die("Can't connect database");
    mysql_select_db(MAINDB,$db) or die("Can't select database");
	mysql_query("SET NAMES 'utf8' ");
    return $db;
}


// GET A LIST OF TABLES IN DATABASE '$database' EXEPT THE TABELS LISTED IN '$NOT'
function getTableList()
{
	$tabel_array = array();
	if (isset($_GET['table']))
	{
		$tabel_array[] = $_GET['table'];
		return $tabel_array;
	}
	
    $database = MAINDB;
    
    $db                = connect_db();
    $result            = mysql_list_tables($database, $db);
    $tabel_index    = 0;
    $max            = mysql_num_rows($result);
	$NotTable		= array('articles');
	  

    for($x=0; $x < $max ; $x++) 
    {
        $tabel = mysql_tablename($result,$x);
                        
	    // If the table name isn't empty and the table is not in '$NOT' than the table can be out in the list of tables
        if ($tabel <> '' AND !@in_array($tabel, $NotTable))     
        {
            $tabel_array[] = mysql_tablename($result,$x);
        }
    }
    
    return $tabel_array;
}



/*
    RETURNS THE DEFENITION-PART (the field-names, type of the fields, length of the fields, etc.) OF THE TABLE
    '$tabel'
*/
function getDefinition($tabel)
{
    global $DefenitieTekst;
        
    $db = connect_db();
    
    $def  = "";
    $def .= $DefenitieTekst;
    $def .= "\n";
    $def .= "DROP TABLE IF EXISTS $tabel;\n";
    $def .= "CREATE TABLE $tabel (\n";
        
    $result = mysql_query("SHOW FIELDS FROM $tabel", $db);
       
    
    while($row = mysql_fetch_array($result))
    {
        $def .= "   $row[Field] $row[Type]";
        
        if ($row["Default"] != "")
        {
			if ('CURRENT_TIMESTAMP' == $row["Default"] )
				   $def .= " DEFAULT CURRENT_TIMESTAMP ";
			else 
            $def .= " DEFAULT '$row[Default]'";

        }
        
        if ($row["Null"] != "YES")
        {
            $def .= " NOT NULL";
        }
        
        if ($row['Extra'] != "")
        {
            $def .= ' ' . $row['Extra'];
        }
        
            $def .= ",\n";
        }
        
        $def = ereg_replace(",\n$","", $def);

    
    $qkey =  mysql_query("SHOW INDEX FROM $tabel", $db);

    /* retrieve the key info if it exists and use it */
    $knames = array();
    if($rkey = @mysql_fetch_array($qkey))
    {
        do
        {
            /* add the key info to the arrays */
            $keys[$rkey["Key_name"]]["nonunique"] = $rkey["Non_unique"];
            if(!$rkey["Sub_part"])
            {
                $keys[$rkey["Key_name"]]["order"][$rkey["Seq_in_index"]-1] = $rkey["Column_name"];
            }
            else
            {
                $keys[$rkey["Key_name"]]["order"][$rkey["Seq_in_index"]-1] = $rkey["Column_name"]."(".$rkey["Sub_part"].")";
            }
            
            if(!in_array($rkey["Key_name"], $knames))
            {
                $knames[] = $rkey["Key_name"];
            }
            
        }
        while($rkey = @mysql_fetch_array($qkey));

        /* add the key information to the $creatinfo creation variable */
        for($kl=0; $kl<sizeof($knames); $kl++)
        {
            if($knames[$kl] == "PRIMARY")
            {
                $def .=",\n   PRIMARY KEY";
            }
            else
            {
                if($keys[$knames[$kl]]["nonunique"] == "0")
                {
                    $def .= ",\n   UNIQUE ". $knames[$kl];
                }
                else
                {
                    $def .= ",\n   KEY ". $knames[$kl];
                }
            }
            
            $temp = @implode(",", $keys[$knames[$kl]]["order"]);
            $def .= " (". $temp .")";
        }                
    }
    
    $def .= "\n);";
    
    $def .= "\n";
    $def .= "\n";
        
    return $def;       
}


// RETURNS THE DATA-PART (the field-names and field values) OF THE TABLE '$tabel'
function getData($tabel,  $is_full_insert)
{
    $db = connect_db();
        
    if ($tabel > "")
    {
        $result =  mysql_query("SELECT * FROM $tabel", $db);
        $aantal_rij = mysql_num_rows ($result);
        $aantal_kolom = mysql_num_fields ($result);
        
        $data ="\n";
        
        for ($i=0; $i < $aantal_rij; $i++)
        {
            $myrow = mysql_fetch_array($result);
                                
            $data .="INSERT INTO $tabel";
            
            if ($is_full_insert)
            {
                $data .= "(";
                            
                for ($a = 0; $a < $aantal_kolom; $a++)
                {
                     $veldnaam = mysql_field_name($result, $a);
                     
                     if($a == ($aantal_kolom - 1))
                     {
                         $data.= $veldnaam;
                     }
                     else 
                     {
                         $data.= $veldnaam.",";
                     }
                }
                
                $data .=")";
            }
            
            $data .= " VALUES (";
            
            for ($k=0; $k < $aantal_kolom; $k++)
            {
                if($k == ($aantal_kolom - 1))
                {
                    $data.="'".addslashes($myrow[$k])."'";
                }
                else
                {
                    $data.="'".addslashes($myrow[$k])."',";
                }
            }
            
            $data.= ");\n"; 
        }
        
        $data.= "\n";
        
    }
    else
    {
        $data = "Error";
    }
        
    return $data;
}

function getStructCaption($table)
{
    $data = "\n\n";
    $data .= "#\n";
    $data .= "# Struct of `$table` table \n";
    $data .= "#";
    return $data;
}


function getDataCaption($table)
{
    $data = "\n";
    $data .= "#\n";
    $data .= "# Dump of `$table` table \n";
    $data .= "#";
    return $data;
}

function getFinalLine()
{
    return '# --------------------------------------------------------' . "\n\n";
}


function getHeader()
{
    $data      = "#\n";
    $data      .= "# Generated by : Diamond Framework export script\n";
    $data     .= "# Host            : ". DBHOST ."\n";
    $data     .= "# Generation time : ". date ("d M Y \o\m H:i:s",time()). "\n";
    $data     .= "# Server version  : ". php_uname() ."\n";
    $data     .= "# PHP Version     : ". phpversion() ."\n";
    $data     .= "# Database        : ". MAINDB ."\n";
    $data     .= "# Script owner    : ". get_current_user() ."\n";
    $data     .= getFinalLine();
    return $data;
}

?>
