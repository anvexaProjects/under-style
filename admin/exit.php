<?php                    
/*
  Description: Exit from admin zone
  Autor:       Oleg Koshkin
  Data:        14-05-2005
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
*/

    session_start();

    require_once '../core/settings.inc';
    require_once '../lib/smarty/Smarty.class.php';

     
    // clear smarty cache
    $smarty = new Smarty;
    $smarty->caching = true;
    $smarty->clear_all_cache();


    $_SESSION=array();
    session_unset();
    session_destroy();
	setcookie('login', '', time() - COOKIE_TIME, '/');
	setcookie('pass', '', time() - COOKIE_TIME, '/');
	$_COOKIE['pass'] = '';
    header('Location: ../');
    exit;
?>
