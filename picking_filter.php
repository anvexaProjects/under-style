<?

include_once 'core/settings.inc';

$template = sprintf("mysql:host=localhost;dbname=%s;", MAINDB);
$dbh = new PDO($template, DBUSER, DBPASSWD);

$category = $_GET['category_filter'];
$vendor = $_GET['vendor_filter'];
$group = $_GET['group_filter'];

$whereBrand = $vendor ? " AND t1.psevdonim ='$vendor' " : '';
$whereCategory = $category ? " WHERE t1.struct_id ='$category' " : '';
$whereGroup = $group ? "AND t1.sprop4 IN (
  	SELECT DISTINCT t1.name
  	FROM category_psevdonims AS t1
  	WHERE t1.psevdonim = '$group'
 	)" : '';

$mysqlRequest = "
    SELECT t1.sprop13 AS col_title, cp.psevdonim AS col_name
    FROM art_struct AS t1
    LEFT JOIN collection_psevdonims AS cp ON cp.name = t1.sprop13
    WHERE t1.parent_id IN (
    SELECT t2.struct_id
    FROM art_struct AS t1
    LEFT JOIN art_struct AS t2 ON t2.parent_id = t1.struct_id AND t2.is_deleted = 0
    
    $whereCategory

    ) 

    $whereGroup

    AND t1.sprop13 != ''
	AND t1.is_deleted = '0'
 	AND t1.is_hidden = '0'
 	AND sprop1 != '1'

    $whereBrand

    GROUP BY t1.sprop13";

// print_r($mysqlRequest);


$sth = $dbh->prepare($mysqlRequest);

$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);



// print_r($result);


$category_json = json_encode(array_values($result));


echo $category_json;

