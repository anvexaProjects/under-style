<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    require_once('lib/email.php');

	if (isset($_POST['action']) && $_POST['action'] == "callme") {
		#$toEmail 	= "sergej-123@yandex.ru";
		$toEmail 	= "zapros@under-style.ru";	
		
		$headers  	= 'MIME-Version: 1.0' . "\r\n";
		$headers 	.= 'Content-type: text/html; charset=utf-8' . "\r\n";

		$headers 	.= 'To: '.$toEmail . "\r\n";
		$headers 	.= 'From: ' .$toEmail. "\r\n";
		
		//$subject	= "Сообщение с сайта";
		$subject	= substr($_POST['text'], 0, 147);

		$message 	= "Имя: {$_POST['name']}<BR/>";
		$message   .= "Телефон: {$_POST['phone']}<BR/>";
		$message   .= "Время звонка: {$_POST['time']}<BR/>";
		$message   .= "Сообщение: {$_POST['text']}";

		Email::$addAddress = "zapros@under-style.ru";
        $ok = Email::send($toEmail, $_POST['name'], 'Новый заказ звонка', $message);
		
//		$ok = mail($toEmail, $subject, $message, $headers);
		$result = array("status" => "ok");
	}
	else if (isset($_POST['action']) && $_POST['action'] == "subscribe") {
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
			$result = array("status" => "fail", "error" => "Email введен некорректно");
		else {
			// Создаём POST-запрос
//			$POST = array (
//				'api_key' => "57dx4pfimufjs8ez5fiwtyipurgdifbyxeby86xe",
//				'double_optin' => 0,
//				'list_ids' => "2635964,2635979,2635984",
//				'fields[email]' => $_POST['email'],
//				'fields[Name]' => $_POST['name'],
////				'request_ip' => $_SERVER['REMOTE_ADDR']
//				'request_ip' => $_SERVER['REMOTE_ADDR']
//			);
//
//			// Устанавливаем соединение
//			$ch = curl_init();
//			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//			curl_setopt($ch, CURLOPT_POST, 1);
//			curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
//			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
//			curl_setopt($ch, CURLOPT_URL, 'http://api.unisender.com/ru/api/subscribe?format=json');
//			$result = curl_exec($ch);

			Email::$addAddress = "zapros@under-style.ru";
            $result = Email::send($_POST['email'], $_POST['name'], 'Новый Подписчик', "Email: {$_POST['email']} \n\rИмя: {$_POST['name']}");

			if ($result) {
				$jsonObj = json_decode($result);

				if(null===$jsonObj) {
					$result = array("status" => "fail", "error" => "Произошла ошибка. Попробуйте еще раз");
				}
				elseif(!empty($jsonObj->error)) {
					$result = array("status" => "fail", "error" => "Произошла ошибка: ".$jsonObj->error);
				} else {
					$result = array("status" => "ok");
				}

			} else {
				$result = array("status" => "fail", "error" => "Произошла ошибка. Попробуйте еще раз");
			}
		}
	}
	else
		$result = array("status" => "fail", "error" => "Недопустимая операция");
		
	echo json_encode($result);
	
?>