<?php
    require_once '../lib/smarty/Smarty.class.php';
    $path_abs = SITE_ABS.'/upload/video/';
    $path_res = SITE_REL.'/upload/video/';

    if (is_file($path_abs.$params['i1']))
        $img = $path_res.$params['i1'];
    elseif (is_file($path_abs.$params['i2']))
        $img = $path_res.$params['i2'];
    else 
        $img = $path_res.'noimg.gif';
    echo $img;
?>