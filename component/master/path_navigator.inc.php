<?php
/*
  Description: Top menu renderer
  Autor:       Oleg Koshkin
  Data:        08-05-2007
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
*/

require_once '../component/libs.inc';


    $tpl = new Smarty;  
    $template =  URL::GetTDir() . '/path_navigator.tpl';
	
	$struct = SiteMap::GetStruct();
	$title = $struct[1];
	

	if (is_array($struct))
	{
		$rstruct = is_array($struct) ? array_reverse($struct) : array();
		$tpl->assign('rstruct', $rstruct); 

		array_shift($struct);
		$tree =  is_array($struct) ? array_reverse($struct) : array();
	}


	
    $tpl->assign('title', $title);
    $tpl->assign('items', $tree);
   
	
    $tpl->display($template);
?>
