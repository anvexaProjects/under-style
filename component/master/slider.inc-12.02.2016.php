<?php
include ("../slider/functions.php");
$sliders = getTree();

?>
<link rel="stylesheet" href="../component/slider/slider.css" type="text/css" />

<script type="text/javascript" src="../component/slider/tabs.js"></script>
<script type="text/javascript" src="../component/slider/slider.js"></script>
<script type="text/javascript" src="../component/slider/init.js"></script>


	<div id="tabsholder" class="tabsholder clearfix">

        <ul class="tabs">
            <?php foreach ($sliders as $tab => $cat) { ?>
            <li id="tab<?php echo $tab ?>"><?php echo $cat['cat_name'] ?></li>
            <?php } ?>
        </ul>
        <div class="contents marginbot">
			<?php foreach ($sliders as $tab => $cat) { ?>
            <div id="tabcontent<?php echo $tab ?>" class="tabclass">
            
            <div class="slider">
				<div class="sliderContent">
				<?php foreach ($cat['sliders'] as $i => $slider) { ?>
                    <div class="item">
                    <?php //echo '<pre>'; print_r($slider); echo '</pre>'; ?>
                    <?php //if($slider['logo'] != '' || $slider['description'] != '') { ?>
                    <div class="item_desc">
                    	<?php if($slider['logo'] != '') { ?>
                        <img class="slider_logo" src="../slider/<?php echo $slider['logo'] ?>" alt="" />
						<?php } else { ?>
                        <br />
						<?php } ?>
                        <?php if($slider['description'] != '') { ?>
						<?php echo $slider['description'] ?>
						<?php } else { ?>
                        <div style="height:42px;"></div>
                        <?php } ?>
                        <?php if($slider['ankor'] != '') { ?>
                        <a class="slider_more" href="<?php echo $slider['ankor'] ?>">Подробнее</a>
                        <?php } ?>
                    </div>
                    <?php //} ?>
					
                    <div class="item_img">
					<?php if($slider['ankor'] != '') { ?>
                    <a href="<?php echo $slider['ankor'] ?>">
                    <?php } ?>
                    <img src="../slider/<?php echo $slider['image'] ?>" alt="" />
                    <?php if($slider['ankor'] != '') { ?>
                    </a>
                    <?php } ?>
                    </div>
                    
                    <div class="clearfix"></div>
                    </div>
                <?php } ?>
                </div>
            </div>
            
            </div>
            <?php } ?>
		</div>
            
    </div>
	<div class="clearfix">&nbsp;</div>






