<?php

/*
  Description: Catalog renderer
  Autor:       Oleg Koshkin
  Data:        16-06-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
 */

require_once '../component/libs.inc';

$tpl = & new Smarty;
$tpl->caching = ENABLE_CACHING;
$template = $params['tpl'];
$key = 'sitemap|menu_main|' . $_GET['path'];

if (!$tpl->is_cached($template, $key)) {
    $lang = $_GET['global']['lan'];
    $cond = isset($params['cond']) ? $params['cond'] : '1';
    // initialize
    $l1 = array();
    $l2 = array();
    $l3 = array();

    $oDb = Database::get();

    $Query = "SELECT id, path, parent_id, level, name, control, ord, spath
			   FROM articles WHERE is_deleted = 0 AND lang = '$lang' AND control NOT IN('category', 'project', 'projitem')  ORDER BY ord";
    $aRows = $oDb->getRows($Query);

    foreach ($aRows as $row) {
        if ($row['level'] == 2)
            array_push($l1, $row);
        else if ($row['level'] == 3)
            array_push($l2, $row);
    }

    $items = SiteMap::getTree2($l1, $l2, $l3);

    $l1 = array();
    $l2 = array();
    $l3 = array();

    $sql = "SELECT id, path, parent_id, level, name, control, ord, spath
			   FROM articles WHERE is_deleted = 0 AND lang = '$lang' AND control IN('category', 'project', 'projitem')  ORDER BY ord";
    $aRows = $oDb->getRows($sql);
    foreach ($aRows as $row) {
        if ($row['level'] == 3)
            array_push($l1, $row);
        else if ($row['level'] == 4)
            array_push($l2, $row);
        else if ($row['level'] == 5)
            array_push($l3, $row);

    }
    $pt_items = SiteMap::getTree3($l1, $l2, $l3);
    foreach ($items as $k => $v)
    {
        if ($v['control'] == 'portfolio')
        {
            $items[$k]['items'] = $pt_items;
        }
    }
    $tpl->assign('items', $items);
}
$tpl->display($template, $key);
?>
