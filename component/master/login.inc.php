<?php


require_once '../component/bl/notification.class.php';
require_once '../component/bl/login.class.php';

$message = '';
$is_admin = false;
$roles = array();
$paths = array('login');

if (!$_GET['admin_zone'])
{
    require_once '../component/libs.inc';
    $template = 'content/user_login.tpl';
    $is_admin = false;
    $roles = array(USER, ADMIN);
}
else
{
    require_once '../component/admin_libs.inc';
    $template = 'admin/'.CMS.'/login.tpl';
    $is_admin = true;
    $roles = array(ADMIN, SALERS_REDACTOR, DESIGNERS_REDACTOR);
}

$obj = new Login();
// login
if  (isset($_POST['submit_login']))
{
     $obj->doLogin($_POST['login'], $_POST['pass'], $roles);
     if (!$obj->e)
     {
         if ($obj->role == ADMIN || $obj->role == SALERS_REDACTOR || $obj->role == DESIGNERS_REDACTOR)
             UI::Redirect( SITE_REL . '/admin/index.php');

     }
}
/*
// restore pass
if  (isset($_POST['submit_restore']))
{
     $message = restorePass();
}
*/
$tpl = new Smarty;
$tpl->assign('error_login', $obj->e);
$tpl->display($template);

/*function restorePass()
{
    if (Val::IsEmpty($_POST['login']) || 'admin' == $_POST['login'])
        return ERR_ADD_LOGIN;

    $oDb = Database::get();
    $login = strtoupper(Val::ToDb($_POST['login']));

    $sSqlUser = "SELECT * FROM users WHERE UPPER(login) = '$login' ";
    $aUser = $oDb->getRows($sSqlUser);
    if(1 != count($aUser))
        return ERR_NO_USER_FOUND;

    // set new password
    srand( (double) microtime()*1000000);
    $sNewPass = substr(md5(uniqid(rand(),1)), 0, 10);
    $sNewPassMD5 = md5(SALT . $sNewPass . SALT);
    $sSqlNewPass = "UPDATE users SET pass = '$sNewPassMD5' WHERE UPPER(login) = '$login' ";

    if(!$oDb->query($sSqlNewPass))
        return ERR_WHILE_RESTORE;

    // mail new pass
    $oTplMsg = new Smarty;
    $oTplMsg->assign('user', $aUser[0]);
    $oTplMsg->assign('pass', $sNewPass);
    $strMessage = $oTplMsg->fetch('notifications/restore.tpl');

    if (!Notification::SendBody($aUser[0]['email'], SUBJ_RESTORE_PASS, $strMessage))
         return WRN_MAIL_ERR;


    echo UI::GetBlockWarning(LBL_PASS_RESTORED);
}*/

?>
