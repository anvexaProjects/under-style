<?php
/*
  Description: Articles
  Autor:       Oleg Koshkin
  Data:        24-09-2008
  Version:     1.1

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/

require_once '../component/libs.inc';


    $tpl = new Smarty;  
    $tpl->caching = ENABLE_CACHING;
    $template = URL::GetTDir() . '/l_menu.tpl';
	
	$oSiteMap = SiteMap::singleton();
    $rstruct = $oSiteMap->GetRStruct();
	$isCurrUsrAuthorized = ($_SESSION['role'] > 0);
    $key = 'art|'.URL::GetPath().'|'.$isCurrUsrAuthorized;

    if  (!$tpl->is_cached($template, $key))
    { 
		$oDb = Database::get();

	    $sRoot = $rstruct[1]['path'];
		$sAuthSql = '';
		$sSql = "SELECT  id, parent_id, level, control, name, ord,path  FROM articles
		 WHERE is_deleted = 0 $sAuthSql AND is_hidden = 0 AND ((level=1 AND path='$sRoot') OR level>1)  ORDER BY ord, id";

        $items = $oDb->getRows($sSql);
   		$level_1 = array();
		$level_2 = array();
		$level_3 = array();
		


  		foreach($items as $one_item)
		{
			if ($one_item['level'] == 1)  array_push($level_1, $one_item);
			if ($one_item['level'] == 2)  array_push($level_2, $one_item);
			if ($one_item['level'] == 3)  array_push($level_3, $one_item);
		}
		

        $tpl->assign('tree', SiteMap::getTree3($level_1, $level_2, $level_3));
    }
    $tpl->display($template, $key);
	
?>

