<?php
       
require_once '../component/libs.inc';
require_once '../component/bl/cart.class.php';

    if (isset($_GET['add'])) {
        Cart::AddToCart($_GET['id'], $_GET['add'], $_GET['l']);
        echo 'The product is added';
    }
    
    elseif (isset($_GET['delete'])) {
        Cart::DeleteFromCart($_GET['id']);
        echo 'The product is deleted';
    }
    
    else {
        $oDb = Database::get();
        $sCartInfo = Cart::GetCartInfo($oDb);
        echo !empty($sCartInfo["items"]) ? json_encode($sCartInfo) :  json_encode("empty"); 
    }
 
?>
