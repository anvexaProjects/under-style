<?php
include("../slider/functions.php");
$sliders = getTree();
$i = 0;
?>
<div id="promo" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php foreach ($sliders as $tab => $cat) { ?>
            <?php foreach ($cat['sliders'] as $i => $slider) { ?>
                <li data-target="#promo" data-slide-to="<?php echo $i++; ?>" <?php if ($i == 1) {
                    echo 'class="active"';
                } ?>></li>
            <?php } ?>
        <?php } ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php foreach ($sliders as $tab => $cat) { ?>
        <?php foreach ($cat['sliders'] as $i => $slider) { ?>
        <?php if ($i == 0) {
            echo '<div class="item active">';
        } else {
            echo '<div class="item">';
        } ?>
        <a href="<?php echo $slider['ankor'] ?>">
            <img src="../slider/<?php echo $slider['image'] ?>" alt="<?php echo strip_tags($slider['description']) ?>" title="<?php echo strip_tags($slider['description']) ?>">
        </a>
        <div>
            <img src="../slider/<?php echo $slider['logo'] ?>" alt="<?php echo strip_tags($slider['description']) ?>" title="<?php echo strip_tags($slider['description']) ?>">
            <?php echo $slider['description'] ?>
            <a class="btn btn-primary navbar-btn" href="<?php echo $slider['ankor'] ?>">Подробнее</a>
        </div>
    </div>
    <?php } ?>
    <?php } ?>
</div>
</div>
