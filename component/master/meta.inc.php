<?php
/*
  Description: Get Meta Tags
  Autor:       Oleg Koshkin
  Data:        20-02-2010
  Version:     1.1

  mailto:      WizardF@narod.ru
  copyright:   (C) 2010 Oleg Koshkin
*/
       
require_once '../component/libs.inc';
require_once '../component/bl/lister/lister.class.php';
require '../templates/config/enum.inc';

$sPath = URL::GetPath();

$params['plevel'] = 0;
$params['fields'] = "
a1.title, a1.name, a1.meta_text, a1.meta, DATE_FORMAT(a1.modify, '%a, %d %b %Y %H:%i:%S') as modify, a1.content2 as properties, as1.sprop3 as brand, as1.sprop5 as country, as1.control
";
$params['secuence'] = 'none';
$params['tpl_name'] = 'master/meta.tpl';

// handle index page
if(('home' == $sPath || 'index' == $sPath) && count($enum['langs']) > 1)
{ 
	$aLangs = array_keys($enum['langs']);
	$params['cond'] = " a1.path='".$aLangs[0]."' ";
}
elseif('home' == $sPath || 'index' == $sPath)
{ 
	$params['cond'] = " as1.control='home' ";
}
else
{ 
	$params['cond'] = " a1.path='".Database::escape(URL::GetPath())."' ";
}



$lst = new Lister($params);

#print_r($lst);
$lst->render();
