<?php
//  convert [APP_VERSION] lexem to real value 
$params['output'] = str_replace('[APP_VERSION]', APP_VERSION, $params['output']);
// build resource file
if (!file_exists(SITE_ABS.$params['output']) && $params['enable'])
{  
    // prepare directories (azbookvarik edition)
    $sDir = array();
    $sDir[0] = SITE_ABS.'/templates_c/css';
    $sDir[1] = SITE_ABS.'/templates_c/css/site';
    $sDir[2] = SITE_ABS.'/templates_c/js';
    $sDir[3] = SITE_ABS.'/templates_c/js/site';
    foreach ($sDir as $v)
    {
        if (!file_exists($v))
        {
            mkdir($v, 0777);
            chmod($v, 0777);
        }
    }   
    // look through the files
    $counter = 0;
    $files = explode(',', $params['files']);
    foreach ($files as $k => $v)
    {
        $files[$k] = trim($v);
    }
    foreach($files as $f)
    {
        if (!file_exists(SITE_ABS . $params['rel_dir'] . $f))
        {
            echo 'File &lt;' . SITE_ABS . $params['rel_dir'] . $f . '&gt; doesn\'t exists.<br />'; 
            $counter ++;
        }
    }
    if (0 == $counter)
    {
        $handle = fopen(SITE_ABS . $params['output'], 'w');
        foreach ($files as $f)
        {
            $content = file_get_contents(SITE_ABS . $params['rel_dir'] . $f);
            fwrite($handle, '/* ------------------------ ' . strtoupper($f) . ' ------------------------ */');
            fwrite($handle, "\n");
            fwrite($handle, $content);
            fwrite($handle, "\n\n\n\n");
        }
        fclose($handle);
    }
    $file_name = SITE_ABS . $params['output'];
    if ($params['compress'] && file_exists($file_name))
    {
    	$content = file_get_contents($file_name);
    	$handle = fopen(SITE_ABS . $params['output'], 'w');
    	// compress resources
		switch($params['type'])
		{
		    default:
		    case 'css':
				require_once '../lib/3rdparty/csstidy/class.csstidy.php';
				$css = new csstidy();
				$css->parse($content);
				$content = $css->print->plain();
		    break;
		    case 'js':
		    	require_once '../lib/3rdparty/jsmin.php';
		    	$content = JSMin::minify($content);
		    break;
		}  
		fwrite($handle, $content); 
		fclose($handle);	
    }
}
// get resource templete
switch($params['type'])
{
    default:
    case 'css':
        $sTemplate = '<link href="%s%s" type="text/css" rel="stylesheet" />\n';
    break;
    
    case 'js':
        $sTemplate = "<script src='%s%s' type='text/javascript'></script>\n";
    break;
}
// build resource connection path
if($params['enable'])
{
  $sResources = sprintf($sTemplate, SITE_REL, $params['output']);
}
else
{
    $files2 = explode(',', $params['files']);
    foreach ($files2 as $k => $v)
      $sResources .= sprintf($sTemplate, SITE_REL, $params['rel_dir'].trim($v).'?'.APP_VERSION);
}
switch($params['type'])
{
    default:
    case 'css':
        ?>
		<script>$(document).ready(function() { $("head").append('<?=$sResources?>'); }); </script><?
    break;
    
    case 'js':
        echo $sResources;
    break;
}
?>