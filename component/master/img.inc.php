<?php
    require_once '../lib/smarty/Smarty.class.php';

    // get server path
    $path =  sprintf("%s%s/%s", SITE_ABS, $params['path'], $params['img']);
    $big_path =  sprintf("%s%s/%s", SITE_ABS, $params['big_path'], $params['img']);
    $big_img_path = '';

    $tpl_name = Val::IsEmpty($params['tpl_name']) ? 'block/img.tpl' : $params['tpl_name'];

    // get html path
    if (file_exists($path) && !is_dir($path))
    {
        $img_path =  sprintf("%s/%s", $params['path'], $params['img']);
        $is_exist = 1;
        
        if(file_exists($big_path) && !is_dir($big_path))
            $big_img_path =  sprintf("%s/%s", $params['big_path'], $params['img']);
    }
    else
    {
        $img_path =  (Val::IsEmpty($params['no_img'])) ? '/images/no_img.png' : $params['no_img'];
        $is_exist = 0;
        if (isset($params['spacer']) && $params['spacer'] == true) $params['height'] = 1;
    }


    if ($is_exist == 0 && isset($params['render']) && $params['render'] == false)
        echo '';
    else
    {
        $tpl = new Smarty;
        $tpl->caching = false;
        $tpl->assign('prop', $params);
        $tpl->assign('img_path', $img_path);
        $tpl->assign('big_img_path', $big_img_path);
        $tpl->assign('is_exist', $is_exist);
        $tpl->display($tpl_name);
    }
?>