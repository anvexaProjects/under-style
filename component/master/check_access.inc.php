<?php
require_once '../component/libs.inc';
require_once '../component/bl/entity/user.class.php';

session_start();
if (!isset($_SESSION['login']))
{
	if (isset($_COOKIE['login']) && isset($_COOKIE['pass']))
	{
		$user = new User();
		$pass = $_COOKIE['pass'];
		$login = $user->oDb->escape($_COOKIE['login']);
		$is_exist = $user->loadByCondition("login = '$login' AND pass = '$pass'");
		if ($is_exist)
		{
			$data = $user->aData;
			$_SESSION['login'] = $data['login'];
			$_SESSION['name'] = $data['name'];
			$_SESSION['lname'] = $data['lname'];
			$_SESSION['role']  = $data['role'];
			$_SESSION['user_id'] = $data['id'];
			$_SESSION['email'] = $data['email'];
			$last_acccess = date('Y-m-d H:i:s');
			$user->setData(array(
				'last_access' => $last_acccess,
				'ip' => ip2long($_SERVER['REMOTE_ADDR']),
				'id' => $data['id'] 
			));	
			$user->sId = 'id';
			$user->update();
			$_SESSION['last_access']  = strtotime($last_acccess);
			$_SESSION['ip']  = $_SERVER['REMOTE_ADDR'];
			$_SESSION['last_update'] = strtotime($data['last_updated']);
		}
	}
}
?>