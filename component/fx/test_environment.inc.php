<?php                                
/*
  Description: Environment refgession test for Crystal Framework
  Autor:       Oleg Koshkin
  Data:        17-01-2006
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
*/

/*    
    check_php:
+ check connect
+ check failed connect
+ check QForm
+ check templates
  + error
  + warning
  - error 404
+ try write to folders
- check constants
*/

    require_once '../component/libs.inc';
    require_once '../component/shared.inc.php';
    require_once '../lib/QuickForm.php';
    require_once '../lib/HTML/QuickForm/Renderer/ArraySmarty.php';


    //$_SERVER['SCRIPT_FILENAME'];

    $output = '';
                         
    function write($param)
    {
        return "<h2>$param</h2>";
    }

    function write_file($path)
    {
       $file = fopen ($path, 'wb');
       $out2 = fwrite ($file, 'Test file');
          $out3 = fclose ($file);
    }


    $output .= write('Test connection');
    $db = mysql_connect(DBHOST, DBUSER, DBPASSWD) or die(CANT_CONNECT_MYSQL);
    mysql_select_db(MAINDB,$db) or die(CANT_SELECT_DB);
    
    $Query = "SELECT COUNT(*) as num FROM users" ;
    if ( !MySqlQueryEx($Query, $db) ) $output .= 'Test connection failed<br>'; else $output .= 'OK<br>'; 
    mysql_close($db);
                             
                             
    $output .= write('Test failed connection');
    @unlink(LOG_FILE_PATH);
    $db = mysql_connect(DBHOST, DBUSER, DBPASSWD) or die(CANT_CONNECT_MYSQL);
    mysql_select_db(MAINDB,$db) or die(CANT_SELECT_DB);
    $Query2 = "SELECT COUNT(id) as num FROM users_fail_test";
    if( !MySqlQueryEx($Query2, $db) ) $output .= 'Test failed connection - OK. Check "error.log" file<br>';
    mysql_close($db);



    $output .= write('Test write to folder<br>');
    $output .=  'INNOVA_IMG folder test:';
    write_file(SITE_ABS . INNOVA_IMG . '/tst.inc');
    unlink(SITE_ABS . INNOVA_IMG . '/tst.inc');
    
    $output .= '<br>SMARTY_COMPILE_DIR folder test:';
    write_file(SMARTY_COMPILE_DIR . '/tst.inc');
    unlink(SMARTY_COMPILE_DIR . '/tst.inc');
    
    $output .= '<br>UPLOAD_DIR folder test:';
    write_file(UPLOAD_DIR . '/tst.inc');
    unlink(UPLOAD_DIR . '/tst.inc');
    
    $output .= '<br>UPLOAD_DIR folder test:';
    write_file(UPLOAD_DIR . '/tst.inc');
    unlink(UPLOAD_DIR . '/tst.inc');

               


    $output .= write('Test QForm<br>');
    $form = new HTML_QuickForm('newsedit', 'post');
    $form->addElement('text', 'label', '');
    $form->addElement('submit', 'btnSubmit', LBL_SAVE);
    $form->addRule('label', 'Enter text', 'required', '', 'client'); 
    if ($form->validate())
    {
         $form->freeze();
    }
    else
    {
      $default = array('text'=>'default text');
      $form->setDefaults($default);

      $output .= '<div>' . $form->toHtml() . '</div>';
      //  $output .= $form->display(); 
    }
   
    
    $output .= write('Test messages<br>');
    $output .= GetBlockError('Test error');
    $output .= GetBlockWarning('Test warning');
    
    echo $output;
    
?>
