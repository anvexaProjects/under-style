<?php
/*
  Description: Lister (list view)
  Autor:       Oleg Koshkin
  Data:        05-02-2008
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/
require_once '../component/bl/lister/searchlister.class.php';
$lst = new SearchLister($params);
$lst->render();
?>