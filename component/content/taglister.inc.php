<?php
/*
  Description: Lister (list view)
  Autor:       Oleg Koshkin
  Data:        05-02-2008
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/
require_once '../component/bl/lister/taglister.class.php';
require_once '../component/bl/lister/lister.class.php';

$taglister = new TagLister(',');
$tpl = new Smarty();

if (isset($params['article_id']))
{
    $items = $taglister->getTagsByArticleId($params['article_id']);
    $tpl->assign('items', $items);
    $tpl->display($params['tpl_name']);
}
elseif (isset($params['tag']))
{
    require_once '../component/bl/lister/lister.class.php';
    $ids = $taglister->getArticleIdsByTag($params['tag']);
    $ids = "'".implode("','", $ids)."'";
    $params['cond'] = "a1.struct_id IN ($ids)";
    $lst = new Lister($params);
    $lst->render();
}
else 
{
    $items = $taglister->getTags();
    $items = $taglister->getTagsRelation($items);
    $tpl->assign('items', $items);
    $tpl->display($params['tpl_name']);    
}
    



?>