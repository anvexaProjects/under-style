<?php
require_once ('getHash.php');
require_once ('../component/libs.inc');

if (isset($_GET['property']))
{
    switch ($_GET['property']){
        case 'available':
            $prop = "as1.sprop7 = '1' AND as1.sprop1 != '1'";
            break;
        case 'hold':
            $prop = "as1.sprop8 = '1' AND as1.sprop1 != '1'";
            break;
        case 'offers':
            $prop = "as1.sprop9 = '1' AND as1.sprop1 != '1'";
            break;
        case 'sale':
            $prop = "as1.sprop10 = '1' AND as1.sprop1 != '1'";
            break;
        case 'archives':
            $prop = "as1.sprop1 = '1'";
            break;
        case 'catalog':
            $prop = "as1.sprop14 = '1' AND as1.sprop1 != '1'";
            break;
        default:
            $prop = "";
            break;
    }
}

$template = $params['tpl_name'];
$cond     = $params['cond'];
$field    = $params['field'];
$let      = $params['letters'];
$params['table'] ? $table = $params['table'] : $table = 'art_struct';

$prop ? $cond = $prop.' AND '.$cond : $cond = $cond;

$items     = getHash($field, $cond, $table);

$letters = array('A','B','C','D','E','F',
	'G','H','I','J','K','L', 'M','N','O','P','Q','R',
	'S','T','U','V','W','X','Y','Z');



//var_dump($cond);
if ($let){
	$arr = array();
	foreach ($letters as $letter) {
		foreach ($items as $v){
			$ven = strtoupper($v);
			if (preg_match("/^$letter\w*/", $ven)){
				$arr[$letter][] = $v;
			}
		}
	}
}


$tpl = new Smarty;
if ($let) { 
	$tpl->assign('letters', $letters);
	$tpl->assign('vendors', $arr);
} else {
	$tpl->assign('items', $items);
}
$tpl->display($template);