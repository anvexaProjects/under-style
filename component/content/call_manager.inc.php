<?php
/**
 * Created by PhpStorm.
 * User: evgeni.zhuk
 * Date: 23.12.13
 * Time: 23:50
 */

require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';

$form = BaseForm::GetBaseForm('/' . $_GET['path'] . '/');
$form = customizeForm($form);
$subject = $enum['subjects_en'][$_POST['subject']];
BaseForm::ProcessForm($form, 'content/call_manager.tpl', 'notifications/call_manager.tpl', $subject, $default, WRN_FORM_SENDED, MANAGER_MAIL);

function customizeForm($form)
{
    require '../templates/config/enum.inc';
    $form->addElement('text', 'name', '', 'id="name" class="terms_inp"');
    $form->addElement('text', 'phone', '', 'id="phone" class="terms_inp"');
    $form->addElement('select', 'subject', '', $enum['subjects'], 'class="terms_inp"');
    $form->addElement('text', 'time', '', 'id="time" class="terms_inp"');

    $form->addRule('name', sprintf(FLB_REQUIRED, S_INTRODUCTION), 'required', '', 'client');
    $form->addRule('name', sprintf(FLB_REQUIRED, S_INTRODUCTION), 'required');
    $form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required', '', 'client');
    $form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required');
    return $form;
}