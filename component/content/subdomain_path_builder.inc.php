<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgeni.zhuk
 * Date: 12.07.13
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
require_once '../component/libs.inc';
require '../templates/config/enum.inc';
$path = $params['path'];

function path_builder($path, $subdomains){
       $subdomain_name = substr(0, strpos($path, '/'));
       foreach ($subdomains as $alias => $domain){
           if ($subdomain_name == $alias){
               return $domain.'.'.SITE_REL;
           }
       }
    return SITE_REL;
}

$path = path_builder($path, $enum['subdomains']);
$tpl = new Smarty();
$tpl->assign('path', $path);
$tpl->display('content/sub_link.tpl');