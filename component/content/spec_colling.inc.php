<?php



require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';



$form = BaseForm::GetBaseForm('/' . $_GET['path'] . '/',"_colling");
$form = customizeForm_s($form);
BaseForm::ProcessForm($form, 'content/spec_colling.tpl', 'notifications/spec_colling.tpl', SUBJ_FEEDBACK, $default, WRN_FORM_SENDED, ZAPROS_MAIL);

function customizeForm_s($form)
{
    require '../templates/config/ru_enum.inc';
    $form->addElement('text', 'name', '', 'id="name" style="height:26px" class="terms_inp"');
	$form->addElement('text', 'phone', '', 'id="phone" style="height:26px" class="terms_inp"');
    $form->addElement('text', 'email', '', 'id="email" style="height:26px" class="terms_inp"');
    $form->addElement('textarea', 'message', '', 'id="message" class="trm_message" style="width:262px; height:106px"');

	$form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required', '', 'client');
    $form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required');
	$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required', '', 'client');
	$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required');
    $form->addRule('email', sprintf(FLB_EMAIL_FORMAT), 'email', '', 'client');
    $form->addRule('email', sprintf(FLB_EMAIL_FORMAT), 'email');

    return $form;
}

?>