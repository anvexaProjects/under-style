<?php

require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require_once '../component/bl/entity/user.class.php';

$db = Database::get();

if (isset($_POST['type'])) $type = $_POST['type'];
else $type = $params['type'];
switch($type) {
	default:
	case 'prodinfo':
		$tpl = 'prodinfo';
		$subj = S_SUBJ_PRODINFO;
	break;
	case 'friend':
		$tpl = 'friend';
		$subj = S_SUBJ_FRIEND;
	break;
	case 'discount':
		$tpl = 'discount';
		$subj = S_SUBJ_DISCOUNT;
	break;
}
$action = '/' . $_GET['path'] . '/form='.$tpl.'/';
$form = getForm($form, $tpl, $action);
$hash = $db->getField("SELECT discount_hash FROM users WHERE id = '".$_SESSION['user_id']."'");
$default['discount_hash'] = $hash;
$default['product'] = $_GET['global']['struct'][1]['name'];
$default['product_path'] = $_GET['global']['struct'][1]['path'];
$default['type'] = $tpl;
$default['field_1'] = '';
$default['field_2'] = CHECK_SPAM;
if (isset($_SESSION['login'])) {
	$default['email'] = $_SESSION['login'];
	$default['name'] = $_SESSION['name'].' '.$_SESSION['lname'];
}
			
$form->setDefaults($default);

if ($form->validate()) {
	if (!BaseForm::IsSpam()){
		BaseForm::Perform($_POST, 'notifications/feedback_'.$tpl.'.tpl', $subj, isset($_POST['email_to']) ? $_POST['email_to'] : ADMIN_MAIL, $_POST['email']);
		UI::Redirect(SITE_REL . $action . 's=1/#form');
	}
}
$t = new Smarty;
$renderer = new HTML_QuickForm_Renderer_ArraySmarty($tpl);

$form->accept($renderer);
$t->assign('form_data', $renderer->toArray());
$t->assign('default', $default);
$t->display('content/feedback_'.$tpl.'.tpl');

function getForm($form, $type, $action)
{
	$form = new HTML_QuickForm('fssrm', 'post', SITE_REL . $action);
	$form->addElement('text', 'field_1', '');
	$form->addElement('text', 'field_2', '');
    $form->addElement('text', 'name', '', 'id="name"');
    $form->addElement('text', 'email', '', 'id="phone" class="inp"');
    $form->addElement('textarea', 'message', '', 'id="message" style="width:100%; height:100px"');
	$form->addElement('hidden', 'product');
	$form->addElement('hidden', 'type');
	$form->addElement('hidden', 'discount_hash');
	$form->addElement('hidden', 'product_path');

    $form->addRule('name', sprintf(FLB_REQUIRED, S_NAME), 'required', '', 'client');
    $form->addRule('name', sprintf(FLB_REQUIRED, S_NAME), 'required');
    $form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required', '', 'client');
    $form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required');
    $form->addRule('email', FLB_EMAIL_FORMAT, 'email', '', 'client');
    $form->addRule('email', FLB_EMAIL_FORMAT, 'email');
	
	if ($type == 'friend' || $type == 'discount') {
		$form->addElement('text', 'email_to', '', 'id="email_to"');
		$form->addRule('email_to', sprintf(FLB_REQUIRED, S_RECIEVER_EMAIL), 'required', '', 'client');
		$form->addRule('email_to', sprintf(FLB_REQUIRED, S_RECIEVER_EMAIL), 'required');
		$form->addRule('email_to', FLB_EMAIL_FORMAT, 'email', '', 'client');
		$form->addRule('email_to', FLB_EMAIL_FORMAT, 'email');
	}
	
    //$form->addRule('message', sprintf(FLB_REQUIRED, S_MESSAGE), 'required', '', 'client');
    //$form->addRule('message', sprintf(FLB_REQUIRED, S_MESSAGE), 'required');
    return $form;
}


?>