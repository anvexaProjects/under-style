<?php
session_start();
$prod_id = $params['id'];

if (!isset($_SESSION['vprod'])) $_SESSION['vprod'] = array();

$is_isset = false;
foreach ($_SESSION['vprod'] as $vprod){
	if ($vprod == $prod_id) $is_isset = true;
}
if (!$is_isset) array_unshift($_SESSION['vprod'], $prod_id);
if (count($_SESSION['vprod']) > 100) array_pop($_SESSION['vprod']);
?>