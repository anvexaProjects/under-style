<?php
require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require_once '../component/bl/entity/user.class.php';

$db = database::get();
$form = BaseForm::GetBaseForm('/' . $_GET['path'] . '/', '_registration');
$form = customizeForm($form, $enum);

if ( isset ( $_POST['action']) && $_POST['action'] == 'registration')
{
    $name = trim(htmlspecialchars($_POST['name']));
    $email = trim(htmlspecialchars($_POST['email']));
    $pass = md5(SALT . trim(htmlspecialchars($_POST['pass'])) . SALT);
    $conf_pass = trim(htmlspecialchars($_POST['conf_pass']));
    $role = (int) $_POST['role'];
}  

if ( !empty ( $_POST['email']))
{  
  $email = trim(htmlspecialchars($_POST['email']));
  $sql = "SELECT 1 FROM users WHERE email = '" . $email  . "'";
  $check_email = $db->getField($sql);
  if ( !empty ($check_email) )
       $form->setElementError('email', ERR_USER_ALREADY_REGISTERED); 
}

if ($form->validate())
{
   /*
   $sql = "INSERT INTO users
         VALUES ('', '" . $name  . "', '" . $pass . "', '" . $role . "', 0, '" . $email . "',
                  '" . $name . "', '" . date("Y-m-d H:i:s") . "', '', 0, '')";
   */
   $values = array(
        'login'      => $email,
        'email'      => $email,
        'name'       => $name,
        'pass'       => $pass,
        'role'       => $role,
        'registered' => date("Y-m-d H:i:s"),
        'is_locked'  => 1
   );
   $db->insert('users', $values);
}  


BaseForm::ProcessForm($form, 'content/registration.tpl', 'notifications/registration.tpl', SUBJ_FEEDBACK, $default, WRN_FORM_SENDED, $_POST['email'], 1, 'notifications/registration_copy.tpl');




function customizeForm($form, $enum)
{
    //require '../templates/config/ru_enum.inc';
    $form->addElement('hidden', 'action', 'registration');
    $form->addElement('text', 'email', '', 'id="email" style="height: 26px;" autocomplete="off"  class="terms_inp"');
    $form->addElement('text', 'name', '', 'id="name" style="height:26px" autocomplete="off"  class="terms_inp"');
    $form->addElement('password', 'pass', '', 'id="pass" style="height:26px" autocomplete="off"  class="terms_inp"');
    $form->addElement('password', 'conf_pass', '', 'id="conf_pass" style="height:26px" autocomplete="off"  class="terms_inp"');
    $form->addElement('select', 'role', '', $enum['roles_public']);
	  
	$form->addRule('email', sprintf(FLB_REQUIRED, S_U_EMAIL), 'required', '', 'client');
	$form->addRule('email', sprintf(FLB_REQUIRED, S_U_EMAIL), 'required');
    $form->addRule('email', sprintf(FLB_EMAIL_FORMAT), 'email', '', 'client');
    $form->addRule('email', sprintf(FLB_EMAIL_FORMAT), 'email');
    $form->addRule('name', sprintf(FLB_REQUIRED, S_NAME), 'required', '', 'client');
	$form->addRule('name', sprintf(FLB_REQUIRED, S_NAME), 'required');
    $form->addRule('pass', sprintf(FLB_REQUIRED, S_PASSWORD), 'required', '', 'client');
	$form->addRule('pass', sprintf(FLB_REQUIRED, S_PASSWORD), 'required');
    $form->addRule('conf_pass', sprintf(FLB_REQUIRED, S_CONF_PASSWORD), 'required', '', 'client');
	$form->addRule('conf_pass', sprintf(FLB_REQUIRED, S_CONF_PASSWORD), 'required');
    $form->addRule('pass', sprintf(S_MIN_PASS_LENGTH), 'minlength', 5, 'client'); 
    $form->addRule('pass', sprintf(S_MIN_PASS_LENGTH), 'minlength', 5);
    $form->addRule('conf_pass', sprintf(S_MIN_PASS_LENGTH), 'minlength', 5, 'client'); 
    $form->addRule('conf_pass', sprintf(S_MIN_PASS_LENGTH), 'minlength', 5);
    $form->addRule(array('pass', 'conf_pass'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq', 'client');
    $form->addRule(array('pass', 'conf_pass'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq');
    

    return $form;
}



?>