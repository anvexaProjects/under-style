<?php
//die();
/*
  Description: Lister (list view)
  Autor:       Oleg Koshkin
  Data:        05-02-2008
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
 */
require_once '../component/libs.inc';
require_once '../component/bl/baseform.class.php';
require '../templates/config/enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';

$subject = $enum['subjects_en'][$_POST['subject']];
$form = BaseForm::GetBaseForm('/' . $_GET['path'] . '/');
$form = customizeForm($form);
BaseForm::ProcessForm($form, 'content/contact_form.tpl', 'notifications/feedback.tpl', $subject, '', '', CONTACTS_MAIL, 1, '', ZAPROS_MAIL);

function customizeForm($form)
{
    require '../templates/config/enum.inc';

    $subjects = $enum['subjects'];
    unset($subjects['0']);
    $form->addElement('text', 'name', '', 'id="name" class="terms_inp"');
    $form->addElement('text', 'phone', '', 'id="phone" class="terms_inp"');
    $form->addElement('select', 'subject', '', $subjects, 'class="terms_inp"');
    $form->addElement('text', 'time', '', 'id="phone" class="terms_inp"');
    $form->addElement('text', 'email', '', 'id="phone" class="terms_inp"');
    $form->addElement('textarea', 'message', '', 'id="message" class="trm_message" style="width:100%; height:100px"');

    $form->addRule('name', sprintf(FLB_REQUIRED, S_INTRODUCTION), 'required', '', 'client');
    $form->addRule('name', sprintf(FLB_REQUIRED, S_INTRODUCTION), 'required');
    $form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required', '', 'client');
    $form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required');
    $form->addRule('email', FLB_EMAIL_FORMAT, 'email', '', 'client');
    $form->addRule('email', FLB_EMAIL_FORMAT, 'email');
    $form->addRule('message', sprintf(FLB_REQUIRED, S_MESSAGE), 'required', '', 'client');
    $form->addRule('message', sprintf(FLB_REQUIRED, S_MESSAGE), 'required');
    return $form;
}