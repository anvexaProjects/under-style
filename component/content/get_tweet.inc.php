<?
require_once '../component/libs.inc';
	
$tpl = & new Smarty;
$tpl->caching = true;


$template = ($params['tpl_name'])? $params['tpl_name']:TWEET_DEFAULT_TEMPLATES_PATH;
$items[] = array(
	"tweet_name" 		=> ($params['tweet_name'])? $params['tweet_name']:TWEET_DEFAULT_NAME,
	"tweet_count" 		=> ($params['tweet_count'])? $params['tweet_count']:TWEET_DEFAULT_COUNT,
	"tweet_footer" 		=> ($params['tweet_footer'])? $params['tweet_footer']:TWEET_DEFAULT_FOOTER_TEXT,
	"tweet_width" 		=> ($params['tweet_width'])? $params['tweet_width']:TWEET_DEFAULT_WIDTH,
	"tweet_height" 		=> ($params['tweet_height'])? $params['tweet_height']:TWEET_DEFAULT_HEIGHT,
	"tweet_interval" 	=> ($params['tweet_interval'])? $params['tweet_interval']:TWEET_DEFAULT_INTERVAL
);

$tpl->assign('items', $items);

$tpl->compile_check = false;
$tpl->cache_lifetime = 120;
$tpl->display($template);
?>