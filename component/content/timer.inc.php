<?php
require_once ('../component/libs.inc');

$end_time=strtotime($params['time']);
$rest_time = explode('.', ($end_time - time())/86400);
$time = $rest_time[0];
$time > 0 ? true : $time = 1;
$tpl = new Smarty;
$tpl->assign('timer', $time);
$tpl->display('block/timer.tpl');