<?php
/*
  Description: Cart page (Lambretta)
  Autor:       Oleg Koshkin
  Data:        06-12-2009
  Version:     1.1

  mailto:      WizardF@narod.ru
  copyright:   (C) 2009 Oleg Koshkin
*/

	require_once '../component/libs.inc';
    require_once '../component/bl/cart.class.php';
    require_once '../component/bl/notification.class.php';



/*
external interface
GET
no patam  - View Cart => STEP1
id=ProdID - product id
add=NUM   - add NUM (count) product to cart
delete    - delete product from cart
step=num  - step number, 1 by default


POST
prod_id=NUM - count (NUM) of product with prod_id


ADD        - add product to cart
DELETE    - delete product from cart
CALCULATE - update product count STEP1
SIGNOFF    - sign off order STEP1 -> STEP3
APPROVE    - save feedback form STEP2 -> STEP3 - skipped
STEP1 - show all cart products
STEP2 - feed back form - skipped
STEP3 - confirmation info
STEP4 - final step
*/

//TODO  ignor step 2
//TODO ClearCartStatus

    $_GET['global']['pdir'] = 'content';
   // init variables
   $ProdId = -1;
   $AddNum = 0;
   $CardToRecharge = 0;
   $Step = 1;
   $Action = 'STEP1';
   $error = array();

    // get Request variables
    if  (@$_GET['add'])
    {
        $AddNum = $_GET['add'];
        $Action = 'ADD';

        if ( empty($AddNum) )
        {
           $AddNum = 0;
           $Action = 'STEP1';
        }
    }

    // get product id
    if  (@$_GET['id'])
    {
        $ProdId = $_GET['id'];
        if ( empty($ProdId) )  $ProdId = -1;
    }

    if  (@$_GET['delete'])  $Action = 'DELETE';
    if ( (('ADD' == $Action) || ('DELETE' == $Action)) && -1 == $ProdId )  $Action = 'STEP1';
	if (('ADD' == $Action) && (0 != $CardToRecharge))  $Action = 'RECHARGE';

    if (@$_GET['step'])
    {
        $Step = $_GET['step'];
        if ( !Val::IsNumber($Step) )  $Step = 1;
    }

    // set calculate status when remove button where clicked
    foreach (@$_POST as $key => $el)
		if ( 'remove_' == substr($key, 0, 7))
			$Action = 'CALCULATE';

    if (@$_POST['calculate_x'])   $Action = 'CALCULATE';
    if (@$_POST['signoff_x'])     $Action = 'SIGNOFF';
    if (@$_POST['approve_x'])     $Action = 'APPROVE';
    if (2 == $Step)             $Action = 'STEP2';
    if (3 == $Step)             $Action = 'STEP3';
    if (4 == $Step)             $Action = 'STEP4';



    ///////////////////////////////////////////

    if ('ADD' == $Action)
    {
        if ( Cart::AddToCart($ProdId, $AddNum, $_GET['l'], $CardToRecharge) )
        {
	       if ( strlen(@$_SERVER['HTTP_REFERER']) > 0 )
                UI::Redirect($_SERVER['HTTP_REFERER']); //return back
        }
        else
        {
           echo UI::GetBlockError(CANT_ADD_THING);
        }
    }

    if ('DELETE' == $Action)
    {
        if ( Cart::DeleteFromCart($ProdId) )
        {
	            if ( strlen(@$_SERVER['HTTP_REFERER']) > 0 )
    	            UI::Redirect($_SERVER['HTTP_REFERER']); //return back
        }
        else
        {
              echo UI::GetBlockError(CANT_DELETE_THING);
        }
    }


    if (('CALCULATE' == $Action)  || ('SIGNOFF' == $Action))
    {
        $IsOK = true;
        $arr = array();
	    $to_remove = array();
        if(isset($_POST['shipping']))
		{
			 include '../templates/config/enum.inc';
			 $_SESSION['ship_price'] = $enum['shipping_price'][$_POST['shipping']];
			 $_SESSION['ship_price_id'] = $_POST['shipping'];
		}
			
        // gets associative array arr['ID'] = num from POST vars
        foreach (@$_POST as $key => $el)                // look throw POST array
		{
            if ( 'prod_' == substr($key, 0, 5) )        // filter product count fields only
            {
                $el = trim($el);
                if ( Val::IsNumber($el) && ($el >= 0) )         // if produt count is positive number
                {
                    $index = substr($key, 5, strlen($key)-5);
                    if ($el != 0) $arr[$index] = $el;
                }
                else
                {
                  $IsOK = false;
                  break;
                }
            }

            if ( 'remove_' == substr($key, 0, 7))      // filter remove img
            {
                $price_id = substr($key, 7, strlen($key)-7);
                $price_id = str_replace('_x', '', $price_id);
                $price_id = str_replace('_y', '', $price_id);
				array_push($to_remove, trim($price_id));
            }
 		}
        // save to database
        if ($IsOK)
            if ( Cart::UpdateProducts($arr) )
            {
				// remove items
				foreach($to_remove as $price_id)
					if (!Cart::DeleteFromCart($price_id))
					{
						Cart::PrintStep1(UI::GetError(ERR_WHILE_DELETE));
						$IsOK = false;
					}

				 if ($IsOK)
				 {
	                if ('SIGNOFF' == $Action)
					{
					    if (!isset($_SESSION['user_id']))
					    {
					        UI::Redirect(GetUrlPrefix() . 'account/?checkout=true');
					    }
					 	UI::Redirect(GetUrlPrefix() . 'cart/?step=2');
					}
       		        else UI::Redirect(GetUrlPrefix() . 'cart/');
				}

            }
            else $IsOK = false;



        if (!$IsOK)
        {
              echo Cart::PrintStep1(UI::GetError(WRN_WRONG_COUNT));
        }
		else
		{
	        if (!is_array($arr) || (count($arr) == 0))
    	    {
        	   Cart::ClearCartStatus();
    	       UI::Redirect(GetUrlPrefix() . 'cart/');
	        }
		}
    }


    if ('STEP1' == $Action)
    {
       echo Cart::PrintStep1('');
    }

    // 'STEP2' handler should be placed after 'APPROVE'  handler in source code
    if ('STEP2' == $Action)
    {
       if (!Cart::IfOrderExistEx()) UI::Redirect(GetUrlPrefix() . 'cart/');

	   echo	Cart::PrintStep2('');
    }

    // 'STEP3' handler should be placed after 'APPROVE'  handler in source code
    if ('STEP3' == $Action)
    {
        $IsExist = Cart::IfOrderExistEx();
        if (!$IsExist) UI::Redirect(GetUrlPrefix() . 'cart/');

        echo Cart::PrintStep3();
		 Cart::FinalizeCart();

		 // send e-mail to admin and customer
		 Cart::SendOrderNotificationText();

		 // clear cart (order have already submitted)
		 Cart::ClearCartStatus();

		 // delete all old orders
		 //Cart::DeleteAllOldOrders();		
    }

    if ('STEP4' == $Action)
    {
        $IsExist = Cart::IfOrderExistEx();
        if (!$IsExist) UI::Redirect(GetUrlPrefix() . 'cart/');
		echo Cart::PrintStep4();
		 // finalaize status, assign pins
		 Cart::FinalizeCart();

		 // send e-mail to admin and customer
		 Cart::SendOrderNotificationText();

		 // clear cart (order have already submitted)
		 Cart::ClearCartStatus();

		 // delete all old orders
		 //Cart::DeleteAllOldOrders();
    }

	function GetUrlPrefix()
	{
		return $_GET['global']['root'] . '/';
	}
?>
