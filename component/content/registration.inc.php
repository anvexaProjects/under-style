<?php

require_once '../component/libs.inc';
require_once '../component/bl/notification.class.php';
require_once '../component/bl/entity/user.class.php';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require '../templates/config/'.$_GET['global']['lan'].'_enum.inc';

$tpl = new Smarty();
$obj = new User();
$mode = isset($_SESSION['user_id']) ? 'edit' : 'add';
$discount = 0;

$form = getRegisterForm($enum, $mode);

if ($mode == 'edit') {
	$obj->load($_SESSION['user_id']);
	$discount = $obj->getDiscount($_SESSION['user_id'], DISCOUNT_MAX);
	$defaults = $obj->aData;
	list($defaults['birth_year'], $defaults['birth_month'], $defaults['birth_day']) = explode('-', $defaults['birthday']);
	$form->setDefaults($defaults);
}


if ($form->validate()) {
	$login = $_POST['email'];
	$data = array();
	$data = $_POST;
	$data['birthday'] = $_POST['birth_year'].'-'.$_POST['birth_month'].'-'.$_POST['birth_day'];
	if ($mode == 'edit') {
		$res = $obj->loadByCondition('login = "'.$_POST['email'].'" AND id != "'.$_SESSION['user_id'].'"');
		if (!empty($res)) $form->setElementError('email', ERR_USER_ALREADY_REGISTERED);
		else {
			$data['id'] = $_SESSION['user_id'];
			if ($_POST['pass1'] != '') $data['pass'] = md5(SALT.$_POST['pass1'].SALT);
			$obj->setData($data);
			$obj->update();
			UI::Redirect($_GET['global']['root'].'/account/?s=2');
		}
	} else {
		$res = $obj->loadByCondition('login = "'.$_POST['email'].'"');
		if (!empty($res)) $form->setElementError('email', ERR_USER_ALREADY_REGISTERED); 
		else {
			$data['role'] = USER;
			$data['login'] = $_POST['email'];
			$data['pass'] = md5(SALT.$_POST['pass1'].SALT);
			$data['discount_hash'] = md5(time().rand(100, 999).$_SESSION['user_id']);
			$obj->setData($data);
			$data['id'] = $obj->insert();
			$_SESSION['login'] = $data['login'];
			$_SESSION['name'] = $data['name'];
			$_SESSION['lname'] = $data['lname'];
			$_SESSION['role']  = $data['role'];
			$_SESSION['user_id'] = $data['id'];
			if (isset($_REQUEST['bur']) && $_REQUEST['bur'] == 'cart') UI::Redirect($_GET['global']['root'].'/cart/step=2/');
			else UI::Redirect($_GET['global']['root'].'/account/?s=1');
		}
	}
}
$renderer = new HTML_QuickForm_Renderer_ArraySmarty($tpl);
$form->accept($renderer);
$tpl->assign('form_data', $renderer->toArray());
$tpl->assign('mode', $mode);
$tpl->assign('discount', (float)$discount);
$tpl->display('content/registration.tpl');

function getRegisterForm($enum, $mode)
{
	$form = new HTML_QuickForm('fssrm_register', 'post', $_GET['global']['root'].'/'.$_GET['global']['path'].'/');
	
	$birth_day = array();
	for ($i = 1; $i <= 31; $i ++ ) $birth_day[$i] = $i;
	
	$birth_year = array();
	$start_year = date('Y') - 5;
	$end_year = date('Y') - 90;
	for ($i = $end_year; $i <= $start_year; $i ++) $birth_year[$i] = $i;
	
	$form->addElement('select', 'birth_day', '', $birth_day, 'id="birth_day" style="width:44px; margin-right:5px"');
	$form->addElement('select', 'birth_month', '', $enum['month'], 'id="birth_month" style="width:90px; margin-right:5px"');
	$form->addElement('select', 'birth_year', '', $birth_year, 'id="birth_year" style="width:54px"');
	
	$form->addElement('text', 'email', '', 'id="email"');
	
	$form->addElement('password', 'pass1', '', 'id="pass"');
	$form->addElement('password', 'pass2', '', 'id="pass2"');

	$form->addElement('select', 'sex', '', $enum['sex'], 'style="width:auto"');	
	
	$form->addElement('text', 'name', '', 'id="name"');
	$form->addElement('text', 'lname', '', 'id="lname"');
	$form->addElement('text', 'mname', '', 'id="mname"');
	
	$form->addElement('text', 'street', '', 'id="street" style="width:182px; margin-right:5px"');
	$form->addElement('text', 'house', '', 'id="house" style="width:154px"');
	
	$form->addElement('text', 'plz', '', 'id="plz" style="width:60px; margin-right:5px"');
	$form->addElement('text', 'ort', '', 'id="ort" style="width:276px"');
	
	$form->addElement('select', 'country', '', $enum['country'], 'style="width:auto"');
	
	$form->addElement('text', 'phone', '', 'id="phone"');
	$form->addElement('text', 'fax', '', 'id="fax"');
	
	
	$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required', '', 'client');
	$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required');
	$form->addRule('email', FLB_EMAIL_FORMAT, 'email', '', 'client');
	$form->addRule('email', FLB_EMAIL_FORMAT, 'email');
	
	if ($mode == 'add') {
		$form->addRule('pass1', sprintf(FLB_REQUIRED, S_PASSWORD), 'required', '', 'client');
		$form->addRule('pass1', sprintf(FLB_REQUIRED, S_PASSWORD), 'required');
		$form->addRule('pass2', sprintf(FLB_REQUIRED, S_CONFIRM_PASSWORD), 'required', '', 'client');
		$form->addRule('pass2', sprintf(FLB_REQUIRED, S_CONFIRM_PASSWORD), 'required');
	}
	$form->addRule('name', sprintf(FLB_REQUIRED, S_FIRST_NAME), 'required', '', 'client');
	$form->addRule('name', sprintf(FLB_REQUIRED, S_FIRST_NAME), 'required');
	$form->addRule('lname', sprintf(FLB_REQUIRED, S_LAST_NAME), 'required', '', 'client');
	$form->addRule('lname', sprintf(FLB_REQUIRED, S_LAST_NAME), 'required');
	$form->addRule('street', sprintf(FLB_REQUIRED, S_STREET), 'required', '', 'client');
	$form->addRule('street', sprintf(FLB_REQUIRED, S_STREET), 'required');
	$form->addRule('house', sprintf(FLB_REQUIRED, S_HOUSE_N), 'required', '', 'client');
	$form->addRule('house', sprintf(FLB_REQUIRED, S_HOUSE_N), 'required');
	$form->addRule('plz', sprintf(FLB_REQUIRED, S_PLZ), 'required', '', 'client');
	$form->addRule('plz', sprintf(FLB_REQUIRED, S_PLZ), 'required');
	$form->addRule('ort', sprintf(FLB_REQUIRED, S_ORT), 'required', '', 'client');
	$form->addRule('ort', sprintf(FLB_REQUIRED, S_ORT), 'required');
	$form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required', '', 'client');
	$form->addRule('phone', sprintf(FLB_REQUIRED, S_PHONE), 'required');
	
    $form->addRule(array('pass1', 'pass2'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq', 'client');
    $form->addRule(array('pass1', 'pass2'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq');
	

	return $form;
}



?>