<?php

require_once '../component/libs.inc';
require_once '../lib/3rdparty/unisender_api.php';
require '../templates/config/'.$_GET['global']['lan'].'_enum.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';

$unisender = new UniSenderApi(UNISENDER_KEY);
$feeds = json_decode($unisender->__call('getLists', null), true);

$tpl = new Smarty;

$form = new HTML_QuickForm('fssrm_register', 'post', $_GET['global']['root'].'/'.$_GET['global']['path'].'/');

$form->addElement('text', 'email', '', 'id="email class="inp_brd""');
$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required', '', 'client');
$form->addRule('email', sprintf(FLB_REQUIRED, S_EMAIL), 'required');
$form->addRule('email', FLB_EMAIL_FORMAT, 'email', '', 'client');
$form->addRule('email', FLB_EMAIL_FORMAT, 'email');

$form->addElement('text', 'name', '', 'id="name class="inp_brd"');
$form->addRule('name', sprintf(FLB_REQUIRED, S_NAME2), 'required', '', 'client');
$form->addRule('name', sprintf(FLB_REQUIRED, S_NAME2), 'required');

foreach ($feeds['result'] as $feed)
{
	$form->addElement('checkbox', 'list['.$feed['id'].']', '', $feed['title'], 'class="feed"', $feed['id']);
}
foreach ($enum['subs_sources'] as $source)
{
	$form->addElement('checkbox', 'source['.$source.']', '', $source, '', $source);
}

if ($form->validate())
{
	if (empty($_POST['list']))
	{
		$form->setElementError('name', FLB_SELECT_FEED);
	}
	else
	{
		$src = array();
		if (isset($_POST['source']))
		{
			$src = array_keys($_POST['source']);
		}
		if (trim($_POST['source_other']) !== '')
		{
			$src[] = $_POST['source_other'];
		}
		$subs_result = json_decode($unisender->subscribe(array(
			'list_ids' => implode(',', array_keys($_POST['list'])),
			'fields[name]' => $_POST['name'],
			'fields[email]' => $_POST['email'],
			'fields[how]' => implode(',', $src)
		)), true);
	}
	if (isset($subs_result['result']['person_id']))
	{
		UI::Redirect($_GET['global']['root'].'/'.$_GET['global']['path'].'/s=1/');
	}
	else
	{
		$form->setElementError('name', $subs_result['error']);
	}
}

$renderer = new HTML_QuickForm_Renderer_ArraySmarty($tpl);
$form->accept($renderer);
$tpl->assign('form_data', $renderer->toArray());
$tpl->display('content/subscription.tpl');

?>