<?php

/*
  Description: Catalog renderer
  Autor:       Oleg Koshkin
  Data:        16-06-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2005 Oleg Koshkin
 */

require_once '../component/libs.inc';

$tpl = & new Smarty;
$tpl->caching = ENABLE_CACHING;
$template = isset($params['tpl']) ? $params['tpl'] : 'master/menu_main.tpl';
$cond = isset($params['cond']) ? $params['cond'] : 1;
$levels = isset($params['levels']) ? $params['levels'] : 2;
$start_level = isset($params['start_level']) ? $params['start_level'] : 1;
$key = array('sitemap', URL::GetPath(), $template, $cond, $levels);
$key = implode('|', $key);

if (!$tpl->is_cached($template, $key))
{
    $lang = $_GET['global']['lan'];
    // initialize
    $l1 = array();
    $l2 = array();
    $l3 = array();

    $oDb = Database::get();

    $sql = "SELECT id, path, parent_id, level, name, control, ch_control, ord, spath
			   FROM articles WHERE is_deleted = 0 AND lang = '$lang' AND $cond ORDER BY ord";
    $rows = $oDb->getRows($sql);

    foreach ($rows as $row)
    {
        if ($row['level'] == $start_level) array_push($l1, $row);
        if ($row['level'] == $start_level+1) array_push($l2, $row);
		if ($levels == 3 && $row['level'] == $start_level+2) array_push($l3, $row);
    }

	if ($levels == 2) $tree = SiteMap::getTree2($l1, $l2);
	if ($levels == 3) $tree = SiteMap::getTree3($l1, $l2, $l3);

    foreach ($tree as $k => $v)
    {
        $tree[$k]['ch_count'] = sizeof($v['items']);
    }

    $tpl->assign('items', $tree);
}
$tpl->display($template, $key);
?>
