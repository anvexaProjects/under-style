<?php
require_once '../component/bl/notification.class.php';
require_once '../component/bl/login.class.php';

$message = '';
$is_admin = false;
$roles = array();
$paths = array('login');
$location = $_REQUEST['location'];

if (!$_GET['admin_zone'])
{
    require_once '../component/libs.inc';
    $template = 'content/user_login.tpl';
    $is_admin = false;
	switch ($location) {
		case 'salers': $roles = array(SALER, SALERS_REDACTOR, ADMIN); break;
    	case 'des_arc': $roles = array(DESIGNER, DESIGNERS_REDACTOR, ADMIN); break;
    	case 'all': $roles = array(DESIGNER, DESIGNERS_REDACTOR, ADMIN, SALER, SALERS_REDACTOR); break;
		default: $roles = array(ADMIN); break;
	}
}
else
{
    require_once '../component/admin_libs.inc';
    $template = 'admin/'.CMS.'/login.tpl';
    $is_admin = true;
    $roles = array(ADMIN);
}

$obj = new Login();
// login
if  (isset($_POST['submit_login']))
{
	$obj->doLogin($_POST['login'], $_POST['pass'], $roles);
	if (!$obj->e)
	{
		if (($obj->role == SALER || $obj->role == SALERS_REDACTOR) && ($location == 'salers' || $location == 'all')) {
				UI::Redirect( SITE_REL . '/partnership/wholesale_buyers/');
		} elseif (($obj->role == DESIGNER || $obj->role == DESIGNERS_REDACTOR) && ($location == 'des_arc' || $location == 'all')) {
				UI::Redirect( SITE_REL . '/partnership/architects_designers/');
		} elseif ($obj->role == ADMIN) {
				UI::Redirect( SITE_REL . '/admin/index.php');
		}
		/*
		if ($obj->role == USER) {
			if (isset($_REQUEST['bur']) && $_REQUEST['bur'] == 'cart') UI::Redirect(SITE_REL . '/cart/step=2/');
			else UI::Redirect(SITE_REL . '/account/'.$remember);
		}
		*/
	}
}
// restore pass
if  (isset($_POST['submit_restore']))
{
     $message = restorePass();
	 if (!$message) {
		 UI::Redirect( SITE_REL . '/pub/login.php?location=salers&recovery=1');
	 }
}
$restored = false;
if (isset($_GET['u']) && isset($_GET['h']) && isset($_GET['recovery']) && $_GET['recovery'] == 2)
{
	$db = Database::get();
	$rs = $db->getRow(sprintf('SELECT * FROM users WHERE id = "%d" AND pass_hash = "%s"',
		intval($_GET['u']), Val::ToDb($_GET['h'])));
	if (!empty($rs)) {
		$db->query('UPDATE users SET pass = pass_new, pass_new = "", pass_hash="" WHERE id = '.intval($_GET['u']));
		$restored = true;
	}
}

$tpl = new Smarty;
$tpl->assign('restored', $restored);
$tpl->assign('error_login', $obj->e);
$tpl->assign('error_restore', $message);
$tpl->display($template);

function restorePass()
{
    if (Val::IsEmpty($_POST['login_restore']) || 'admin' == $_POST['login_restore'])
        return ERR_ADD_LOGIN;

    $oDb = Database::get();
    $login = strtoupper(Val::ToDb($_POST['login_restore']));

    $sSqlUser = "SELECT * FROM users WHERE UPPER(login) = '$login' ";
    $aUser = $oDb->getRows($sSqlUser);
    if(1 != count($aUser))
        return ERR_NO_USER_FOUND;

    // set new password
    srand( (double) microtime()*1000000);
    $sNewPass = substr(md5(uniqid(rand(),1)), 0, 10);
    $sNewPassMD5 = md5(SALT . $sNewPass . SALT);
	$sNewPassHash = md5(SALT . $sNewPassMD5 . SALT);
    $sSqlNewPass = "UPDATE users SET pass_new = '$sNewPassMD5', pass_hash = '$sNewPassHash' WHERE UPPER(login) = '$login' ";

    if(!$oDb->query($sSqlNewPass))
        return ERR_WHILE_RESTORE;

    // mail new pass
    $oTplMsg = new Smarty;
    $oTplMsg->assign('user', $aUser[0]);
    $oTplMsg->assign('pass', $sNewPass);
	$oTplMsg->assign('pass_hash', $sNewPassHash);
    $strMessage = $oTplMsg->fetch('notifications/restore.tpl');
	Notification::SendBody($aUser[0]['email'], SUBJ_RESTORE_PASS, $strMessage);
	return null;
}

?>
