<?php
require_once ('../component/libs.inc');


$vendors = getHash('name');


$template = $params['tpl_name'];

$letters = array('A','B','C','D','E','F',
	'G','H','I','J','K','L', 'M','N','O','P','Q','R',
	'S','T','U','V','W','X','Y','Z');

function getHash($field){
	$db = database::get();
	$array = $db->getRows("SELECT DISTINCT $field 
								FROM articles AS a1 WHERE control = 'company'");
	$arr = array();
	foreach ($array as $i){
		$arr[] = $i["$field"];
	}
	return $arr;
}

//var_dump($vendors);
$arr = array();
foreach ($letters as $letter) {
	foreach ($vendors as $v){
		$ven = strtoupper($v);
		if (preg_match("/^$letter\w*/", $ven)){
			$arr[$letter][] = $v;
		}
	}
}



$tpl = new Smarty;
$tpl->assign('letters', $letters);
$tpl->assign('vendors', $arr);
$tpl->assign('collections', $collections);
$tpl->assign('countres', $countres);
$tpl->assign('locations', $locations);
$tpl->display($template);