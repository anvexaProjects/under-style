<?php
/*
  Description: Authorization module
  Autor:       Oleg Koshkin
  Data:        20-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
  session_start();
  require_once '../lib/smarty/Smarty.class.php';
  require_once '../component/admin_libs.inc';

   $location = $params['location'];
  $flag = false;
  // control acess
 
  if (isset($_SESSION['role']) && !empty($_SESSION['role']) && $_SESSION['role'] != 1){
  		if (($_SESSION['role'] == SALER || $_SESSION['role'] == SALERS_REDACTOR)&&($location == 'salers' || $location == 'all')) $flag = false;
  		elseif (($_SESSION['role'] == DESIGNER || $_SESSION['role'] == DESIGNERS_REDACTOR)&&($location == 'des_arc' || $location == 'all')) $flag = false;
  		else $flag = true;
  }

// skip role check for now
//$flag = true;

  if ($flag || !isset($_SESSION['login']) || empty($_SESSION['login']))
  {
    header('Location: ../../pub/login.php?location='.$location.'');
     UI::Redirect('../../pub/login.php?location='.$location.'');
  }

?>