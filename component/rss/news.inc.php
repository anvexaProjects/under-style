<?php                                
/*
  Description: RSS-feed for last news
  Autor:       Sergey Pechenyuk
  Data:        19.01.2014
  Version:     1.0

  mailto:      sergey.pechenyuk@gmail.com
*/

	$count = 20;

    require_once '../component/libs.inc';

	$oDb = Database::get();
	$sql = "SELECT s.created, c.content_id, c.path, c.name, c.content FROM `art_struct` s, art_content c WHERE  s.parent_id=35 and s.struct_id=c.struct_id and s.is_deleted = 0 order by s.created desc limit {$count}";
	$rowset = $oDb->getRows($sql);

	header("Content-Type: application/rss+xml; charset=utf-8");
	echo '<?xml version="1.0" encoding="utf-8"?>'."\n";

	echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'."\n";
	echo "\t<channel>\n";
	echo "\t<atom:link href=\"http://".DOMAIN_0."/rss/news\" rel=\"self\" type=\"application/rss+xml\" />\n";
	echo "\t<title>Новости сайта</title>\n";
	echo "\t<link>http://".DOMAIN_0."/</link>\n";
	echo "\t<description>Новости сайта</description>\n";
	echo "\t<language>ru</language>\n";
	echo "\t<image>\n";
	echo "\t\t<url>http://".DOMAIN_0."/images/site/logo.gif?v=2</url>\n";
	echo "\t\t<title>Новости сайта</title>\n";
	echo "\t\t<link>http://".DOMAIN_0."/</link>\n";
	echo "\t</image>\n";

	if (!empty($rowset) && count($rowset)>0) {
		foreach ($rowset as $row) {
			echo "\t<item>\n";
			echo "\t\t<title>".htmlspecialchars($row['name'])."</title>\n";
			echo "\t\t<link>http://".DOMAIN_0."/{$row['path']}</link>\n";
			echo "\t\t<description>".str_replace("\n", "", str_replace("\r", "", htmlspecialchars(strip_tags($row['content']))))."</description>\n";
			echo "\t\t<pubDate>".date("r", strtotime($row['created']))."</pubDate>\n";
			echo "\t\t<guid>http://".DOMAIN_0."/{$row['path']}</guid>\n";
			echo "\t</item>\n";
		}
	}
	echo "\t</channel>\n";
	echo "\t</rss>\n";
?>