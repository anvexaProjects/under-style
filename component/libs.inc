<?php

    require_once '../core/settings.inc';
    require_once '../lib/infrastruct/url.class.php';
	
    $url = new URL();
    $url->Init(SITE_REL);
	
    require_once SMARTY_TEMPLATE_DIR . '/config/user_settings.inc';
    require_once SMARTY_TEMPLATE_DIR.'/config/'.URL::GetLan().'_labels.inc';
    require_once SMARTY_TEMPLATE_DIR.'/config/'.URL::GetLan().'_strings.inc';
    require_once SMARTY_TEMPLATE_DIR.'/config/'.URL::GetLan().'_enum.inc';
    require_once SMARTY_TEMPLATE_DIR.'/config/enum.inc';
                                          

    require_once '../lib/infrastruct/logger.class.php';
    require_once '../lib/infrastruct/util.class.php';
    require_once '../lib/infrastruct/validator.class.php';
    require_once '../lib/smarty/Smarty.class.php';
    require_once '../component/bl/ui.class.php';
    require_once '../lib/pear/PEAR.php';

	// core
    require_once '../lib/core/sorter.class.php';
    require_once '../lib/core/filter.class.php';
    require_once '../lib/core/pager.class.php';
    require_once '../lib/core/core_url.class.php';
    require_once '../lib/core/database.class.php';
    require_once '../lib/core/request.class.php';

    require_once '../lib/core/debug.inc.php';

    // content values replacer
    $cont_from = array('../../images/', '../images/');
    $cont_to   = array(URL::GetRoot() . '/images/', URL::GetRoot() . '/images/');
 
?>