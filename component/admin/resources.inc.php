<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Abratchuk
 * Date: 26.12.11
 * ver 1.2
 * 1.1: working mode with resources - add|edit|delete
 * 1.2: no QuickForm, added checkings , moved to _ajax
 * 1.3: no escaping
 */

require_once '../component/admin_libs.inc';
require_once '../component/bl/admin/resource_manager.class.php';



if('XMLHttpRequest' == $_SERVER["HTTP_X_REQUESTED_WITH"])
{
//all code moved to resources_ajax.php
}
else
{
    if ( ! isset($_GET['m']))
    {
        $mode = 'labels';
    }
    else
    {
        $mode = $_GET['m'];
    }

    if ( ! isset($_GET['l']))
    {
        $lang = DEFAULT_LANG;
    }
    else
    {
        $lang = $_GET['l'];
    }

    $resman = new ResourceManager($lang);

    $items = $resman->GetResource($mode);

    $itemsArray = array();

// For correct viewing in text-inputs double quotations replace them to quot   -- moved to template |escape
    foreach ($items as $resource)
        $itemsArray = array_merge($itemsArray, array($resource['resKey']=>
                        array("resVal"=>$resource['resVal'] , "canDelete"=>$resource['canDelete']) )) ;

    $tpl = new Smarty;


    $tpl->assign('items', $itemsArray);
    $tpl->assign('mode', $mode);
    $tpl->assign('lang', $lang);

	$tpl->display('admin/'.CMS.'/resources_list.tpl');
}

?>