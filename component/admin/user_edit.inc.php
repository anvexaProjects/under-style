<?php
/*
Description: Edit user
Autor:       Oleg Koshkin
Data:        05-02-2008
Version:     1.3

mailto:      WizardF@narod.ru
copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../component/admin_libs.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require '../templates/config/enum.inc';
require_once '../component/bl/entity/user.class.php';
$message = ''; // error message
$id = 0;

if  (isset($_REQUEST['id']) && Val::IsNumber($_REQUEST['id']))  
{    
    $id = $_REQUEST['id'];
}

$obj = new User();
$mode  = (!$id) ? 'add' : 'edit';
$form  = renderForm($mode, $id, $enum);

if ('edit' == $mode)
{
    $obj->load($id);
    $role = $obj->aData['user_type'];
    if (empty($obj->aData))
    {
        $message = ERR_USER_NOT_FOUND;
    }
    else
    {
        $form->setDefaults($obj->aData);
    }
}

if ($form->validate())
{
   
    if ('add' == $mode) 
    {
        // check if specified user already exists
        $res = $obj->loadByCondition('login = "'.addslashes($_POST['login']).'"');
        if (!empty($res))
        {
           $form->setElementError('login', ERR_USER_ALREADY_REGISTERED); 
        }
        else
        {
            $obj->setData($_POST);
            $obj->aData['pass'] = md5(SALT . $_POST['pass1'] . SALT);
            $insert_id = $obj->insert();
        }
    }
    elseif ('edit' == $mode)
    {
        // delete items which isn't exist in DB
        $aFields = $_POST;
        foreach ($aFields as $k=>$v)
        {
            if (!isset($obj->aFields[$k])) unset($aFields[$k]);
        } 
        
        if (!Val::IsEmpty($_POST['pass1']))
        {
            $obj->aData['pass'] = md5(SALT . $_POST['pass1'] . SALT);
            $aFields['pass'] = $obj->aData['pass'];
            $_POST['pass'] = $obj->aData['pass'];
        } 
        $obj->setData($_POST);
        $insert_id = $id;
        
          
        $obj->update(array_keys($aFields));
    }
    if (isset($_POST['save_close'])) {
        UI::Redirect('users.php');
    }
    elseif (isset($_POST['save'])) {
        UI::Redirect('user_edit.php?id='.$insert_id.'&s=1');
    }        
}     

if (empty($message))
{
    $tpl = new Smarty;      
    $renderer = new HTML_QuickForm_Renderer_ArraySmarty($tpl);
    
    // build the HTML for the form
    $form->accept($renderer);
    $tpl->assign('form_data', $renderer->toArray());
    $tpl->assign('mode', $mode);
    $tpl->assign('user_fields', $user_fields);
    
    $sTemplate = 'admin/'.CMS.'/user_edit.tpl';
    $tpl->display($sTemplate);
}
else
{
    echo UI::GetWarning($message);
}

function renderForm($mode, $id, $enum) 
{
    if ('edit' == $mode) 
    {
        $disabled = 'disabled="disabled"';
    }
    $form = new HTML_QuickForm('fssrm', 'post');

    $form->addElement('hidden', 'id', $id);
    $form->addElement('hidden', 'registered', date('Y-m-d H:i:s'));
    
    $form->addElement('text', 'login', '', ' class="text" id="login" autocomplete="off" ' . $disabled);
    $form->addElement('text', 'name', '', ' class="text" id="name" ');
    
    $form->addElement('select', 'role', '', $enum['roles'], " id='prop4' ");
    
    $form->addElement('password', 'pass1', '', ' class="text" id="pass1" autocomplete="off" ');
    $form->addElement('password', 'pass2', '', ' class="text" id="pass2" autocomplete="off" ');
    
    $form->addElement('text', 'email', '', ' class="text" id="email" ');
    $form->addElement('advcheckbox', 'is_locked', '','', " class='check' id = 'is_locked' ", array('0','1'));
    
    $form->addElement('textarea', 'comment', '', ' class="text" id="comment" rows="5" ');
    
    $form->addRule(array('pass1', 'pass2'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq', 'client');
    $form->addRule(array('pass1', 'pass2'), FLB_INVALID_PASS_AND_CONFIRM, 'compare', 'eq');
    
    
    if ('edit' != $mode)
    {
        $form->addRule('login', FLB_ADD_LOGIN, 'required', '', 'client');
        $form->addRule('login', FLB_ADD_LOGIN, 'required');
        $form->addRule('pass1', FLB_ADD_PASS, 'required', '', 'client');
        $form->addRule('pass1', FLB_ADD_PASS, 'required');
        $form->addRule('pass2', FLB_ADD_PASS_CONFIRM, 'required', '', 'client');
        $form->addRule('pass2', FLB_ADD_PASS_CONFIRM, 'required');
        //$form->addRule('login', VAL_SYM_LOGIN, 'regex', RX_LOGIN);
    }
    $form->addRule('pass1', FLB_INVALID_PASS_LENGTH, 'minlength', 5, 'client');
    $form->addRule('pass1', FLB_INVALID_PASS_LENGTH, 'minlength', 5);
    $form->addRule('pass1', FLB_INVALID_PASS_LENGTH, 'maxlength', 32, 'client');
    $form->addRule('pass1', FLB_INVALID_PASS_LENGTH, 'maxlength', 32);
    $form->addRule('email', FLB_INVALID_EMAIL, 'email', '', 'client');
    $form->addRule('email', FLB_INVALID_EMAIL, 'email');
    
    
    return $form;
}   
?>