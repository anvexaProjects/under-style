<?php                                
    require_once '../lib/smarty/Smarty.class.php';
    require '../templates/config/enum.inc';
	
	$tpl = new Smarty();
	$tpl->assign('langs', $enum['langs']);
	$tpl->display('admin/'.CMS.'/articles.tpl');
?>