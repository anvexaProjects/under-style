<?php

/*
Description: Edit article handler
Autor:       Oleg Koshkin
Data:        16-06-2008
Version:     1.1

mailto:      WizardF@narod.ru
copyright:   (C) 2008 Oleg Koshkin
*/

require '../templates/config/enum.inc';
require '../templates/config/'.CMS_LANG.'_enum.inc';
require_once '../component/admin_libs.inc';
require_once '../component/bl/admin/imager.class.php';
require_once '../component/bl/admin/artlocaleditor.class.php';
require_once '../component/bl/admin/arttageditor.class.php';
require_once '../component/bl/lister/taglister.class.php';

//if ('top' == $_REQUEST['control'] || 'left' == $_REQUEST['control'])
	$arteditor = new ArtTagEditor();
//else
//	$arteditor = new ArtLocalEditor();

/*$query = 'SELECT * FROM art_struct LIMIT 1, 100';
$res = mysql_query($query);
while($row = mysql_fetch_array($res))
{
    print_r('<div class="man666" style="display:none;">');
    foreach($row as $key=>$value)
    {
         if($key=="control")print_r('['.$key.']=>'.$value.' ');
    }
   
    print_r('</div>');
}
*/	
$arteditor->loadData();
$form = $arteditor->getForm();
customizeForm($arteditor, $arteditor->loadUniqueProperty('name', "control='company' ORDER BY name ASC ", 'articles'));


//if ($_POST) {p($_POST);exit;}
if ($form->validate()) {
	// process form
	
    $data = $form->process('processData', false);
    $rs = $arteditor->perform($data);

	// adding watermarks
	if (WATERMARK_ENABLE && $_REQUEST['control'] != 'header_banner')
	{
		foreach (ArtConfig::getUploadConfig($_REQUEST['control']) as $index => $uploadItem)
		{
			foreach ($uploadItem as $item)
			{
				if (!empty($_FILES['img'.$index]))
				{
					$imagePath = SITE_ABS.$item['dir'].'/'.$arteditor->defaultValues['img'.$index];
					list($width, $height) = getimagesize($imagePath);
					$watermarkPath = SITE_ABS.sprintf(WATERMARK_PATH, Util::getValueFromRange($enum['watermarks'], $width));
					Util::addWatermark($imagePath, $watermarkPath);
				}
			}
		}
	}
	$iobg = new Imager();
	if ($_REQUEST['control'] == 'designer_project')
	{
		$iobg->deleteDots($_POST['showroom_image_id']);
		if (sizeof($_POST['showroom_x']) > 1)
		{
			for ($i = 0; $i < sizeof($_POST['showroom_x']) - 1; $i ++)
			{
				$data = array(
					'image_id' => $_POST['showroom_image_id'],
					'x' => $_POST['showroom_x'][$i],
					'x_pct' => $_POST['showroom_x_pct'][$i],
					'y' => $_POST['showroom_y'][$i],
					'y_pct' => $_POST['showroom_y_pct'][$i],
					'description' => $_POST['showroom_description'][$i],
					'link' => $_POST['showroom_link'][$i]
				);
				$iobg->addDot($data);
			}
		}
	}

    processTags($_POST['tag'], $arteditor->articleId);

	$id = ($_REQUEST['id']) ? $_REQUEST['id'] : $arteditor->articleId.preg_replace('/\d+/', '', $_REQUEST['parent_id']);
    if (true === $rs) {
		$iobg->fillArtIds($arteditor->articleId, $_POST['image_hash']);
        if (isset($_POST['save_close'])) {
            if (!Val::IsEmpty($_POST['bur'])) UI::Redirect($_POST['bur']);
            else UI::Redirect('index.php?bid='.$_REQUEST['parent_id'].($_REQUEST['parent_id'] > 1 ? preg_replace('/\d+/', '', $_REQUEST['id']) : ''));
        }
        elseif (isset($_POST['save'])) {
            UI::Redirect('art_edit.php?control='.$_REQUEST['control'].'&id='.$id.'&s=1');
        }
    } else {
        foreach ($rs as $el => $msg) $form->setElementError($el, $msg);
    }

}


// display form
$form->setDefaults($arteditor->defaultValues);

$tpl = new Smarty;
$renderer = new HTML_QuickForm_Renderer_ArraySmarty($tpl);

// build the HTML for the form
$form->accept($renderer);
$tpl->assign('form_data',  $renderer->toArray());
#echo "<pre>";
#print_r($renderer->toArray());
$tpl->assign('user_agent',  strpos($_SERVER["HTTP_USER_AGENT"],"MSIE"));
$tpl->assign('default',   $arteditor->defaultValues);
$tpl->assign('langs', $enum['langs']);
$tpl->assign('enum', $enum);
$tpl->assign('tabs', ArtConfig::getTabsConfig($_REQUEST['control']));
$tpl->assign('upl',  ArtConfig::getUploadConfig($_REQUEST['control']));
$tpl->assign('bur', 'index.php?bid='.$arteditor->defaultValues['parent_id'].($arteditor->defaultValues['parent_id'] > 1 ? preg_replace('/\d+/', '', $_REQUEST['id']) : ''));
if ($_REQUEST['control'] == 'designer_project')
{
	$show_room_cfg = ArtConfig::getUploadConfig($_REQUEST['control']);
	$tpl->assign('showroom_dir', $show_room_cfg[0][0]['dir']);

	$imager = new Imager();
	$tpl->assign('showroom_image', $imager->getItem(intval($_REQUEST['iid'])));
	$tpl->assign('showroom_dots', $imager->getDots(intval($_REQUEST['iid'])));
}

//$template_name = $arteditor->getTemplateName(SITE_ABS.'/templates/admin/'.CMS.'/edit/', $_REQUEST['control']);
//$tpl->display("admin/".CMS."/edit/$template_name");
if ($_REQUEST['control'] == 'admin')
    $tpl->display("admin/".CMS."/edit/edit_admin.tpl");
else
    $tpl->display("admin/".CMS."/edit/edit.tpl");


function customizeForm($arteditor, $vendors)
{
    if ($_REQUEST['control'] == 'subdomen')
    {
        for ($i = 1; $i < 7; $i++){
            $arteditor->localizeElements('advcheckbox', 'sprop'.$i, '', ' class="check" id="sprop'.$i.'"', array('0','1'));
        }
    }
    if ($_REQUEST['control'] == 'prod'){
        $arteditor->localizeElements('advcheckbox', 'sprop1', '', 'class="check" id="sprop1"', array('0','1'));
        $arteditor->localizeElements('advcheckbox', 'sprop2', '', 'class="check" id="sprop2"', array('0','1'));
    } else{
        $arteditor->localizeElements('text', 'sprop1', '', 'class="text" id="sprop1"');
        $arteditor->localizeElements('text', 'sprop2', '', 'class="text" id="sprop2"');
    }
    if ($_REQUEST['control'] == 'collection' || $_REQUEST['control'] == 'prod'){
        $arteditor->localizeElements('select', 'sprop3', '', 'id="sprop3"', $vendors);
    }
    else{
        $arteditor->localizeElements('text', 'sprop3', '', 'class="text" id="sprop3"');
    }
    $arteditor->localizeElements('text', 'sprop4', '', 'class="text" id="sprop4"');
    $arteditor->localizeElements('text', 'sprop5', '', 'class="text" id="sprop5"');
    $arteditor->localizeElements('text', 'sprop6', '', 'class="text" id="sprop6"');
       $arteditor->localizeElements('checkbox', 'sprop7', '', 'class="text" id="sprop7"');

	$arteditor->localizeElements('text', 'sprop11', '', 'class="text" id="sprop11"');
    $arteditor->localizeElements('text', 'sprop12', '', 'class="text" id="sprop12"');
    $arteditor->localizeElements('text', 'sprop13', '', 'class="text" id="sprop13"');
    $arteditor->localizeElements('advcheckbox', 'sprop14', '', 'class="check" id="sprop14"', array('0','1'));
    if ($_REQUEST['control'] == 'home'){
    	$arteditor->localizeElements('text', 'sprop7', '', 'class="text" id="sprop7"');
    	$arteditor->localizeElements('text', 'sprop8', '', 'class="text" id="sprop8"');
    	$arteditor->localizeElements('text', 'sprop9', '', 'class="text" id="sprop9"');
    	$arteditor->localizeElements('text', 'sprop10', '', 'class="text" id="sprop10"');
    } else {
    	$arteditor->localizeElements('advcheckbox', 'sprop7', '', 'class="check" id="sprop7"', array('0','1'));
    	$arteditor->localizeElements('advcheckbox', 'sprop8', '', 'class="check" id="sprop8"', array('0','1'));
    	$arteditor->localizeElements('advcheckbox', 'sprop9', '', 'class="check" id="sprop9"', array('0','1'));
    	$arteditor->localizeElements('advcheckbox', 'sprop10', '', 'class="check" id="sprop10"', array('0','1'));
    }
	
	$arteditor->localizeElements('text', 'cprop1', '', 'class="text" id="cprop1"');
    $arteditor->localizeElements('text', 'cprop2', '', 'class="text" id="cprop2"');
    $arteditor->localizeElements('text', 'cprop3', '', 'class="text" id="cprop3"');
    $arteditor->localizeElements('text', 'cprop4', '', 'class="text" id="cprop4"');
    $arteditor->localizeElements('text', 'cprop5', '', 'class="text" id="cprop5"');
    $arteditor->localizeElements('text', 'cprop6', '', 'class="text" id="cprop6"');
  	
    $arteditor->localizeElements('text', 'tag', '', 'class="text" id="tag"');


    //$arteditor->localizeElements('textarea', 'video_code', '', 'class="text" id="video_code"');

    return $arteditor;
}

function processData($data)
{
    return $data;
}

function processTags($tags, $article_id)
{
    $taglister = new TagLister();
    $taglister->processTags($tags, $article_id);
}


?>