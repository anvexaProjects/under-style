<?php
require '../component/admin_libs.inc';
require '../templates/config/enum.inc';
require_once '../component/bl/subscription/subscription_contact.class.php';
require_once '../component/bl/subscription/subscription_contact_group.class.php';
require_once '../component/bl/subscription/subscription_contact_group.class.php';

require_once '../lib/3rdparty/swift/swift_required.php';
require_once '../lib/3rdparty/swift/classes/Swift/Mime/Headers/MailboxHeaderValidator.php';



unset($_GET['path']);

$obj = new SubscriptionContact();
$obj_g = new SubscriptionContactGroup();
$oUrl = new CoreUrl();
$oReq = new Request($oUrl);
$errors = array();

if (isset($_POST['contact_add'])) {
	$mailValidator = new Swift_Mime_Headers_MailboxHeaderValidator();
	if (!$mailValidator->isEmail($_REQUEST['email'])) $errors[] = FLB_INVALID_EMAIL;
	elseif ($obj->isContactInGroupExists($_REQUEST['email'], $_REQUEST['group'])) $errors[] = sprintf(ERR_USER_GROUP_ALREADY_REGISTERED, $_REQUEST['email']);
	if (empty($errors)) {
		$data = array();
		$_POST['created'] = date('Y-m-d H:i:s');
		$_POST['hash'] = md5(time().rand(1, 999));
		$obj->setData($_POST);
		$obj->insert();
		UI::Redirect ('subscription.php?a=manage_contacts&s=1');
	}
}



//filter
$sFilterName = 'fltr-subs-contacts-list';
$oFilter = new Filter($sFilterName, getFilterFields($enum['bool'], $obj_g->getHashNames()));
$aCond = $oFilter->getSql();

// Sorter
$aFields = array('fname', 'lname', 'email', 'is_active', 'is_test', 'created', 'group_name');
$aSortField[$oReq->get('field', 'created')] = $oReq->get('order', 'down');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder();

// Pager
$iPagePerFrame = MAX_PAGES;
$iPageSize = $oReq->get('page_size', USERS_LIMIT);
$iPage = $oReq->getInt('page', 1);

list($itemsList, $iCnt) = $obj->getList($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);
$tpl = new Smarty;
$tpl->assign('items', $itemsList);
$tpl->assign('aSorting', $oSorter->getSorting($oUrl));
$tpl->assign('aPaging', $oPager->getInfo($oUrl));
$tpl->assign('aFilter', $oFilter->aHtml);
$tpl->assign('bool', $enum['bool']);
$tpl->assign('groups', $obj_g->getHashIdName());
$tpl->assign('error_msg', $errors);
$tpl->display('admin/'.CMS.'/subscription/manage_contacts.tpl');



function getFilterFields($bool, $groups)
{
    $aFilterFields = array(
        'fname' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
        'lname' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
        'email' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
		'group_name' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $groups,
            'all_option' => array(
                'name' => '---',
                'value' => '',
            ),
        ),
		'is_active' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $bool,
            'all_option' => array(
                'name' => '---',
                'value' => '',
            ),
        ),
		'is_test' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $bool,
            'all_option' => array(
                'name' => '---',
                'value' => '',
            ),
        ),
    );
    return $aFilterFields;
}
?>