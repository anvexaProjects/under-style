<?php
require '../component/admin_libs.inc';
require '../templates/config/enum.inc';
require_once '../component/bl/subscription/subscription_contact_group.class.php';

unset($_GET['path']);

$obj = new SubscriptionContactGroup();
$oUrl = new CoreUrl();
$oReq = new Request($oUrl);
$errors = array();

if (isset($_POST['group_add'])) {
	if (!isset($_REQUEST['name']) || Val::IsEmpty($_REQUEST['name'])) $errors[] = sprintf(FLB_REQUIRED, S_NAME);
	if (empty($errors)) {
		$data = array();
		$_POST['created'] = date('Y-m-d H:i:s');
		$obj->setData($_POST);
		$obj->insert();
		UI::Redirect('subscription.php?a=manage_contacts_groups&s=1');
	}
}

//filter
$sFilterName = 'fltr-subs-contacts-groups-list';
$oFilter = new Filter($sFilterName, getFilterFields($enum['bool']));
$aCond = $oFilter->getSql();

// Sorter
$aFields = array('id', 'name', 'created');
$aSortField[$oReq->get('field', 'name')] = $oReq->get('order', 'up');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder();

// Pager
$iPagePerFrame = MAX_PAGES;
$iPageSize = $oReq->get('page_size', USERS_LIMIT);
$iPage = $oReq->getInt('page', 1);

list($itemsList, $iCnt) = $obj->getList($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);
$tpl = new Smarty;
$tpl->assign('items', $itemsList);
$tpl->assign('aSorting', $oSorter->getSorting($oUrl));
$tpl->assign('aPaging', $oPager->getInfo($oUrl));
$tpl->assign('aFilter', $oFilter->aHtml);
$tpl->assign('bool', $enum['bool']);
$tpl->assign('error_msg', $errors);
$tpl->display('admin/'.CMS.'/subscription/manage_contacts_groups.tpl');



function getFilterFields($bool)
{
    $aFilterFields = array(
        'name' => array(
            'type' => 'text',
            'condition' => 'like',
        )
    );
    return $aFilterFields;
}
?>