<?php
require '../component/admin_libs.inc';
require '../templates/config/enum.inc';
require_once '../component/bl/subscription/subscription_queue.class.php';

unset($_GET['path']);

$obj = new SubscriptionQueue();
$oUrl = new CoreUrl();
$oReq = new Request($oUrl);

//filter
$sFilterName = 'fltr-subs-stat-list';
$oFilter = new Filter($sFilterName, getFilterFields($enum['bool']));
$aCond = $oFilter->getSql();

// Sorter
$aFields = array('subject', 'contacts_num', 'is_test', 'created');
$aSortField[$oReq->get('field', 'created')] = $oReq->get('order', 'down');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder();

// Pager
$iPagePerFrame = MAX_PAGES;
$iPageSize = $oReq->get('page_size', USERS_LIMIT);
$iPage = $oReq->getInt('page', 1);

list($itemsList, $iCnt) = $obj->getStatistics($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);
$tpl = new Smarty;
$tpl->assign('items', $itemsList);
$tpl->assign('aSorting', $oSorter->getSorting($oUrl));
$tpl->assign('aPaging', $oPager->getInfo($oUrl));
$tpl->assign('aFilter', $oFilter->aHtml);
$tpl->assign('bool', $enum['bool']);
$tpl->display('admin/'.CMS.'/subscription/statistics.tpl');

function getFilterFields($bool)
{
    $aFilterFields = array(
        'subject' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
		'is_test' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $bool,
            'all_option' => array(
                'name' => '---',
                'value' => '',
            ),
        ),
    );
    return $aFilterFields;
}
?>