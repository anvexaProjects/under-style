<?php
require '../component/admin_libs.inc';
require_once '../component/bl/subscription/subscription_contact.class.php';
require_once '../component/bl/subscription/subscription_contact_group.class.php';

require_once '../lib/3rdparty/swift/swift_required.php';
require_once '../lib/3rdparty/swift/classes/Swift/Mime/Headers/MailboxHeaderValidator.php';


$obj = new SubscriptionContact();
$obj_g = new SubscriptionContactGroup();

$errors = array();

if (isset($_POST['import_text'])) {
	if (empty($_POST['import_separator'])) $errors[] = sprintf(FLB_REQUIRED, S_SEPARATOR);
	if (empty($_POST['import_emails'])) $errors[] = sprintf(FLB_REQUIRED, S_EMAILS);
	
	if (empty($errors)) $errors = processTextImport($obj, $errors);
    
    if (empty($errors)) UI::Redirect ('subscription.php?a=contacts_import&s=it');
}

if (isset($_POST['import_csv'])) {
    if (empty($_FILES['import_file']['name'])) $errors[] = S_UPLOAD_EMPTY_FILE;
    
    if (empty($errors)) $errors = processCsvImport($obj, $errors);
    
    if (empty($errors)) UI::Redirect ('subscription.php?a=contacts_import&s=if');
    
}

$tpl = new Smarty;
$tpl->assign('error_msg', $errors);
$tpl->assign('groups', $obj_g->getHashIdName());
$tpl->display('admin/'.CMS.'/subscription/contacts_import.tpl');


function processTextImport($obj, $errors) {
	$emails = explode($_POST['import_separator'], $_POST['import_emails']);
	$mailValidator = new Swift_Mime_Headers_MailboxHeaderValidator();

	foreach($emails as $email) {
		$email = trim($email);
		if (!$mailValidator->isEmail($email)) $errors[] = FLB_INVALID_EMAIL .': '.$email;		
		elseif ($obj->isContactInGroupExists($email, $_REQUEST['group'])) $errors[] = sprintf(ERR_USER_GROUP_ALREADY_REGISTERED, $email);
		else {
            $data = array();
			$data['email'] = $email;
			$data['is_active'] = 1;
            $data['is_test'] = 0;
			$data['hash'] = md5(time().rand(1, 999));
			$data['created'] = date('Y-m-d H:i:s');
			$data['group'] = $_REQUEST['group'];
			$obj->setData($data);
			$obj->insert(true, true);
		}
	}
    return $errors;
}

function processCsvImport($obj, $errors) {
    Util::UploadFile('import_file', SITE_ABS.'/upload/', 'csv_import.csv');
    $data = file(SITE_ABS.'/upload/csv_import.csv');
//	$str = file_get_contents(SITE_ABS.'/upload/csv_import.csv');
//	$data = parse_csv($str);
    unlink(SITE_ABS.'/upload/csv_import.csv');
	$mailValidator = new Swift_Mime_Headers_MailboxHeaderValidator();
	
    foreach($data as $k => $v) {
        $exp = explode(';', $v);
        foreach($exp as $k2 => $v2) $exp[$k2] = trim($v2);
        
        if (sizeof($exp) < 1) $errors[] = S_UPLOAD_INCORRECT_COLUMNS_QTY.sprintf (S_UPLOAD_ROW, $k + 1);
        elseif (!$mailValidator->isEmail($exp[0])) $errors[] = FLB_INVALID_EMAIL.' - '.$exp[0].sprintf (S_UPLOAD_ROW, $k + 1);
        else {
            $data = array();
			$data['email'] = !empty($exp[0]) ? trim($exp[0]) : '';
            $data['fname'] = !empty($exp[1]) ? trim($exp[1]) : '';
            $data['lname'] = !empty($exp[2]) ? trim($exp[2]) : '';
			$data['group'] = !empty($exp[3]) ? trim($exp[3]) : 1;
			$data['is_active'] = 1;
            $data['is_test'] = 0;
			$data['hash'] = md5(time().rand(1, 999));
			$data['created'] = date('Y-m-d H:i:s');
			$obj->setData($data);
			$obj->insert(true);            
            
        }
    }
    return $errors;
    
}

function parse_csv($file,$comma=',',$quote='"',$newline="\n") {
     
     $db_quote = $quote . $quote;
     
     // Clean up file
     $file = trim($file);
     $file = str_replace("\r\n",$newline,$file);
     
     $file = str_replace($db_quote,'&quot;',$file); // replace double quotes with &quot; HTML entities
     $file = str_replace(',&quot;,',',,',$file); // handle ,"", empty cells correctly
   
     $file .= $comma; // Put a comma on the end, so we parse last cell  
   
       
     $inquotes = false;
     $start_point = 0;
     $row = 0;
     
     for($i=0; $i<strlen($file); $i++) {
         
         $char = $file[$i];
         if ($char == $quote) {
             if ($inquotes) {
                 $inquotes = false;
                 }
             else { 
                 $inquotes = true;
                 }
             }        
         
         if (($char == $comma or $char == $newline) and !$inquotes) {
             $cell = substr($file,$start_point,$i-$start_point);
             $cell = str_replace($quote,'',$cell); // Remove delimiter quotes
             $cell = str_replace('&quot;',$quote,$cell); // Add in data quotes
             $data[$row][] = $cell;
             $start_point = $i + 1;
             if ($char == $newline) {
                 $row ++;
                 }
             }
         }
     return $data;
     }

?>