<?php
require '../component/admin_libs.inc';
require_once '../component/bl/subscription/subscription_queue.class.php';
require_once '../component/bl/subscription/subscription_contact.class.php';
require_once '../component/bl/subscription/subscription_template.class.php';
require_once '../component/bl/subscription/subscription_mailing.class.php';
require_once '../component/bl/subscription/subscription_contact_group.class.php';

$obj_c = new SubscriptionContact();
$obj_t = new SubscriptionTemplate();
$obj_g = new SubscriptionContactGroup();

$obj_t->sId = 'id';
$errors = array();
$tpls = array();

$id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : 0;

// load selected/default template
if ($id != 0) {
	$obj_t->load((int) $id);
} else {
	$obj_t->loadByCondition('is_default = 1 AND is_deleted = 0');
}
$ctpl = $obj_t->aData;
$id = $ctpl['id'];

// load templates list
list($tpls, $n) = $obj_t->getList(array("is_deleted = 0"));

// update template
if (isset($_POST['newmail_save'])) {
	if (empty($_POST['newmail_subj'])) $errors[] = sprintf(FLB_REQUIRED, S_SUBJECT);

	if (empty($errors) && $id == 0) list($id, $errors) = saveTemplate($obj_t, $id, $errors);
    if (empty($errors) && $id != 0) list($id, $errors) = updateTemplate($obj_t, $id, $errors);

	if (empty($errors)) UI::Redirect ('subscription.php?s=1&id='.$id);
}

// save template as new
if (isset($_POST['newmail_save_as_new'])) {
    if (empty($_POST['newmail_subj'])) $errors[] = sprintf(FLB_REQUIRED, S_SUBJECT);
    list($id, $errors) = saveTemplate($obj_t, $id, $errors);
    if (empty($errors)) UI::Redirect ('subscription.php?s=1&id='.$id);
}

// set default template
if (isset($_POST['newmail_set_default'])) {
    setDefaultTemplate($obj_t, $id, $errors);
    if (empty($errors)) UI::Redirect ('subscription.php?s=1&id='.$id);
}

// adding queue and its items
if (isset($_POST['newmail_send']) || isset($_POST['newmail_send_test']))
{

    if (isset($_POST['newmail_send_test'])) $is_test = 1;
    else $is_test = 0;

	if ($is_test == true) $cond = 'is_active = 1 AND is_test = 1';
	else $cond = 'is_active = 1';
	$cond .= " AND group_id = '".$_POST['group']."'";
	list($contacts, $n) = $obj_c->getList(array($cond));

    $obj_q = new SubscriptionQueue();
    $data = array(
        'subject' => $_POST['newmail_subj'],
        'template' => $_POST['newemail_text'],
        'template_translated' => $_POST['newemail_trans_text'],
        'is_test' => $is_test,
        'created' => date('Y-m-d H:i:s')
    );
    $queue_id = $obj_q->addQueue($data, $contacts);
    $obj_q->processQueue($queue_id, $contacts);
}

$tpl = new Smarty;
$tpl->assign('id', $id);
$tpl->assign('groups', $obj_g->getHashIdName());
$tpl->assign('tpls', $tpls);
$tpl->assign('ctpl', $ctpl);
$tpl->assign('error_msg', $errors);
$tpl->display('admin/'.CMS.'/subscription/new_email.tpl');


function setDefaultTemplate($obj, $id, $errors) {
    $obj->setDefaultTemplate($id);
    return array($id, $errors);
}

function saveTemplate($obj, $id, $errors) {
	$data = array();
	$data['subject'] = $_POST['newmail_subj'];
	$data['template'] = $_POST['newemail_text'];
	$data['template_translated'] = $_POST['newemail_trans_text'];
	$data['is_default'] = 0;
	$data['is_deleted'] = 0;
	$data['created'] = date('Y-m-d H:i:s');
    $obj->setData($data);
    unset($obj->aData['id']);
    $id = $obj->insert();
	return array($id, $errors);
}

function updateTemplate($obj, $id, $errors) {
	$data = array();
	$data['subject'] = $_POST['newmail_subj'];
	$data['template'] = $_POST['newemail_text'];
	$data['template_translated'] = $_POST['newemail_trans_text'];
    $data['id'] = $id;
    $obj->setData($data);
    $obj->update();
	return array($id, $errors);
}

?>