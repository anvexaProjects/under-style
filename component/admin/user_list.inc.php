<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require_once '../templates/config/enum.inc';
require_once '../component/bl/entity/user.class.php';
unset($_GET['path']);


$oUrl = new CoreUrl();
$oReq = new Request($oUrl);

//filter
$sFilterName = 'fltr-user-list';
$oFilter = new Filter($sFilterName, getFilterFields());
$aCond = $oFilter->getSql();
$aCond[] =  ' login != "admin" '; // hide admin

// Sorter
$aFields = array('is_locked', 'login', 'full_name', 'email', 'role', 'registered', 'logged');
$aSortField[$oReq->get('field', 'login')] = $oReq->get('order', 'up');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder(); 

// Pager
$iPagePerFrame = MAX_PAGES; 
$iPageSize = $oReq->get('page_size', USERS_LIMIT);
$iPage = $oReq->getInt('page', 1);

$obj = new User();
list($itemsList, $iCnt) = $obj->getList($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);
require '../templates/config/enum.inc';
$oTpl = new Smarty();
$oTpl->assign('roles', $enum['roles']);
$oTpl->assign('items', $itemsList);
$oTpl->assign('aSorting', $oSorter->getSorting($oUrl));
$oTpl->assign('aPaging', $oPager->getInfo($oUrl));
$oTpl->assign('aFilter', $oFilter->aHtml);  


$oTpl->display('admin/'.CMS.'/user_list.tpl');

function getFilterFields()
{
    require '../templates/config/enum.inc';
    $aFilterFields = array(
        'login' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
        'full_name' => array(
            'type' => 'text',
            'condition' => 'like',
        ),  
		'role' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $enum['roles'],
            'all_option' => array(
                'name' => '--Все роли--',
                'value' => 0,
            ),
        ),
    );
    return $aFilterFields;
}
?>