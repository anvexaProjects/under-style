<?php

require '../component/admin_libs.inc';
require '../templates/config/enum.inc';

$admin_menu['sitemap'] = array(
    'title' => S_SITEMAP,
    'path'  => 'index.php',
    'child' => array());


$admin_menu['users'] = array(
    'title' => S_USERS,
    'path'  => 'users.php',
    'child' => array(
        array(
            'title' => S_USERS_LIST,
            'path'  => 'users.php'),
        array(
            'title' => S_USER_ADD,
            'path'  => 'user_edit.php')
    )
);
$admin_menu['robots'] = array(
    'title' => S_ROBOTS,
    'path'  => 'robots.php',
    'child' => array());
// render config menu
if (isset($enum['langs']) && sizeof($enum['langs']) > 0)
{
	$admin_menu['config'] = array(
        'title' => S_CONFIG,
        'path'  => 'config.php'
	);
}
$admin_menu['slider'] = array(
    'title' => S_SLIDER,
    'path'  => 'slider.php',
    'child' => array());
foreach ($enum['langs'] as $id => $lang)
{
	$admin_menu['config']['child'][] = array(
		'title' => S_CONFIG_STRINGS." ($lang)",
		'path'  => "config.php?m=strings&l=$id"
	);
/*
	$admin_menu['config']['child'][] = array(
		'title' => S_CONFIG_LABELS." ($lang)",
		'path'  => "config.php?m=labels&l=$id"
	);
*/	
}

/*
// render new config menu - from database
if (isset($enum['langs']) && sizeof($enum['langs']) > 0)
{
    $admin_menu['resources'] = array(
        'title' => S_RESOURCES,
        'path'  => 'resources.php',
    );
}
foreach ($enum['langs'] as $id => $lang)
{
    $admin_menu['resources']['child'][] = array(
        'title' => S_CONFIG_STRINGS." ($lang)",
        'path'  => "resources.php?m=strings&l=$id"
    );
    $admin_menu['resources']['child'][] = array(
        'title' => S_CONFIG_LABELS." ($lang)",
        'path'  => "resources.php?m=labels&l=$id"
    );
}
*/


/*
$admin_menu['subscription'] = array(
    'title' => S_SUBSCRIPTION,
    'path'  => 'subscription.php',
    'child' => array(
        array(
            'title' => S_SUBSCRIPTION_NEW,
            'path' => 'subscription.php'
        ),
        array(
            'title' => S_SUBSCRIPTION_MANAGE_CONTACTS_GROUPS,
            'path' => 'subscription.php?a=manage_contacts_groups'
        ),
        array(
            'title' => S_SUBSCRIPTION_MANAGE_CONTACTS,
            'path' => 'subscription.php?a=manage_contacts'
        ),
        array(
            'title' => S_SUBSCRIPTION_CONTACTS_IMPORT_EXPORT,
            'path' => 'subscription.php?a=contacts_import'
        ),
        array(
            'title' => S_SUBSCRIPTION_STATISTICS,
            'path' => 'subscription.php?a=statistics'
        ),
        
    ));
*/

$admin_menu['db_admin'] = array(
    'title' => S_DBADMIN,
    'path'  => 'db_admin.php',
    'child' => array());


$admin_menu['exit'] = array(
    'title' => S_LOGOUT,
    'path'  => 'exit.php',
    'child' => array());

if('admin' != $_SESSION['login'] && 'slava' != $_SESSION['login'])
{
	unset($admin_menu['db_admin']);
	//unset($admin_menu['config']);
	
}

// render
$oTpl = new Smarty;
$oTpl->assign('amenu', $admin_menu);
$oTpl->assign('script_file', basename($_SERVER['SCRIPT_NAME']));
$oTpl->assign('query_string', explode('&', $_SERVER['QUERY_STRING']));
$oTpl->display( empty($params['tpl_name']) ? 'ablock/menu_main.tpl' : $params['tpl_name']);

?>