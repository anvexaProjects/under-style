<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require_once '../component/bl/entity/dictionary.class.php';

$ent = '';
$items = array();

$obj = new Dictionary();
$ents = $obj->getEntities();

if (isset($_GET['ent']) && array_key_exists($_GET['ent'], $ents)) $ent = $_GET['ent'];
else
{
    $keys = array_keys($ents);
    $ent = $keys[0];
}
$items = $obj->getValuesByEntity($ent);

$tpl = new Smarty;
$tpl->assign('ents', $ents);
$tpl->assign('ent', $ent);
$tpl->assign('items', $items);
$tpl->display('admin/'.CMS.'/dicts.tpl');


?>