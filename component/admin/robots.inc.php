<?php
/*
Description: Edit user
Autor:       Oleg Koshkin
Data:        05-02-2008
Version:     1.3

mailto:      WizardF@narod.ru
copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../component/admin_libs.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require_once '../templates/config/enum.inc';
require_once '../component/bl/entity/user.class.php';

$filename = '../robots.txt';

if (isset($_POST['robots']))
{
    if (file_exists("../robots.txt.bak"))  unlink("../robots.txt.bak");
    rename($filename, "../robots.txt.bak");
    $fh = fopen($filename, "w");
        $robots_content = fwrite ($fh, $_POST['robots']);
    fclose($fh);    
    if (isset($_POST['save_close'])) {
        UI::Redirect('index.php');
    }
    elseif (isset($_POST['save'])) {
        UI::Redirect('robots.php?s=1');
    }     
} 

$fh = fopen($filename, "r");
    $robots_content = fread($fh, filesize($filename));
fclose($fh);

    $tpl = new Smarty;      
    $tpl->assign('robots', $robots_content);
    
    $sTemplate = 'admin/'.CMS.'/robots.tpl';
    $tpl->display($sTemplate);

?>