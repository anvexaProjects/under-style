<?php


require_once '../component/admin_libs.inc';
ini_set('include_path', QUICK_FORM_PATH);
require_once '../lib/pear/Config.php';

//$separator = (substr(PHP_OS, 0, 3) == 'WIN') ? ':;' : ':';



$mode = isset($_GET['m']) ? $_GET['m'] : 'strings';
$lang = isset($_GET['l']) ? $_GET['l'] : DEFAULT_LANG;
$target_dir = '../templates/config';
$types = array();
switch($mode)
{
	case 'labels':
		$types['labels'] = array(
			'config_type' => 'PHPConstants',
			'resource'    => $target_dir.'/'.$lang.'_labels.inc',
			'node_type'   => 'directive'
		);
	break;
    
	case 'strings':
		$types['strings'] = array(
			'config_type' => 'PHPConstants',
			'resource'    => $target_dir.'/'.$lang.'_strings.inc',
			'node_type'   => 'directive'
		);
	break;    
}

$obj_cfg = new Config();
$root = &$obj_cfg->parseConfig($types[$mode]['resource'], $types[$mode]['config_type']);
if('XMLHttpRequest' == $_SERVER["HTTP_X_REQUESTED_WITH"])
{
	$item = $root->getItem($types[$mode]['node_type'], $_GET['id']);
	$item->setContent(str_replace("'", "&rsquo;", $_GET['content']));
	$item->setAttributes(array('comment' => $_GET['comment']));
	$content = $item->toString($types[$mode]['config_type']);
	$obj_cfg->writeConfig($types[$mode]['resource'], $types[$mode]['config_type']);
	
}
else
{
	$items = $root->toArray();
	ksort($items['root']);
	$tpl = new Smarty;
	$tpl->assign('items', $items['root']);
	$tpl->assign('mode', $mode);
	$tpl->assign('lang', $lang);
	$tpl->display('admin/'.CMS.'/config_list.tpl');
}
?>