<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require '../templates/config/enum.inc';
require_once '../component/bl/entity/order.class.php';
unset($_GET['path']);


$oUrl = new CoreUrl();
$oReq = new Request($oUrl);

//filter
$sFilterName = 'fltr-order-list';
$oFilter = new Filter($sFilterName, getFilterFields());
$aCond = $oFilter->getSql();


// Sorter
$aFields = array('order_id', 'ordered', 'status', 'total', 'ideal_payment', 'customer', 'email'); // 'full_address',
$aSortField[$oReq->get('field', 'order_id')] = $oReq->get('order', 'down');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder(); 

// Pager
$iPagePerFrame = MAX_PAGES; 
$iPageSize = $oReq->get('page_size', ORDERS_ADMIN_LIMIT);
$iPage = $oReq->getInt('page', 1);

$obj = new Order();
list($itemsList, $iCnt) = $obj->getList($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);

$oTpl = new Smarty;
$oTpl->assign('status', $enum['order_status']);
$oTpl->assign('items', $itemsList);
$oTpl->assign('aSorting', $oSorter->getSorting($oUrl));
$oTpl->assign('aPaging', $oPager->getInfo($oUrl));
$oTpl->assign('aFilter', $oFilter->aHtml);  


$oTpl->display('admin/'.CMS.'/order_list.tpl');

function getFilterFields()
{
    require '../templates/config/enum.inc';
    $aFilterFields = array(
        'order_id' => array(
            'type' => 'text',
            'condition' => '=',
        ),
        
		'ordered' => array(
            'type' => 'date_range',
            'is_calendar' => true,
            'is_from_to'  => true,
        ),
		'status' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => $enum['order_status'],
            'all_option' => array(
                'name' => '--All statuses--',
                'value' => -1,
            ),
        ),  
		'ideal_payment' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => array('1' => S_PAY_WITH_IDEAL, 0 => S_PAY_WITHOUT_IDEAL),
            'all_option' => array(
                'name' => '--',
                'value' => -1,
            ),
        ),  		
        'total' => array(
            'type' => 'range',
            'is_from_to'  => true,
        	//'cond' => 'HAVING (sub_total+(orders.is_qship*qship_price)) > 10'
        ),

        'customer' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
        'email' => array(
            'type' => 'text',
            'condition' => 'like',
        ),
/*        'full_address' => array(
            'type' => 'text',
            'condition' => 'like',
        ), */
     
    );
    return $aFilterFields;
}
?>