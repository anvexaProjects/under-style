<?php
/*
  Description: Authorization module
  Autor:       Oleg Koshkin
  Data:        20-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
  session_start();
  require_once '../lib/smarty/Smarty.class.php';
  require_once '../component/admin_libs.inc';

  // control acess
  if (!isset($_SESSION['login']) || empty($_SESSION['login']))
  {
     header('Location: ../admin/login.php');
     UI::Redirect('../admin/login.php');
  }
  elseif($_SESSION['role'] == SALERS_REDACTOR || $_SESSION['role'] == DESIGNERS_REDACTOR){
      if (isset($_REQUEST['id']) && $_REQUEST['control'] != 'pricelist'){
        //  header('Location: ../admin/login.php');
        //  UI::Redirect('../admin/login.php');
      }
  }



  // clear smarty cache
  $smarty = new Smarty;
  $smarty->caching = true;
  $smarty->clear_all_cache();

?>