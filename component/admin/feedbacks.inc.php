<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require_once '../templates/config/enum.inc';
require_once '../component/bl/entity/feedback.class.php';
unset($_GET['path']);


$oUrl = new CoreUrl();
$oReq = new Request($oUrl);

//filter
$sFilterName = 'fltr-fdb-list';
$oFilter = new Filter($sFilterName, getFilterFields());
$aCond = $oFilter->getSql();


// Sorter
$aFields = array('name', 'email', 'message', 'is_subs', 'created');
$aSortField[$oReq->get('field', 'login')] = $oReq->get('order', 'up');

$oSorter = new Sorter($aFields, $aSortField);
$oSorter->getSorting($oUrl);
$sOrder = $oSorter->getOrder(); 

// Pager
$iPagePerFrame = MAX_PAGES; 
$iPageSize = $oReq->get('page_size', USERS_LIMIT);
$iPage = $oReq->getInt('page', 1);

$obj = new Feedback();
list($itemsList, $iCnt) = $obj->getList($aCond, $iPage, $iPageSize, $sOrder);
$oPager = new Pager($iCnt, $iPage, $iPageSize, $iPagePerFrame);

$oTpl = new Smarty;
$oTpl->assign('items', $itemsList);
$oTpl->assign('aSorting', $oSorter->getSorting($oUrl));
$oTpl->assign('aPaging', $oPager->getInfo($oUrl));
$oTpl->assign('aFilter', $oFilter->aHtml);  


$oTpl->display('admin/'.CMS.'/feedbacks.tpl');

function getFilterFields()
{
    require '../templates/config/enum.inc';
    $aFilterFields = array(
        'name' => array(
            'type' => 'text',
            'condition' => 'like',
        ),  
        'email' => array(
            'type' => 'text',
            'condition' => 'like',
        ),                
		'is_subs' => array(
            'type' => 'drop_down',
            'condition' => '=',
            'select' => array(1 => 'Y', 0 => 'N'),
            'all_option' => array(
                'name' => '---',
                'value' => -1,
            ),
        ),
        'created' => array(
            'type' => 'date_range',
            'is_from_to' => true,
            'is_calendar' => true,
            'is_date_period' => true,
            'date_range' => true
        ),
    );
    return $aFilterFields;
}
?>