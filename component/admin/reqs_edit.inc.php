<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require_once '../component/bl/entity/frequest.class.php';
unset($_GET['path']);

$obj = new Frequest();
$id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
$obj->load($id);
$template = ($obj->aData['type'] == S_ORDER_TYPE_1) ? 'reqtype_1.tpl' : 'reqtype_2.tpl';

$oTpl = new Smarty;
$oTpl->assign('item', $obj->aData);
$oTpl->display('admin/'.CMS.'/'.$template);

?>