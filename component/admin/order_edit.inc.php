<?php
/*
  Description: Users list
  Autor:       Oleg Koshkin
  Data:        19-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/
require '../component/admin_libs.inc';
require_once '../component/bl/entity/order.class.php';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require '../templates/config/enum.inc';
require '../templates/config/'.CMS_LANG.'_enum.inc';


$oOrder = new Order();
$orderId = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$aOrder = $oOrder->load(intval($orderId));

$form = renderForm($enum, $orderId);
$form->setDefaults(array('status' => $aOrder['items'][0]['status']));

if ($form->validate())
{
	$oOrder->sId = 'order_id';
	$oOrder->aData = $_POST;
	$oOrder->aData['order_id'] = $_POST['id'];
	$oOrder->update();
	UI::Redirect('orders.php');
}

$tpl = new Smarty;      
$renderer =& new HTML_QuickForm_Renderer_ArraySmarty($tpl);
$form->accept($renderer);
$tpl->assign('form_data', $renderer->toArray());
$tpl->assign('order', $aOrder);
$tpl->assign('enum', $enum);
$tpl->display('admin/'.CMS.'/order_edit.tpl');

function renderForm($enum, $orderId)
{
    $form = new HTML_QuickForm('fssrm', 'post', '', '', ' onsubmit="return checkForm(this)" ');
    $form->addElement('select', 'status', '', $enum['order_status'], '  onchange="setTrackNumber(this)" id="status" ');
    $form->addElement('hidden', 'id', $orderId);
    return $form; 
}

?>