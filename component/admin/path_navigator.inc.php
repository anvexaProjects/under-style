<?php
/*
Description: Admin path navigator renderer
Autor:       Oleg Koshkin
Data:        08-05-2007
Version:     1.0

mailto:      WizardF@narod.ru
copyright:   (C) 2005 Oleg Koshkin
*/
require_once '../component/admin_libs.inc';
require_once '../lib/infrastruct/sitemap.class.php';
require '../templates/config/enum.inc';


$tpl = new Smarty;
$template = $params['tpl_name'];

$oSiteMap = SiteMap::singleton();
$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? $_REQUEST['id'] : $_REQUEST['parent_id'];

$oSiteMap->initStruct($id, false);
$struct = $oSiteMap->getStruct();
$title = $struct[1];
//unset($struct[count($struct)-1]);

if (is_array($struct))
{
    $rstruct = is_array($struct) ? array_reverse($struct) : array();
    $tpl->assign('rstruct', $rstruct);

    array_shift($struct);
    $tree =  is_array($struct) ? array_reverse($struct) : array();
}

$tpl->assign('title', $title);
$tpl->assign('items', $tree);

$tpl->display($template);
?>
