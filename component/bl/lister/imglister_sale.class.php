<?php

require_once '../component/bl/lister/lister_sale.class.php';

class ImgListerSale extends Lister
{
    public function __construct($inParams)
    {
		$inParams['parent'] = "index";
		$inParams['cond'] = "as1.control = 'subdomen'";

        parent::__construct($inParams);
        $this->curParams['iord']       = !isset($this->inParams['iord']) || Val::IsEmpty($this->inParams['iord']) ? 'image_ord' :  Val::ToDb($this->inParams['iord']);
        $this->curParams['isequence']  = !isset($this->inParams['isequence']) || ('up' == $this->inParams['isequence']) ? 'ASC' : 'DESC';
        $this->curParams['ipage_size'] = !isset($this->inParams['ipage_size']) || Val::IsEmpty($this->inParams['ipage_size']) ? 100 :  Val::ToDb($this->inParams['ipage_size']);

    }

     protected function retreiveData()
     {
         $items = parent::retreiveData();

         foreach($items as $k => $v)
         {
             $sid = $v['struct_id'];
             $sql = "SELECT * FROM images WHERE art_id = '$sid' AND art_id != 0 GROUP BY image_name ORDER BY ".$this->curParams['iord']." ".$this->curParams['isequence']." LIMIT ".$this->curParams['ipage_size'];
             $items[$k]['images'] = $this->oDb->getRows($sql);
             $items[$k]['total'] = count($items[$k]['images']);	

         }
         return $items;
     }
}

?>