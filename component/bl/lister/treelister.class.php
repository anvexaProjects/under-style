<?php

require_once '../component/bl/lister/lister.class.php';

class TreeLister extends Lister
{
	public function __construct($inParams)
	{
		parent::__construct($inParams);
		$this->curParams['autotree'] = (!isset($this->inParams['autotree']) || $this->inParams['autotree'] == false) ? false :  true;
	}
	
	protected function processItemsBeforeDisplay() { 
		if ($this->curParams['autotree'] == true) {
			$pids = array();
			foreach ($this->items as $i) $pids[] = $i['struct_id'];
			if ($pids) {
				$pids = implode("','", $pids);
				$lang = $this->curParams['lang'];
				$sql = "SELECT a1.*, as1.*, parent_id, as1.parent_id AS pid FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
						WHERE as1.parent_id IN ('$pids') AND a1.lang = '$lang'";
				$children = $this->oDb->getRows($sql);
				$items = array_merge($this->items, $children);
			} else $items = $this->items;
		} else $items = $this->items;
		$this->items = $this->transform2forest($items, 'struct_id', 'parent_id');
	}
	
	public function transform2forest($rows, $idName, $pidName) {
        $children = array(); // children of each ID
        $ids = array();
        foreach ($rows as $i=>$r) {
            $row =& $rows[$i];
            $id = $row[$idName];
            $pid = $row[$pidName];
            $children[$pid][$id] =& $row;
            if (!isset($children[$id])) $children[$id] = array();
            $row['items'] =& $children[$id];
            $ids[$row[$idName]] = true;
        }
        // Root elements are elements with non-found PIDs.
        $forest = array();
        foreach ($rows as $i=>$r) {
            $row =& $rows[$i];
            if (!isset($ids[$row[$pidName]])) {
                $forest[$row[$idName]] =& $row;
            }
            unset($row[$idName]); unset($row[$pidName]);
        }
        return $forest;
    }
}
?>