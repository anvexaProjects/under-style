<?php

require_once '../component/bl/lister/baseinnerlister.class.php';

class PickingFilter extends BaseInnerLister
{
    public $category_filter;
    public $vendor_filter;
    public $group_filter;
    public $col_filter;
    public $place_filter;
    public $oTpl;

    public $sqlForPager;

    public function __construct($inParams)
    {
        parent::__construct($inParams);
    }

    public function getCategory($str)
    {
        switch ($str) {
            case 53:
                return 'Мебель';
                break;
            case 'svet':
                return 'Свет';
                break;
            case 55:
                return 'Сантехника';
                break;
            default:
                return 'Плитка';
        }
    }

    public function getVendor($str)
    {
        $oDb = &Database::get();

        $vendor = $oDb->getRow("
                                SELECT a1.psevdonim, a1.sprop3
                                FROM art_struct as a1
                                JOIN art_struct as a2 ON a2.parent_id=a1.struct_id
                                  WHERE a1.psevdonim = '" . $str . "'
                                  AND a1.control = 'collection'
                                  AND a1.is_deleted = '0'
                                  AND a1.is_hidden = '0'
                                  AND a2.sprop1 != '1'
                                GROUP BY a1.psevdonim
                                ");
        return $vendor['sprop3'];
    }

    public function getCol($str)
    {
        $oDb = &Database::get();

        $col = $oDb->getRow("
                              SELECT `t1`.`name`, `t1`.`psevdonim`
                              FROM `collection_psevdonims` `t1`
                              JOIN `art_struct` `t2` ON `t2`.`sprop13` = `t1`.`name`
                              WHERE `t1`.`psevdonim` = '" . $str . "'
                              GROUP BY `t1`.`name`");
        return $col['name'];
    }

    public function render()
    {
        $this->initializeParams();
        // create template
        $this->oTpl->caching = ($this->curParams['disable_caching']) ? false : ENABLE_CACHING;
        $template = $this->curParams['tpl_name'];

        if (!$this->oTpl->is_cached($template, $this->curParams['tpl_key'])) {
            $oUrl = new CoreUrl();
            $this->items = $this->retreiveData();

            if (true == $this->curParams['allow_paging']) {
                $oPager = $this->getPager();
            }

            $this->processItemsBeforeDisplay();
            $this->oTpl->assign('items', $this->items);
            $this->oTpl->assign('category_filter', $this->getCategory($_GET['category_filter']));
            $this->oTpl->assign('vendor_filter', $this->getVendor($_GET['vendor_filter']));
            $this->oTpl->assign('col_filter', $this->getCol($_GET['col_filter']));
            $this->oTpl->assign('itemsCount', $this->itemsCount);

            if (true == $this->curParams['allow_paging']) {
                $this->oTpl->assign('aPaging', $oPager->getInfo($oUrl));
            }
        }
        $this->oTpl->display($template, $this->curParams['tpl_key']);
    }

    public function getItems()
    {
        $this->vendor_filter = $_GET['vendor_filter'];
        $this->col_filter = $_GET['col_filter'];
        $this->group_filter = $_GET['group_filter'];
        $this->place_filter = $_GET['place_filter'];
        $this->category_filter = $_GET['category_filter'];

        $oDb = &Database::get();
        $this->items = array();
        $where = "WHERE a1.control = 'prod'";

        if ($this->vendor_filter != '') {
            $where .= " AND (a1.psevdonim LIKE '" . $this->vendor_filter . "' OR a1.sprop3 LIKE '" . $this->vendor_filter . "') ";
        }

        if ($this->col_filter != '') {
            $where .= " AND a1.sprop13 LIKE '" . $this->getCol($this->col_filter) . "' ";
        }

        if ($this->group_filter != '') {
            $sprop4 = $oDb->getField("SELECT name FROM category_psevdonims WHERE psevdonim = '$this->group_filter' LIMIT 1;");
//            print_r($sprop4);die();

            $where .= " AND a1.sprop4 LIKE '" . $sprop4 . "' ";
        }

        if ($this->place_filter != '') {
            $where .= " AND a1.sprop6 LIKE '" . $this->place_filter . "' ";
        }

//		$sql = "
//		        SELECT *
//		        FROM `art_struct` as a1
//		        INNER JOIN `art_content` as a2 ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
//		/* INNER JOIN images ON (images.art_id = a1.struct_id)*/ "

        $sql = "
		        SELECT *
		        FROM `art_struct` as a1
		        INNER JOIN `art_content` as a2 ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
        "

            . $where .

            " AND a2.lang = 'ru'
		AND a1.sprop1 != '1'
		GROUP BY a1.struct_id
		";

        $nPage = ($this->curParams['allow_paging']) ? UI::GetPage() : 1;
        $nItemToPage = $this->curParams['page_size'];

        $this->sqlForPager = $sql;


        $sql = sprintf(
//			"SELECT %s %s %s ORDER BY %s LIMIT %s, %s  ",
            $sql . " LIMIT %s, %s  ",
            Pager::getOffset($nPage, $nItemToPage),
            $nItemToPage
        );


//---------------------------------------------------------------------------------------

        $whereBrand = $this->vendor_filter ? " AND t1.psevdonim ='$this->vendor_filter' " : '';
        $whereCategory = $this->category_filter ? " WHERE t1.struct_id ='$this->category_filter' " : '';
        $whereCollection .= $this->col_filter ? " AND t1.sprop13 LIKE '" . $this->getCol($this->col_filter) . "' " : '';
        $whereGroup = $this->group_filter ? " AND t1.sprop4 IN (
            SELECT DISTINCT t1.name
            FROM category_psevdonims AS t1
            WHERE t1.psevdonim = '$this->group_filter'
        )" : '';

        $mysqlRequest = "
            SELECT *
            FROM art_struct AS t1
            LEFT JOIN art_content AS ac ON (t1.struct_id = ac.struct_id AND t1.is_deleted = 0)
            WHERE t1.parent_id IN (
                SELECT t2.struct_id
                FROM art_struct AS t1
                LEFT JOIN art_struct AS t2 ON t2.parent_id = t1.struct_id AND t2.is_deleted = 0
                $whereCategory
            ) 
        
            $whereGroup
            
            $whereCollection
            
            AND t1.sprop13 != ''
            AND t1.is_deleted = '0'
            AND t1.is_hidden = '0'
            AND sprop1 != '1'
        
            $whereBrand
        ";

        $sql = sprintf(
            $mysqlRequest . " LIMIT %s, %s  ",
            Pager::getOffset($nPage, $nItemToPage),
            $nItemToPage
        );

//        print_r($mysqlRequest);die();


        $this->items = $oDb->getRows($sql);


        if (!empty($this->items) and count($this->items) > 0) {
            foreach ($this->items as $key => $tItem) {
                $images = $oDb->getRows("SELECT * FROM images where art_id={$tItem['struct_id']} order by image_ord asc limit 1");
                if (!empty($images) && count($images) > 0) {
                    $this->items[$key] = array_merge($this->items[$key], $images[0]);
                }
            }
        }


        return $this->items;
    }

    protected function getPager()
    {
        // get pager
//        $sSqlCount = sprintf("SELECT * %s %s %s", $this->sSqlBody, $this->sSqlFilter, $this->curParams['cond']);
        $sSqlCount = $this->sqlForPager;

//        print_r($this->sqlForPager);die();

        $nCount = $this->oDb->countRows($sSqlCount);
        $this->itemsCount = $nCount;
        $nPage = UI::GetPage();
        $nItemToPage = $this->curParams['page_size'];
        $oPager = new Pager($nCount, $nPage, $this->curParams['page_size'], MAX_PAGES);

        return $oPager;
    }
}

?>