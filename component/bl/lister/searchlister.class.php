<?php
/*
  Description: Lister (list view)
  Autor:       Oleg Koshkin
  Data:        05-02-2008
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../component/libs.inc';
require_once '../component/bl/lister/lister.class.php';
require_once '../lib/infrastruct/dates.class.php';

/*
Input params:
	@tpl_name
	@page_tpl
	@page_size
	@ord
	@sequence
	@cond
*/
class SearchLister extends Lister
{
   var $_query;

   function SearchLister($inParams)
   {
	   $this->__construct($inParams);

	   $this->_query = Val::ToDbEx($_GET['query']);
	   $this->_query = str_replace('%', '', $this->_query);
   }

   function initializeParams()
   {
   		parent :: initializeParams();
	    $this->curParams['plevel'] = 0;
   }

   function retreiveData(&$pager)
   {
      $this->curParams['allow_paging'] = true;
   		$aRows = parent :: retreiveData($pager);
      $isUnicodAvail = function_exists("mb_stripos");



      $count = count($aRows);
      for ($i = 0; $i < $count; $i++)
      {
        $this->processRow($aRows, $i);
      }

		return $aRows;
   }

   function processRow(&$aRows, $i)
   {
        $query = $this->_query;
        $margin = SEARCH_CHARS;
        $content = strip_tags($aRows[$i]['content']);

        $pos =  strpos(strtolower($content), strtolower($query));

        $begin = ($pos > $margin)?($pos - $margin):0;
        $length = strlen($query)+2*$margin;

        $substr = substr($content, $begin, $length);
        $begin = strpos($substr, ' ');
        $end = strrpos($substr, ' ');
        $result = substr($substr, $begin, $end - $begin);

        $text = preg_replace("/$query/i", "<span class='marked'>\$0</span>", $result);
        $aRows[$i]['text'] = $text.'...';
        $aRows[$i]['mname'] = preg_replace("/$query/i", "<span class='marked'>\$0</span>", $aRows[$i]['name']);
   }


   function getFilter()
   {
		$query = $this->_query;
		if (Val::IsEmpty($this->_query)) return ' AND 1=0 ';
		//return " AND (a1.content LIKE '%$query%' OR a1.name LIKE '%$query%') AND a1.control = 'article' ";
		return " AND (a1.content LIKE '%$query%' OR a1.name LIKE '%$query%') ";
   }

    function render()
	{
		$this->initializeParams();

		// create template
	    $tpl = new Smarty;
	    $tpl->caching = false;
		$template = $this->curParams['tpl_name'];


		$oUrl = new CoreUrl();

		$oUrl->clearParams();
		$oUrl->setParams(array('query'=>$this->_query));
		$items = $this->retreiveData($pager);
		$oPager = $this->getPager();

		$tpl->assign('items', $items);
		$tpl->assign('aPaging', $oPager->getInfo($oUrl));
        //p($items);
	    $tpl->display($template, $this->curParams['tpl_key']);
	}


}

?>