<?php

class TagLister
{
    protected $db;
    
    public function __construct()
    {
        $this->db = Database::get();    
    }
    
    public function getArticleIdsByTag($tag)
    {
        $tag = $this->db->escape($tag);
        $sql = "SELECT tags_rel.article_id, tags_rel.article_id FROM tags_rel LEFT JOIN tags USING(tag_id) WHERE tags.tag_name = '$tag'";
        $article_ids = $this->db->getHash($sql);
        return $article_ids;
    }
    
    public function getTagsByArticleId($article_id)
    {
        $sql = "SELECT tags.tag_id, tags.tag_name FROM tags_rel LEFT JOIN tags USING(tag_id) WHERE tags_rel.article_id = '$article_id'";
        $tags = $this->db->getHash($sql);
        return $tags;
    }
    
    public function getTags()
    {
        $sql = "SELECT tag_id, tag_name, COUNT(tags_rel.article_id) as cnt FROM tags LEFT JOIN tags_rel USING(tag_id) GROUP BY tag_id HAVING cnt > 0";
        $tags = $this->db->getRows($sql);
        return $tags;
    }
    
    public function getTagsRelation($tags)
    {
        $max = $min = 1; 
        foreach ($tags as $k => $t)
        {
            if ($max < $t['cnt']) $max = $t['cnt'];
            //if ($min > $t['cnt']) $min = $t['cnt'];
        }
        foreach ($tags as $k => $t)
        {
            $tags[$k]['rel'] = $t['cnt']/$max;
        }
        return $tags;
    }
    
    public function getTagsArray($article_id = '')
    {
        $tags = $this->getTagsByArticleId($article_id);
        return $tags;
    }
    
    public function getTagsString($article_id = '', $separator = ',')
    {
        $tags = $this->getTagsByArticleId($article_id);
        return implode($separator, $tags);
    }    
    
    public function processTags($tags, $article_id, $separator = ',')
    {
        $sql = "DELETE FROM tags_rel WHERE article_id = '$article_id'";
        $this->db->query($sql);
        
        if (!trim($tags)) return;
        $tags = explode($separator, $tags);
        
        foreach ($tags as $tag)
        {
            $tag_id = $this->getTagId($tag);
            $this->addTagRelation($tag_id, $article_id);
        }
    }
    
    protected function getTagId($tag = '')
    {
        $tag = trim($this->db->escape($tag));
        $sql = "SELECT tag_id FROM tags WHERE tag_name = '$tag'";
        $id = $this->db->getField($sql);
        if ($id) return $id;
        
        $data = array('tag_name' => $tag);
        $id = $this->db->insert('tags', $data);
        return $id;
    }
    
    protected function addTagRelation($tag_id, $article_id)
    {
        $data = array('tag_id' => $tag_id, 'article_id' => $article_id);
        $this->db->insert('tags_rel', $data);
    }
}
?>