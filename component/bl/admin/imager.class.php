<?php

class Imager
{
    private $db;
    
    public function  __construct() {
        $this->db = Database::get();
    }

    public function getList($art_id) {
        $sql = "SELECT * FROM images WHERE art_id = '$art_id' AND art_id != 0 ORDER BY image_ord, image_id";
        $rs = $this->db->getRows($sql);
        return $rs;
    }

    public function insert($data) {
        $sql = "SELECT MAX(image_ord) + 1 FROM images";
        $ord = $this->db->getField($sql);
        $data['image_ord'] = $ord;
        $this->db->insert('images', $data);
    }

    public function reorderImages($image_ids) {
        $i = 1;
        foreach ($image_ids as $id) {
            if ($id != '') {
                $sql = "UPDATE images SET image_ord = '$i' WHERE image_id = '$id'";
                $this->db->query($sql);
                $i ++;
            }
        }
    }

    public function deleteImages($image_ids, $control) {
        $ids = empty($image_ids) ? 'NULL' : implode("', '", $image_ids);
		$sql = "SELECT image_id, image_name FROM images WHERE image_id IN ('$ids')";
		$rs = $this->db->getHash($sql);
		
        $sql = "DELETE FROM images WHERE image_id IN ('$ids')";
        $this->db->query($sql);
		
		$cfg = ArtConfig::getUploadConfig($control);
		if (!empty($cfg)) {
			foreach ($cfg['0'] as $c) {
				foreach ($rs as $i) {
					$fname = SITE_ABS . $c['dir'] . '/' . $i;
					if (is_file($fname)) unlink($fname);
				}
			}
		}
    }
    
    public function updateDescription($id = 0, $descr = '', $title = '')
    {
        $id = (int) $id;
        $descr = $this->db->escape($descr);
		$title = $this->db->escape($title);
        $this->db->query("UPDATE images SET image_desc = '$descr', image_title = '$title' WHERE image_id = '$id'");
        return '<?xml version="1.0"?>
				<response>
				<img_description>'.$descr.'</img_description>
				<img_title>'.$title.'</img_title>
				</response>';
    }
	
	
	public function fillArtIds($art_id, $hash)
	{
		$sql = "UPDATE images SET art_id = '$art_id' WHERE art_id = 0 AND image_hash = '$hash'";
		$this->db->query($sql);
	}

	public function getItem($id)
	{
		return $this->db->getRow('SELECT * FROM images WHERE image_id = '.intval($id));
	}

	public function getDots($id)
	{
		return $this->db->getRows('SELECT * FROM showrooms WHERE image_id = '.intval($id));
	}


	public function deleteDots($id)
	{
		$this->db->query('DELETE FROM showrooms WHERE image_id = '.intval($id));
	}

	public function addDot($data)
	{
		$this->db->insert('showrooms', $data);
	}

}
?>