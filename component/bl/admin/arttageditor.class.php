<?php

require_once '../component/bl/admin/artlocaleditor.class.php';
require_once '../component/bl/lister/taglister.class.php';


class ArtTagEditor extends ArtLocalEditor
{
    public function __construct($uploadParams = array()) {
        parent::__construct($uploadParams);
    }
    
    protected function preProcessData()
    {
        $taglister = new TagLister(',');
        $this->defaultValues[DEFAULT_LANG]['tag'] = $taglister->getTagsString($this->articleId);
    }
}
?>