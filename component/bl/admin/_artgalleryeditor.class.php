<?php

require_once '../component/bl/admin/artlocaleditor.class.php';
require_once '../component/bl/imageresizer.class.php';

class ArtGalleryEditor extends ArtLocalEditor
{
    public $e = array();

    public function __construct($uploadParams = array()) {
        parent::__construct($uploadParams);
    }

    protected function processFiles() {
        for ($i = 1; $i <= $this->uploadParams['number']; $i ++) {
            if(!Val::IsEmpty($_FILES['img'.$i]['name'])) {
                $fpath = Util::translit($_FILES['img'.$i]['name'], array('tolower' => true, 'symbols' => '\.'));
                $fname = Util::getNonExistentFilename(SITE_ABS.$this->uploadParams['upl_dir'].'/'.$fpath);
                Util::UploadFile('img'.$i, SITE_ABS.$this->uploadParams['upl_dir'], basename($fname));
				
				copy($fname, SITE_ABS.'/upload/original/'.$fpath);
				if ('gallerymt' == $_REQUEST['control']) ImageResizer::ResizeImgAsymetric($fname, $fname, 300, 300, 'auto', 0xFFFFFF, 90, false);
				else ImageResizer::ResizeImgAsymetric($fname, $fname, 900, 615);
					
                ImageResizer::ResizeImgAsymetric($fname, SITE_ABS.$this->uploadParams['upl_dir'].'_th/'.basename($fname), $this->uploadParams['width'], $this->uploadParams['height']);

                $data = array(
                    'art_id' => $this->articleId,
                    'image_name' => basename($fname),
                    'image_desc' => $_POST['desc_img'.$i],
                    'image_code' => $_POST['code_img'.$i],
                    'image_title' => $_POST['title_img'.$i],
                    'image_hash' => $_POST['alias'],
                    'is_main' => isset($_POST['is_main'.$i]) ? 1 : 0
                );
                if (!$this->articleId) $data['image_hash'] = $_POST['prop3'];
                $this->oDb->insert('images', $data);
            }
        }
        if (isset($_POST['img_id']))
        {
            foreach ($_POST['img_id'] as $k => $id)
            {
                if (isset($_POST['delete']) && in_array($id, $_POST['delete']))
                {
                    $sql = "DELETE FROM images WHERE image_id = '$id'";
                    $this->oDb->query($sql);
                    $fname = SITE_ABS.$this->uploadParams['upl_dir'].'/'.$_POST['img_name'][$k];
                    if (is_file($fname)) unlink($fname);
                    $fname = SITE_ABS.$this->uploadParams['upl_dir'].'_th/'.$_POST['img_name'][$k];
                    if (is_file($fname)) unlink($fname);
                }
                else
                {
                    unset($data);

					if (is_array($_POST['img_main']))
	                    $data['is_main'] = in_array($id, $_POST['img_main']) ? 1 : 0;
					else
					   $data['is_main'] = 0;
					   
                    $data['image_desc'] = $_POST['img_desc'][$k];
                    $data['image_title'] = $_POST['img_title'][$k];
                    $data['image_code'] = $_POST['img_code'][$k];
                    $this->oDb->update('images', $data, "image_id = '$id'");
                }
            }
        }
        return array();
    }

    public function validateCodes()
    {
        $codes = array();
        // chekc if new images contains empty code
        for ($i = 1; $i <= $this->uploadParams['number']; $i ++) {
            if(!Val::IsEmpty($_FILES['img'.$i]['name'])) {
                if (empty($_POST["code_img$i"])) $this->e['empty'] = true;
                else $codes[] = $_POST["code_img$i"];
            }
        }
        $skip_ids = array();
        if (!$this->e['empty'])
        {
            if (isset($_POST['img_id']))
            {
                foreach ($_POST['img_id'] as $k => $id)
                {
                    if (!isset($_POST['delete']) || !in_array($id, $_POST['delete']))
                    {
                        $codes[] = $_POST['img_code'][$k];
                    } else $skip_ids[] = $id;
                }
            }
/*            if ($skip_ids)
            {
                $ids = "'".implode("','", $skip_ids)."'";
                $sql = "SELECT image_code, image_code FROM images WHERE image_id NOT IN ($ids)";
                $codes += $this->oDb->getHash($sql);
            }*/
            if (sizeof($codes) != sizeof(array_unique($codes))) $this->e['dupl'] = true;
        }
    }

    public function updateArticleId()
    {
        $sql = "SELECT image_hash, image_hash FROM images WHERE art_id = 0";
        $img = $this->oDb->getHash($sql);
        if (!empty($img))
        {
            $img = "'".implode("','", $img)."'";
            $sql = "SELECT struct_id, prop3 AS hash FROM articles HAVING hash IN ($img)";
            $art = $this->oDb->getHash($sql);
            foreach($art as $id => $hash)
            {
                $sql = "UPDATE images SET art_id = '$id' WHERE image_hash = '$hash'";
                $this->oDb->query($sql);
            }
        }

    }

    protected function preProcessData()
    {
        $id = $this->defaultValues['struct_id'];
        $sql = "SELECT * FROM images WHERE art_id = '$id'";
        $this->defaultValues['images'] = $this->oDb->getRows($sql);
    }
}
?>