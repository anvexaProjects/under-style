<?php

class ArtConfig
{
	public static $cfg;
	
	// set control, returns 'default' by default
    public static function getControl($control = '') {
		include '../templates/config/enum_cfg.inc';
		self::$cfg = $enum_cfg;
		
		if (empty($control)) return 'default';
		else if (array_key_exists($control, self::$cfg)) return $control;
		else return 'default';
    }
	// returns full config for the control
	public static function getFullConfig($control)	{
		$control = ArtConfig::getControl($control);
		return self::$cfg[$control];
	}
	
	// returns upload config for the control
	public static function getUploadConfig($control) {
		$control = ArtConfig::getControl($control);
		return isset(self::$cfg[$control]['upl']) ? self::$cfg[$control]['upl'] : array();
	}
	
	// returns tabs config for the control
	public static function getTabsConfig($control) {
		$control = ArtConfig::getControl($control);
		return self::$cfg[$control]['tabs'];
	}

	// returns sub contols(control & ch_control) config for the control
	public static function getControlConfig($control) {
		$control = ArtConfig::getControl($control);
		return isset(self::$cfg[$control]['ctrl']) ? self::$cfg[$control]['ctrl'] : array();
	}

}
?>