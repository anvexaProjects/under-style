<?php

require_once '../lib/pear/QuickForm.php';
require_once '../lib/3rdparty/cyrlat.class.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require_once '../component/bl/imageresizer.class.php';
date_default_timezone_set('Europe/Moscow');
class ArtLocalEditor
{

    private $artStructFields = array();
    private $artContentFields = array();

    protected $langs = array();
    protected $form;
	protected $cfgUpload;
    
    public $uploadParams = array();
    public $articleId = 0;
    public $defaultValues = array();

    public function __construct() {
    	require '../templates/config/enum.inc';
        $this->oDb = Database::get();
        $this->langs = $enum['langs'];
		$this->cfgUpload = ArtConfig::getUploadConfig($_REQUEST['control']);
		if (isset($this->cfgUpload[0])) unset($this->cfgUpload[0]);
    }
    
    public function getTemplateName($path, $control, $defaultTemplate = 'edit_article.tpl') {
        if(file_exists($path . 'edit_' . $control . '.tpl')) {
            return 'edit_' . $control . '.tpl';
        }
        else {
            return $defaultTemplate;
        }
    }

    public function loadData() {
        $this->artStructFields  = $this->oDb->getHash('SHOW COLUMNS FROM art_struct');
        $this->artContentFields = $this->oDb->getHash('SHOW COLUMNS FROM art_content');
        $this->articleId = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

        if ($this->articleId) {
            // edit mode
            $dbdata = $this->oDb->getRows('SELECT * FROM art_content LEFT JOIN art_struct USING(struct_id) WHERE art_content.struct_id = "' . $this->articleId . '" AND art_struct.is_deleted = 0');
            $this->defaultValues = $this->parseLocalData($dbdata);
            if (array_key_exists('path', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $path_parts = explode('/', $this->defaultValues[$k]['path']);
                    $this->defaultValues['alias'] = $path_parts[count($path_parts) - 1];
                }
            }
            else {
                $path_parts = explode('/', $this->defaultValues['path']);
                $this->defaultValues['alias'] = $path_parts[count($path_parts) - 1];
            }

            if (array_key_exists('content', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $this->defaultValues[$k]['content'] = Val::FromCms($this->defaultValues[$k]['content']);
                }
            }
            else {
                $this->defaultValues['content'] = Val::FromCms($this->defaultValues['content']);
            }
            
            if (array_key_exists('content2', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $this->defaultValues[$k]['content2'] = Val::FromCms($this->defaultValues[$k]['content2']);
                }
            }
            else {
                $this->defaultValues['content2'] = Val::FromCms($this->defaultValues['content2']);
            }            

            $this->defaultValues['created'] = $this->dateToPrint($this->defaultValues['created']);
			$this->defaultValues['end_time'] = $this->dateToPrint($this->defaultValues['end_time']);

            if ($_REQUEST['control'] == 'collection')
            {

                $sql = "SELECT file, image FROM collections WHERE article_id = '".$this->defaultValues['struct_id']."'";
                $collection_files = $this->oDb->getRows($sql);
                $this->defaultValues['collections'] = count($collection_files);
                foreach ($collection_files as $k=>$val)
                {
                    $this->defaultValues['file'][$k] = $val['file'];
                    $this->defaultValues['image'][$k] = $val['image'];
                }
            }
        }
        else {
            // add mode
            $this->defaultValues['alias'] = date("y-m-d-His");
            $this->defaultValues['parent_id'] = Val::IsEmpty($_POST['parent_id']) ?  $_GET['parent_id'] : $_POST['parent_id'];
            $this->defaultValues['ord']       = 10 + $this->oDb->getField('SELECT MAX(ord) FROM art_struct WHERE parent_id = "' . $this->defaultValues['parent_id'] . '"');
            $this->defaultValues['plevel']    = $this->oDb->getField('SELECT level FROM art_struct WHERE struct_id = "' . $this->defaultValues['parent_id'] . '"');
            $this->defaultValues['control']   = isset($_REQUEST['control']) ?  $_REQUEST['control'] : '';
            $this->defaultValues['created']   = date("d.m.Y");
			$this->defaultValues['end_time']   = date("d.m.Y");
            if (array_key_exists('path', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $path_parts = explode('/', $this->defaultValues[$k]['path']);
                    $this->defaultValues[$k]['alias'] = date("y-m-d-His");
                }
            }
            else {
                $this->defaultValues['alias'] = date("y-m-d-His");
            }

            if (array_key_exists('content', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $this->defaultValues[$k]['content'] = Val::FromCms($this->defaultValues[$k]['content']);
                }
            }
            else {
                $this->defaultValues['content2'] = Val::FromCms($this->defaultValues['content2']);
            }
            if (array_key_exists('content2', $this->artContentFields)) {
                foreach ($this->langs as $k => $v) {
                    $this->defaultValues[$k]['content2'] = Val::FromCms($this->defaultValues[$k]['content2']);
                }
            }
            else {
                $this->defaultValues['content2'] = Val::FromCms($this->defaultValues['content2']);
            }   
        }
        $this->preProcessData();
    }

    public function perform($data) {
        $errors = array();
    	$data = array_merge($this->processFiles(), $data);
    	$data = $this->postProcessData($data);
        $this->articleId = $data['struct_id'];

        // data values cutomization
        $parent_data = $this->oDb->getRows('SELECT * FROM articles WHERE struct_id = "' . $data['parent_id'] . '"');
        $parent_data = $this->parseLocalData($parent_data);

        $data['created'] = $this->dateToDb($data['created']);
		$data['end_time'] = $this->dateToDb($data['end_time']);
        $data['content'] = Val::ToCms($this->updateTinyMceContent($data['content']));

        if (array_key_exists('content', $this->artContentFields)) {
            foreach ($this->langs as $k => $v) {
                $data[$k]['content'] = Val::FromCms($this->updateTinyMceContent($data[$k]['content']));
            }
        }
        else {
            $data['content'] = Val::FromCms($this->updateTinyMceContent($data['content']));
        }
        if (array_key_exists('content2', $this->artContentFields)) {
            foreach ($this->langs as $k => $v) {
                $data[$k]['content2'] = Val::FromCms($this->updateTinyMceContent($data[$k]['content2']));
            }
        }
        else {
            $data['content2'] = Val::FromCms($this->updateTinyMceContent($data['content2']));
        }        

		$aPath = array();
		
        if (array_key_exists('path', $this->artContentFields)) {
            foreach ($this->langs as $k => $v){

			if ($parent_data[$k]['path'] == 'index') {$parent_data[$k]['path'] = '';}
                $data[$k]['path'] = trim($parent_data[$k]['path'] . '/' . $data['alias'], '/');
				$aPath[$k] = $data[$k]['path'];
				
				$e = $this->ifPathExists($aPath[$k]);
				if ($e) $errors['alias'] = $e;
            }
        }
        else {
			
			
            $data['path'] = trim($parent_data['path'] . '/' . $data['alias'], '/');
			$aPath[1] = $data[$k]['path'];
            $e = $this->ifPathExists($aPath[$k]);
            if ($e) $errors['alias'] = $e;
	    }
        unset($data['alias']);
        if (!empty($errors)) return $errors;
        $data_struct  = array_intersect_key($data, $this->artStructFields);
        if (isset($this->articleId) && Val::IsNumber($this->articleId) && $this->articleId != 0) {
            // update articles
            //echo '13';

            if(isset($data_struct['sprop11']) && isset($data_struct['struct_id'])){
            $newdata = $this->oDb->getRows('SELECT `sprop11` FROM  `art_struct` WHERE  `struct_id` = '.intval($data_struct['struct_id']));
            if($newdata[0]['sprop11']!=$data_struct['sprop11']){
                $data_struct['created'] = date( 'Y-m-d H:i:s' );
                }
            }

            $this->oDb->update('art_struct', $data_struct, 'struct_id = ' . $data['struct_id'], true);


            //print_r($data_struct);
            foreach($this->langs as $k => $d) {
                unset($data[$k]['content_id']);

                //if (0 == $this->defaultValues['level']) unset($data[$k]['path']);
                if (1 == $this->defaultValues['level'] && sizeof($this->langs) > 1) $data[$k]['path'] = $k;
                $this->oDb->update('art_content', $data[$k], 'struct_id = ' . $data['struct_id'] . ' AND lang = "' . $k . '"');
                $old_path = $this->defaultValues[$k]['path'];
                $new_path = $data[$k]['path'];
                
                $sql = "UPDATE art_content SET path = REPLACE(path, '$old_path', '$new_path') WHERE path = '$old_path' OR path LIKE '$old_path/%'";
                $rs = $this->oDb->query($sql);
                if (!$rs) {
                    $errors['alias'] = FLB_SQL_ERROR_OCCURS;
                    return $errors;
                }
               // print_r($this->oDb);die();
                $rs = $this->oDb->update('art_content', $data[$k], 'struct_id = ' . $data['struct_id'] . ' AND lang = "' . $k . '"');
                if (!$rs) {
                    $errors['alias'] = FLB_SQL_ERROR_OCCURS;
                    return $errors;
                }
            }

        }
        
        else {

            $ctrl = $parent_data['ch_control'];
            $ctrl_cfg = ArtConfig::getControlConfig($ctrl);
            $data_struct['mode'] = $parent_data['ch_mode'];
            $data_struct['control'] = $parent_data['ch_control'];
            $data_struct['ch_mode'] = $ctrl_cfg['ch_mode'] ? $ctrl_cfg['ch_mode'] : '011';
            $data_struct['ch_control'] = $ctrl_cfg['ch_control'] ? $ctrl_cfg['ch_control'] : 'article';
            
            // get level
            $data_struct['level'] = 0 == $data_struct['parent_id'] ? 1 : 1 + $parent_data['level'];
            // insert data
            unset($data_struct['struct_id']);
            $this->oDb->insert('art_struct', $data_struct, true);
			$this->articleId = $this->oDb->getLastID();
            foreach($this->langs as $k => $v) {
                unset($data[$k]['content_id']);
                $data[$k]['struct_id'] = $this->articleId;
                $data[$k]['lang'] = $k;
                $rs = $this->oDb->insert('art_content', $data[$k], true);
                if (!$rs) {
                    $errors['alias'] = FLB_SQL_ERROR_OCCURS;
                    return $errors;
                }
                
            }
        }
		
		$this->clearCache($aPath);
        return true;
    }

    public function loadUniqueProperty($field, $cond, $table){
        $sql = "SELECT DISTINCT $field FROM $table WHERE $cond";
        $array = $this->oDb->getRows($sql);
        $arr = array();
        foreach ($array as $i){
            $arr[] = $i["$field"];
        }
        $list = array();
        foreach($arr as $a){
            $list[$a] = $a;
        }
        return $list;
    }
		
	private function clearCache($aPath) {
		  // clear smarty cache
		  $smarty = new Smarty;
		  $smarty->caching = true;
  		  $smarty->clear_cache(null,"sitemap");
		  foreach($aPath as $k => $v)
			  $smarty->clear_cache(null,"art|".$v);
	}

    private function parseLocalData($data) {
        $local_data = array();
        if (!is_array($data[0])) $data[0] = array();

        foreach ($data as $d) {
            $local_data[$d['lang']] = array_intersect_key($d, $this->artContentFields);
        }
        $local_data += array_intersect_key($data[0], $this->artStructFields);
        return $local_data;
    }

    protected function preProcessData() {
    	
    }
    
    protected function postProcessData($data) {
        if ($_REQUEST['control'] == 'sponsor' && $data['img1'])
        {
            $file = SITE_ABS.$this->cfgUpload[1][0]['dir'].'/'.$data['img1'];
            if (is_file($file)) {
                list($w, $h) = getimagesize($file);
                // 90 - max width
                // 40 - max height
                $ratio = max($w/90, $h/40);
                $data['sprop2'] = floor($w/$ratio);
                $data['sprop3'] = floor($h/$ratio);
            }
        }
        
        if ($_REQUEST['control'] == 'video' && $_REQUEST['id'] == '' && !empty($_POST['video_code']) && !$data['img1'])
        {
            $vcode = $_POST['video_code'];
            preg_match('/www\.youtube\.com\/v\/([^&\?\/]+)/', $vcode, $out);
            if (isset($out[1])) $data['sprop1'] = $out[1];
            $url = "http://img.youtube.com/vi/".$out[1]."/0.jpg";
            $img = SITE_ABS.'/upload/video/'.$out[1].'.jpg';
            file_put_contents($img, file_get_contents($url));
            if (is_file($img))
                ImageResizer::ResizeImgAsymetric($img, $img, 191, 128);
        }
    	return $data;
    }    
    
    protected function processFiles() {
        $data = array();
        for ($i = 1; $i <= sizeof($this->cfgUpload); $i ++) {
            // delete or upload file
			$current_cfg = $this->cfgUpload[$i];
            if ($_POST['delete'][$i] || !Val::IsEmpty($_FILES['img'.$i]['name'])) {
				foreach ($current_cfg as $c)
				{
					$fname = SITE_ABS . $c['dir'] . '/' . $_POST['img_path'][$i];
					if (is_file($fname)) unlink($fname);
					$data['img'.$i] = '';
				}
            }
            if (!Val::IsEmpty($_FILES['img'.$i]['name'])) {
                if (!$current_cfg[0]) {
                    $path[$i] = $this->getFilePrefix($_FILES['img'.$i]['name']) .  substr(md5(uniqid(true)), 0, 14) . '.' . pathinfo($_FILES['img'.$i]['name'], PATHINFO_EXTENSION);
                }
                else {
                    $path[$i] = Util::translit($_FILES['img'.$i]['name'], array('tolower' => true, 'symbols' => '\.'));
                    $path[$i] = Util::getNonExistentFilename(SITE_ABS.$current_cfg[0]['dir'].'/'.$path[$i]);
                    $path[$i] = basename($path[$i]);
                }                   



				foreach ($current_cfg as $k=>$v) {
					if(isset($v['enlarge']) && ($v['enlarge'] == false)){
						$new = $current_cfg[0];
						$current_cfg[0] = $v;
						$current_cfg[0]['m'] = 'ar';
						$current_cfg[$k] = $new;
						break;
						}
				}
				$fpath = SITE_ABS.$current_cfg[0]['dir'];
                Util::UploadFile('img'.$i, SITE_ABS.$current_cfg[0]['dir'], $path[$i]);
				foreach ($current_cfg as $c) {
					if($c['m'] == 'ar') {
						ImageResizer::ResizeImgAsymetric(
                        $fpath.'/'.$path[$i], SITE_ABS.$c['dir'].'/'.$path[$i],
							$c['w'], $c['h']
						);
					}
					elseif($c['m'] == 'sr') {
						ImageResizer::ResizeImgSymetric(
							$fpath.'/'.$path[$i], SITE_ABS.$c['dir'].'/'.$path[$i],
							$c['w'], $c['h']
						);
					}
					elseif ($c['m'] == 'rc')
						ImageResizer::cutAndResizeImage(
							$fpath.'/'.$path[$i], SITE_ABS.$c['dir'].'/'.$path[$i],
							$c['w'], $c['h']
						);
				}
                $data['img'.$i] = $path[$i];
            }

        }
        if ($_REQUEST['control'] == 'collection')
        {
            for ($i = 0; ($this->defaultValues['collections'] + 1) >= $i; $i++)
            {
                if ($_POST['delete'][$i] || !Val::IsEmpty($_FILES['file']['name'][$i])) {
                    $fname = SITE_ABS .'/upload/collections/' . $_POST['file_path'][$i];
                    if (is_file($fname)) unlink($fname);
                    $fname = SITE_ABS .'/upload/collections/' . $_POST['image_path'][$i];
                    if (is_file($fname)) unlink($fname);
                    if ($_POST['delete'][$i])
                    {
                        $sql = "DELETE FROM collections WHERE id = '".$_POST['coll_id'][$i]."'";
                        $this->oDb->query($sql);
                    }
                }
                if (!Val::IsEmpty($_FILES['file']['name'][$i]))
                {
                    $path = Util::translit($_FILES['file']['name'][$i], array('tolower' => true, 'symbols' => '\.'));
                    $path = Util::getNonExistentFilename(SITE_ABS.'/upload/collections/'.$path);
                    $path = basename($path);
                    Util::UploadFile('file', SITE_ABS.'/upload/collections/', $path, true, $i);

                    if (!Val::IsEmpty($_FILES['image']['name'][$i]))
                    {
                        $path_img = Util::translit($_FILES['image']['name'][$i], array('tolower' => true, 'symbols' => '\.'));
                        $path_img = Util::getNonExistentFilename(SITE_ABS.'/upload/collections/'.$path_img);
                        $path_img = basename($path_img);
                        Util::UploadFile('image', SITE_ABS.'/upload/collections/', $path_img, true, $i);
                        ImageResizer::ResizeImgAsymetric(SITE_ABS.'/upload/collections/'.$path_img, SITE_ABS.'/upload/collections/'.$path_img, '342', '166');
                    }
                    else
                    {
                        $path_img = '';
                    }
                    $collection = array(
                        'image' => $path_img,
                        'file' => $path,
                        'article_id' => $this->defaultValues['struct_id']
                    );
                    if ($_POST['coll_id'][$i])
                    {
                        $this->oDb->update('collections', $collection, 'id = '.$_POST['coll_id'][$i]);
                    }
                    else
                    {
                        $this->oDb->insert('collections', $collection);
                    }
                }
            }
        }
        /*$f = SITE_ABS.$upload_params['upl_dir'] . '/' . $path[1];
        if (is_file($f))
        {
            list($width, $height, $type, $attr) = getimagesize($f);
            $data['prop1'] = $width;
            $data['prop2'] = $height;
        }*/
        return $data;
    }

    public function getForm() {
        //$diasable_flag = (0 == $this->articleId) ? '' : HTML_DISABLED;
        $diasable_flag = '';
        // create form
        $this->form = new HTML_QuickForm('item_edit', 'post');
        $this->form->addElement('text', 'alias', '', 'class="text" id="alias" ' . $diasable_flag );
        $this->localizeElements('text', 'name', '', 'class="text" id="name"');

        $this->localizeElements('text', 'ord', '', 'class="text" id="ord"');
        $this->localizeElements('text', 'title', '', 'class="text" id="title"');
        $this->localizeElements('text', 'created', '', 'class="text" id="created"');
		$this->localizeElements('text', 'end_time', '', 'class="text" id="end_time"');

        $this->localizeElements('advcheckbox', 'is_hidden', '', 'class="check" id="is_hidden"', array('0','1'));
		$this->localizeElements('advcheckbox', 'is_footer', '', 'class="check" id="is_footer"', array('0','1'));

        $this->localizeElements('textarea', 'meta', '', 'class="text" id="meta"');
        $this->localizeElements('textarea', 'meta_text', '', 'class="text" id="meta_text"');
        $this->localizeElements('textarea', 'description', '', 'class="text" id="description"');
        $this->localizeElements('textarea', 'content', '', 'id="content" class="tiny_mce"');
        $this->localizeElements('textarea', 'content2', '', 'id="content2" class="tiny_mce"');

        $this->form->addElement('hidden', 'parent_id', $this->defaultValues['parent_id']);
        //$form->addElement('hidden', 'path', $default['path']);
        $this->form->addElement('hidden', 'struct_id', $default['struct_id']);
        $this->form->addElement('hidden', 'control', $default['control']);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->localizeElements('rule', 'name', '', '', '', FLB_ADD_NAME, 'required', '', 'client');
        $this->localizeElements('rule', 'name', '', '', '', FLB_ADD_NAME, 'required');
        $this->localizeElements('rule', 'alias', '', '', '', FLB_ADD_ALIAS, 'required', '', 'client');
        $this->localizeElements('rule', 'alias', '', '', '', FLB_ADD_ALIAS, 'required');
        $this->localizeElements('rule', 'alias', '', '', '', FLB_INVALID_ALIAS, 'regex', RX_ALIAS);
        //$this->localizeElements('rule', 'id', '', '', '', FLB_INVALID_ID, 'numeric');

        $this->getUploadForm();
        return $this->form;
    }

    protected function getUploadForm() {
        if ($_REQUEST['control'] == 'collection')
        {
            $query = "SELECT * FROM collections WHERE article_id = '".$this->defaultValues['struct_id']."'";
            $collection_files = $this->oDb->getRows($query);

                for($i = 0; count($collection_files) >= $i; $i++)
                {
                    $this->form->addElement('file', 'image['.$i.']', 'id="image_'.$i.'" class="text"');
                    $this->form->addElement('file', 'file['.$i.']', 'id="file_'.$i.'" class="text"');
                    $this->form->addElement('advcheckbox', 'delete['.$i.']', '', '', 'id="delete_'.$i.'" class="check"', array('0','1'));
                    $this->form->addElement('hidden', 'image_path['.$i.']', $collection_files[$i]['image']);
                    $this->form->addElement('hidden', 'file_path['.$i.']', $collection_files[$i]['file']);
                    $this->form->addElement('hidden', 'coll_id['.$i.']', $collection_files[$i]['id']);
                }

        }
        elseif (sizeof($this->cfgUpload) > 0) {
            for($i = 1; $i <= sizeof($this->cfgUpload); $i ++) {
                $this->form->addElement('file', 'img'.$i, 'id="img'.$i.'" class="text"');
                $this->form->addElement('advcheckbox', 'delete['.$i.']', '', '', 'id="delete_'.$i.'" class="check"', array('0','1'));
                $this->form->addElement('hidden', 'img_path['.$i.']', $this->defaultValues['img'.$i]);

                $max_size = preg_replace('/[a-zA-Z]/', '', ini_get('upload_max_filesize'));
                $this->form->addRule('img'.$i, sprintf(FLB_INVALID_FILE_SIZE, $max_size), 'maxfilesize', 1024*1024*$max_size*(9/10));
                //$form->addRule('img'.$i, sprintf(VAL_ERR_FILE_SIZE, $max_size), 'maxfilesize', 20);
            }
        }
    }

    public function localizeElements($type, $name, $label, $attrs, $data = null, $ruleMessage = null, $ruleType = null, $ruleFormat = null, $ruleValidation = null) {
        switch ($type) {
            case 'text':
            case 'textarea':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addElement($type, $k.'['.$name.']', $label, preg_replace('/id="(\w+)"/', 'id="\1_' . $k . '"', $attrs));
                }
            }
            else {
                $this->form->addElement($type, $name, $label, $attrs);
            }
            break;

            case 'select':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addElement($type, $k.'['.$name.']', $label, $data, preg_replace('/id="(\w+)"/', 'id="\1_' . $k . '"', $attrs));
                }
            }
            else {
                $this->form->addElement($type, $name, $label, $data, $attrs);
            }
            break;            
            
            case 'advcheckbox':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addElement($type, $k.'['.$name.']', $label, '', preg_replace('/id="(\w+)"/', 'id="\1_' . $k . '"', $attrs), $data);
                }
            }
            else {
                $this->form->addElement($type, $name, $label, '', $attrs);
            }
            break;
            
            case 'file':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addElement($type, $k.'['.$name.']', $label, '', preg_replace('/id="(\w+)"/', 'id="\1_' . $k . '"', $attrs));
                }
            }
            else {
                $this->form->addElement($type, $name, $label, $attrs);
            }
            
            case 'hidden':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addElement($type, $k.'['.$name.']', $data);
                }
            }
            else {
                $this->form->addElement($type, $name, $data);
            }            
            break;            

            case 'rule':
            if (IS_LOCALIZATION && array_key_exists($name, $this->artContentFields)) {
                foreach($this->langs as $k => $v) {
                    $this->form->addRule($k.'['.$name.']', $ruleMessage . " ($k)", $ruleType, $ruleFormat, $ruleValidation);
                }
            }
            else {
                $this->form->addRule($name, $ruleMessage, $ruleType, $ruleFormat, $ruleValidation);
            }
        }
    }

    public function ifPathExists($path) {
        //p($this->articleId); exit;
        $cond = 
            (isset($this->articleId) && Val::IsNumber($this->articleId) && $this->articleId != 0)
            ? "struct_id != $this->articleId"
            : '1';
        $sql = "SELECT COUNT(*) FROM art_content WHERE path = '$path' AND $cond";
        $cnt = $this->oDb->getField($sql);
        if ($cnt > 0) return FLB_EXISTENT_PATH;
        else return;
    }

    protected function getFilePrefix($type) {
        return 'img';
    }

    private function dateToDb($input) {
        // convert 05.06.2007 to 2007-06-05 16:25:19
        $arr  = explode('.', $input);
        return  $arr[2] . '-' . $arr[1] . '-' . $arr[0] . date(" H:i:s");
    }

    private function dateToPrint($input) {
        // convert 2007-06-05 16:25:19 to 05.06.2007
        $arr  = explode(' ', $input);
        $arr2 = explode('-', $arr[0]);

        return  $arr2[2] . '.' . $arr2[1] . '.' . $arr2[0];
    }

    private function updateTinyMceContent($string) {
        $lexem = '[$root]';

        $patterns = array('/(href=")(.*)(\[\$root])/U',
                    '/(href=\')(.*)(\[\$root])/U',
                    //'/(href=)(.*)(\[\$root])/U',
                    '/(src=")(.*)(\[\$root])/U',
                    '/(src=\')(.*)(\[\$root])/U',
                    //'/(src=)(.*)(\[\$root])/U',
                    '/(value=")(.*)(\[\$root])/U',
                    '/(value=\')(.*)(\[\$root])/U'
                    );

        $replacements = array("href=\"$lexem",
                        "href='$lexem",
                        //"href=$lexem",
                        "src=\"$lexem",
                        "src='$lexem",
                        //"src=$lexem",
                        "value=\"$lexem",
                        "value='$lexem"
                        );

        return preg_replace($patterns, $replacements, $string);
    }
}
?>
