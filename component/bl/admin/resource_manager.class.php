<?php
/**
 * Works with strings and labels resources
 * User: Abratchuk
 * Date: 23.12.11
 * v.1.2
 * 1.1: renamed Set()->add()
 * 1.2: new field: mode_delete - can we delete resource or not
 *      checking for success SQL-queries and error-messaging
 */

require_once '../component/libs.inc';

define('SQL_GETRESOURCEBYKEY',
"SELECT res_value AS resVal FROM resources WHERE res_key = '__ResKey__' AND lang = '__Lang__' AND res_group = '__Group__'");

define('SQL_GETRESOURCES',
"SELECT res_key AS resKey, res_value AS resVal, mode_delete AS canDelete FROM resources WHERE lang = '__Lang__' AND res_group = '__Group__'");

define('SQL_SETNEWRESOURCE',
"INSERT INTO resources(lang, res_group, res_key, res_value) VALUES('__Lang__', '__Group__', '__ResKey__', '__Value__')");

define('SQL_DELETERESOURCE',
"DELETE FROM resources WHERE lang = '__Lang__' AND res_group = '__Group__' AND res_key = '__ResKey__' AND mode_delete = 1");




class ResourceManager
{
    private $language = DEFAULT_LANG;
    private $resourcegroup = 'strings';
    private $occured_error = '';
    private $last_id ;
    private $last_operation = ''; //for differs add new value and update with existing key old value

    public function __construct($language = DEFAULT_LANG)
    {
         $this->language = $language;
    }

//Gets one resourse from DataBase by its Key
//strings by default if not specified labels
//returns only string value
//
    public function Get($ResKey, $ResGroup = 'strings')
    {
        $oDB = &Database::get();
        $sqlQuery = str_replace('__Lang__', $this->language, SQL_GETRESOURCEBYKEY);
        $sqlQuery = str_replace('__ResKey__', $ResKey, $sqlQuery);
        $sqlQuery = str_replace('__Group__', $ResGroup, $sqlQuery);

        $res = $oDB->getRow($sqlQuery);
        if(count($res)==0) $resval = S_RESOURCE_NOT_DEFINED;
        else $resval = $res['ResVal'];

        $this->last_operation = 'get';
        return $resval;
    }

//Gets all resourses from DataBase by their type
//strings by default if not specified labels
//returns array of key->value arrays like 'KEY' -> "Value'
//
    public function GetResource($ResGroup = 'strings')
    {
        $oDB = &Database::get();
        $sqlQuery = str_replace('__Lang__', $this->language, SQL_GETRESOURCES);
        $sqlQuery = str_replace('__Group__', $ResGroup, $sqlQuery);

        $res = $oDB->getRows($sqlQuery);

        $this->last_operation = 'get_resource';
        return $res;
    }

//Sets one resourse to DataBase by its Key
//strings by default if not specified labels
//returns boolean
//
public function add($ResKey, $ResValue, $ResGroup = 'strings')
    {
        if( (preg_match('/[^A-Za-z0-9_]/', $ResKey) ) OR ( ! preg_match('/^[A-Za-z]/', $ResKey) ) )
        {
            $this->occured_error = FLB_BAD_RESOURCEKEY;
            return false; //allowed only latins, digits and _  || First symbol must be letter
        }
        $oDB = &Database::get();

        if ( $this->Get($ResKey, $ResGroup) != S_RESOURCE_NOT_DEFINED )
        {
            return $this->Update($ResKey, $ResValue, $ResGroup);
        }

        /*
        $sqlQuery = str_replace('__Lang__', $this->language, SQL_SETNEWRESOURCE);
        $sqlQuery = str_replace('__ResKey__', strtoupper($ResKey), $sqlQuery);
        $sqlQuery = str_replace('__Group__', $ResGroup, $sqlQuery);
        $sqlQuery = str_replace('__Value__', mysql_real_escape_string($ResValue), $sqlQuery);
//echo $sqlQuery;
        //$res = @ $oDB->query($sqlQuery);
        */

        $ValuesArray = array('res_key'=>"'".strtoupper($ResKey)."'",
                             'lang'=>"'".$this->language."'",
                             'res_group'=>"'".$ResGroup."'",
                             'res_value'=>"'".mysql_real_escape_string($ResValue)."'" );

        $res = $oDB->insert('resources', $ValuesArray, false);

        if( $res == 0 )
        {
            $this->occured_error = $oDB->getErrors();
            return false;
        }
        else
        {
            $this->last_id = $res;
            $this->last_operation = 'add';
            return true;
        }
    }

//Updates resource
//
public function Update($ResKey, $ResValue, $ResGroup)
{
    $oDB = &Database::get();

    //$ValuesArray = array('res_value'=>"'".mysql_real_escape_string($ResValue)."'" );
    $ValuesArray = array('res_value'=>"'".$ResValue."'" );

    $condition = "res_key = '".strtoupper($ResKey)."' AND res_group = '".$ResGroup."' AND lang = '".$this->language."'";
    $res = $oDB->update('resources', $ValuesArray, $condition, false);

    if( ! $res )
    {
        $this->occured_error = $oDB->getErrors();
        return false;
    }
    else
    {
        if ( mysql_affected_rows() == 0)
        {
            $this->occured_error = FLB_SQL_UPDATE_FAILED;
            return false;
        }
        $this->last_operation = 'update';
        return true;
    }

}
//Deletes resourse by Key and group
//no default values here!
//
public function Delete($ResKey, $ResGroup)
    {
        $oDB = &Database::get();
        $sqlQuery = str_replace('__Lang__', $this->language, SQL_DELETERESOURCE);
        $sqlQuery = str_replace('__ResKey__', $ResKey, $sqlQuery);
        $sqlQuery = str_replace('__Group__', $ResGroup, $sqlQuery);

        $res = $oDB->query($sqlQuery);
        if( ! $res )
        {
            $this->occured_error = $oDB->getErrors();
            return false;
        }
        else
        {
            if ( mysql_affected_rows() == 0)
            {
                $this->occured_error = FLB_SQL_DELETE_FAILED;
                return false;
            }
            $this->last_operation = 'delete';
            return true;
        }

    }

// returns error stored in private var by user's demand
//
public function get_error()
{
    return $this->occured_error;
}

// returns lastID after successfully insert by user's demand
//
public function get_last_id()
{
    return $this->last_id;
}

// returns last executed operation after if successfully by user's demand
//
public function get_last_operation()
{
    return $this->last_operation;
}

}

?>