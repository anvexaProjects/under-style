<?php

	require_once '../lib/pear/mime.php';

	class Notification
	{
		// send raw body
		function SendBody($to, $subj, $content, $attach = null, $from = LBL_FROM)
		{
			$time = date("r");
		
			$msg_tpl = new Smarty;
			$msg_tpl->assign('time', $time);
		
			//create MIME message
			$mime = new Mail_Mime("\n");
	
			// add image 
			//$cid2 = $mime->addHTMLImage(SITE_ABS . '/images/logo-print.gif' );
			//$msg_tpl->assign('img2', $cid2);
			
			$msg_tpl->assign('content', $content);
			$text = $msg_tpl->fetch('notifications/common.tpl');

//            print_r($content);die();
			
			$mime->setHTMLBody($text);
			$mime->setSubject('=?UTF-8?B?'.base64_encode($subj).'?=');
			$mime->setFrom($from);
	
			
            if ($attach != null)
            {
                $mime->addAttachment($attach['file'], $attach['mime']);
            }
            //do not try to call these lines in reverse order
			$body = $mime->get();	
			$head = $mime->txtHeaders(array('Content-Type'=>'text/html; charset="UTF-8"'), true);

            require_once(dirname(__FILE__).'/../../lib/email.php');
//			Email::$addAddress = $to;
			Email::$addAddress = 'zapros@under-style.ru';
            return Email::send($to, 'subscriber', 'C Формы обратной связи', $content);

//			return mail($to, $subj, $body, $head);
		}
	
	}
?>