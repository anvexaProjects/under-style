<?php
define('GR_AUTH_URL', 'https://www.google.com/accounts/ClientLogin');
define('GR_TOKEN_URL', 'http://www.google.com/reader/api/0/token');
define('GR_RSS_LABEL_URL', 'http://www.google.com/reader/atom/user/-/label/%s?xt=user/-/state/com.google/read&n='.GOOGLE_READER_MAX_ITEMS);
define('GR_RSS_EDIT_URL', 'http://www.google.com/reader/api/0/edit-tag?client=scroll');

class GoogleRssReader
{
	private $curl;
    private $headers;

	public function __construct()
	{
	}

	public function login($login, $password)
	{
		// Construct an HTTP POST request
		$post_data = array();
		$post_data['accountType'] = 'HOSTED_OR_GOOGLE';
		$post_data['Email']       = $login;
        $post_data['Passwd']      = $password;
        $post_data['service']     = 'reader';
		$post_data['source']      = '';

		$this->curl = curl_init(GR_AUTH_URL);
		curl_setopt($this->curl, CURLOPT_POST, true);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$rs = curl_exec($this->curl);
		// Get the Auth string and build google headers array
        
		preg_match('/Auth=([a-z0-9_\-]+)/i', $rs, $matches);

        if (!isset($matches[1]))
        {
            Logger::logStackTrace('Google error: '.$rs);
            return false;
        }

		$auth_id = $matches[1];
        $this->headers = array(
			'Authorization: GoogleLogin auth=' . $auth_id,
			'GData-Version: 3.0',
		);
        return true;
	}

    protected function getNewsByLabel($label)
	{
		curl_setopt($this->curl, CURLOPT_URL, sprintf(GR_RSS_LABEL_URL, $label));
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($this->curl, CURLOPT_POST, false);
		return curl_exec($this->curl);
    }

    public function process($login, $password, $label)
    {
        require '../templates/config/enum.inc';
        
        $db = Database::get();
		$rslogin = $this->login($login, $password);
        if (!$rslogin) return 'Auth error';
        $curl_rs = $this->getNewsByLabel($label);
        $token = $this->getToken();
        
		$rs = simplexml_load_string($curl_rs);

		foreach ($rs->entry as $r)
		{
			$news_array = array(
				'title' => (string)$r->title,
				'created' => (string)date('Y-m-d H:i:s', strtotime($r->published)),
				'description' => (string)$r->summary,
				'url' => (string)$r->link['href'],
                'source' => (string)$r->source->title
			);
            $return = $this->saveNews($db, $news_array, $enum['langs']);
            if ($return == true)
            {
                $data = array(
                    'T' => $token,
                    'a' => 'user/-/state/com.google/read',
                    'i' => (string)$r->id,
                    's' => preg_replace('/^(.+)feed\/(.+)$/', 'feed/$2', (string)$r->source->id)
                );
                //$this->setNewsAsRead($data);
            }
		}
		Logger::logItem(SITE_ABS.'/upload/news.log', date("[Y/m/d (d F, D) H:i:s O]").' Google reader: processed '.sizeof($rs->entry).' items');
        return sizeof($rs->entry);
	}

    private function saveNews($db, $data, $langs)
    {
        $sql = "SELECT struct_id, ch_control, level, path FROM articles WHERE control='news'";
        $pdata = $db->getRow($sql);
        if (!$pdata)  return false;
        $struct = array();
        $struct['level'] = $pdata['level'] + 1;
        $struct['parent_id'] = $pdata['struct_id'];
        $struct['ord'] = 5000000000 - date('U', strtotime($data['created']));
        $struct['control'] = $pdata['ch_control'];
        $struct['ch_control'] = 'article';
        $struct['created'] = $data['created'];
        $struct['sprop1'] = $data['url'];
        $struct['sprop2'] = $data['source'];
        
        $strcut_id = $db->insert('art_struct', $struct);
        if (!$strcut_id)  return false;
        
        foreach ($langs as $l => $v)
        {
            $content = array();
            $content['struct_id'] = $strcut_id;
            $content['lang'] = $l;
            if (sizeof($langs) > 1) 
            {
                $content['path'] = $l;
                $content['path'] .= preg_replace('/^([^\/]+)/', '', $pdata['path']).'/';
            }
            else $content['path'] = $pdata['path'];
            $content['path'] .= '/'.str_replace(array(' ', ':'), array('-', ''), $data['created']).substr(md5(uniqid()), 0, 4);
            $content['name'] = $data['title'];
            $content['content'] = Val::ToDb($data['description']);
            $rs = $db->insert('art_content', $content);
            if (!$rs)  return false;
        }
        return true;
    }

    private function setNewsAsRead($data)
    {
        curl_setopt($this->curl, CURLOPT_URL, GR_RSS_EDIT_URL);
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($this->curl);
    }

    private function getToken()
    {
        curl_setopt($this->curl, CURLOPT_POST, 0);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($this->curl, CURLOPT_URL, GR_TOKEN_URL);
        return curl_exec($this->curl);
    }
}
?>