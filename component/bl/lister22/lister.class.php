<?php

require_once '../component/bl/lister/baselister.class.php';

class Lister extends BaseLister
{
	public $aSqlFields = array();

	public function __construct($inParams)
	{
		parent::__construct($inParams);
		$this->setSelectFields();
	}

	protected function initializeAdditionalParams()
	{
		$this->curParams['plevel'] = !isset($this->inParams['plevel']) || Val::IsEmpty($this->inParams['plevel']) ? 1 :  Val::ToDb($this->inParams['plevel']);
		$this->curParams['parent'] = !isset($this->inParams['parent']) ? '' : Database::escape($this->inParams['parent']);
		$this->curParams['fields'] = !isset($this->inParams['fields']) || Val::IsEmpty($this->inParams['fields']) ? '' : $this->inParams['fields'];
		$this->curParams['lang']   = !isset($this->inParams['lang']) || Val::IsEmpty($this->inParams['lang']) ? (isset($_GET['global']['lan']) ? $_GET['global']['lan'] : DEFAULT_LANG) :  Val::ToDb($this->inParams['lang']);
        $this->curParams['spath']   = !isset($this->inParams['spath']) || $this->inParams['spath'] == false ? 'path' :  'spath';

		if ('none' == $this->inParams['sequence'])	
			$this->curParams['ord'] = '';
		else
			$this->curParams['ord'] = !isset($this->inParams['ord']) || Val::IsEmpty($this->inParams['ord']) ? 'as1.ord' :  Val::ToDb($this->inParams['ord']);		
	}

   	protected function getSqlBody()
   	{
		$sql = '';
		$parent = $this->curParams['parent'];
		$lang   = $this->curParams['lang'];

		if($this->curParams['tpl_name'] == 'content/subdomen_catalog_lvl_1.tpl')
		{
		//echo 'plevel='.$this->curParams['plevel'];
		//print_r($_GET);
}
//echo 'tpl'.var_export($this->curParams['tpl_name'], true).'/tpl';

		switch($this->curParams['plevel'])
		{
			case -1:
                $sSql = "
                    FROM art_content AS a1
                    INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
                    INNER JOIN art_struct AS as2 ON (as1.parent_id = as2.struct_id AND as2.is_deleted = 0)
                    INNER JOIN art_content AS a2 ON (a2.struct_id = as2.struct_id AND a2.lang = '$lang')
                    WHERE a1.lang = '$lang' ";               
                break;
	
			case 0:
				$sSql = "
					FROM art_content AS a1 
					INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
					WHERE a1.lang = '$lang'  ";
				break;
	
			default:
				
			case 1:
				$sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as2.struct_id AND a2.lang = '$lang')
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";
				break;
	
			case 2:
				$sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_struct AS as3 ON (as3.struct_id = as2.parent_id AND as3.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as3.struct_id AND a2.lang = '$lang') 
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";

					die($sSql);
				break;
				
			case 3:
				$sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_struct AS as3 ON (as3.struct_id = as2.parent_id AND as3.is_deleted = 0)
						INNER JOIN art_struct AS as4 ON (as4.struct_id = as3.parent_id AND as4.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as4.struct_id AND a2.lang = '$lang') 
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";
				break;
		}

		return $sSql;
	}

	protected function setSelectFields()
	{
		$this->aSqlFields['default'] = 'a1.*, as1.*';
	}
	
	// update defined aSqlFields to another values
	public function updateSqlFields($fields = array())
	{
		if (!empty($fields))
		{
			foreach ($fields as $k => $v)
			{
				$this->aSqlFields[$k] = $v;
			}
		}
	}	
	
	protected function getSelectFields()
   	{
   		// check if sSqlField exists for corresponding level, if not set default
   		$level = $this->curParams['plevel'];
		$fields = isset($this->aSqlFields[$level]) && !Val::IsEmpty($this->aSqlFields[$level]) ? $this->aSqlFields[$level] : $this->aSqlFields['default'];

		// adding defined colums for table fields to select
		if (!Val::IsEmpty($this->curParams['fields'])) $fields = $this->curParams['fields'];
		return $fields;
   	}
}

?>