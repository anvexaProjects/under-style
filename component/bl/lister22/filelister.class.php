<?php

require_once '../component/bl/lister/lister.class.php';

class filelister extends Lister
{
    public function __construct($inParams)
    {
        parent::__construct($inParams);
    }

    protected function retreiveData()
    {
        $items = parent::retreiveData();
        foreach($items as $k => $v)
        {
            $sid = $v['struct_id'];
            $sql = "SELECT * FROM collections WHERE article_id = '$sid' AND article_id != 0";
            $items[$k]['files'] = $this->oDb->getRows($sql);
        }
        return $items;
    }
}

?>