<?php

require_once '../component/bl/lister/imglister.class.php';

class ImgDotsLister extends ImgLister
{
    public function __construct($inParams)
    {
        parent::__construct($inParams);
    }

     protected function retreiveData()
     {
         $articles = parent::retreiveData();
		 foreach ($articles as $k => $article)
		 {
			 if (isset($article['images']))
			 {
				 foreach ($article['images'] as $k2 => $image)
				 {
					 $dots = $this->oDb->getRows('SELECT * FROM showrooms WHERE image_id = '.$image['image_id']);
					 if (!empty($dots))
					 {
						 $articles[$k]['images'][$k2]['dots'] = $dots;
					 }
				 }
			 }
		 }
         return $articles;
     }
}

?>