<?php
/*
  Description: Lister (list view)
  Autor:       Oleg Koshkin
  Data:        05-02-2008
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/
       
require_once '../component/libs.inc';
require_once '../lib/infrastruct/dates.class.php';

/*
Input params:
	@tpl_name
	@page_tpl
	@page_size
	@ord
	@sequence
	@cond
	
specify @sequence = none to avoid ordering	
	
*/

/*
General method workflow	
	render()
	    initializeParams()
		retreiveData()
			_initializeSql()
		_getPager();

*/

class BaseLister
{
	protected $oDb;
	protected $oTpl;
	protected $curParams;
	protected $inParams;
	
    protected $sSqlSelect;
    protected $sSqlBody;
	protected $sSqlFilter;
	protected $sSqlSorter;

	public $itemsCount;
	public $items;
	 
    public function __construct($inParams)
	{
		$this->inParams = $inParams;
	    $this->oTpl = new Smarty;
	}


    protected function initializeParams()
	{
		$this->curParams['tpl_name']  = !isset($this->inParams['tpl_name']) || Val::IsEmpty($this->inParams['tpl_name']) ? 'default.tpl' :  $this->inParams['tpl_name'];
		$this->curParams['page_tpl']  = ('en' == URL::GetLan()) ? 'block/pager_en.tpl' : 'block/pager.tpl' ;
		$this->curParams['page_size'] = !isset($this->inParams['page_size']) || Val::IsEmpty($this->inParams['page_size']) ? 100 :  Val::ToDb($this->inParams['page_size']);
		
		if ('none' == $this->inParams['sequence'])	{
			$this->curParams['ord'] = '';
			$this->curParams['sequence'] = '';
		} else {
			$this->curParams['ord']       = !isset($this->inParams['ord']) || Val::IsEmpty($this->inParams['ord']) ? 'ord' :  Val::ToDb($this->inParams['ord']);
			$this->curParams['sequence']  = !isset($this->inParams['sequence']) || ('up' == $this->inParams['sequence']) ? 'up' : 'down';
		}
		$this->curParams['cond']      = !isset($this->inParams['cond']) || Val::IsEmpty($this->inParams['cond']) ? '' : 'AND '.$this->inParams['cond'];
		$this->curParams['disable_caching'] = !isset($this->inParams['disable_caching']) || Val::IsEmpty($this->inParams['disable_caching']) ? false : $this->inParams['disable_caching'];
		
		$this->curParams['allow_paging'] = !isset($this->inParams['allow_paging']) || Val::IsEmpty($this->inParams['allow_paging']) ? false : $this->inParams['allow_paging'];
		if (0 == $this->curParams['page_size'])
		{
		 	$this->curParams['page_size'] = MAX_VALUE;
			$this->curParams['allow_paging'] = false;
		}
		$this->initializeAdditionalParams();
		
		$this->curParams['tpl_key'] =  sprintf("art|%s|baselister|%s|ps%s_l%s_par%s_ord%s_pg%s_cond%s", URL::GetPath(), URL::GetLan(), $this->curParams['page_size'], $this->curParams['plevel'],  $this->curParams['parent'], $this->curParams['ord'], UI::GetPage(), md5($this->curParams['cond'] . $this->curParams['tpl_name']));
		$this->curParams['tpl_key'] = md5($this->curParams['tpl_key']);
	}

    protected function initializeAdditionalParams()	{ }
	

    public function render()
	{
		$this->initializeParams();
	#print_r($this);
		// create template
   		$this->oTpl->caching = ($this->curParams['disable_caching']) ? false : ENABLE_CACHING;
		$template = $this->curParams['tpl_name']; 
	
        if (!$this->oTpl->is_cached($template, $this->curParams['tpl_key']))
        {
	 		 $oUrl = new CoreUrl();
			 $this->items = $this->retreiveData();
			 
#			 print_r($this->items);
/*
			if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= strtotime($this->items[0]['modify'])){
				header('HTTP/1.1 304 Not Modified');
				die; 
*/
		 	 if (true == $this->curParams['allow_paging']) $oPager = $this->getPager();
		 	 
		 	 $this->processItemsBeforeDisplay();
		 	 
             $this->oTpl->assign('items', $this->items);
             $this->oTpl->assign('itemsCount', $this->itemsCount);  
 			 if (true == $this->curParams['allow_paging']) $this->oTpl->assign('aPaging', $oPager->getInfo($oUrl)); 
	    }
	    $this->oTpl->display($template, $this->curParams['tpl_key']);        
	}

    protected function retreiveData()
	{
		$this->oDb = &Database::get();
		$this->initializeSql();

		// get items
		$nPage = ($this->curParams['allow_paging']) ? UI::GetPage() : 1;
		$nItemToPage = $this->curParams['page_size'];
		$sSqlMain = sprintf(
			"SELECT %s %s %s ORDER BY %s LIMIT %s, %s  ", 
			$this->sSqlSelect, 
			$this->sSqlBody, 
			$this->sSqlFilter . $this->curParams['cond'], 
			$this->sSqlSorter, 
			Pager::getOffset($nPage, $nItemToPage), 
			$nItemToPage
		);
		#echo $this->sSqlBody;
#echo $sSqlMain;
		$aRows = $this->oDb->getRows($sSqlMain);
		return $aRows;
	}

    protected function processItemsBeforeDisplay() { }	
	
    protected function getSqlBody()
    {
        $sSql = " FROM articles AS a1 WHERE a1.is_deleted = 0 AND a1.is_hidden = 0  ";
        return $sSql;
    }

    protected function getSelectFields()
    {
    	return 'a1.*';
    }

    protected function getFilter()
    {
        return '';
    }

    protected function getSorter()
    {
		// avoid ordering
		if(empty($this->curParams['ord'])) return '';
	
        $aFields = array($this->curParams['ord']);
        $aSortField[$this->curParams['ord']] = $this->curParams['sequence'];
        $oSorter = new Sorter($aFields, $aSortField);
        
        return $oSorter->getOrderSql(); 
    }

    protected function getPager()
    {
        // get pager
        $sSqlCount = sprintf("SELECT * %s %s %s", $this->sSqlBody, $this->sSqlFilter, $this->curParams['cond'] );
        $nCount = $this->oDb->countRows($sSqlCount);  
        $this->itemsCount = $nCount; 
        $nPage =  UI::GetPage();
        $nItemToPage = $this->curParams['page_size'];
        $oPager = new Pager($nCount, $nPage, $this->curParams['page_size'], MAX_PAGES);

        return $oPager; 
    }

    protected function initializeSql()
    {
        $this->sSqlSelect = $this->getSelectFields();	
        $this->sSqlBody   = $this->getSqlBody();
        $this->sSqlFilter = $this->getFilter();
        $this->sSqlSorter = $this->getSorter();
    }

}

?>