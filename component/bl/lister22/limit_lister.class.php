<?php

require_once '../component/bl/lister/lister.class.php';

class Limitlister extends Lister
{

	public function __construct($inParams)
	{
		parent::__construct($inParams);
		$this->curParams['limit'] = !isset($this->inParams['limit']) || Val::IsEmpty($this->inParams['limit']) ? 0 :  Val::ToDb($this->inParams['limit']);
	}

    protected function getPager()
    {
        // get pager
        $sSqlCount = sprintf("SELECT * %s %s %s", $this->sSqlBody, $this->sSqlFilter, $this->curParams['cond'] );
        $nCount = $this->oDb->countRows($sSqlCount);
        
        $this->itemsCount = $nCount; 
        if ($this->curParams['limit'] > 0 && $this->curParams['limit'] < $nCount) $nCount = $this->curParams['limit'];
        
        $nPage =  UI::GetPage();
        $nItemToPage = $this->curParams['page_size'];
        $oPager = new Pager($nCount, $nPage, $this->curParams['page_size'], MAX_PAGES);

        return $oPager; 
    }
}

?>