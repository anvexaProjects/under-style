<?php

require_once '../component/bl/lister/lister.class.php';

class MonthLister extends Lister
{
    public function __construct($inParams)
    {
        parent::__construct($inParams);
    }

    protected function retreiveData()
    {
		require '../templates/config/'.$this->curParams['lang'].'_enum.inc';
		$items = parent::retreiveData();
		$items_ = array();
		foreach($items as $k => $v)
		{
			list($y, $m, $d) = explode('-', $v['cdate']);
			$m = $enum['months'][$m];
			if (!isset($items_[$y])) $items_[$y] = array();
			if (!isset($items_[$y][$m])) $items_[$y][$m] = array();
			$items_[$y][$m][] = $v;
		}
		return $items_;
    }
}

?>