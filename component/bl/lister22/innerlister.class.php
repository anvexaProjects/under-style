<?php

require_once '../component/bl/lister/baseinnerlister.class.php';

class InnerLister extends BaseInnerLister
{
	public $aSqlFields = array();

	public function __construct($inParams)
	{
		parent::__construct($inParams);
		$this->setSelectFields();
	}

	protected function initializeAdditionalParams()
	{
		$this->curParams['plevel'] = !isset($this->inParams['plevel']) || Val::IsEmpty($this->inParams['plevel']) ? 1 :  Val::ToDb($this->inParams['plevel']);
		$this->curParams['parent'] = !isset($this->inParams['parent']) ? '' : Database::escape($this->inParams['parent']);
		$this->curParams['fields'] = !isset($this->inParams['fields']) || Val::IsEmpty($this->inParams['fields']) ? '' : $this->inParams['fields'];
		$this->curParams['lang']   = !isset($this->inParams['lang']) || Val::IsEmpty($this->inParams['lang']) ? (isset($_GET['global']['lan']) ? $_GET['global']['lan'] : DEFAULT_LANG) :  Val::ToDb($this->inParams['lang']);
        $this->curParams['spath']   = !isset($this->inParams['spath']) || $this->inParams['spath'] == false ? 'path' :  'spath';

		if ('none' == $this->inParams['sequence'])	
			$this->curParams['ord'] = '';
		else
			$this->curParams['ord'] = !isset($this->inParams['ord']) || Val::IsEmpty($this->inParams['ord']) ? 'as1.ord' :  Val::ToDb($this->inParams['ord']);




        if($this->curParams['plevel'] > 1)
        {
            $this->oDb = &Database::get();
            if($this->curParams['plevel'] == 2)
            {
//                print_r($_GET['global']['struct']);
                $res = $this->oDb->getRow("SELECT `id` FROM `articles` WHERE `parent_id` = '" . $_GET['global']['struct'][1]['id'] . "' AND `name` = 'SEO производителя'");
                $items = $this->oDb->getRow("SELECT * FROM `articles` WHERE `parent_id` = '" . $res['id'] . "' AND `is_deleted` = 0");
                $prop = $this->oDb->getRow("select * from art_struct where struct_id = '".$items['struct_id']."'");

                $this->oTpl->assign('seotext', $items['content']);
                $this->oTpl->assign('bottom_text', $prop['sprop7']);
            }

            if($this->curParams['plevel'] == 3)
            {
//                print_r($_GET['global']['struct']);
                $res = $this->oDb->getRow("SELECT `id` FROM `articles` WHERE `parent_id` = '" . $_GET['global']['struct'][1]['parent_id'] . "ru' AND `name` = 'SEO коллекции'");
                $items = $this->oDb->getRow("SELECT * FROM `articles` WHERE `parent_id` = '" . $res['id'] . "' AND `name` = '" . $_GET['global']['struct'][1]['name'] . "'");
                $prop = $this->oDb->getRow("select * from art_struct where struct_id = '".$items['struct_id']."'");

                $this->oTpl->assign('seotext', $items['content']);
                $this->oTpl->assign('bottom_text', $prop['sprop7']);
            }

            if($this->curParams['plevel'] == 4)
            {
//                print_r($_GET['global']['struct']);
//                print_r($this->oDb->getRow("SELECT `id` FROM `articles` WHERE `parent_id` = '" . $_GET['global']['struct'][3]['parent_id'] . "ru' AND `name` = '" . $_GET['global']['struct'][3]['name'] . "'"));
                $res = $this->oDb->getRow("SELECT `id` FROM `articles` WHERE `parent_id` = '" . $_GET['global']['struct'][3]['parent_id'] . "ru' AND `name` = '" . $_GET['global']['struct'][3]['name'] . "'");
                $res = $this->oDb->getRow("SELECT `id` FROM `articles` WHERE `parent_id` = '" . $res['id'] . "' AND `name` = 'SEO товарной группы'");
                $items = $this->oDb->getRow("SELECT * FROM `articles` WHERE `parent_id` = '" . $res['id'] . "' AND `name` = '" . $_GET['global']['struct'][2]['name'] . "'");
                $prop = $this->oDb->getRow("select * from art_struct where struct_id = '".$items['struct_id']."'");

                $this->oTpl->assign('seotext', $items['content']);
                $this->oTpl->assign('bottom_text', $prop['sprop7']);
            }

            //echo '<pre>'; print_r($_GET['global']); echo '</pre>';
        }

	}

   	protected function getSqlBody()
   	{
		$sql = '';
		$parent = $this->curParams['parent'];
		$lang   = $this->curParams['lang'];

		if($this->curParams['tpl_name'] == 'content/subdomen_catalog_lvl_1.tpl')
		{
		//echo 'plevel='.$this->curParams['plevel'];
		//print_r($_GET);
}
//echo 'tpl'.var_export($this->curParams['tpl_name'], true).'/tpl';
//die($this->curParams['plevel']);
//$this->curParams['plevel'] = 0;
		switch($this->curParams['plevel'])
		{
			case -1:
                $sSql = "
                    FROM art_content AS a1
                    INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
                    INNER JOIN art_struct AS as2 ON (as1.parent_id = as2.struct_id AND as2.is_deleted = 0)
                    INNER JOIN art_content AS a2 ON (a2.struct_id = as2.struct_id AND a2.lang = '$lang')
                    WHERE a1.lang = '$lang' ";    
                break;
	
			case 0:

				$sSql = "
					FROM art_content AS a1 
					INNER JOIN art_struct AS as1 ON (a1.struct_id = as1.struct_id AND as1.is_deleted = 0)
					WHERE a1.lang = '$lang'  ";      
				break;
	
			default:
				
			case 1:
				$sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as2.struct_id AND a2.lang = '$lang')
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";
				break;
	
			case 2:
				/*$sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_struct AS as3 ON (as3.struct_id = as2.parent_id AND as3.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as3.struct_id AND a2.lang = '$lang') 
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";*/

				$this->oDb = &Database::get();
	//	$this->initializeSql();


				$aRowPred = $this->oDb->getRow('SELECT * FROM art_struct WHERE psevdonim="'.$_GET['global']['alias'].'" and control="collection"');
				/*$aRows = $this->oDb->getRows('SELECT * FROM art_content WHERE struct_id IN (SELECT struct_id FROM art_struct WHERE parent_id='.$aRowPred['struct_id'].' and control="prod")');*/
				$sSql =  'FROM art_content WHERE struct_id IN (SELECT struct_id FROM art_struct WHERE parent_id='.$aRowPred['struct_id'].' and control="prod")';


				break;
				
			case 3:
				    $sSql = "
					FROM art_content AS a1
						INNER JOIN art_struct AS as1 ON (as1.struct_id = a1.struct_id AND as1.is_deleted = 0)
						INNER JOIN art_struct AS as2 ON (as2.struct_id = as1.parent_id AND as2.is_deleted = 0)
						INNER JOIN art_struct AS as3 ON (as3.struct_id = as2.parent_id AND as3.is_deleted = 0)
						INNER JOIN art_struct AS as4 ON (as4.struct_id = as3.parent_id AND as4.is_deleted = 0)
						INNER JOIN art_content AS a2 ON (a2.struct_id = as4.struct_id AND a2.lang = '$lang') 
					WHERE a2.".$this->curParams['spath']." = '$parent' AND a1.lang = '$lang' ";
				break;
		}

		return $sSql;
	}

	protected function setSelectFields()
	{
		$this->aSqlFields['default'] = 'a1.*, as1.*';
	}
	
	// update defined aSqlFields to another values
	public function updateSqlFields($fields = array())
	{
		if (!empty($fields))
		{
			foreach ($fields as $k => $v)
			{
				$this->aSqlFields[$k] = $v;
			}
		}
	}	
	
	protected function getSelectFields()
   	{
   		// check if sSqlField exists for corresponding level, if not set default
   		$level = $this->curParams['plevel'];
		$fields = isset($this->aSqlFields[$level]) && !Val::IsEmpty($this->aSqlFields[$level]) ? $this->aSqlFields[$level] : $this->aSqlFields['default'];

		// adding defined colums for table fields to select
		if (!Val::IsEmpty($this->curParams['fields'])) $fields = $this->curParams['fields'];
		return $fields;
   	}


   	protected function getItems()
   	{
         $_SERVER['CLEAN_REQUEST_URI'] = $_SERVER['REQUEST_URI'];
        if (strpos($_SERVER['REQUEST_URI'], '/sale/') !== false) {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/sale/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['sale'] = true;
        }
        if (strpos($_SERVER['REQUEST_URI'], '/offers/') !== false) {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/offers/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['offers'] = true;
        }
        if (strpos($_SERVER['REQUEST_URI'], '/archives/') !== false) {
            $_SERVER['CLEAN_REQUEST_URI'] = str_replace('/archives/', '/', $_SERVER['REQUEST_URI']);
            $total -= 1;
            $_GET['archives'] = true;
        }
   		$level = $_GET['global']['struct'][1]['level'];
   		$this->oDb = &Database::get();
   		//var_dump($level);
   		switch ($level) {
   			case 2:

   				$subdomen_id = $_GET['global']['struct'][2]['struct_id'];
   				$category_name = $_GET['global']['struct'][1]['name'];
   				$collections = $this->oDb->getRows('SELECT * FROM art_struct WHERE parent_id='.$subdomen_id.' and control="collection"');

				$ids = array();


				foreach($collections as $collection)
				{
					$ids[] = $collection['struct_id'];
				}

				/*$items = $this->oDb->getRows('SELECT * FROM art_struct 
INNER JOIN art_content ON (art_struct.struct_id = art_content.struct_id AND art_struct.is_deleted = 0)
INNER JOIN images ON (images.art_id = art_content.content_id)
WHERE art_struct.parent_id IN ('.implode(',', $ids).') AND sprop4="'.$category_name.'" GROUP BY art_content.content_id');*/

                  if ($_GET['sale']) {
                      $cond = " AND a1.sprop10 = '1' AND a1.sprop1 != '1' ";
                  }elseif ($_GET['offers']) {
                      $cond = " AND a1.sprop9 = '1' AND a1.sprop1 != '1' ";
                  }elseif ($_GET['archives']) {
                      $cond = " AND a1.sprop1 = '1' ";
                  }else {
                      $cond = " AND a1.sprop1 != '1' ";
                  }
                  $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                  
                   echo '<!-- x2 -->';
/*                  echo '<!--'. "SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                          INNER JOIN images ON (images.art_id = a2.struct_id)
                          WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id" .'-->';
  */                
                  $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                          INNER JOIN images ON (images.art_id = a2.struct_id)
                          WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                  if (empty($items)) {
                      $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                          INNER JOIN images ON (images.art_id = a1.struct_id)
                          WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                  }
               
   			break;
   			case 3:
   				$subdomen_id = $_GET['global']['rstruct'][1]['struct_id'];
   				$category_psevd = $_GET['global']['rstruct'][2]['psevdonim'];
   				$category_name = $_GET['global']['rstruct'][2]['name'];
   				$man_id = $_GET['global']['rstruct'][3]['struct_id'];

   				//var_dump($_GET['global']['rstruct']);
   				//var_dump($_GET['global']['rstruct']);
   				//die();



   				//$collection = $this->oDb->getRow('SELECT * FROM art_struct WHERE parent_id='.$subdomen_id.' and control="collection" and struct_id="'.$man_id.'"');





				/*$items = $this->oDb->getRows('SELECT * FROM art_struct 
INNER JOIN art_content ON (art_struct.struct_id = art_content.struct_id AND art_struct.is_deleted = 0)
INNER JOIN images ON (images.art_id = art_content.content_id)
WHERE art_struct.parent_id='.$collection['struct_id'].' AND sprop4="'.$category_name.'" GROUP BY art_content.content_id');*/

/*die('SELECT * FROM art_struct 
INNER JOIN art_content ON (art_struct.struct_id = art_content.struct_id AND art_struct.is_deleted = 0)
INNER JOIN images ON (images.art_id = art_content.content_id)
WHERE art_struct.parent_id='.$collection['struct_id'].' AND sprop3="'.$category_name.'" GROUP BY art_content.content_id');*/
                    if ($_GET['sale']) {
                        $cond = " AND a1.sprop10 = '1' AND a1.sprop1 != '1' ";
                    }elseif ($_GET['offers']) {
                        $cond = " AND a1.sprop9 = '1' AND a1.sprop1 != '1' ";
                    }elseif ($_GET['archives']) {
                        $cond = " AND a1.sprop1 = '1' ";
                    }else {
                        $cond = " AND a1.sprop1 != '1' ";
                    }
                $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                if ($uri[2] == 'place') {
		    
		    
                    $row = $this->oDb->getRow('SELECT * FROM place_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[3]) . '"');
                    if (!empty($row)) {
			
// 		    echo "<!--"."SELECT * FROM `art_struct` as a3, `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
//                             INNER JOIN images ON (images.art_id = a2.content_id)
//                             WHERE a1.parent_id = a3.struct_id AND a3.parent_id='$subdomen_id' AND a1.sprop6 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond"."-->";

			
                        $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a3, `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                            INNER JOIN images ON (images.art_id = a2.struct_id)
                            WHERE a1.parent_id = a3.struct_id AND a3.parent_id='$subdomen_id' AND a1.sprop6 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                        if (empty($items)) {
                            $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a3, `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                                INNER JOIN images ON (images.art_id = a1.struct_id)
                                WHERE a1.parent_id = a3.struct_id AND a3.parent_id='$subdomen_id' AND a1.sprop6 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond  GROUP BY a1.struct_id");
                        }
                    }
                }elseif (strpos($_SERVER['REQUEST_URI'], '/collection/')) {
                    $sSql = " FROM art_content WHERE struct_id is NULL";
                }else {
                    if (preg_match('#/sale/$#', $_SERVER['REQUEST_URI'])) {
                        $items = $this->oDb->getRows("SELECT a1.*, images.*, a2.name, a2.path FROM `art_struct` as a1 
                        INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                        INNER JOIN images ON (images.art_id = a1.struct_id)
                        INNER JOIN `art_struct` as a3 ON (a1.parent_id = a3.struct_id)
                        WHERE  a1.control = 'prod' AND a1.sprop10 = '1' AND a1.sprop1 != '1' AND a3.parent_id =$subdomen_id  GROUP BY a1.struct_id");
                    }elseif (preg_match('#/offers/$#', $_SERVER['REQUEST_URI'])) {
                        $items = $this->oDb->getRows("SELECT a1.*, images.*, a2.name, a2.path FROM `art_struct` as a1 
                        INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                        INNER JOIN images ON (images.art_id = a1.struct_id)
                        INNER JOIN `art_struct` as a3 ON (a1.parent_id = a3.struct_id)
                        WHERE  a1.control = 'prod' AND a1.sprop9 = '1' AND a1.sprop1 != '1' AND a3.parent_id =$subdomen_id  GROUP BY a1.struct_id");
                    }elseif (preg_match('#/archives/$#', $_SERVER['REQUEST_URI'])) {
                        $items = $this->oDb->getRows("SELECT a1.*, images.*, a2.name, a2.path FROM `art_struct` as a1 
                        INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                        INNER JOIN images ON (images.art_id = a1.struct_id)
                        INNER JOIN `art_struct` as a3 ON (a1.parent_id = a3.struct_id)
                        WHERE  a1.control = 'prod' AND a1.sprop1 = '1' AND a3.parent_id =$subdomen_id  GROUP BY a1.struct_id");
                    }else { 
                      $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                      $psevdonim = $this->oDb->getRows("SELECT * FROM `collection_psevdonims` WHERE `psevdonim` = '".htmlspecialchars($uri[3])."'");
                      
//                       echo "<!-- x4 -->";
//                       echo "<!--". "SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
//                               INNER JOIN images ON (images.art_id = a2.content_id)
//                               WHERE a1.sprop13 = '".$psevdonim[0]['name']."' AND a1.control = 'prod' $cond" ."-->";
                      
                      
                      $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                              INNER JOIN images ON (images.art_id = a2.struct_id)
                              WHERE a1.sprop13 = '".$psevdonim[0]['name']."' AND a1.control = 'prod' $cond  GROUP BY a1.struct_id");
                      if (empty($items)) {
                          $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                              INNER JOIN images ON (images.art_id = a1.struct_id)
                              WHERE a1.sprop13 = '".$psevdonim[0]['name']."' AND a1.control = 'prod' $cond  GROUP BY a1.struct_id");
                      }
                    }
                    if(empty($items)) {
                      if (strpos($_SERVER['REQUEST_URI'], '/offers/')) {
                        echo '<p><span>В настоящий момент данный раздел не содержит ни одного предложения</span></p>';
                      }else {
                        echo '<p><span>В настоящий момент данный раздел не содержит ни одного товара.</span></p>';
                      }
                    }
                }
   			break;
   			case 4:
   				$subdomen_id = $_GET['global']['rstruct'][1]['struct_id'];
   				$category_psevd = $_GET['global']['rstruct'][2]['psevdonim'];
   				$category_name = $_GET['global']['rstruct'][2]['name'];
   				$man_id = $_GET['global']['rstruct'][3]['struct_id'];
   				$col_name = $_GET['global']['rstruct'][4]['name'];
   				//var_dump($_GET['global']['rstruct']);
   				//die();



   				//$collection = $this->oDb->getRow('SELECT * FROM art_struct WHERE parent_id='.$subdomen_id.' and control="collection" and struct_id="'.$man_id.'"');


//var_dump($_GET['global']['rstruct']);
//die('tert');


				/*$items = $this->oDb->getRows('SELECT * FROM art_struct 
INNER JOIN art_content ON (art_struct.struct_id = art_content.struct_id AND art_struct.is_deleted = 0)
INNER JOIN images ON (images.art_id = art_content.content_id)
WHERE art_struct.parent_id='.$collection['struct_id'].' AND sprop4="'.$category_name.'" AND art_struct.sprop13="'.$col_name.'" GROUP BY art_content.content_id');*/
//$imgs = $this->oDb->getRows('SELECT * FROM images where art_id in ('.implode(',', $ids).') ');
                    if ($_GET['sale']) {
                        $cond = " AND a1.sprop10 = '1' AND a1.sprop1 != '1' ";
                    }elseif ($_GET['offers']) {
                        $cond = " AND a1.sprop9 = '1' AND a1.sprop1 != '1' ";
                    }elseif ($_GET['archives']) {
                        $cond = " AND a1.sprop1 = '1' ";
                    }else {
                        $cond = " AND a1.sprop1 != '1' ";
                    }
                $uri = explode('/', $_SERVER['CLEAN_REQUEST_URI']);
                if ($uri[3] == 'place') {
                    $row = $this->oDb->getRow('SELECT * FROM place_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[4]) . '"');
                    if (!empty($row)) {
                        $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                            INNER JOIN images ON (images.art_id = a2.struct_id)
                            WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.sprop6 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                        if (empty($items)) {
                            $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                                INNER JOIN images ON (images.art_id = a1.struct_id)
                                WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.sprop6 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                        }
                    }
                }else {
                    $row = $this->oDb->getRow('SELECT * FROM category_psevdonims WHERE psevdonim = "' . htmlspecialchars($uri[4]) . '"');
                    if (!empty($row)) {
                        $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                            INNER JOIN images ON (images.art_id = a2.struct_id)
                            WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.sprop4 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                        if (empty($items)) {
                            $items = $this->oDb->getRows("SELECT * FROM `art_struct` as a1 INNER JOIN `art_content` a2  ON (a1.struct_id = a2.struct_id AND a1.is_deleted = 0)
                                INNER JOIN images ON (images.art_id = a1.struct_id)
                                WHERE (a1.psevdonim = '".htmlspecialchars($uri[2])."' OR a1.sprop3 = '".htmlspecialchars($uri[2])."') AND a1.sprop4 = '".$row['name']."' AND a1.control = 'prod' AND a2.lang = 'ru' $cond GROUP BY a1.struct_id");
                        }
                    }
                }


   				break;
   			
   			default:
   				# code...

   				break;
   		}


   		return $items;
   	}
}

?>