<?php
/*
  Description: User operations
  Autor:       Viacheslav Markovski
  Data:        03-05-2008
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../lib/core/dbitem.class.php';

class User extends DbItem 
{
    var $aMap;
    
    // constructor
    function User()
    {
        parent::__construct();
        $this->_initTable('users', 'id', 'user');
        $this->aMap = $this->aFields;
        $this->aMap['full_name'] = 'user.name';
        
    }
    
    function getList($aCond=array(), $iPage=0, $iPageSize=0, $sSort='', $aFields=array())
    {
        $sCond = $this->_parseCond($aCond, $this->aMap);
        $sSql = 'SELECT COUNT(*) '.
                ' FROM `'.$this->sTable.'` AS '.$this->sAlias.
                ' WHERE '.$sCond;
        $iCnt = $this->oDb->getField($sSql);
        $aRows = array();
        if ($iCnt)
        {
            $iOffset = $this->_getOffset($iPage, $iPageSize, $iCnt);
            $sSql = 'SELECT '.$this->_joinFields($this->aMap, $aFields).
                    ' FROM '.$this->sTable.' AS '.$this->sAlias.
                    ' WHERE '.$sCond.
                    ($sSort?(' ORDER BY '.$sSort):'').
                    ($iPageSize?(' LIMIT '.$iOffset.','.$iPageSize):'');
            $aRows = $this->oDb->getRows($sSql);
        }
        return array($aRows, $iCnt);
    }  
    

    function isLoginExists($login = null)
    {
        $data = $this->aData;
        $res = $this->loadByCondition('login = "'.addslashes($login).'"');
        $this->aData = $data;
        return empty($res) ? false : true;
    }
    
    
    // Generation secure key
    function generateUserKey($uId) 
    {
        $sSql = 'SELECT '.$this->_joinFields($this->aMap, $aFields).
            	' FROM '.$this->sTable.' AS '.$this->sAlias.
            	' WHERE role = 1 AND id = '.$uId;
        $aRow = $this->oDb->getRow($sSql);
        if (!empty($aRow)) 
        {
            $key = md5(date('U') . $aRow['login'] . $aRow['full_name']);
            
            $this->sId = 'id';
            $this->aData = array(
            	'id' => $uId,
                'user_key' => $key
            );
            $this->update();
        }
        else 
        {
            echo UI::GetBlockError(ERR_GET_USER);
        }
    }
    
    // Export user key into text file
    function exportUserKey($uId)
    {
        $sSql = 'SELECT '.$this->_joinFields($this->aMap, $aFields).
            	' FROM '.$this->sTable.' AS '.$this->sAlias.
            	' WHERE role = 1 AND id = '.$uId;
        $aRow = $this->oDb->getRow($sSql);
        if (!empty($aRow)) 
        {
            $file_name = str_replace(' ', '_', $aRow['full_name']) . '_key';
            // Send key file to user 
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename=' . $file_name . '.txt');
            header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
            echo $aRow['user_key'];
            exit();
        }
        else 
        {
            echo UI::GetBlockError(ERR_GET_USER);
        }        
    }
	
	function getDiscount($id, $discount_max) {
		$sql = "SELECT SUM(discount) FROM discounts WHERE user_to = '$id'";
		$discount = $this->oDb->getField($sql);
		return ($discount < $discount_max) ? $discount : $discount_max;
	}
}
?>