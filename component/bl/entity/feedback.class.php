<?php

require_once '../lib/core/dbitem.class.php';

class Feedback extends DbItem
{
    // constructor
    public function __construct($sTable = 'feedback', $sTableId = 'id')
    {
        parent::__construct();
        $this->_initTable($sTable, $sTableId);
        $this->aMap = $this->aFields;
    }

}
?>