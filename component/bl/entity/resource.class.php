<?php

require_once '../lib/core/dbitem.class.php';

class Resource extends DbItem
{
    // constructor
    public function __construct($sTable = 'resources', $sTableId = 'res_id')
    {
        parent::__construct();
        $this->_initTable($sTable, $sTableId);
        $this->aMap = $this->aFields;
    }	
}


?>