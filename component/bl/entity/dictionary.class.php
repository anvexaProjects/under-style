<?php
/*
  Description: User operations
  Autor:       Viacheslav Markovski
  Data:        03-05-2008
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../lib/core/dbitem.class.php';

class Dictionary extends DbItem
{
    
    // constructor
    public function  __construct($sTable = '', $sTableId = '')
    {
        parent::__construct();
        $this->_initTable('dictionaries', 'id');
        $this->aMap = $this->aFields;
    }

    public function getEntities()
    {
        $sql = "SELECT DISTINCT(entity), entity_id FROM $this->sTable";
        $rs = $this->oDb->getHash($sql);
        return array_flip($rs);
    }

    public function getValuesByEntity($id)
    {
        $id = (int) $id;
        $sql = "SELECT * FROM $this->sTable WHERE entity_id = $id";
        $rs = $this->oDb->getRows($sql);
        return $rs;
    }

    public function getHashByEntityName($name, $lang)
    {
        $rs = array();
        if (array_key_exists("value_$lang", $this->aFields))
        {
            $name = $this->oDb->escape($name);
            $sql = "SELECT value_$lang, value_$lang FROM $this->sTable WHERE entity = '$name'";
            $rs = $this->oDb->getHash($sql);
        }
        return $rs;
    }

    public function getHashByEntityId($id, $lang)
    {
        $rs = array();
        $id = (int) $id;
        if (array_key_exists("value_$lang", $this->aFields))
        {
            $sql = "SELECT value_$lang, value_$lang FROM $this->sTable WHERE entity_id = '$id'";
            $rs = $this->oDb->getHash($sql);
        }
        return $rs;
    }

    public function getColors()
    {
        return $this->oDb->getHash("SELECT id, color FROM colors");
    }

}
?>