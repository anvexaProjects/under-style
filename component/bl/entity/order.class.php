<?php
/*
  Description: User operations
  Autor:       Viacheslav Markovski
  Data:        03-05-2008
  Version:     1.0

  mailto:      WizardF@narod.ru
  copyright:   (C) 2008 Oleg Koshkin
*/

require_once '../lib/core/dbitem.class.php';

class Order extends DbItem 
{
    var $aMap;
    
    // constructor
    public function __construct()
    {
        parent::__construct();
        $this->_initTable('orders', 'order_id', 'orders');
        $this->aMap = $this->aFields;
        $this->aMap['sub_total'] = 'sub_total';
        $this->aMap['customer'] = 'CONCAT(orders.name, " ", orders.lname)';
        $this->aMap['full_address'] = 'CONCAT_WS(", ", orders.country, orders.street, orders.house)';
        $this->aMap['total'] = 'sub_total*(100 - regular_discount - ticket_discount)/100+shipping_price';
    }
    
    public function getList($aCond=array(), $iPage=0, $iPageSize=0, $sSort='', $aFields=array())
    {
        $aMap = $this->aFields;
        $aMap['customer'] = 'CONCAT(orders.name, " ", orders.lname)';
        $aMap['full_address'] = 'CONCAT_WS(", ", orders.country, orders.street, orders.house)';
        $aMap['ticker'] = 'tickers.ticker';

        $sCond = $this->_parseCond($aCond, $aMap);
		$sSql = 'SELECT COUNT(*) FROM `'.$this->sTable.'` AS '.$this->sAlias. 
		' LEFT JOIN (SELECT order_id, SUM(price*num) AS sub_total FROM order_items GROUP BY order_id) AS items USING(order_id)'.
		' WHERE '.$sCond;
        $sSql = str_replace('{#total}', 'sub_total*(100 - regular_discount)/100+shipping_price', $sSql);
        $iCnt = $this->oDb->getField($sSql);
        $aRows = array();
        if ($iCnt)
        {
            $iOffset = $this->_getOffset($iPage, $iPageSize, $iCnt);
            $sSql = 'SELECT '.$this->_joinFields($this->aMap, $aFields).
                    ' FROM '.$this->sTable.' AS '.$this->sAlias.
            		' LEFT JOIN (SELECT order_id, SUM(price*num) AS sub_total FROM order_items GROUP BY order_id) AS items USING(order_id)'.
                    ' WHERE '.$sCond.
                    ($sSort?(' ORDER BY '.$sSort):'').
                    ($iPageSize?(' LIMIT '.$iOffset.','.$iPageSize):'');
            $sSql = str_replace('{#total}', 'sub_total*(100 - regular_discount)/100+shipping_price', $sSql);
            $aRows = $this->oDb->getRows($sSql);

        }    	
    	return array($aRows, $iCnt);
    } 

    public function load($id)
    {
		$Query = "SELECT *, price*num AS item_price, orders.name AS oname
		    FROM order_items
		INNER JOIN orders ON orders.order_id = order_items.order_id
		INNER JOIN articles ON (order_items.prod_id = articles.struct_id)
		WHERE orders.order_id = '$id'";
		$items['items'] = $this->oDb->getRows($Query);
        $items['price'] = 0;
        $items['total'] = 0;
        foreach ($items['items'] as $v) {
            $items['price'] += $v['item_price'];
            $items['total'] += $v['num'];
        }
        $items['subtotal'] = $items['price'];
        $items['discount'] = $items['items'][0]['regular_discount'] + $items['items'][0]['ticket_discount'];
        $items['total'] = $items['subtotal']*(100 - $items['discount'])/100;
        $items['total'] = round($items['total']+$items['items'][0]['shipping_price'], 2);
		$items['order'] = $id;
		return $items;  	
    }
    
    

}
?>