<?php
/*
  Description: Base Feedback Form (aurocar usa)
  Autor:       Oleg Koshkin
  Data:        09-30-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin

  1.1 Plain text edition
  1.2 Send all notifications in MIME format
*/

require_once '../component/libs.inc';
require_once '../lib/pear/QuickForm.php';
require_once '../lib/pear/HTML/QuickForm/Renderer/ArraySmarty.php';
require_once '../component/bl/notification.class.php';

    define('CHECK_SPAM', '445DD124-EBB6-4ad2-802C-0C5BEFF64A95');

 	class BaseForm
	{
		public $action;
		
		function GetBaseForm($action)
		{
			 // create form
			 $form = new HTML_QuickForm('fssrm', 'post', SITE_REL.$action);
			 $form->addElement('text', 'field_1', '');
			 $form->addElement('text', 'field_2', '');
			 $this->action = $action;

			 return $form;
		}


		function IsSpam()
	    {
		     return (!Val::IsEmpty($_POST['field_1'])) || (CHECK_SPAM != $_POST['field_2']);
		}


		function ProcessForm($form, $page_tpl_name, $mail_tpl_name, $topic, $default = '', $scsMsg = WRN_FORM_SENDED, $address = ADMIN_MAIL)
		{

			 if ($form->validate())
			 {
				 $values = $form->process('ProcessData', false);
				 if (!BaseForm::IsSpam()) BaseForm::Perform($values, $mail_tpl_name, $topic, $address);
				 $default = array('is_success'=>'1');
				 UI::Redirect(SITE_REL.$this->action.'s=1/');
				 //echo UI::GetBlockWarning($scsMsg);
			 }
			// else
			 {
	 			$default['field_1'] = '';
				$default['field_2'] = CHECK_SPAM;
				$form->setDefaults($default);


				$tpl =& new Smarty;
				$renderer =& new HTML_QuickForm_Renderer_ArraySmarty($tpl);


				 // build the HTML for the form
				 $form->accept($renderer);
				 $tpl->assign('form_data', $renderer->toArray());
 				 $tpl->assign('default', $default);
				 $tpl->display($page_tpl_name);
			 }
		}


	   function Perform($values, $tpl_name, $topic, $address = ADMIN_MAIL)
	   {
            require '../templates/config/enum.inc';
	   		$time = date("r");
			$tpl2 =& new Smarty;
			$tpl2->assign('item', $values);
			$tpl2->assign('time', $time);
			$tpl2->assign('site_addr', SITE_ADR);
			$tpl2->assign('enum', $enum);
			$msg_text = $tpl2->fetch($tpl_name);

			// send message
			$adr = explode(';', $address);
			$res = true;

			foreach ($adr as $one_adr)
			{
         $res = Notification::SendBody(trim($one_adr), $topic . $time, $msg_text);
			   //$res = mail(trim($one_adr), $topic . $time, $msg_text, $headers);
			   //if (!$res) $res = false;
			}

			//if (!$res) echo (UI::GetBlockWarning(WRN_MAIL_ERR));
	   }
	}


   function ProcessData($values)
   {
       return $values;
   }

 ?>