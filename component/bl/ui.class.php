<?php
/*
  Description: Shared UI functions
  Autor:       Oleg Koshkin
  Data:        12-05-2007
  Version:     1.2

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/

class UI
{

	static function GetBlockError($param)
	{
		$tpl = new Smarty;      
		$tpl->assign('caption', LBL_ERROR);               
		$tpl->assign('text', UI::GetError($param));               
		return $tpl->fetch(UI::GetTplFolder() . 'block.tpl');
	}


	static function GetBlockWarning($param)
	{
		$tpl = new Smarty;      
		$tpl->assign('caption', LBL_WARNING);               
		$tpl->assign('text', UI::GetWarning($param));               
		return $tpl->fetch(UI::GetTplFolder() . 'block.tpl');
	}

	
	static function GetError($param)
	{
		return UI::GetHtmlMessage(UI::GetTplFolder() . 'error.tpl', $param);
	}

	static function GetWarning($param)
	{
		return UI::GetHtmlMessage(UI::GetTplFolder() . 'warning.tpl', $param);
	}


	static function GetHtmlMessage($template, $param)
	{        
		 $tpl = new Smarty;      
		 $tpl->assign('param', $param);               
		 return $tpl->fetch($template);
	}

	static function GetLink($text, $link)
	{
		 $tpl = new Smarty;      
		 $tpl->assign('text', $text);               
		 $tpl->assign('link', $link);               
		 return $tpl->fetch(UI::GetTplFolder() . 'link.tpl');
	
	}
	
	// create a pager
    static function GetPager($ActivePage, $StartPage, $EndPage, $InitUrl, $template)
    {
    	 $pages = array();
         $content = '';
         if ($ActivePage > $EndPage) $ActivePage = $EndPage;
         if ($ActivePage < 1) $ActivePage = 1;

		 $tpl = new Smarty;               
         
         // pages
         for ($i=$StartPage; $i<=$EndPage; $i++)
         {
			array_push($pages, $i);
    	 }
    	     	 
    	 $tpl->assign('pages', $pages);               
    	 $tpl->assign('active_page', $ActivePage);               
    	 $tpl->assign('init_url', $InitUrl);               
         return $tpl->fetch($template);
    }

        
     static function UpdatePagerBorders($PageCount, $page, &$MinPage, &$MaxPage)
     {
			$BorderNumber = ceil(MAX_PAGES/2);
			
			if ($PageCount > MAX_PAGES)
			{
				 $MinPage = $page - $BorderNumber;
				 $MaxPage = $page + $BorderNumber;
				 if ($MinPage <= 0)
				 {
					$MinPage = 1;
					$MaxPage = MAX_PAGES;
				 }
				 if ($MaxPage > $PageCount)
				 {
					$MinPage = $PageCount - MAX_PAGES;
					$MaxPage = $PageCount;
				 }         	        
			}
			else
			{
			   $MinPage = 1;
			   $MaxPage = $PageCount;
			}
     }          
     
	static function GetTplFolder()
	{
		return  (isset($_GET['global']['has_mod_rewrite']) && 1 == $_GET['global']['has_mod_rewrite']) ? 'block/' : 'ablock/';
	}

    
    static function Redirect($location)
 	{
        echo "<script>window.location='$location'</script><noscript>JavaScript is not supported</noscript>";
		exit;
  	}
	
	static function GetPage()
	{
	    if  (isset($_GET['page']))  
		{
			$page = (Val::IsNumber($_GET['page'])) ? $_GET['page'] : 1;
		}
		else
		{
			$page = 1;
		}
		
		return $page;
	}

}
?>