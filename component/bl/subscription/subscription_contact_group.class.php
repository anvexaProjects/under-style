<?php

require_once '../lib/core/dbitem.class.php';

class SubscriptionContactGroup extends DbItem 
{
    public function __construct() {
        parent::__construct('subscription_contacts_groups', 'id');
    }
	
    public function getHashNames() {
        return $this->oDb->getHash("SELECT name, name FROM subscription_contacts_groups");
    }
	
    public function getHashIdName() {
        return $this->oDb->getHash("SELECT id, name FROM subscription_contacts_groups");
    }	
	
	public function delete($id)	{
		$id = (int)$id;
		parent::delete($id);
		//$this->oDb->query("DELETE FROM subscription_contacts WHERE id IN (SELECT contact_id FROM subscription_contacts_groups_maps WHERE group_id = '$id')");
		$this->oDb->query("DELETE FROM subscription_contacts_groups_maps WHERE group_id = '$id'");
	}
	
}
?>