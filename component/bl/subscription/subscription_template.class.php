<?php

require_once '../lib/core/dbitem.class.php';

class SubscriptionTemplate extends DbItem 
{
    public function __construct() {
        parent::__construct('subscription_templates', 'id');
    }
    
    public function setDefaultTemplate($id) {
        $this->oDb->query("UPDATE $this->sTable SET is_default = 0");
        $data['is_default'] = 1;
        $data['id'] = $id;
        $this->setData($data);
        $this->update();        
    }
	
	public function insert() {
		$id = parent::insert();
		$is_default_exists = $this->oDb->getField("SELECT COUNT(*) FROM subscription_templates WHERE is_default = 1 AND is_deleted = 0");
		if ($is_default_exists == 0) $this->setDefaultTemplate($id);
		return $id;
	}

}
?>