<?php

require_once '../lib/core/dbitem.class.php';
require_once '../component/bl/subscription/subscription_helper.class.php';

class SubscriptionQueue extends DbItem
{
    public function __construct() {
        parent::__construct('subscription_queue', 'id');
    }
	
	
	public function addQueue($data, $contacts) {
        $this->aData = $data;
        $queue_id = $this->insert();
        $this->addQueueItems($queue_id, $contacts);
        return $queue_id;
	}

    public function addQueueItems($queue_id, $contacts)
    {
        foreach ($contacts as $k => $v)
        {
            $data = array(
                'queue_id' => $queue_id,
                'contact_id' => $v['id']
            );
            $this->oDb->insert('subscription_queue_items', $data);
        }
    }

    public function setContactsSent($queue_id, $contacts)
    {
        $contact_ids = array();
        if (sizeof($contacts) > 0)
        {
            foreach ($contacts as $v)
            {
                $contact_ids[] = (int)$v['id'];
            }
            $contact_ids = implode(',', $contact_ids);
            $this->oDb->update('subscription_queue_items', array('is_sent' => 1), "contact_id IN ($contact_ids) AND queue_id = $queue_id");
        }
    }

    public function getStatistics($aCond=array(), $iPage=0, $iPageSize=0, $sSort='', $aFields=array())
    {
        $aMap = $this->aFields;

        $sCond = $this->_parseCond($aCond, $aMap);
        $sSql = "SELECT COUNT(*) FROM subscription_queue as $this->sAlias WHERE $sCond";
        $iCnt = $this->oDb->getField($sSql);
        $aRows = array();
        if ($iCnt)
        {
            $iOffset = $this->_getOffset($iPage, $iPageSize, $iCnt);
            $sSql = "SELECT
                subscription_queue.*,
                COUNT(contact_id) AS contacts_num,
                SUM(is_sent) AS sent_items,
                SUM(is_email_viewed) AS viewed_items,
                SUM(is_email_viewed_in_browser) AS viewed_in_browser_items,
                SUM(is_translated_email_viewed) AS viewed_translated_items
                FROM subscription_queue LEFT JOIN subscription_queue_items ON (subscription_queue.id = subscription_queue_items.queue_id)
                GROUP BY (subscription_queue_items.queue_id)".
                    ($sSort?(' ORDER BY '.$sSort):'').
                    ($iPageSize?(' LIMIT '.$iOffset.','.$iPageSize):'');
            $aRows = $this->oDb->getRows($sSql);
        }
        return array($aRows, $iCnt);
    }
    
    public function getUserEmail($hash, $queue_id, $is_email_viewed, $is_email_viewed_in_browser, $is_translated_email_viewed)
    {
        $queue_id = (int)$queue_id;
        $hash = $this->oDb->escape($hash);
        $field = '';
        $rs = '';
        if ($is_translated_email_viewed === true) $field = 'template_translated';
        else $field = 'template';
            

        $sql = "SELECT $field, subscription_contacts.fname, subscription_contacts.lname, subscription_queue_items.id AS queue_item_id FROM subscription_queue
                    INNER JOIN subscription_queue_items ON(subscription_queue_items.queue_id = subscription_queue.id AND subscription_queue_items.queue_id = '$queue_id')
                    INNER JOIN subscription_contacts ON(subscription_contacts.id = subscription_queue_items.contact_id AND subscription_contacts.hash = '$hash')";
        $data = $this->oDb->getRow($sql);

        if (!empty($data))
        {
            if ($is_email_viewed === true)
            {
                $this->updateQueueItem($data['queue_item_id'], array('is_email_viewed' => 1));
                return '';
            }
            else
            {
                $rdata = array('hash' => $hash, 'mail_id' => $queue_id, 'user_name' => $data['fname'].' '.$data['lname']);
                $replacements = SubscriptionHelper::replaceBodyPlaceholders($rdata);
                $rs = str_replace(array_keys($replacements), array_values($replacements), $data[$field]);
                $rs = SubscriptionHelper::processEmailBody($rs);

                if ($is_translated_email_viewed === true) $key = 'is_translated_email_viewed';
                elseif ($is_email_viewed_in_browser === true)  $key = 'is_email_viewed_in_browser';

                $this->updateQueueItem($data['queue_item_id'], array($key => 1));
            }
        }

        return $rs;
    }

    private function updateQueueItem($queue_item_id, $data)
    {
        $this->oDb->update('subscription_queue_items', $data, "id = $queue_item_id");
    }


    public function processQueue($queue_id, $contacts = array())
    {
        $obj_m = new SubscriptionMailing();

        if (empty($contacts))
        {
            $sql = "SELECT subscription_contacts.* FROM subscription_queue_items
                    INNER JOIN subscription_contacts ON (subscription_queue_items.contact_id = subscription_contacts.id AND subscription_queue_items.queue_id = '$queue_id')
                    WHERE subscription_queue_items.is_sent = 0";
            $contacts = $this->oDb->getRows($sql);
        }
        $this->sId = 'id';
        $this->load($queue_id);

        $n = sizeof($contacts);
		$n = ($n > MAILER_QUEUE) ? MAILER_QUEUE : $n;
//        $iterations = ceil($n/MAILER_QUEUE);

        for ($i = 0; $i < $n; $i ++)
        {
//            if ($i == $iterations) $remains = $n%100;
//            else $remains = MAILER_QUEUE;
//            $contacts_to_send = array_slice($contacts, $i * MAILER_QUEUE - 100, $remains);				
            $contacts_to_send = array_slice($contacts, $i, 1);

			try
			{
				$obj_m->sendMail($this->aData['subject'], $this->aData['template'], $this->aData['template_translated'], LBL_FROM, $contacts_to_send, $this->aData['is_test'], $queue_id);
				$this->setContactsSent($queue_id, $contacts_to_send);
			}
			catch(Exception $e)
			{
				Logger::logStackTrace("Mailer error: ".$e);
			}
        }
    }


    public function getNotCompletedQueues()
    {
        return $this->oDb->getHash("SELECT queue_id, queue_id FROM subscription_queue_items WHERE is_sent = 0 GROUP BY queue_id");
    }
	
	public function getTemplate($queue_id)
	{
		$queue_id = $this->oDb->escape($queue_id);
		return $this->oDb->getField("SELECT template FROM subscription_queue WHERE id = '$queue_id'");
	}
}
?>