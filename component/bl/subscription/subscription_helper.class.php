<?php

require_once '../lib/core/dbitem.class.php';

class SubscriptionHelper extends DbItem
{
    public function __construct() { }

    public static function processEmailBody($body) {
		if (get_magic_quotes_gpc()) $body = stripslashes($body);
		// update images paths
        $tpl = new Smarty();
        $tpl->assign('body', $body);
        $body = $tpl->fetch('notifications/subscription_common.tpl');

//		$body = str_replace('src="', 'src="http://'.$_SERVER['HTTP_HOST'].SITE_REL.'/', $body);
//		$body = str_replace("src='", "src='http://".$_SERVER['HTTP_HOST'].SITE_REL.'/', $body);
		return $body;
	}

	public static function replaceBodyPlaceholders($data) {
		$required = array('hash', 'mail_id', 'user_name');
		foreach ($required as $k => $v) if (!isset($data[$v])) $data[$v] = '';

		$replacements = array();
		$replacements['[UNSUBSCRIBE]'] = 'http://'.$_SERVER['HTTP_HOST'].SITE_REL.'/pub/subscription_handler.php?u='.$data['hash'];
		$replacements['[VIEW_IN_BROWSER]'] = 'http://'.$_SERVER['HTTP_HOST'].SITE_REL.'/pub/subscription_view.php?u='.$data['hash'].'&m='.$data['mail_id'].'&view_in_browser=1';
		$replacements['[VIEW_TRANSLATED_IN_BROWSER]'] = 'http://'.$_SERVER['HTTP_HOST'].SITE_REL.'/pub/subscription_view.php?u='.$data['hash'].'&m='.$data['mail_id'].'&view_translated_in_browser=1';
		$replacements['[USERNAME]'] = $data['user_name'];
        $replacements['[VIEW_IN_MAILBOX]'] = 'http://'.$_SERVER['HTTP_HOST'].SITE_REL.'/pub/subscription_view.php?u='.$data['hash'].'&m='.$data['mail_id'].'&view_in_mailbox=1';

		return $replacements;
	}
	

}
?>