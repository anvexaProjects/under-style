<?php

require_once '../lib/core/dbitem.class.php';
require_once '../component/bl/subscription/subscription_queue.class.php';
require_once '../component/bl/subscription/subscription_helper.class.php';

class SubscriptionMailing extends DbItem 
{
    public function __construct() { }

	public function sendMail($subject, $body, $body_translated, $from, $contacts, $is_test, $queue_id) {
		require_once '../lib/3rdparty/swift/swift_required.php';
        $replacements = array();
        $to = array();
        foreach ($contacts as $c) {
            $to[$c['email']] = $c['fname'].' '.$c['lname'];
			$rdata = array('hash' => $c['hash'], 'mail_id' => $queue_id, 'user_name' => $c['fname'].' '.$c['lname']);
            $replacements[$c['email']] = SubscriptionHelper::replaceBodyPlaceholders($rdata);
        }

		try 
		{
			$body = SubscriptionHelper::processEmailBody($body);
			// create message
			$message = Swift_Message::newInstance();
			$message->setSubject($subject);
			$message->setBody($body, 'text/html');
			$message->setFrom($from);
			$message->setTo($to);
			
			$transport = Swift_SmtpTransport::newInstance(MAILER_HOST, MAILER_PORT, MAILER_TYPE);
			$transport->setUsername(MAILER_LOGIN);
			$transport->setPassword(MAILER_PASSWORD);
			
			$mailer = Swift_Mailer::newInstance($transport);
			
			$decorator = new Swift_Plugins_DecoratorPlugin($replacements);
			$mailer->registerPlugin($decorator);
			//Use AntiFlood to re-connect after 100 emails
			$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100)); 
		
			$mailer->batchSend($message);
		}
		catch (Exception $e)
		{
			throw $e;
		}		
	}
	
	public function getUserEmail($user_data, $mail) {
		$rdata = array('hash' => $user_data['hash'], 'mail_id' => $mail['id'], 'user_name' => $user_data['fname'].' '.$user_data['lname']);
		$replacements = $this->getReplacements($rdata);
		$rs = str_replace(array_keys($replacements), array_values($replacements), $mail['template']);
		$rs = $this->processEmailBody($rs);
		return $rs;
	}
	
	public function load($mail_id) {
		$this->sId = 'id';
		parent::load((int) $mail_id);
		return $this->aData;
	}
}
?>