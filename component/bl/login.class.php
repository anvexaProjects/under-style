<?php

require_once '../lib/core/dbitem.class.php';

class Login extends DbItem
{
    public $e = '';
    public $role;
    
    public function  __construct() {
        parent::__construct();
    }

    public function doLogin($login, $pass, $roles = array(USER), $set_session = true) {

        if(trim($login) == '' || trim($pass) == '') {
            $this->e = ERR_ADD_LOGIN_PASS;
        } else {
            $login = $this->oDb->escape($login);
            $pass = md5(SALT.$pass.SALT);
            $roles = empty($roles) ? 'NULL' : implode("', '", $roles);

            $sql = "SELECT * FROM users WHERE login = '$login' AND pass = '$pass' AND role IN ('$roles')";
            $rs = $this->oDb->getRow($sql);
            
            if (empty($rs)) {
                $this->e = ERR_LOGIN;
            } elseif ($rs['is_locked'] == 1) {
                $this->e = ERR_ACCOUNT_LOCKED;
            } else {
                $this->oDb->update('users', array('logged' => date('Y-m-d H:i:s')), "id='".$rs['id']."'");
                if ($set_session) $this->setSessionVars ($rs);
                $this->role = $rs['role'];
            }
			if (isset($_POST['remember']))
			{
				$_SESSION['cookie']['login'] = $login;
				$_SESSION['cookie']['pass'] = $pass;
			}			
        }
    }

    public function setSessionVars($data) {
        $_SESSION['login'] = $data['login'];
        $_SESSION['name'] = $data['name'];
		//$_SESSION['lname'] = $data['lname'];
        $_SESSION['role']  = $data['role'];
        $_SESSION['user_id'] = $data['id'];
    }
}
 ?>