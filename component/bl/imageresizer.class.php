<?php
/*
  Description: Resize images
  Autor:       Oleg Koshkin
  Data:        03-02-2008
  Version:     1.1

  mailto:      WizardF@narod.ru
  copyright:   (C) 2007 Oleg Koshkin
*/

class ImageResizer
{
	// Resize image to fit new $width and $height. Will change image proportion
	function ResizeImgSymetric($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=90)
	{
		if (!file_exists($src)) return false;

		$size = getimagesize($src);

		if ($size === false) return false;

		// not nessesary to resize
		if($size[0] == $width)
		{
			@copy($src, $dest);
			return true;
		}


		// get format by MIME
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
		$icfunc = "imagecreatefrom" . $format;
		if (!function_exists($icfunc)) return false;

		$x_ratio = $width / $size[0];
		$y_ratio = $height / $size[1];

		$ratio = min($x_ratio, $y_ratio);
		$use_x_ratio = ($x_ratio == $ratio);


		$new_width = $use_x_ratio ? $width : floor($size[0] * $ratio);
		$new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);
		$new_left = $use_x_ratio ? 0 : floor(($width - $new_width) / 2);
		$new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

		$isrc = $icfunc($src);
		$idest = imagecreatetruecolor($width, $height);

		imagefill($idest, 0, 0, $rgb);
		imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
		$new_width, $new_height, $size[0], $size[1]);

		imagejpeg($idest, $dest, $quality);

		imagedestroy($isrc);
		imagedestroy($idest);

		return true;
	}

	// Resize image to fit new $width and $height. Will NOT change image proportion
	function ResizeImgAsymetric($src, $dest, $width, $height, $ratio_type='auto', $rgb=0xFFFFFF, $quality=90, $allowEnlarge=false)
	{
		if (!file_exists($src)) return false;

		$size = getimagesize($src);

		if ($size === false) return false;

		// do not resize images
		if($size[0] == $width)
		{
			@copy($src, $dest);
			return true;
		}


		// get format by MIME
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
		$icfunc = "imagecreatefrom" . $format;
		if (!function_exists($icfunc)) return false;

		$x_ratio = $width / $size[0];
		$y_ratio = $height / $size[1];

		$ratio = min($x_ratio, $y_ratio);
		$use_x_ratio = ($x_ratio == $ratio);

		switch($ratio_type)
		{
			case 'x':
				$use_x_ratio = true;
				$ratio = $x_ratio;
				break;
			case 'y':
				$use_x_ratio = false;
				$ratio = $y_ratio;
				break;
		}


		$new_width = $use_x_ratio ? $width : floor($size[0] * $ratio);
		$new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);


//		echo $size[0] .'--'. $new_width .'--'. $size[1] .'--'. $new_height;
//		exit;
		
		if(!$allowEnlarge && $size[0] < $new_width && $size[1] < $new_height )
		{
			// do not resize images
			@copy($src, $dest);
			return true;
		}

		$isrc = $icfunc($src);
		$idest = imagecreatetruecolor($new_width, $new_height);

		imagefill($idest, 0, 0, $rgb);

		imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $new_width, $new_height, $size[0], $size[1]);

		imagejpeg($idest, $dest, $quality);

		imagedestroy($isrc);
		imagedestroy($idest);

		return true;
	}

	public static function cutImage($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=90, $center_x = false)
	{
	    if (!file_exists($src)) return false;
	    $size = getimagesize($src);
	    if ($size === false) return false;
		
		
		
        // get format by MIME
        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
        $icfunc = "imagecreatefrom" . $format;
        $icfunc2 = "image" . $format;
        if (!function_exists($icfunc)) return false;
        if (!function_exists($icfunc2)) return false;

        $isrc = $icfunc($src);
	    $idest = imagecreatetruecolor($width, $height);

	    imagefill($idest, 0, 0, $rgb);

		$src_x = 0;
		if ($center_x)
		{
			$src_x = round(($size[0]-$width)/2);
			if ($src_x < 0) $src_x = 0;
		}


        //imagecopy($idest, $isrc, 0, 0, $src_x, 0, $width, $height);
		imagecopy($idest, $isrc, 0, 0, 0, 0, $size[0] < $width ? $size[0] : $width, $size[1] < $height ? $size[1] : $height);

        $icfunc2($idest, $dest, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);
        return true;

	}

	static function cutAndResizeImage($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=90, $center_x = false)
	{
		list($width_src, $height_src) = getimagesize($src);
		ImageResizer::ResizeImgAsymetric(
		    $src, $dest, $width, $height, ImageResizer::getLargerSide($width_src, $height_src, $width, $height)
		);
		ImageResizer::cutImage(
		    $dest, $dest, $width, $height, $rgb, $quality, $center_x               	
		);
	}
	
	static function getLargerSide($src_width, $src_height, $dest_width, $dest_height)
	{
	    if (0 === $src_width || 0 === $src_width) return false;
	    if ($src_width/$dest_width < $src_height/$dest_height)
	    {
	        return 'x';
	    }
	    else
	    {
	        return 'y';
	    }
	}

	function addBorder($src, $dest, $border, $rgb=0xFFFFFF, $quality=90)
	{
	    if (!file_exists($src)) return false;
	    $size = getimagesize($src);
	    if ($size === false) return false;
	
        // get format by MIME
        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
        $icfunc = "imagecreatefrom" . $format;
        $icfunc2 = "image" . $format;
        if (!function_exists($icfunc)) return false;
        if (!function_exists($icfunc2)) return false;

        $isrc = $icfunc($src);
	    $idest = imagecreatetruecolor($size[0]+2*$border, $size[1]+2*$border);

	    imagefill($idest, 0, 0, $rgb);


		imagecopyresampled($idest, $isrc, $border, $border, 0, 0, $size[0], $size[1], $size[0], $size[1]);

		imagejpeg($idest, $dest, $quality);

		imagedestroy($isrc);
		imagedestroy($idest);

		return true;
	}

}
?>