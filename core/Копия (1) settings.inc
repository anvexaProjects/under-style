<?php
// db settings
define('DBHOST', '127.0.0.1');
define('MAINDB', 'db1018222_main');
define('DBUSER', 'root');
//define('DBPASSWD', 'QazBgDvg');
define('DBPASSWD', 'zxcqwe');
define('SYS_ADMIN_MAIL', 'admin@beltop.msk.ru');   // for mail notifications
define('LBL_FROM', 'info@under-style.ru');   // mailer deamon
define('SITE_ADR', 'net.under-style');

//domains
// DOMAIN_0 is base site domain
// DOMAIN_1 - DOMAIN_SOME is sub domains
define('DOMAIN_0', 'net.under-style');
define('DOMAIN_1', 'net.under-style');
define('DOMAIN_2', 'net.under-style');
define('DOMAIN_3', 'net.under-style');
define('DOMAIN_4', 'net.under-style');

// path
define('SITE_ABS', '/var/www/under-style');
define('SITE_REL', '');
define('QUICK_FORM_PATH', SITE_ABS . '/lib/pear');
define('INNOVA_IMG', '/upload/cms_images');
define('UPLOAD_DIR', '/upload/images');
define('UPLOAD_THUMB', '/upload/thumb');
define('LOG_FILE_PATH', '../logs/error.log');
define('WRN_FILE_PATH', '../logs/warning.log');
define('SMARTY_COMPILE_DIR', '../templates_c');
define('SMARTY_CACHE_DIR', '../templates_ch');
define('SMARTY_TEMPLATE_DIR', '../templates');
define('SMARTY_TRUSTED_DIR', '../component');
define('FEEDBACK_DIR', '/upload/feedback');

//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_STRICT);
//ini_set('log_error', 'on');
//ini_set('error_log', SITE_ABS . '/upload/php_error.log');

// CMS settigs
define('IS_LOCALIZATION', '1');
define('CMS', 'diamondcms');   // cms name
define('CMS_LANG', 'ru');   // cms lang
define('ALLOW_SINGLE_TAB', '0'); // 1 - show all tabs in one while edit article
define('ALLOW_ADVANCED_MODE', '1'); // 1 - show full icon set for TinyMCE
define('TRUNCATE_ARTICLES_NAMES_IN_SIMPvLE_TREE', '120'); // number of symbols which will be shown in article name in simpletree and table


// additional options
define('SALT', 'diamond_shop_engine');
define('IS_SEND_ERRORS', '1');
define('DEFAULT_LANG', 'ru');
define('DATE_FORMAT', '%m.%d.%Y');

define('ADMIN', '1');
define('USER', '3');
define('SALER', '4');
define('DESIGNER', '5');
define('SALERS_REDACTOR', '6');
define('DESIGNERS_REDACTOR', '7');

// define log formats
define('MAIL_LOG_DIR', '/upload/mail');
define('LANG_RUS', 1);
define('LANG_ENG', 2);
define('ERROR', '-1');
define('NOERROR', '0');
define('LOG_FRM_SQL', 'SQL "%s",  error: "%s"');
define('LOG_FRM_SQL_OLD', "[%s] 
\n Error '%s' occured in '%s' page  '%s' 
\n");
define('LOG_FRM_GENERAL', "[%s] 
\n Error '%s' occured in '%s' page  '%s' 
\n");


ini_set('upload_max_filesize', '50M');
ini_set('post_max_size', '50M');
ini_set('max_file_uploads', '50');
?>
