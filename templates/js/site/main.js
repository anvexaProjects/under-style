$(document).ready(function () {
    $('.active').children('a').eq(0).click(function () {
        $('.active ul.nav').toggleClass('hidden');
        return false;
    });
    if (document.location.hash == '#cat') {
        $('.vendor ul').toggleClass('hidden');
        document.location.hash = "";
    }

    // $(".banners_right").carouFredSel({
    //     circular: true,
    //     auto: {play: true, timeoutDuration: 5000},
    //     items: {visible: 1},
    //     width: 240,
    //     height: 530
    // });

    if ($(".salers_carousel").length) {
        $("#salers").carouFredSel({
            circular: false,
            infinite: false,
            auto: false,
            responsive: true,
            prev: {button: "#salers_prev", key: "left"},
            next: {button: "#salers_next", key: "right"},
            pagination: "#salers_pag"
        });
    }
    if ($(".showroom_carousel").length) {
        var b = $("#showroomImgs");
        var a = $("#showroomThumbs");
        $("a", b).each(function (d) {
            $(this).attr("cntr", d);
            $(this).addClass("item_" + d);
        });
        b.carouFredSel({
            synchronise: ["#showroomThumbs", false, true],
            circular: false,
            infinite: false,
            auto: false,
            width: 950,
            height: 438,
            align: "left",
            items: {visible: 1},
            scroll: {
                fx: "directscroll", onAfter: function () {
                    var d = $(this).triggerHandler("currentPosition");
                    a.find(".current").removeClass("current");
                    a.find(".item_" + d).addClass("current");
                }
            },
            prev: "#showroom_prev",
            next: "#showroom_next"
        });
        a.carouFredSel({circular: false, infinite: false, auto: false, responsive: true, items: {visible: 7}});
        $("img", a).each(function (d) {
            $(this).attr("cntr", d);
            $(this).addClass("item_" + d);
            $(this).attr("style", "");
            $(this).click(function () {
                var e = $(this).attr("cntr");
                b.trigger("slideTo", [".item_" + e]);
            });
        });
        $("#showroomThumbs img:eq(0)").addClass("current");
    }
    if ($(".fancybox-thumb").length) {
        $(".fancybox-thumb").fancybox({
            padding: 0,
            margin: 0,
            loop: false,
            onStart: function () {
                $("#showroomImgs").trigger("pause");
            },
            onClosed: function () {
                $("#showroomImgs").trigger("play");
            },
            prevEffect: "none",
            nextEffect: "none",
            tpl: {
                prev: '<a title="Предыдущая страница" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
                next: '<a title="Следующая страница" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>'
            },
            helpers: {title: {type: "outside"}, thumbs: {width: 116, height: 80}},
            autoCenter: false,
            afterLoad: function () {
                $.extend(this, {
                    aspectRatio: true,
                    autoSize: true,
                    type: "html",
                    content: '<div class="fancybox-image ip_tooltipImg" rel="' + this.href + '" style="background-image:url(' + this.href + '); background-size: cover; background-position:50% 50%;background-repeat:no-repeat;height:100%;width:100%;" /></div>'
                });
            },
            afterShow: c
        });
    }
    if ($(".viewType").length) {
        $(".viewType a").click(function () {
            if ($(this).hasClass("cur")) {
                return false;
            } else {
                $(".viewType a").removeClass("cur");
                $(this).addClass("cur");
                $("#products").removeClass("productsBlock").removeClass("productsList");
                $("#products").addClass($(this).attr("rel"));
            }
            return false;
        });
        $(".prodItem, .prodShortInfo").hover(function () {
            var e = $(this).parent();
            var d = $(document).width();
            $(".prodShortInfo", e).show();
            var f = $(".prodShortInfo", e).offset().left + $(".prodShortInfo", e).width() + 2;
            if (d < f) {
                l_coord = Math.round(40 + (f - d) + 25);
                $(".prodShortInfo", e).css("left", "-" + l_coord + "px");
            }
        }, function () {
            var d = $(this).parent();
            $(".prodShortInfo", d).hide();
            $(".prodShortInfo", d).css("left", "-40px");
        });
    }
    if (!$(".catSelect").length) {
    } else {
        $(".collections").slideUp(1);
        $("div.submenu").one("mousedown", function () {
            $("div.prodItemWrapper").hide();
        });
        $(".checkbox").click(function () {
            var k = $("div.prodItemWrapper");
            k.hide().removeClass("vis");
            if ($(this).hasClass("checked")) {
                $(this).removeClass("checked");
            } else {
                $(this).addClass("checked");
            }
            var h = [];
            var j = [];
            var e = [];
            var n = [];
            var f = $(".checkbox.checked");
            if (f.length == 0) {
                k.show().addClass("vis");
                $(".collections").slideUp(1);
            } else {
                for (var g = 0; f.length > g; g++) {
                    var o = $(f[g]).attr("rel");
                    var m = $(f[g]).text();
                    m = m.replace(/ /g, "_");
                    m = m.replace(/&/g, "\\&");
                    m = m.replace(/,/g, "\\,");
                    switch (o) {
                        case"factory":
                            h.push("[" + o + "=" + m + "]");
                            break;
                        case"collection":
                            j.push("[" + o + "=" + m + "]");
                            break;
                        case"place":
                            e.push("[" + o + "=" + m + "]");
                            break;
                        case"rub":
                            n.push("[" + o + "=" + m + "]");
                            break;
                    }
                }
                if (h.length > 0) {
                    k = k.filter(h.join(", "));
                }
                if (j.length > 0) {
                    k = k.filter(j.join(", "));
                }
                if (e.length > 0) {
                    k = k.filter(e.join(", "));
                }
                if (n.length > 0) {
                    k = k.filter(n.join(", "));
                }
                k.show().addClass("vis");
                var l = $(".checked", $(".factory"));
                if (l.length > 0) {
                    var d = new Array();
                    for (var g = 0; g < l.length; g++) {
                        var p = $(l[g]).text();
                        d.push(p);
                    }
                    $.ajax({
                        type: "POST",
                        url: root + "/pub/ajax_get_collections.php",
                        data: {factories: d},
                        dataType: "json",
                        success: function (q) {
                            var i = $(".checkbox", "#collection");
                            i.hide();
                            i.each(function () {
                                for (var r = 0; r <= q.length; r++) {
                                    if ($(this).text() == q[r]) {
                                        $(this).show();
                                    }
                                }
                            });
                            $(".collections").slideDown();
                        }
                    });
                } else {
                    $(".collections").slideUp();
                }
            }
            return false;
        });
        $(".scroll-pane").jScrollPane();
    }
    $("div.letters a").click(function () {
        $("div.letters a").removeClass("bold");
        $(this).addClass("bold");
        $("div.catEl[l!=" + $(this).text() + "], div.cmpEl[l!=" + $(this).text() + "]").hide();
        $("div.catEl[l=" + $(this).text() + "], div.cmpEl[l=" + $(this).text() + "]").show();
        return false;
    });
    $("#descr").hide();
    $("#a_descr").click(function () {
        $("#descr").show();
        $("#feat").hide();
        $(this).addClass("cur");
        $("#a_feat").removeClass("cur");
        return false;
    });
    $("#a_feat").click(function () {
        $("#feat").show();
        $("#descr").hide();
        $(this).addClass("cur");
        $("#a_descr").removeClass("cur");
        return false;
    });
    if ($("div.prodItemWrapper").length > 0) {
        $("div.cattopnavbar p > b:first").text($("div.prodItemWrapper").length);
    } else {
        $(".filter").hide();
        $("div.cattopnavbar p > b:first").text("");
        //$("div.cattopnavbar p > span").text(no_prods);
    }
    $(".checkbox").click(function () {
        if (($(".vis").length > 0)) {
            $("div.cattopnavbar p > b:first").text($(".vis").length);
            $("div.cattopnavbar p > span").text("Товаров в категории " + $(".title h1").text());
            paging();
        } else {
            $("div.cattopnavbar p > b:first").text("");
            $("div.cattopnavbar p > span").text(no_prods);
        }
    });
    $("#pricing").hide();
    $("#spec").hide();
    $("#a_pricing").click(function () {
        $("#pricing").slideToggle();
        return false;
    });
    $("#a_spec").click(function () {
        $("#spec").slideToggle();
        return false;
    });
    $("a.price").click(function () {
        $("div.catElp[price!=" + $(this).text() + "]").hide();
        $("div.catElp[price=" + $(this).text() + "]").show();
        var d = $("div.catElp:visible");
        if (d.length == 0) {
            $("#no_prices").show();
            $("#no_prices_value").text($(this).text());
        } else {
            $("#no_prices").hide();
        }
        $("a.price").css("font-weight", "normal");
        $(this).css("font-weight", "bold");
        return false;
    });
    $("a#save_item").next().hide();
    $("a#save_item").click(function () {
        $.get("../../../pub/saved_item.php", {item: $(this).attr("item")});
        $(this).slideUp("slow");
        $(this).next().slideDown("slow");
        return false;
    });
    function c() {
        var g = $(".fancybox-image").attr("rel");
        var d = $('a[href="' + g + '"]').index();
        if ($("#showroom_dots div.i_" + d).length > 0) {
            $("div.ipicture:visible").remove();
            $(".fancybox-image").empty();
            var f = $("#showroom_dots div.i_" + d);
            var e = $(f).html().replace(new RegExp("_tmp_", "gm"), "");
            $(".fancybox-image").html(e);
            $(".fancybox-image div.ipicture_" + d + ":visible").iPicture();
        }
    }

    paging();
});
function paging(j, f) {
    if (!j) {
        j = 1;
    }
    if (!f) {
        f = 40;
    }
    $("#paging_block").html("");
    $("#nav_pager").html("");
    var d;
    (j == 1) ? d = 0 : d = (j - 1) * f;
    var a = $(".vis");
    var b = a.length / f;
    if (b > 1) {
        var c = $("#paging_block");
        c.append('<p class="floatl">Вы смотрите страницу:</p>');
        for (var e = 1; e <= b + 1; e++) {
            if (e == j) {
                c.append('<a href="" class="pager_item" onclick="return false">' + e + "</a>");
            } else {
                c.append('<a href="" class = "cur" onclick="paging(' + e + ');return false">' + e + "</a>");
            }
        }
        var h = $("#nav_pager");
        if (j > 1) {
            h.append('<a href="" onclick="paging(' + (j - 1) + ');return false" class="cur">&larr; Предыдущая страница</a>');
        }
        if (j < e) {
            h.append('<a href="" onclick="paging(' + (j + 1) + ');return false">Следующая страница &rarr;</a>');
        }
        var g = a.slice(d, d + f);
        a.hide();
        g.show();
    }
}


jQuery(document).ready(function () {
    $('.changelink').attr('href', 'tel:+74959843260');
});