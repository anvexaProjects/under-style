function submitByImage(pfrm, action){ var obj = document.getElementById(pfrm);obj.action = action;}
function db_dump(message){ $.get(
        "dump_db.php",
        function(data){alert(data);}
    );
}
function toggleStat(classname){ var items = $("tr." + classname);
    if ($(items).is(":visible")) $(items).hide();
    else $(items).show();
}
function inlineEdit(index, mod){ $("#view__" + index).hide(); $("#edit__" + index).show();}
function inlineInitState(index, mod){ $("#view__" + index).show(); $("#edit__" + index).hide();}
function inlineApply(index, mod){ var params = '';
    $("#edit__" + index + " input, #edit__" + index + " select").each(function(){
        params = params + $(this).attr("name") + "=" + encodeURIComponent($(this).val()) + "&";
    });
    params = params + mod + '_update';
    $.getJSON('../admin/subscription_ajax.php?' + params,
        function(rs){
            if(rs.e) alert(rs.e);
            else{             var i = 0;
                $.each(rs.data, function(k, v){
					$("#view__" + index + " td:eq(" + i + ")").text(v);
                    i ++;
                });
                inlineInitState(index);
				if (typeof rs.is_active !== undefined) {
					if (rs.is_active == 0) $("#view__" + index + " td").addClass("dis");
					if (rs.is_active == 1) $("#view__" + index + " td").removeClass("dis");
				}
            }
        }
    );
}
function inlineDelete(index, mod, message) {
	if ((message != undefined && message != '' && confirm(message)) || (message == undefined || message == ''))
	{
		$.getJSON('../admin/subscription_ajax.php?' + mod + '_delete&id=' + index,
			function(rs){
				$("#view__" + index).fadeOut(300);
			}
		);
	}
}
//var yaCounter23717377 = new Ya.Metrika({id: 23717377});
//yaCounter23717377.replacePhones();


