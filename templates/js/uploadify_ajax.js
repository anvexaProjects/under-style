function initImagesTable() {
    $.get(asyncImagerUrl,
        {action: 'init', art_id: art_id, control: control},
        function(data) {
            $("#images_list").empty();
            $("#images_list").append(data);
			if (!data) $("#delete_images_link").hide();
			else $("#delete_images_link").show();
            $("#images_list").tableDnD({
                onDrop: function(table, row) {
                    var ids = '';
                    $.each($("tr", table), function(){
                        ids += this.id.toString() + '|';
                    })
                    $.get(asyncImagerUrl,
                        {action: 'order', ids: ids}
                    );
                }
            });
        }
    );
}
function deleteImages() {
    var ids = '';
    $.each($("#images_list .delete_image:checked").parents("#images_list tr"), function(){
        ids += this.id.toString() + '|';
        $("#" + this.id.toString()).hide();
    });
    $.get(asyncImagerUrl,
        {action: 'delete', ids: ids, control: control}
    );
}
function toggleInlineEdit(id, toggle) {
    var td = $("#iedit_" + id);
	var td_title = $("#tiedit_" + id);
    var tdc = $("td.iedit_" + id);
    if (toggle == true) {
        $("span", td).hide();
        $("input", td).show();
		
		$("span", td_title).hide();
        $("input", td_title).show();
		
        $("a.iedit", tdc).hide();
        $("a.inline", tdc).show();
    } else {
        $("span", td).show();
        $("input", td).hide();
        $("input", td).val($("span", td).text());
		
		$("span", td_title).show();
        $("input", td_title).hide();
        $("input", td_title).val($("span", td_title).text());
		
        $("a.iedit", tdc).show();
        $("a.inline", tdc).hide();
    }
};
function updateItem(id) {
    var td = $("#iedit_" + id);
    var inp = $("input", td);
    var txt = $("span", td);
	var td_title = $("#tiedit_" + id);
	var inp_title = $("input", td_title);
    var txt_title = $("span", td_title);
    $.get(asyncImagerUrl, 
        {id: id,
		 value_desc: $(inp).val(),
		 value_title: $(inp_title).val(),
		 action: 'dupdate'}, 
        function(rs) {
            $(inp).val($(rs).find('img_description').text());
            $(txt).text($(rs).find('img_description').text());
			
			$(inp_title).val($(rs).find('img_title').text());
            $(txt_title).text($(rs).find('img_title').text());
			
            toggleInlineEdit(id, false);
        }
    );
}