$(document).ready(function(){
    if ($('#tree').length > 0) loadInitTree(tid);
});

tree_nodes = ''; 

function loadInitTree(tree_id) {
    if (tree_id == null) tree_id = '';
    $.get(asyncUrl,
        {action: 'init', tree_id: tree_id},
        function(data) {
            $("#tree").empty();
            $("#tree").append(data);
            expandTreeNodes(tree_id, true);
            initSimpleTree();
            $("img.trigger:eq(0)").trigger("click");
        }
    );
}

function initSimpleTree(nodes) {
    simpleTreeCollection = $('.simpleTree').simpleTree({
        autoclose: false,
        drag: $("#js").getUrlParam("drag"),
        drag_message: 'Элемент будет перемещен. Продолжить?',
        afterClick:function(node){
            if (!$(node).hasClass('doc') && !$(node).hasClass('doc-last'))
              loadTableView($('span:first',node).attr('id'));
        },
        afterDblClick:function(node){
            //alert("text-"+$('span:first',node).text());
        },
        afterMove:function(destination, source, pos){
            move(destination.attr('id'), source.attr('id'), pos);
        },
        afterAjax:function(){
        	func = this;
        	if (tree_nodes.length > 0) {
	        	$.each(tree_nodes, function(i){
	        		var el = $("#" + this + " > ul.ajax");
	        		if (el.length > 0) {
	        			simpleTreeCollection.get(0).setAjaxNodes(el, this, func);
	        		} else if ($("#" + this + " > ul:hidden").length > 0) {
	        			$("#" + this + " > img").trigger('click');
	        		}
	        	});
        	}
        },
        animate:true,
        afterContextMenu:function(){
            
        }
        //,docToFolderConvert:true
    }); 
}

function expandTreeNodes(tree_id, remove_first) {
	$.getJSON(asyncUrl,
		{action: 'tree_path', tree_id: tree_id}, 
        function(data){
    		tree_nodes = data;
    	}
    );
}

function loadTableView(id) {
    $("#progress").show();
    var date = new Date();
    
    $.get(asyncUrl,
        {action: 'table', id: id, dt: date.getUTCMilliseconds()},
        function(data) {
            $("#progress").hide();
            $("#data").empty();
            $("#data").append(data);
        }
    );    
}

function deleteArticle(id, parent_id) {
    $.get(asyncUrl,
        {action: 'delete', id: id, tree_id: parent_id},
        function() {
            loadInitTree(parent_id);
            loadTableView(parent_id);
        }
    );  
}

function move(destination, source, pos) {
    $.get(asyncUrl,
        {action: 'move', destination: destination, source: source, pos: pos},
        function() {
            //loadInitTree(id);
        }
    );  
}

function changeOrder(id, order) {
    $.get(asyncUrl,
        {action: 'change_order', order: order, id: id},
        function(data) {
            loadTableView(data);
        }
    );    
}

