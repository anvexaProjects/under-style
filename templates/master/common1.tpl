<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {*<title>Андерграунд</title>*}
    {include_php file="master/meta.inc.php"}

    {*{include_php file="master/files_aggregator.inc.php" type="css" rel_dir="/css" output="/templates_c/css/site/monolit_[APP_VERSION].css" enable=false compress=$smarty.const.ENABLE_COMPRESSION files="/fancybox/jquery.fancybox.css, /fancybox/jquery.fancybox-thumbs.css, /site/jquery-ui.css, /jquery.prettyphoto.css, /lightbox.css, /jquery.thickbox.css, /jquery.ipicture.css"}*}

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font.robotoslab.css" rel="stylesheet">
    <link href="/css/font.helvetica.css" rel="stylesheet">
    <link href="/css/font.ptsans.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    {*<link href="/jquery.formstyler.css" rel="stylesheet" />*}
    <link href="/css/style.css" rel="stylesheet">
    <link href="/templates/css/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
    <link href="/templates/css/fancybox/jquery.fancybox-thumbs.css" type="text/css" rel="stylesheet">
    <link href="/templates/css/jquery.ipicture.css?14.01.2014" type="text/css" rel="stylesheet">
    <link href="/templates/css/jquery.thickbox.css?14.01.2014" type="text/css" rel="stylesheet">
    <link href="/templates/css/lightbox.css?14.01.2014" type="text/css" rel="stylesheet">
    <link href="/templates/css/jquery.prettyphoto.css?14.01.2014" type="text/css" rel="stylesheet">
    <link href="/templates/css/fancybox/jquery.fancybox.css?14.01.2014" type="text/css" rel="stylesheet">
    <link href="/templates/css/fancybox/jquery.fancybox-thumbs.css?14.01.2014" type="text/css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
{assign var=is_home value=0}
{if $smarty.get.global.struct.1.path eq 'index' || $smarty.get.global.struct.1.path eq $smarty.get.global.lan}{assign var=is_home value=1}{/if}
<body>
<div class="page-loader">
    <div class="loader">Загрузка...</div>
</div>
<div class="container">

    <nav id="header" class="navbar">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <span>Андеграунд</span>
                    <span>интерьерный салон</span>
                    {*<img src="/images/imgpsh_fullsize.png">*}
                </a>
            </div>

            <!--<div class="collapse navbar-collapse">-->
            <div class="nav navbar-nav">
                <p class="navbar-text">+7 (495) 984-32-60</p>
                <a href="#callme" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#myModal">Заказать
                    звонок</a>
            </div>

            <div class="nav navbar-right">
                <address class="navbar-right">
                    Пн - Пт: с 10:00 до 20:00<br>
                    Сб: с 11:00 до 18:00<br>
                    <a href="#writeme" data-toggle="modal" data-target="#myModalSecond">Написать нам</a>
                </address>


            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    {include_php file = "content/treelister.inc.php" tpl_name = "master/menu_main.tpl" plevel=0 cond = "(as1.level IN (1, 2)) AND (as1.control IN ('fldr', 'actions', 'article', 'contacts', 'completed_projects', 'sale', 'designer_project', 'wholesale_buyers', 'subscription', 'company_list', 'catalogs') AND as1.is_hidden = 0)" }


    {include file="master/content.tpl"}
    {*{include file="master/content-12.02.2016.tpl"}*}

</div>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                {include_php file="content/lister.inc.php" tpl_name="master/footer_catalog.tpl" plevel=0 cond="as1.control='subdomen'" fields="a1.path, a1.name" }
            </div>
            <div class="col-sm-4">
                <header>
                    <h2>{$smarty.const.S_CONTACTS}</h2>
                    <hr class="border-light">
                </header>
                {include_php file="content/lister.inc.php" tpl_name="master/footer_contact.tpl" plevel=0 cond="as1.control='contacts'" fields="a1.content2" }

                {*<h4>Присоединяйтесь:</h4>*}
                {*<a href="#">*}
                {*<i class="fa fa-facebook" style="font-size: 24px;"></i>*}
                {*</a>*}
            </div>
        </div>
    </div>
</div>
<div id="copyright">
    <div class="container">
        <p>© 2010
            <script>new Date().getFullYear() > 2010 && document.write("-" + new Date().getFullYear());</script>
            Андеграунд<br/>
            Все права защищены
        </p>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel1">Заказать звонок</h4>
            </div>


            <div class="modal-body">
                <form id="feedbackform" class="form-horizontal" style="padding: 20px 20px 0 20px;">
                    <div class="form-group" style="width: auto;">
                        <!-- <label for="callme_name" class="col-sm-1 control-label">Имя</label> -->
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="callme_name" placeholder="Имя"
                                   style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">

                            <p class="danger_name" style="color: rgb(175, 68, 66); font-size: 14px; display: none;">
                                Введите правильное имя! </p>
                        </div>
                        <!-- <label for="callme_phone" class="col-sm-2 control-label" style="text-align: left;">Телефон</label> -->
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="callme_phone" placeholder="Телефон"
                                   style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">

                            <p class="danger_phone" style="color: rgb(175, 68, 66); font-size: 14px; display: none;">
                                Введите правильный номер телефона! </p>
                        </div>
                    </div>
                    <div class="form-group" style="width: auto;">
                        <!-- <label class="col-sm-2 control-label" style="width: auto;">Время звонка</label> -->
                        <div class="col-sm-6">
                            <i class="fa fa-chevron-down"></i>
                            <select id="callme_time" class="form-control"
                                    style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">
                                <option value="0" selected="">Время звонка</option>
                                <option value="10:00-11:00">10:00-11:00</option>
                                <option value="11:00-12:00">11:00-12:00</option>
                                <option value="12:00-13:00">12:00-13:00</option>
                                <option value="13:00-14:00">13:00-14:00</option>
                                <option value="14:00-15:00">14:00-15:00</option>
                                <option value="15:00-16:00">15:00-16:00</option>
                                <option value="16:00-17:00">16:00-17:00</option>
                                <option value="17:00-18:00">17:00-18:00</option>
                                <option value="18:00-19:00">18:00-19:00</option>
                                <option value="19:00-20:00">19:00-20:00</option>
                            </select>

                            <p class="danger_time" style="color: rgb(175, 68, 66); font-size: 14px; display: none;">
                                Выберите время звонка! </p>
                        </div>
                    </div>
                    <div class="form-group" style="width: auto;">
                        <div class="col-lg-12">
                            <textarea class="form-control" rows="3" id="callme_text" placeholder="Ваше сообщение"
                                      style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);"></textarea>

                            <p class="danger_sms" style="color: rgb(175, 68, 66); font-size: 14px; display: none;">
                                Введите Ваше сообщение! </p>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="callme_button">Отправить</button>
            </div>

            <div class="alert alert-success" style="text-align: center;display: none;">
                <strong>Поздравляем!</strong>Вы успешно заказали звонок!
            </div>


        </div>
    </div>
</div>


<!-- ModalSecond -->
<div class="modal fade" id="myModalSecond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
     style="display: none;">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel2">Письмо директору</h4>
            </div>

            <div class="modal-body">
                <form id="feedback" class="form-horizontal" style="padding: 20px 20px 0 20px;">
                    <div class="form-group" style="width: auto;">
                        <!-- <label for="callme_name" class="col-sm-1 control-label">Имя</label> -->
                        <div class="col-sm-6">
                            <input type="text" id="feedback_name" class="form-control terms_inp" name="name"
                                   placeholder="Имя"
                                   style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">

                            <p class="danger_feedback_name"
                               style="color: rgb(175, 68, 66); font-size: 14px; display: none;">Введите правильное
                                имя! </p>
                        </div>
                        <!-- <label for="callme_phone" class="col-sm-2 control-label" style="text-align: left;">Телефон</label> -->
                        <div class="col-sm-6">
                            <input type="text" id="feedback_phone" class="form-control" name="phone"
                                   placeholder="Телефон"
                                   style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">

                            <p class="danger_feedback_phone"
                               style="color: rgb(175, 68, 66); font-size: 14px; display: none;">Введите правильный номер
                                телефона!</p>
                        </div>
                    </div>

                    <div class="form-group" style="width: auto;">
                        <!-- <label for="callme_name" class="col-sm-1 control-label">Имя</label> -->
                        <div class="col-sm-6">
                            <input type="email" id="feedback_email" class="form-control email" name="email"
                                   placeholder="E-mail"
                                   style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);">

                            <p class="danger_feedback_email"
                               style="color: rgb(175, 68, 66); font-size: 14px; display: none;">Введите Ваш e-mail! </p>

                            <p class="validemail" style="color: rgb(175, 68, 66); font-size: 14px; display: none;"></p>
                        </div>
                    </div>
                    <div class="form-group" style="width: auto;">
                        <div class="col-lg-12">
                            <textarea class="form-control" id="feedback_text" rows="3" name="message"
                                      placeholder="Ваше сообщение"
                                      style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(0, 0, 0);"></textarea>

                            <p class="danger_feedback_text"
                               style="color: rgb(175, 68, 66); font-size: 14px; display: none;">Введите Ваше
                                сообщение! </p>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="feedback_button">Отправить</button>
            </div>

            <div class="alert alert-success" style="text-align: center;display: none;">
                Сообщение успешно отправлено!
            </div>

        </div>
    </div>

</div>


<!-- jQuery Version 1.11.1 -->
<script src="/js/jquery.js"></script>
<script src="/js/jQuery.scrollSpeed.js"></script>
{*<script type="text/javascript" src="/jquery.chained.min.js"></script>*}
<script type="text/javascript" src="/js/chained.js"></script>
{*<script type="text/javascript" src="/jquery.formstyler.js"></script>*}
<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>
{*<script src="http://masonry.desandro.com/masonry.pkgd.min.js"></script>*}
<script src="/templates/js/masonry.pkgd.js"></script>
<!-- <script src="http://desandro.github.io/imagesloaded/imagesloaded.pkgd.min.js"></script> -->
<script src="/templates/js/desandrolib.js"></script>
<script src="/templates/js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="/templates/js/fancybox/helpers/jquery.fancybox-thumbs.js" type="text/javascript"></script>
<script src="/templates/js/site/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script>
<script src="/templates/js/site/favorite.js" type="text/javascript"></script>
<script src="/templates/js/jquery/jquery.prettyphoto.js" type="text/javascript"></script>
<script src="/templates/js/site/main.js" type="text/javascript"></script>
<script src="/js/script.js"></script>
{literal}
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter23717377 = new Ya.Metrika({
                        id: 23717377,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () {
                        n.parentNode.insertBefore(s, n);
                    };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/23717377" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })
        (window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-47406713-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!--Openstat-->
    <span id="openstat2365193"></span>
    <script type="text/javascript">
        var openstat = {counter: 2365193, next: openstat, track_links: "all"};
        (function (d, t, p) {
            var j = d.createElement(t);
            j.async = true;
            j.type = "text/javascript";
            j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";
            var s = d.getElementsByTagName(t)[0];
            s.parentNode.insertBefore(j, s);
        })(document, "script", document.location.protocol);
    </script>
    <!--/Openstat-->

{/literal}



{literal}
    <script type="text/javascript">(function (w, doc) {
            if (!w.__utlWdgt) {
                w.__utlWdgt = true;
                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript';
                s.charset = 'UTF-8';
                s.async = true;
                s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                var h = d[g]('body')[0];
                h.appendChild(s);
            }
        })(window, document);
    </script>
{/literal}

<div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12"
     data-top-button="false" data-share-counter-type="disable" data-share-style="1" data-mode="follow" data-like-text-enable="false"
     data-mobile-view="true" data-icon-color="#ffffff" data-orientation="fixed-top" data-text-color="#000000"
     data-share-shape="round-rectangle" data-sn-ids="fb.in." data-share-size="30" data-background-color="#ffffff"
     data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1507502" data-counter-background-alpha="1.0"
     data-follow-in="andegraund_salon/" data-following-enable="false" data-exclude-show-more="false" data-selection-enable="false"
     data-follow-fb="Anderground2014/" class="uptolike-buttons" ></div>


<div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff"
     data-share-counter-size="12" data-top-button="false" data-share-counter-type="disable"
     data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true"
     data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000"
     data-share-shape="round-rectangle" data-sn-ids="fb." data-share-size="30"
     data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb."
     data-pid="1507504" data-counter-background-alpha="1.0" data-following-enable="false"
     data-exclude-show-more="false" data-selection-enable="true" class="uptolike-buttons"></div>

<div data-background-alpha="0.0" data-orientation="horizontal" data-text-color="#000000" data-share-shape="SHARP" data-buttons-color="#FFFFFF"
     data-sn-ids="fb." data-counter-background-color="#ffffff" data-share-size="LARGE" data-background-color="#ffffff"
     data-mobile-sn-ids="fb.vk.ok." data-preview-mobile="false" data-pid="1507506" data-counter-background-alpha="1.0" data-mode="like" data-following-enable="false" data-exclude-show-more="false" data-like-text-enable="true" data-selection-enable="true" data-mobile-view="true" data-icon-color="#ffffff" class="uptolike-buttons"></div>


</body>
</html>
