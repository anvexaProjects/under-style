<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Анерграунд</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font.robotoslab.css" rel="stylesheet">
    <link href="css/font.helvetica.css" rel="stylesheet">
    <link href="css/font.ptsans.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">

    <nav id="header" class="navbar">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <span>Андерграунд</span>
                    <span>интерьерный салон</span>
                </a>
            </div>

            <!--<div class="collapse navbar-collapse">-->
            <div class="nav navbar-nav">
                <p class="navbar-text">+7 (495) 984-32-60</p>
                <button type="button" class="btn btn-primary navbar-btn">Заказать звонок</button>
            </div>

            <div class="nav navbar-right">
                <address class="navbar-right">
                    Пн - Пт: с 10:00 до 20:00<br>
                    Сб: с 11:00 до 18:00<br>
                    <a href="#">Написать нам</a>
                    <!--<abbr title="Phone">P:</abbr> (123) 456-7890-->
                </address>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <nav id="menu-top" class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="padding-left: 35px;">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/img/diamond.png" alt="Анерграунд">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav" style="padding-top: 35px;">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">о компании</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">интерьерные решения</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">каталог товаров</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">партнерам и клиентам</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>



    {*{include file="master/content.tpl"}*}
    {include file="master/content-12.02.2016.tpl"}

</div>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h2>Разное
                    <hr class="border-light">
                </h2>
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="/about/contacts/">Контакты</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Производители</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Вакансии</a></li>
                            <li><a href="#">Распродажа</a></li>
                            <li><a href="#">Заказать звонок</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Карта сайта</a></li>
                            <li><a href="#">Письмо директору</a></li>
                            <li><a href="#">Новости</a></li>
                        </ul>
                    </div>
                </div>
                <h2> Каталог товаров
                    <hr class="border-light">
                </h2>
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Каталог сантехники</a></li>
                            <li><a href="#">Villeroy&Boch</a></li>
                            <li><a href="#">Teuco</a></li>
                            <li><a href="#">Margaroli</a></li>
                            <li><a href="#">Keuco</a></li>
                            <li><a href="#">Jacuzzi</a></li>
                            <li><a href="#">Duravit</a></li>
                            <li><a href="#">Huppe</a></li>
                            <li><a href="#">Hansgrohe</a></li>
                            <li><a href="#">Zehnder</a></li>
                            <li><a href="#">Tece</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Каталог сантехники</a></li>
                            <li><a href="#">Villeroy&Boch</a></li>
                            <li><a href="#">Teuco</a></li>
                            <li><a href="#">Margaroli</a></li>
                            <li><a href="#">Keuco</a></li>
                            <li><a href="#">Jacuzzi</a></li>
                            <li><a href="#">Duravit</a></li>
                            <li><a href="#">Huppe</a></li>
                            <li><a href="#">Hansgrohe</a></li>
                            <li><a href="#">Zehnder</a></li>
                            <li><a href="#">Tece</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Каталог сантехники</a></li>
                            <li><a href="#">Villeroy&Boch</a></li>
                            <li><a href="#">Teuco</a></li>
                            <li><a href="#">Margaroli</a></li>
                            <li><a href="#">Keuco</a></li>
                            <li><a href="#">Jacuzzi</a></li>
                            <li><a href="#">Duravit</a></li>
                            <li><a href="#">Huppe</a></li>
                            <li><a href="#">Hansgrohe</a></li>
                            <li><a href="#">Zehnder</a></li>
                            <li><a href="#">Tece</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <ul>
                            <li><a href="#">Каталог сантехники</a></li>
                            <li><a href="#">Villeroy&Boch</a></li>
                            <li><a href="#">Teuco</a></li>
                            <li><a href="#">Margaroli</a></li>
                            <li><a href="#">Keuco</a></li>
                            <li><a href="#">Jacuzzi</a></li>
                            <li><a href="#">Duravit</a></li>
                            <li><a href="#">Huppe</a></li>
                            <li><a href="#">Hansgrohe</a></li>
                            <li><a href="#">Zehnder</a></li>
                            <li><a href="#">Tece</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <h2> Контакты
                    <hr class="border-light">
                </h2>

                <p>Россия, Москва, ул. Балтийская, дом 9<br/>
                    +7 (495) 984 32 60<br/>
                    +7 (903) 796 73 64<br/>
                    info@under-style.ru</p>

                <h2>График работы:
                    <hr class="border-light">
                </h2>

                <p>Пн - Пт: с 10:00 до 20:00<br/>
                    Сб: с 11:00 до 18:00<br/>
                    Вс: выходной.</p>
                {*<h4>Присоединяйтесь:</h4>*}
                {*<a href="#"><i class="fa fa-facebook" style="font-size: 24px;"></i></a>*}
            </div>
        </div>
    </div>
</div>
<div id="copyright">
    <div class="container">
        <div class="row">
            <p>© 2010-2014 Андеграунд<br/>
                Все права защищены</p>
        </div>
    </div>
</div>

<!-- jQuery Version 1.11.1 -->
<script src="js/jquery.js"></script>
<script src="js/jQuery.scrollSpeed.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>
