
    {assign var="spath" value=$smarty.get.global.struct.1.spath}
    {assign var="path" value=$smarty.get.global.struct.1.path}
    {assign var="spath2" value=$spath|regex_replace:"/^\w\w\//":""}
    {assign var="spath3" value=$smarty.get.global.struct.2.spath|regex_replace:"/^\w\w\//":""}
    {assign var="path2" value=$path|regex_replace:"/^\w\w\//":""}
    {assign var="control" value=$smarty.get.global.struct.1.control}
    {assign var="filter" value=$smarty.get.path}

    {assign var="ch_control" value=$smarty.get.global.struct.1.ch_control}
    {assign var="lang" value=$marty.get.global.lan}

    {if $spath eq 'index' || $spath eq $smarty.get.global.lan || $control eq 'home'}
        {*{include_php file="content/imglister.inc.php" tpl_name="content/index.tpl" plevel=0 cond="as1.control IN ('new', 'company')" ord = "as1.created" sequence = "down"}*}
        {*{include_php file="content/imglister.inc.php" tpl_name="content/index.tpl" plevel=0 cond="as1.control = 'new'" ord = "as1.created" sequence = "down"}*}
        {include_php file="content/imglister.inc.php" tpl_name="content/index.tpl" plevel=0 cond="as1.control = 'home'" }

    {elseif $filter eq 'picking_filter'}
        {*{include_php file="content/picking_filter.inc.php" tpl_name="content/picking_filter.tpl" allow_paging=true page_size=6}*}


        {include_php file="content/picking_filter.inc.php" tpl_name="content/picking_filter.tpl" allow_paging=true page_size=50}

    {elseif $control eq 'article' || $control eq 'fldr' || $control eq 'new' || $control eq 'action'}
        {include_php file="content/lister.inc.php" tpl_name="content/article.tpl" plevel=0 cond="a1.path = '$path'"}
    {elseif $control eq 'company'}
        {include_php file="content/lister.inc.php" tpl_name="content/company.tpl" plevel=0 cond="a1.path='$path'"}
    {elseif $control eq 'news'}
        {include_php file="content/lister.inc.php" tpl_name="content/news.tpl" plevel=1 parent=$path cond="as1.control = 'new'" ord = "as1.created" sequence = "down" allow_paging=true page_size=6}
    {elseif $control eq 'actions'}
        {include_php file="content/lister.inc.php" tpl_name="content/news.tpl" plevel=1 parent=$path cond="as1.control = 'action'" allow_paging=true page_size=6}
    {elseif $control eq 'old_actions'}
        {include_php file="content/lister.inc.php" tpl_name="content/news.tpl" plevel=1 parent=$path cond="as1.control = 'old_action'" allow_paging=true page_size=6}
    {elseif $control eq 'old_action'}
        {include_php file="content/lister.inc.php" tpl_name="content/actions.tpl" plevel=0 cond="a1.path = '$path'" }
    {elseif $control eq 'contacts'}
        {include_php file="content/lister.inc.php" tpl_name="content/contacts.tpl" plevel=0 cond="a1.path = '$path'" disable_caching = true}
    {elseif $control eq 'completed_projects'}
        {*{if $smarty.get.sort}*}
        {*{include_php file="content/lister.inc.php" tpl_name="content/completed_projects.tpl" plevel=1 parent=$path cond="as1.control = 'designer'" ord=a1.name sequence="up"}*}
        {*{else}*}
        {include_php file="content/imglister.inc.php" tpl_name="content/completed_projects.tpl" plevel=1 parent=$path cond="as1.control = 'designer_project'"}
        {*{/if}*}
    {elseif $control eq 'sale'}
        {include_php file="content/lister.inc.php" tpl_name="content/sale_text.tpl" plevel=0 cond="a1.path='$path'"}
        {*{if $smarty.get.sort}*}
        {*{include_php file="content/lister_sale.inc.php" tpl_name="content/sale.tpl" plevel=1 parent=$path cond="as1.control = 'sale'" ord=a1.name sequence="up"}*}
        {*{else}*}
        {include_php file="content/imglister_sale.inc.php" tpl_name="content/sale.tpl" plevel=1 parent=$path cond="as1.control = 'sale_project'"}
        {*{/if}*}
    {elseif $control eq 'designer'}
        {include_php file="content/lister.inc.php" tpl_name="content/designer.tpl" plevel=0 cond="a1.path = '$path'"}
    {elseif $control eq 'prod'}
        {include_php file="content/imglister.inc.php" tpl_name="content/product.tpl" plevel=0 cond="a1.path = '$path'" disable_caching = true}
    {elseif $control eq 'designer_project'}
        {include_php file="content/imgdotslister.inc.php" tpl_name="content/showroom.tpl" plevel=0 cond="a1.path = '$path'"}
    {elseif $control eq 'company_list'}
        {if $smarty.get.furniture}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/company_list.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=53 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.till}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/company_list.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=56 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.sanitary}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/company_list.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=55 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.light}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/company_list.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=54 AND as1.is_deleted = '0'"}
        {else}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/company_list.tpl"
            letters = 1
            field = "name"
            table = "articles"
            cond = "as1.control = 'company' AND as1.is_deleted = '0'"}
        {/if}
    {elseif $control eq 'catalogs'}
        {if $smarty.get.furniture}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/catalogs.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=53 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.till}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/catalogs.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=56 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.sanitary}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/catalogs.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=55 AND as1.is_deleted = '0'"}
        {elseif $smarty.get.light}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/catalogs.tpl"
            letters = 1
            field = "sprop3"
            cond = "as1.control = 'collection' AND as1.parent_id=54 AND as1.is_deleted = '0'"}
        {else}
            {include_php
            file="content/property_selector.inc.php"
            tpl_name="content/catalogs.tpl"
            letters = 1
            field = "name"
            table = "articles"
            cond = "as1.control = 'company' AND as1.is_deleted = '0'"}
        {/if}
    {elseif $control eq 'manufacturer'}
        {include_php file="content/lister.inc.php" tpl_name="content/manufacturer_catalog.tpl" plevel=0 cond="a1.path='$path'" disable_caching = true}
    {elseif $control eq 'subdomen'AND $smarty.get.gate eq '1'}
        {include_php file="content/imglister.inc.php" tpl_name="content/subdomain_gate.tpl" plevel=0 cond="a1.path='$path'"}
    {elseif $control eq 'subdomen' || $control eq 'saved_items' || $control eq 'viewed' || $control eq 'collection' || $control eq 'category'}
        {include file="content/subdomen.tpl"}
        {*{include_php file="content/lister.inc.php" tpl_name="content/subdomen.tpl" plevel=1 parent=$path cond="as1.control = 'new'" ord = "as1.created" sequence = "down" allow_paging=true page_size=6}*}
        {*{include_php file="content/lister.inc.php" tpl_name="content/subdomen.tpl" plevel=1 parent=$path sequence = "down" allow_paging=true page_size=6}*}
    {elseif $smarty.get.path eq '404'}
        {include_php file="content/lister.inc.php" tpl_name="content/404.tpl" plevel=0 cond="as1.control = '404' AND as1.is_deleted=0"}
    {elseif $control eq 'search'}
        {include_php file="content/searchlister.inc.php" tpl_name="content/search.tpl" plevel=0 allow_paging=true page_size=$smarty.const.SEARCH_LIMIT }
    {elseif $control eq 'wholesale_buyers'}
        {include_php file="content/imglister.inc.php" tpl_name="content/salers.tpl" plevel=0 cond="a1.path = '$path'"}
    {elseif $control eq 'sitemap'}
        {include_php file="content/treelister.inc.php" tpl_name="content/sitemap.tpl" plevel=0 cond=" (as1.level IN (1, 2)) AND as1.control NOT IN ('prod', '404', 'new', 'collection', 'manufacturer', 'search', 'saved_items', 'reg', 'sitemap', 'viewed', 'company_list', 'catalogs') AND as1.is_deleted = '0' AND as1.is_hidden = '0'"}
    {elseif $control eq 'reg'}
        {include_php file="content/registration.php" }
    {elseif $control eq 'subscription'}
        {include_php file="content/subscription.inc.php"}
    {elseif $control eq 'call_manager'}
    {else}
        {php} header ('Location: /404/'); {/php}

        {if $smarty.get.s}
            {include_php file = "content/lister.inc.php" tpl_name = "content/article.tpl" plevel = 0 cond = "as1.control = 'call_manager'"}
        {else}
            {include_php file="content/call_manager.inc.php"}
        {/if}
    {/if}