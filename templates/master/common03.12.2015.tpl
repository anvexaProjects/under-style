<!DOCTYPE html>
<html lang=ru>
<head>
    <meta name='yandex-verification' content='7b32d384b8cfab20' />
    <link href="/templates/css/site/main.css" type="text/css" rel="stylesheet" />
    <script src='/templates/js/jquery/jquery.js' type='text/javascript'></script>
    {include_php file="master/files_aggregator.inc.php" type="css" rel_dir="/templates/css" output="/templates_c/css/site/monolit_[APP_VERSION].css" enable=false compress=$smarty.const.ENABLE_COMPRESSION files="/fancybox/jquery.fancybox.css, /fancybox/jquery.fancybox-thumbs.css, /site/jquery-ui.css, /jquery.prettyphoto.css, /lightbox.css, /jquery.thickbox.css, /jquery.ipicture.css"}
    {include_php file="master/meta.inc.php"}
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <!--[if lt IE 7.]> <script defer type=text/javascript async src={$smarty.get.global.root}/templates/js/site/pngfix.js></script> <![endif]--> <!--[if lte IE 8]> {literal}
    <script defer type=text/javascript>function resize(){if($(document).width()>1240){$('.minWidthWrapper').addClass("wide");
    }else{$('.minWidthWrapper').removeClass("wide");}}$(window).resize(function(){resize();}).resize();</script> {/literal} <![endif]-->
	<link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
</head>
<body {if $smarty.get.path eq '404'}class="er404"{/if}>
	{if $smarty.get.path neq '404'}


        <div class="vcard">
            <div>
                <span class="category">Салон-магазин</span>
                <span class="fn org">АНДЕГРАУНД</span>
            </div>
            <div class="adr">
                <span class="locality">г. Москва</span>,
                <span class="street-address">ул. 1905 года, 25</span>
            </div>
            <div>Телефон: <span class="tel">+7 (495) 984 32 60</span></div>
            <div>График работы: <span class="workhours">Понедельник - Пятница: с 10:00 до 20:00
Суббота: с 11:00 до 18:00
Воскресенье - выходной.</span>
                <a href="http://www.under-style.ru/" class="url">
                    http://www.under-style.ru/
                </a>
            </div>
        </div>

<script type="text/javascript">(function(w,doc) {

    w.__utlWdgt = true;
    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
})(window,document);
</script>
	<div data-share-size="30" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1311460" data-mode="follow" data-background-color="#ffffff" data-share-shape="round-rectangle" data-share-counter-size="12" data-icon-color="#ffffff" data-text-color="#000000" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="fixed-left" data-following-enable="false" data-sn-ids="fb.vk.tw.in." data-follow-in="Andegraund_1905" data-selection-enable="false" data-exclude-show-more="false" data-share-style="1" data-follow-vk="club86447244" data-follow-tw="Andegraund_1905" data-counter-background-alpha="1.0" data-top-button="false" data-follow-fb="Anderground2014" class="uptolike-buttons" ></div>

<!--
<script type="text/javascript">(function(w,doc) {
    w.__utlWdgt = true;
    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
})(window,document);
</script>
<div data-share-size="40" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1294055" data-mode="share" data-background-color="#ededed" data-share-shape="round-rectangle" data-share-counter-size="8" data-icon-color="#ffffff" data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="fixed-right" data-following-enable="true" data-follow-in="Andegraund_1905" data-selection-enable="false" data-exclude-show-more="false" data-share-style="4" data-follow-tw="Andegraund_1905" data-counter-background-alpha="1.0" data-top-button="false" data-follow-fb="Anderground2014" class="uptolike-buttons" ></div>
-->

		{literal}
			<div id=fb-root></div>
			<script>(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/ru_RU/all.js#xfbml=1";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));</script>
			<script type='text/javascript'>$(function(){function d(a){return a.split(/,\s*/);}function c(a){return d(a).pop();}$("#searchtext").focus(function(){$(this).attr("value","");});$("#searchtext").autocomplete({source:function(a,b){$.getJSON("{/literal}{$smarty.get.global.root}{literal}/pub/ajax_autocomplete.php",{term:c(a.term)},b);},minLength:2,select:function(b,a){}});$("#subscribe_button").click(function(){error=false;if($("#subscribe_name").val()==""){$("#subscribe_name").css("border","1px solid #FF0000");error=true;}else{$("#subscribe_name").css("border","1px solid #939393");}if($("#subscribe_email").val()==""){$("#subscribe_email").css("border","1px solid #FF0000");error=true;}else{$("#subscribe_email").css("border","1px solid #939393");}if(error==false){$.post("/ajax.php",{action:"subscribe",name:$("#subscribe_name").val(),email:$("#subscribe_email").val()},function(a){if(a.status=="fail"){$.fancybox(a.error);}else{$("#subscribe_name").val("");$("#subscribe_email").val("");$.fancybox.close();}},"json");}});$("#callme_button").click(function(){error=false;if($("#callme_name").val()==""){$("#callme_name").css("border","1px solid #FF0000");error=true;}else{$("#callme_name").css("border","1px solid #939393");}if($("#callme_phone").val()==""){$("#callme_phone").css("border","1px solid #FF0000");error=true;}else{$("#callme_phone").css("border","1px solid #939393");}if($("#callme_text").val()==""){$("#callme_text").css("border","1px solid #FF0000");error=true;}else{$("#callme_text").css("border","1px solid #939393");}if(error==false){$.post("/ajax.php",{action:"callme",name:$("#callme_name").val(),phone:$("#callme_phone").val(),time:$("#callme_time").val(),text:$("#callme_text").val()},function(a){if(a.status=="fail"){$.fancybox(a.error);}else{$("#callme_name").val("");$("#callme_phone").val("");$("#callme_time").val("08:00-09:00");$("#callme_text").val("");$.fancybox.close();alert("Сообщение успешно отправлено");yaCounter23717377.reachGoal('callme');return true;}},"json");}});$(".fancy").fancybox({afterClose:function(){$("#callme_name").val("");$("#callme_phone").val("");$("#callme_time").val("08:00-09:00");$("#callme_text").val("");}});});</script><script type="text/javascript">(function(w,doc){if(!w.__utlWdgt ){w.__utlWdgt = true; var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';s.type='text/javascript';s.charset='UTF-8';s.async=true;s.src=('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';var h=d[g]('body')[0];h.appendChild(s);}})(window,document);</script>{/literal} {assign var=is_home value=0}
		{if $smarty.get.global.struct.1.path eq 'index' || $smarty.get.global.struct.1.path eq $smarty.get.global.lan}{assign var=is_home value=1}{/if}
	<div class='minWidthWrapper'>
		<div class='bannerRight'>
			{include_php file="content/imglister.inc.php" tpl_name = "master/banners.tpl" plevel = 0 cond = "as1.control='home'" page_size=99999999 disable_caching = true }
			<p><br /></p>
			{include_php file="content/imglister.inc.php" tpl_name = "master/banners.tpl" plevel = 0 cond = "as1.struct_id=11885" page_size=99999999 disable_caching = true }
		</div>
	<div class='minWidth'>
		<div class=bannerTop> {include_php file="content/lister.inc.php" tpl_name = "block/check_banners.tpl" plevel = 0 cond = "as1.control='header_banners'" fields = "as1.is_hidden" }</div>
	<div id=header>
    
	<div class="topHeader clearfix">
		{if !$is_home} <a href={$smarty.get.global.root}/ class=logo><img src="{$smarty.get.global.root}/images/site/logo.gif?v=2" width=280 height=63 alt /></a> {else} <span class=logo><img src="{$smarty.get.global.root}/images/site/logo.gif?v=2" width=280 height=63 alt /></span>{/if}
        
		<div class='header_center'>
            <div class='navIcons'>
                <a href='{$smarty.get.global.root}/' class="home"{if $is_home} onClick="return false"{/if}></a><a href='{$smarty.get.global.root}/sitemap/' class="sitemap"{if $smarty.get.path eq 'sitemap'} style=cursor:default onClick="return false"{/if}></a><a href='mailto:{$smarty.const.INFO_MAIL}' class='mail'></a>
            </div>
            <div class='ph'>
                <p><nobr><a class="changelink"><font class="ya-phone">{$smarty.const.S_PHONE_CODE_1} {$smarty.const.S_PHONE_2}</font></a>&nbsp;<span>{$smarty.const.S_PHONE_ADD_2}</span></nobr></p>
             </div>
            <div class='clearfix'></div>
            <div class='topPhones'>
                <p><a href=#callme class=fancy onClick="yaCounter23717377.reachGoal('callme');_gaq.push(['_trackEvent','Обратный звонок','callme'])">{$smarty.const.S_CALL_MANAGER}</a></p>
            </div>
        </div>
        
    	<div class="time_job">Понедельник - Пятница: с 10:00 до 20:00<br />Суббота: с 11:00 до 18:00</div>
    </div>
    
    <div id='mainMenu'> {include_php file = "content/treelister.inc.php" tpl_name = "master/menu_main.tpl" plevel=0 cond = "(as1.level IN (1, 2)) AND (as1.control IN ('fldr', 'actions', 'article', 'contacts', 'completed_projects', 'sale', 'designer_project', 'wholesale_buyers', 'subscription', 'company_list', 'catalogs') AND as1.is_hidden = 0)" }</div><div data-share-size="20" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1294055" data-mode="share" data-background-color="ededed" data-share-shape="round-rectangle" data-icon-color="ffffff" data-share-counter-size="8" data-text-color="000000" data-buttons-color="ff9300" data-counter-background-color="ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.tw.vk.gp.ln." data-selection-enable="true" data-share-style="4" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons"></div>
    </div>
    <div class="sub_header clearfix"> {include_php file="content/lister.inc.php" tpl_name="block/social_nets.tpl" plevel=0 cond="as1.control='home'"}<div class="search floatr"><form method=get action={$smarty.get.global.root}/search/>

	<div class='ui-widget'>
		<input type='text' name='query' id='searchtext' value='{$smarty.get.query|default:$smarty.const.S_SEARCH}' title='{$smarty.const.S_SEARCH}' class="inp labler" />
	</div>
	<input type=submit value class=btn />
	</form></div><div class="btnLinksRight clearfix"><a href='{$smarty.get.global.root}/viewed/'>{$smarty.const.S_VIEWED_PRODUCTS}</a>{include file = "content/new_items.tpl"}<a href={$smarty.get.global.root}/saved_items/>{$smarty.const.S_FAVORITES}</a></div></div> {/if} {include file="master/content.tpl"} {if $smarty.get.path neq '404'}
	<div id='footer' class='clearfix'>
	{include_php file="content/lister.inc.php" tpl_name="master/footer_catalog.tpl" plevel=0 cond="as1.control='subdomen'" fields="a1.path, a1.name" }
	<div class=f_rSide><span class="f_rSide_s_contacts">{$smarty.const.S_CONTACTS}</span><div class="f_contacts floatl"> {include_php file="content/lister.inc.php" tpl_name="master/footer_contact.tpl" plevel=0 cond="as1.control='contacts'" fields="a1.content2" }</div></div><div class=clear></div><div class="copy clearfix"> {include file = "content/yandex.tpl"}<p class=floatl>{$smarty.const.S_COPYWRITING}<br />Все&nbsp;права&nbsp;защищены&nbsp;&nbsp;&nbsp;&nbsp;</p> {include_php file="content/lister.inc.php" tpl_name="block/social_nets.tpl" plevel=0 cond="as1.control='home'"}</div></div> {include file="master/counter.tpl"}</div> {/if}</div><div id=callme style=display:none><table border=0 cellpadding=3 cellspacing=3><tr><td>Имя:</td><td width=10>&nbsp;</td><td><input type=text id=callme_name style=width:300px /></td></tr><tr><td>Телефон:</td><td width=10>&nbsp;</td><td><input type=text id=callme_phone style=width:300px /></td></tr><tr><td>Время звонка:</td><td width=10>&nbsp;</td><td> <select id=callme_time style=width:300px><option value=10:00-11:00>10:00-11:00</option><option value=11:00-12:00>11:00-12:00</option><option value=12:00-13:00>12:00-13:00</option><option value=13:00-14:00>13:00-14:00</option><option value=14:00-15:00>14:00-15:00</option><option value=15:00-16:00>15:00-16:00</option><option value=16:00-17:00>16:00-17:00</option><option value=17:00-18:00>17:00-18:00</option><option value=18:00-19:00>18:00-19:00</option><option value=19:00-20:00>19:00-20:00</option></select></td></tr><tr><td>Сообщение:</td><td width=10>&nbsp;</td><td><textarea id=callme_text style=width:300px;height:200px></textarea></td></tr><tr><td colspan=3 align=right><input type=button value=Отправить onClick="yaCounter23717377.reachGoal('callme');_gaq.push(['_trackEvent','Обратный звонок','callme'])" id="callme_button"/></td></tr></table></div>

	{if $smarty.get.global.struct.1.control eq 'prod'}
		{include_php file="master/files_aggregator.inc.php" type="js" rel_dir="/templates/js" output="/templates_c/js/site/monolit_[APP_VERSION].js" enable=false compress=$smarty.const.ENABLE_COMPRESSION files="/jquery/jquery-ui.js, /site/jquery.mCustomScrollbar.concat.min.js,/site/jquery.carouFredSel-6.2.0-packed.js,/fancybox/jquery.mousewheel-3.0.6.pack.js,/fancybox/jquery.fancybox.pack.js,/fancybox/helpers/jquery.fancybox-thumbs.js,/site/jquery.jscrollpane.min.js,/site/main.js, /site/favorite.js, /jquery/jquery.prettyphoto.js"}
	{else}
		{include_php file="master/files_aggregator.inc.php" type="js" rel_dir="/templates/js" output="/templates_c/js/site/monolit_[APP_VERSION].js" enable=false compress=$smarty.const.ENABLE_COMPRESSION files="/jquery/jquery-ui.js, /site/jquery.mCustomScrollbar.concat.min.js,/site/jquery.carouFredSel-6.2.0-packed.js,/fancybox/jquery.mousewheel-3.0.6.pack.js,/fancybox/jquery.fancybox.pack.js,/fancybox/helpers/jquery.fancybox-thumbs.js,/site/jquery.jscrollpane.min.js,/site/main.js, /site/favorite.js, /jquery/jquery.prettyphoto.js,/jquery/jquery.ipicture.min.js"}
	{/if}
	<script async type=text/javascript>
		root = '{$smarty.get.global.root}';
		var msg_sent = '{$smarty.const.LBL_SENT_SUCCESS}';
		{if $smarty.get.global.struct.1.control eq 'saved_items'}
			var no_prods = '{$smarty.const.S_NO_BOOKMARKS}';
		{elseif $smarty.get.global.struct.1.control eq 'viewed'}
			var no_prods = '{$smarty.const.S_NO_VIEWED}';
		{elseif $smarty.get.global.struct.1.control eq 'subdomen'}
			var no_prods = '{$smarty.const.S_NO_PRODS}';
		{else}
			var no_prods = '';
		{/if}
	</script>
	<script type=text/javascript src=//vk.com/js/api/openapi.js?95></script>


	{literal}
	<!--Openstat-->
	<span id="openstat2365193"></span>
	<script type="text/javascript">
	var openstat = { counter: 2365193, next: openstat, track_links: "all" };
	(function(d, t, p) {
	var j = d.createElement(t); j.async = true; j.type = "text/javascript";
	j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";
	var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
	})(document, "script", document.location.protocol);
	</script>
	<!--/Openstat-->
	{/literal}

	{literal}
	<script type="text/javascript">(function(w,doc) {
	if (!w.__utlWdgt ) {
	    w.__utlWdgt = true;
	    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
	    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
	    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
	    var h=d[g]('body')[0];
	    h.appendChild(s);
	}})(window,document);
	</script>
	{/literal}
	
	<script type="text/javascript" src="//sprosiih.ru/form-js.php?id=17"></script>

</body></html>