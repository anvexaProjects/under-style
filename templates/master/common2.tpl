{include_php file="master/check_access.inc.php"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
{include_php file="master/meta.inc.php"}
{include_php file="master/files_aggregator.inc.php" type="css" rel_dir="/templates/css"
	output="/templates_c/css/site/monolit_[APP_VERSION].css"
	enable=false
	compress=$smarty.const.ENABLE_COMPRESSION
	files="/site/main.css,/jquery.prettyphoto.css"}
<script type="text/javascript">/*<![CDATA[*/root="{$smarty.get.global.root}";var msg_sent="{$smarty.const.LBL_SENT_SUCCESS}";/*]]>*/</script>
{include_php file="master/files_aggregator.inc.php" type="js" rel_dir="/templates/js"
	output="/templates_c/js/site/monolit_[APP_VERSION].js"
	enable=false
	compress=$smarty.const.ENABLE_COMPRESSION
	files="/jquery/jquery-1.5.min.js,/jquery/jquery.prettyphoto.js,/jquery/jquery.supersized.3.1.1.min.js,/site/main.js"}
</head>
{assign var=is_home value=0}{if $smarty.get.global.struct.1.path eq 'index' || $smarty.get.global.struct.1.path eq $smarty.get.global.lan}{assign var=is_home value=1}{/if}
<body>
<div id="minWidth">
<div class="logo{if !$is_home}_sec{/if}">
{if !$is_home}
<a href="{$smarty.get.global.root}/"><img src="{$smarty.get.global.root}/images/site/logo_sec.png" /></a>
{else}
<img src="{$smarty.get.global.root}/images/site/logo_big.png" />
{/if}
</div>
<div id="header">
{include_php file="content/lister.inc.php" tpl_name="master/menu_main.tpl" plevel=0 cond="as1.level = 1 AND as1.is_hidden = 0"}
{include_php file="content/imglister.inc.php" tpl_name="master/block_icons.tpl" plevel=0 cond="as1.control = 'home'"}
<div class="clear"></div>
</div>
{include file="master/content.tpl"}
</div>
<div id="footer">
<div class="tabs">
<div class="tabEl">
<h2>{$smarty.const.S_VRAGEN}</h2>
<form action="" method="post" id="feedback_form">
<div class="l">
<p><span>{$smarty.const.S_NAME}</span><input type="text" name="name" value="" /></p>
<p><span>{$smarty.const.S_EMAIL}</span><input type="text" name="email" value="" /></p>
</div>
<div class="r">
{$smarty.const.S_MESSAGE}<br/>
<textarea name="message"></textarea>
<input type="reset" value="{$smarty.const.S_RESET} &gt;" class="btn" id="btn_reset" /><input type="submit" value="{$smarty.const.S_SEND} &gt;" class="btn2" id="btn_submit" />
</div>
<div style="display:none">
<input type="text" name="f1" value="{$smarty.const.CHECK_SPAM2}" />
<input type="text" name="f2" value="" />
</div>
</form>
</div>
<div class="tabEl scnd">
{include file="master/block_twitter.tpl"}
</div>
<div class="tabEl thd">
{include_php file="content/lister.inc.php" tpl_name="master/block_content2.tpl" plevel=0 cond="as1.control = 'contact'"}
</div>
<div class="clear"></div>
</div>
</div>
</body>
</html>