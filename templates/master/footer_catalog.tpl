<header>
    <h2>Разное</h2>
    <hr class="border-light">
</header>
<div class="row">

    <div class="col-xs-6 col-sm-3">

        <ul>
            <li><a href="/about/contacts/">Контакты</a></li>
            <li><a href="/about/actions/">Акции</a></li>
            <li><a href="/partnership/company_list/">Производители</a></li>
        </ul>

    </div>
    <div class="col-xs-6 col-sm-3">

        <ul>
            <li><a href="/about/contacts/vacancy/">Вакансии</a></li>
            <li><a href="/about/sale/">Распродажа</a></li>
            <li><a href="#callme" class="fancy">Заказать звонок</a></li>
        </ul>

    </div>
    <div class="col-xs-6 col-sm-3">

        <ul>
            <li><a href="/sitemap/">Карта сайта</a></li>
            <li><a href="/about/contacts/feedback/">Письмо директору</a></li>
            <li><a href="/news/?gate=1">Новости</a></li>
        </ul>

    </div>
</div>

<header>
    <h2>{$smarty.const.S_CATALOG_PRODUCTS}</h2>
    <hr class="border-light">
</header>
<div class="row">
    <div class="col-xs-6 col-sm-3">
        <ul>
            <li><a href="http://www.under-style.ru/sanitary/">Каталог сантехники</a>
                {include_php
                file="content/footer_catalog.inc.php"
                tpl_name = "block/footer_catalog.tpl"
                cond="as1.is_footer=1 and as1.parent_id=55"
                plevel = 0
                }
            </li>
        </ul>
    </div>
    <div class="col-xs-6 col-sm-3">

        <ul>
            <li><a href="http://www.under-style.ru/plitka/">Каталог плитки</a>
                {include_php
                file="content/footer_catalog.inc.php"
                tpl_name = "block/footer_catalog.tpl"
                cond="as1.is_footer=1 and as1.parent_id=56"
                plevel = 0
                }
            </li>
        </ul>

    </div>
    <div class="col-xs-6 col-sm-3">
        <ul>
            <li><a href="http://www.under-style.ru/mebel/">Каталог мебели</a>
                {include_php
                file="content/footer_catalog.inc.php"
                tpl_name = "block/footer_catalog.tpl"
                cond="as1.is_footer=1 and as1.parent_id=53"
                plevel = 0
                }
            </li>
        </ul>
    </div>
    <div class="col-xs-6 col-sm-3">
        <ul>
            <li><a href="http://www.under-style.ru/svet/">Каталог света</a>
                {include_php
                file="content/footer_catalog.inc.php"
                tpl_name = "block/footer_catalog.tpl"
                cond="as1.is_footer=1 and as1.parent_id=54"
                plevel = 0
                }
            </li>
        </ul>
    </div>
</div>
