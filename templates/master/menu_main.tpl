<nav id="menu-top" class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" style="padding-left: 35px;">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#main-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img width="132" height="76" src="/img/diamond.png" alt="Андерграунд" title="Андерграунд">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="main-menu" class="collapse navbar-collapse ">

            <ul class="nav navbar-nav">
                {foreach from = $items item=i name=f}
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">{$i.name}</a>
                        <ul class="dropdown-menu">
                            {if $i.spath eq 'actueel'}
                                {include_php
                                file="content/lister.inc.php"
                                tpl_name = "master/menu_main_item.tpl"
                                plevel = 0
                                cond = "as1.control='subdomen'"
                                }
                            {else}
                                {if $i.control eq 'fldr' && $smarty.foreach.f.iteration eq 1}
                                    {include_php file="content/lister.inc.php"
                                    tpl_name = "master/menu_main_item.tpl"
                                    plevel=0
                                    cond="as1.control='news'"}
                                {/if}

                                {foreach from = $i.items item=i2 name=n}
                                    {if $i2.path neq 'about/sale' && $i2.path neq 'about/actions'}

                                        <li{if $i2.path eq $smarty.get.global.struct.1.path} class="current"{/if}>
                                            {if $i2.path neq $smarty.get.global.struct.1.path}
                                            <a href="{$smarty.get.global.root|subdomain:$i2.path}/{$i2.path}/">
                                                {else}
                                                <span class="current">
                                                    {/if}{$i2.name}
                                                    {if $i2.path neq $smarty.get.global.struct.1.path}
                                            </a>
                                            {else}
                                            </span>
                                            {/if}
                                        </li>

                                    {/if}
                                {/foreach}
                            {/if}
                        </ul>
                    </li>
                {/foreach}
                <li class="dropdown">
                    <a href="/about/actions/" class="dropdown-toggle">Акции</a>
                </li>
                <li class="dropdown">
                    <a href="/about/sale/" class="dropdown-toggle">Распродажа</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>