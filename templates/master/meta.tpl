{*{$smarty.get|@print_r}*}
{*{$item|@print_r}*}

{php}

    function extractFromCol($data, $compare) {
        $result = array_filter($data, function($item) use($compare) {
            return ($item['id'] == $compare);
        });

        return current($result);
    }

    function buildQuery($where) {
        return "
            SELECT
            a1.parent_id AS subdomen,
            cp.psevdonim AS group_name, a2.sprop4 AS group_title,
            a1.psevdonim AS vendor_name, a1.sprop3 AS vendor_title,
            a2.sprop13 AS col_name
            FROM art_struct AS a1
            JOIN art_struct AS a2
            ON a2.parent_id = a1.struct_id
            LEFT JOIN category_psevdonims AS cp
            ON cp.name = a2.sprop4
            LEFT JOIN collection_psevdonims AS colp
            ON colp.name = a2.sprop13
            WHERE a1.control = 'collection'
            AND a1.is_deleted = '0'
            AND a1.is_hidden = '0'
            AND a2.sprop1 != '1'
            AND a2.sprop4 != ''
            $where
            GROUP BY vendor_name, group_name
            ORDER BY group_name ASC

        ";
    }

    $oDb = &Database::get();

    $sql = "
        SELECT struct_id as id, psevdonim as title
        FROM art_struct
        WHERE control = 'subdomen'
    ";
    $subdomens = $oDb->getRows($sql);

    $sql = "
        SELECT t1.sprop4 as title, t2.psevdonim as name, t3.parent_id
        FROM art_struct AS t1
        JOIN category_psevdonims AS t2 ON t1.sprop4 = t2.name
        JOIN art_struct AS t3 ON t1.parent_id = t3.struct_id
        WHERE t1.sprop4 <> ''
        AND t3.parent_id > 0
        AND t1.is_deleted = '0'
        AND t1.is_hidden = '0'
        GROUP BY t1.sprop4, t3.parent_id
        ORDER BY t2.psevdonim
    ";
    $productGroups = $oDb->getRows($sql);

    $data = array();
    foreach($productGroups as $group) {
        $data[$group['name']] = $group['title'];
    }


    $where = '';
    $canonical_url = '';


    if(@$_GET['category_filter']) {
        if($subdomen = extractFromCol($subdomens, $_GET['category_filter'])) {
            $canonical_url .= $subdomen['title'].'/';
        }
    }

    if(@$_GET['vendor_filter']) {
        $vendorName = $_GET['vendor_filter'];
        $where = " AND a1.psevdonim = '$vendorName' ";
    }

    if(@$_GET['group_filter']) {
        $group = $_GET['group_filter'];
        $groupName = ucfirst($data[$group]);

        $sql = "
            SELECT COUNT(DISTINCT t2.sprop13) AS count
            FROM art_struct t2
            WHERE t2.sprop4 = '$groupName'
            AND t2.is_deleted = 0
        ";
        $collections = $oDb->getRow($sql);

        $where .= " AND cp.psevdonim = '$group' ";
    }

    if(@$_GET['col_filter']) {
        $col_filter = $_GET['col_filter'];

        $sql = "
            SELECT DISTINCT ast.sprop3 as brand FROM collection_psevdonims cp
            JOIN art_struct ast ON  ast.sprop13 = cp.name
            WHERE cp.psevdonim = '$col_filter';
        ";
        $product = $oDb->getRow($sql);

        $where = " AND colp.psevdonim = '$col_filter' ";
    }

    if($where) {
        $sql = buildQuery($where);
        $data = $oDb->getRow($sql);
        //$subdomen = extractFromCol($subdomens, $data['subdomen']);

        //$canonical_url = $subdomen['title'].'/'.$data['vendor_name'].'/'.$col_filter.'/';

        {*$result = print_r($data, true);*}
        {*echo "<pre>$result</pre>";*}
    }



    if(substr_count($_SERVER['REQUEST_URI'], '/') == 1) {
        $days = 5;
    } elseif(count($_GET['global']['rstruct']) < 4) {
        $days = 14;
    } else {
        $days = 30;
    }

    $from = 1461333202;
    $current = time();
    $interval = $current - $from;
    $everySomedays = 60 * 60 * 24 * $days;

    $lastModified = $current - ($interval % $everySomedays);
    $expires = $lastModified + $everySomedays;

    $lastModifiedGmd = gmdate('D, d M Y H:i:s', $lastModified).' GMT';
    $expiredGmd = gmdate('D, d M Y H:i:s', $expires).' GMT';

    header("Expires: $expiredGmd");
    header("Last-Modified: $lastModifiedGmd");
    header_remove("X-Powered-By");

{/php}

{*<pre>*}
{*{$smarty.get.global.rstruct|@print_r}*}
{*</pre>*}

{*<title>{$items.0.title|default:$title_seo|default:$items.0.name|default:$smarty.const.TITLE}</title>*}
{if $smarty.get.global.rstruct.2.group }

    {*{$smarty.get.global.rstruct.2.group.psevdonim}*}
    <title>{$items.0.title|default:$title_seo|default:$items.0.name|default:$smarty.const.TITLE}</title>
    {*<title>Страница группы</title>*}
    <meta name="Description" content="{$items.0.meta_text|default:$description_seo|default:''}"/>

{elseif $items.0.control eq 'prod'}
    <title>{$items.0.title} купить в Москве. {$items.0.brand}, продажа, получить лучшую цену.</title>
    <meta name="Description" content="{$items.0.title}. Получить лучшую цену. Производство: {$items.0.country}. Официальная гарантия от производителя, официальный дилер."/>

    {*<meta http-equiv="Expires" content="{php} echo gmdate('D, d M Y H:i:s T', strtotime('+30 days')); {/php}"/>*}

{elseif $smarty.get.path eq 'picking_filter'}

    {if $smarty.get.col_filter && $smarty.get.group_filter}
        <title>{$smarty.get.col_filter|capitalize} {php} echo $groupName; {/php} купить в Москве. {$smarty.get.col_filter|capitalize}, продажа, получить лучшую цену.</title>
        <meta name="Description" content="{$smarty.get.col_filter|capitalize} {php} echo $groupName; {/php}, более {php} echo $collections['count']; {/php} коллекций. Получить лучшую цену. Официальный дилер."/>
    {elseif $smarty.get.col_filter}
        <title>{$smarty.get.col_filter|capitalize} купить в Москве. {php} echo $product['brand']; {/php} ,продажа, цены.</title>
        <meta name="Description" content="{$smarty.get.col_filter|capitalize}. Широкий выбор, более {php} echo $collections['count']; {/php} моделей. Широкий ассортимент. Привлекательные цены."/>
    {else}
        <title>{$items.0.title|default:$title_seo|default:$items.0.name|default:$smarty.const.TITLE}</title>
        <meta name="Description" content="{$items.0.meta_text|default:$description_seo|default:''}"/>
    {/if}


    <link rel="canonical" href="http://{$smarty.get.global.struct.2.name}/{php} echo $canonical_url; {/php}" />

{elseif $smarty.get.global.rstruct.1.control eq 'subdomen'}

    {php}

        // print_r($_GET['global']['struct'][1]['struct_id']);

        $sql = "
            SELECT COUNT(DISTINCT t1.struct_id)
                FROM art_struct AS t1
                LEFT JOIN art_content AS ac1 ON ac1.struct_id = t1.struct_id
                WHERE t1.parent_id IN (
                SELECT t2.struct_id
                FROM art_struct AS t1
                LEFT JOIN art_struct AS t2 ON t2.parent_id = t1.struct_id AND t2.is_deleted = 0
                WHERE t1.struct_id = {$_GET['global']['struct'][1]['struct_id']}
            )

            AND t1.sprop13 != ''
            AND t1.is_deleted = '0'
            AND t1.is_hidden = '0'
            AND t1.sprop1 != '1'
        ";

        $tovar_count = $oDb->getField($sql);

    {/php}


    <title>{$smarty.get.global.rstruct.1.name} купить в Москве,  продажа, цены.</title>
    <meta name="Description" content="{$smarty.get.global.rstruct.1.name}, огромный выбор, {php} echo $tovar_count; {/php} вариантов. Бесплатная доставка. Привлекательные цены."/>

{else}
    <title>{$items.0.title|default:$title_seo|default:$items.0.name|default:$smarty.const.TITLE}</title>
    <meta name="Description" content="{$items.0.meta_text|default:$description_seo|default:''}"/>
{/if}




<meta name="Keywords" content="{$items.0.meta|default:$keywords_seo|default:''}"/>

<meta name="Robots" content="All"/>
<meta name="Document-state" content="Dynamic"/>
<meta name="Resource-type" content="document"/>

<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="max-age=31536000"/>
<meta name="Revisit-after" content="21 days"/>

{*<meta http-equiv="Last-Modified" content="{$items.0.modify|default:''}">*}

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta property="og:title" content="{$items.0.title|default:$title_seo|default:$items.0.name|default:$smarty.const.TITLE}">
<meta property="og:url" content="http://www.under-style.ru">
<meta property="og:description" content="{$items.0.meta_text|default:$description_seo|default:''}">
<meta property="og:image" content="http://www.under-style.ru/upload/cms_images/General%20page/Villeroy_Boch%21.jpg">
<meta property="og:type" content="website">

<link rel="shortcut icon" href="{$smarty.get.global.root}/favicon.ico" type="image/x-icon"/>