<?php

/**
 *
 * AUTOMATICALLY GENERATED CODE - 
                DO NOT EDIT BY HAND
 *
**/
define('ADMIN_MAIL', 'petkevich@under-style.ru'); // for notificatins
define('ZAPROS_MAIL', 'zapros@under-style.ru'); // for notificatins
define('INFO_MAIL', 'info@under-style.ru'); // for notificatins
define('MANAGER_MAIL', 'info@under-style.ru'); // for notificatins
define('CONTACTS_MAIL', 'director@under-style.ru'); // cobntact page
define('S_MAIN', 'Главная'); // /
define('S_PHONE_CODE_1', '+7 (495)'); // 
define('S_PHONE_CODE_2', '+7 (499)'); // 
define('S_PHONE_1', '796 73 64'); // 
define('S_PHONE_2', '984 32 60'); // 
define('S_PHONE_3', '253 69 32'); // 
define('S_PHONE_4', '253 53 30'); // 
define('S_PHONE_ADD_2', ''); // 
define('S_PHONE_ADD_3', ''); // 
define('S_PHONE_ADD_4', ''); // 
define('S_FOLLOW_US', 'Следите за нами в социальных сетях:'); // 
define('S_SEARCH', 'Поиск по сайту'); // 
define('S_COPYWRITING', '© 2010-2014 Андеграунд'); // 
define('S_NO_SEARCH_RESULT', 'ничего не найдено'); // 
define('S_SEARCH_RESULT', 'Результаты поиска'); // 
define('S_SEARCH_NEXT', 'Следующая страница'); // 
define('S_SEARCH_PREV', 'Предыдущая страница'); // 
define('S_SEARCH_TEXT', 'Вы смотрите страницу'); // 
define('S_YOU_SEEN_AT_PAGE', 'Вы смотрите страницу:'); // 
define('S_PREVIOUS_PAGE', 'Предыдущая страница'); // 
define('S_NEXT_PAGE', 'Следующая страница'); // 
define('S_SORT', 'Сортировка:'); // 
// define('S_ALL', 'Все'); //
define('S_BY_ALPHABET', 'по алфавиту'); // 
define('S_PORTFOLIO', 'Портфолио'); // 
define('S_PROJECTS_ON_SITE', 'проектов на сайте:'); // 
define('S_DESTINATIONS', 'Направления'); // 
define('S_OTHER_DESTINATIONS', 'Другие направления нашей компании'); // 
define('S_RESULT', 'Товары, соответствующие выбранным критериям:'); // 
define('S_MANUFACTURER', 'Производитель:'); // 
define('S_COUNTRY', 'Страна:'); // 
define('S_CAT', 'Категория товара:'); // 
define('S_GROUP', 'Товарная группа:'); // 
define('S_PLACE', 'Расположение:'); // /
define('S_COLLECTION', 'Коллекция:'); // 
define('S_COLLECTIONS', 'Коллекции'); // 
define('S_SIMILAR_PRODUCTS', 'Товары этого производителя'); // 
define('S_PRODUCRS_CATALOG', 'Каталог товаров'); // 
define('S_MANUFACTURERS', 'Производители'); // 
define('S_TO_LOCATIONS', 'По расположению'); // 
define('S_PROJECTS', 'Проекты'); // 
define('S_IS_AVAILABLE', 'Товары в наличии'); // 
define('S_AVAILABLE', 'В наличии'); // 
define('S_NOT_AVAILABLE', 'Скоро'); // 
define('S_SPEC_OFFER', 'Спеццена!'); // 
define('S_ON_HOLD', 'Товары в ожидании'); // 
define('S_SPECIAL_OFERT', 'Спецпредложения'); // 
define('S_SALE', 'Распродажа'); // 
define('S_TILE', 'Плитка'); // 
define('S_SANITARY_ENGINEERING', 'Сантехника'); // 
define('S_FURNITURE', 'Мебель'); // 
define('S_KITTCHENS', 'Кухни'); // 
define('S_LIGHT', 'Свет'); // 
define('S_PRICE', 'Прайс-листы товаров'); // 
define('S_NO_RESULT', 'Товары с выбранными критериями отсутствуют.'); // 
// define('ERR_ADD_LOGIN_PASS', 'Введите  логин и пароль'); //
// define('ERR_LOGIN', 'Неверный логин или пароль'); //
define('S_KEYWORDS', 'Ключевые слова'); // 
define('S_LOGIN', 'Логин'); // 
define('S_PASSWORD', 'Пароль'); // 
define('S_ENTER', 'Войти'); // 
define('S_LAST_NEWS', 'Новости'); // 
define('S_READ_ALL_NEWS', 'Читать все новости'); // 
define('S_ACTUEL_ACTIONS', 'Актуальные акции'); // 
define('S_PROD_NAME', 'Наименование товара'); // 
define('S_INTODUCTION', 'Представьтесь, пожалуйста'); // 
define('S_PHONE', 'Контактный телефон'); // 
define('S_U_EMAIL', 'Ваш Email'); // 
define('S_MESSAGE', 'Сообщение'); // 
define('S_VIEWED_PRODUCTS', 'Просмотренные товары каталога'); // 
define('S_FAVORITES', 'Закладки в каталоге'); // 
define('S_ABOUT_COMPANY', 'О КОМПАНИИ'); // 
define('S_INTERIOR_SOLUTIONS', 'ИНТЕРЬЕРНЫЕ РЕШЕНИЯ'); // 
define('S_FOR_PARTNERS', 'ПАРТНЕРАМ'); // 
define('S_AUTH', 'Авторизация'); // 
define('S_AUTH_L', 'АВТОРИЗАЦИЯ'); // 
define('S_FORGOT', 'Забыли пароль?'); // 
define('S_ENTER_COND', 'Чтобы войти на сайт, Вы должны зарегистрироваться.'); // 
define('S_REG', 'Зарегистрироваться'); // 
define('S_CATALOG_PRODUCTS', 'Каталог товаров'); // 
define('S_CONTACTS', 'Контакты'); // 
define('S_DESCRIPTION', 'Описание'); // 
define('S_FEATURES', 'Характеристики'); // 
define('S_DETAILS', 'Уточнить цену, условия поставки'); // 
define('FLB_REQUIRED', 'Заполните поле %s'); // 
define('S_ORDER_CALL', 'Заказать звонок специалиста'); // 
define('S_NAME', 'Имя:'); // 
define('S_CONF_PASSWORD', 'Подтверждение пароля'); // 
define('ERR_USER_ALREADY_REGISTERED', 'Пользователь с таким e-mail уже существует'); // 
define('S_MIN_PASS_LENGTH', 'Минимальный пароль - 5 символов'); // 
define('FLB_EMAIL_FORMAT', 'Неверный формат e-mail'); // 
define('S_REG_SUCCESS', 'Регистрация прошла успешно'); // 
define('S_REGISTRATION', 'Регистрация'); // 
define('S_PASSWORD_RECOVERY', 'ВОССТАНОВЛЕНИЕ ПАРОЛЯ'); // 
define('S_DO_RECOVERY', 'Восстановить'); // 
define('S_LOGIN_FORM', 'Форма авторизации'); // 
define('S_DAYS', 'дней'); // 
define('S_MANUFACTURER_COLLECTIONS', 'Каталоги фабрики'); // 
define('S_ADD_FAVORITE', 'Добавить закладку'); // 
define('S_FAVORITE_ADDED', 'Закладка добавлена'); // 
define('S_LOGIN_PAGE', 'Авторизация'); // 
define('S_NO_PRICES_INCEPTION', 'Прайслисты в разделе'); // 
define('S_NO_PRICES_END', 'не найдены'); // 
define('S_YOU_CAN', 'Вы можете'); // 
define('S_EMAIL', 'E-mail'); // /
define('S_EXIT', 'Выход'); // 
define('S_ENTER_NAME', 'Введите свое имя:'); // 
define('S_ENTER_EMAIL', 'Ваш Email:'); // 
define('S_CHOOSE_NEWS', 'Отметьте наиболее интересные для Вас новости:'); // 
define('S_HOW_DO_YOU_KNOW', 'Откуда Вы о нас узнали:'); // 
define('S_ALL2', 'Все'); // 
define('S_OTHER', 'Другое:'); // 
define('FLB_SELECT_FEED', 'Выберите список рассылки'); // 
define('S_SUBSCRIBE', 'Подписаться'); // 
define('S_NAME2', 'Имя'); // 
define('S_THANKS_FOR_SUBSCRIPTION', 'Спасибо за подписку!'); // 
define('S_TILL', 'Плитка'); // /
define('S_SANITARY', 'Сантехника'); // /
define('S_ARCHIVES', 'Архив'); // 
define('S_ALL_PRODUCTS', 'Все товары'); // /
define('LBL_SENT_SUCCESS', 'Сообщение успешно отправлено'); // 
define('S_LINK', 'Ссылка'); // /
define('S_CALL_MANAGER', 'Заказать обратный звонок'); // /
define('S_INTRODUCTION', 'Представьтесь, пожалуйста'); // /
define('FLB_PHONE_LENGHT', 'Номер телефона должен содержать от 5 до 20 чисел'); // /
define('FLB_PHONE_DIGITS', 'Номер телефона должен содержать только числа (0-9)'); // /
define('S_SUBJECT', 'Тема'); // /
define('S_TIME', 'Время звонка'); // /
define('S_REQUEST_CALL', 'Заказать звонок'); // /
define('S_YOU_MESSAGE', 'Ваше сообщение'); // /
define('S_SUBMIT', 'Отправить'); // /
define('S_ACTIONS_ARCHIVE', 'Архив акций'); // /
define('S_NEW_MASSAGE', 'Новое сообщение от'); // 
define('LBL_CONTACT_NOTE_1', 'На сайте under-style.ru размешён нижеследующий запрос.'); // 
define('LBL_CONTACT_NOTE_2', 'Ответственному сотруднику необходимо дать оперативный ответ по сути данного запроса.'); // 
define('S_NO_PRODS', 'В настоящий момент данный раздел не содержит ни одного предложения.'); // 
define('S_NO_BOOKMARKS', 'К сожалению, Вы еще ничего не выбрали!'); // 
define('S_NO_VIEWED', 'К сожалению, Вы еще не просмотрели ни одного товара!'); // 

?>