<?php
    $location = '/';
    // error messages
    define('CANT_CONNECT_MYSQL', 'Connect to MySQL failed');
    define('CANT_SELECT_DB', 'Error load database');
    define('ERR_ADD_LOGIN_PASS', 'Введите логин и пароль');
    define('ERR_LOGIN', 'Неверный логин или пароль');
    define('ERR_DURING_AUTHORIZATION', 'Ошибка при авторизации');
    define('ERR_JS_NOT_SUPPOTED', "Your browser doesn\'t support Java Script. Click <a href='$location'>link</a> to continue.");
    define('ERR_ACCOUNT_LOCKED', 'Ваш аккаунт заблокирован');

	define('ERR_ADD_LOGIN', 'Введите логин');
	define('ERR_NO_USER_FOUND', 'Пользователь не найден');

	define('SUBJ_RESTORE_PASS', 'Новый пароль для сайта '.TITLE);
	define('LBL_RESTORE_PASS_STARTED', 'Инструкции по восстановлению вашего пароля были высланы на электронный адрес, указанный при регистрации.');
	define('LBL_RESTORE_PASSED', 'Ваш пароль был успешно изменен');
	define('LBL_RESTORE_FAILED', 'Ошибка при восстановлении пароля. Используйте ссылку из письма.');

    define('SUBJ_FEEDBACK', 'Message from '.TITLE);
    define('SUBJ_FEEDBACK_COPY', 'You sent a message to '.TITLE);
    
	// user edit
	define('ERR_GET_USERS', 'Error occurs during users load');
	define('ERR_GET_USER', 'Error occurs during user load');
	define('ERR_ADD_USER', 'Error occurs during user adding');
	define('ERR_EDIT_USER', 'Error occurs during user editing');
	define('ERR_DEL_USER', 'Error occurs during user deleting');

    
    // html
    define('HTML_INPUT', 'class="htmlInput"');
    define('HTML_SELECT', 'class="htmlSelect"');
    define('HTML_HIRE_SELECT', 'class="hireSelect"');
    define('HTML_BIG_AREA', 'class="htmlBigArea"');
    define('HTML_MIDDLE_AREA', 'class="htmlMiddleArea"');
    define('HTML_BUTTON', 'class="htmlButton"');
    define('HTML_BUTTON_EX', 'class="htmlButton" onclick="submitForm()"');
    define('HTML_DISABLED', ' readonly="true" style="background-color: #bbbbbb" ');
    define('HTML_INPUT_ST', '  ');
    define('HTML_SELECT_MULTI', 'multiple="multiple" style="height: 130px; width: 250px;"');
    define('FRM_ON', 'on');
    define('HTML_BIG_AREA_DB', 'class="htmlBigAreaDb"');
    define('HTML_MIDDLE_AREA_DB', 'class="htmlMiddleAreaDb"');
    define('HTML_BUTTON_DELETE', ' class="btnDelete" ');
    define('HTML_BUTTON_ADD', ' class="btnAdd" ');
    define('HTML_BUTTON_UNDO', ' class="inp undo" ');
    define('HTML_BUTTON_SAVE', ' class="inp save" name="save" ');
    define('HTML_BUTTON_SAVE_EX', ' class="inp save" name="save_close" ');


?>