<?php

$enum_cfg['default'] = array();
$enum_cfg['default']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['catalogs'] = array();
$enum_cfg['catalogs']['tabs'] = array(
    array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['home'] = array();
$enum_cfg['home']['tabs'] = array(
	array('id' => 'tab_prop_home', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1),
    array('id' => 'tab_multiupload', 'name' => S_BANNERS, 'local' => 0)
);
$enum_cfg['home']['upl'] = array(
    '0' => array(
        '0' => array('dir' => '/upload/banners_resize', 'tr' => true, 'w' => '240', 'h' => '530', 'm' => 'rc'),
        '1' => array('dir' => '/upload/banners_original', 'tr' => true, 'w' => '1000', 'h' => '1500', 'm' => 'rc')
    )
);

$enum_cfg['viewed'] = array();
$enum_cfg['viewed']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['saved_items'] = array();
$enum_cfg['saved_items']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['subdomen'] = array();
$enum_cfg['subdomen']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_subomain_category', 'name' => S_CATEGORIES, 'local' => 0),
	array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_img', 'name' => S_IMAGE_MAIN, 'local' => 0),
	array('id' => 'tab_multiupload', 'name' => S_IMAGES_CONTENT, 'local' => 0),
);
$enum_cfg['subdomen']['upl'] = array(
	'0' => array(
        '0' => array('dir' => '/upload/article_big/', 'tr' => true, 'w' => '762', 'h' => '450', 'm' => 'ar'),
	),
	'1' => array(
		'0' => array('dir' => '/upload/article', 'tr' => true, 'w' => '342', 'h' => '166', 'm' => 'rc'),
	),
);
$enum_cfg['subdomen']['ctrl'] = array(
	'ch_mode' => '111', 'ch_control' => 'collection'
);

$enum_cfg['collection'] = array();
$enum_cfg['collection']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_prop_collection', 'name' => S_COLLECTION_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_file_upload', 'name' => S_FILE_UPLOAD, 'local' => 0),
);
$enum_cfg['collection']['ctrl'] = array(
	'ch_mode' => '111', 'ch_control' => 'prod'
);

$enum_cfg['prod'] = array();
$enum_cfg['prod']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_prop_prod', 'name' => S_PROD_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content2_descr', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_multiupload', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['prod']['upl'] = array(
	'0' => array(
		'0' => array('dir' => '/upload/article_big/', 'tr' => true, 'w' => '950', 'h' => '1200', 'm' => 'ar'),
        '1' => array('dir' => '/upload/article_mid/', 'tr' => true, 'w' => '380', 'h' => '232', 'm' => 'sr'),
		'2' => array('dir' => '/upload/article/', 'tr' => true, 'w' => '165', 'h' => '102', 'm' => 'sr'),
        '3' => array('dir' => '/upload/article_original/', 'tr' => true, 'w' => '10000', 'h' => '10000', 'm' => 'rc'),
	),
);

$enum_cfg['fldr'] = array();
$enum_cfg['fldr']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1),
    array('id' => 'tab_img', 'name' => S_IMAGES, 'local' => 0)
);

$enum_cfg['fldr']['upl'] = array(
    '1' => array(
        '0' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '800', 'h' => '1200', 'm' => 'rc'),
        )
);


$enum_cfg['news'] = array();
$enum_cfg['news']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['new'] = array();
$enum_cfg['new']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content_descr', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_img', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['new']['upl'] = array(
	'1' => array(
		'0' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '800', 'h' => '600', 'm' => 'ar'),
		'1' => array('dir' => '/upload/article', 'tr' => true, 'w' => '155', 'h' => '115', 'm' => 'rc'),
	),
);

$enum_cfg['actions'] = array();
$enum_cfg['actions']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['action'] = array();
$enum_cfg['action']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content_descr', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_img', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['action']['upl'] = array(
	'1' => array(
		'0' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '800', 'h' => '600', 'm' => 'ar'),
		'1' => array('dir' => '/upload/article', 'tr' => true, 'w' => '155', 'h' => '115', 'm' => 'rc'),
        '2' =>array('dir' => '/upload/article_mid', 'tr' => true, 'w' => '285', 'h' => '102', 'm' => 'rc')
	),
    '2' => array(
        '0' =>array('dir' => '/upload/article_mid', 'tr' => true, 'w' => '285', 'h' => '102', 'm' => 'rc')
    )
);

$enum_cfg['contacts'] = array();
$enum_cfg['contacts']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content2', 'name' => S_PROPERTIES, 'local' => 1),
);


$enum_cfg['company_list'] = array();
$enum_cfg['company_list']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['company'] = array();
$enum_cfg['company']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content2', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_img_company', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['company']['upl'] = array(
	'1' => array(
		'0' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '380', 'h' => '230', 'm' => 'ar'),
	),
	'2' => array(
		'0' => array('dir' => '/upload/article', 'tr' => true, 'w' => '150', 'h' => '50', 'm' => 'sr'),
		'1' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '200', 'h' => '105', 'm' => 'sr'),
	),
);


$enum_cfg['designers'] = array();
$enum_cfg['designers']['tabs'] = array(
    array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1)
);


$enum_cfg['designer'] = array();
$enum_cfg['designer']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content2', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_img', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['designer']['upl'] = array(
	'1' => array(
		'0' => array('dir' => '/upload/article_big', 'tr' => true, 'w' => '800', 'h' => '600', 'm' => 'ar'),
		'1' => array('dir' => '/upload/article', 'tr' => true, 'w' => '165', 'h' => '102', 'm' => 'rc'),
	),
);
$enum_cfg['designer']['ctrl'] = array(
	'ch_mode' => '011', 'ch_control' => 'designer_project'
);

$enum_cfg['designer_project'] = array();
$enum_cfg['designer_project']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop_content', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_multiupload', 'name' => S_IMAGES, 'local' => 0),
	array('id' => 'tab_showroom', 'name' => S_SHOWROOM, 'local' => 0),
);
$enum_cfg['designer_project']['upl'] = array(
	'0' => array(
        '0' => array('dir' => '/upload/designers_fullscreen', 'tr' => true, 'w' => '1280', 'h' => '1024', 'm' => 'ar'),
		'1' => array('dir' => '/upload/designers_big', 'tr' => true, 'w' => '950', 'h' => '438', 'm' => 'sr'),
		'2' => array('dir' => '/upload/designers', 'tr' => true, 'w' => '115', 'h' => '80', 'm' => 'rc'),
	),
);

$enum_cfg['wholesale_buyers'] = array();
$enum_cfg['wholesale_buyers']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1),
);
$enum_cfg['wholesale_buyers']['ctrl'] = array(
	'ch_mode' => '011', 'ch_control' => 'pricelist'
);

$enum_cfg['pricelist'] = array();
$enum_cfg['pricelist']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1),
	array('id' => 'tab_multiupload_prices', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
);
$enum_cfg['pricelist']['upl'] = array(
	'0' => array(
		'0' => array('dir' => '/upload/price_lists/', 'tr' => true),
	),
);

$enum_cfg['sitemap'] = array();
$enum_cfg['sitemap']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_sitemap', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['reg'] = array();
$enum_cfg['reg']['tabs'] = array(
	array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
	array('id' => 'tab_reg', 'name' => S_PROPERTIES, 'local' => 1)
);

$enum_cfg['header_banners'] = array();
$enum_cfg['header_banners']['tabs'] = array(
    array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_local_prop', 'name' => S_PROPERTIES, 'local' => 1),
);

$enum_cfg['header_banner'] = array();
$enum_cfg['header_banner']['tabs'] = array(
    array('id' => 'tab_prop', 'name' => S_COMMON_PROPERTIES, 'local' => 0),
    array('id' => 'tab_local_prop_banner', 'name' => S_PROPERTIES, 'local' => 1),
    array('id' => 'tab_upload_banner', 'name' => S_IMAGES, 'local' => 0),
);
$enum_cfg['header_banner']['upl'] = array(
    '1' => array(
        '0' => array('dir' => '/upload/header_banners', 'tr' => true),
    ),
);