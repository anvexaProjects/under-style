<?php
// do not change order. It affect import
$enum['langs'] = array('ru' => 'Russian');

$enum['bool'] = array('0' => 'No', '1' => 'Yes');

// get watermark size based on image width
$enum['watermarks'] = array(
	'small' => array(0, 200),
	'medium' => array(201, 400),
	'big' => array(400, 5500)
);

$enum['roles'] = array (
	'1' => 'Администратор',
	/* '3' => 'user', */
	'4' => 'Оптовый покупатель',
	'5' => 'Архитектор/Дизайнер',
    '6' => 'Редактор для оптовиков',
    '7' => 'Редактор для дизайнеров'
);
$enum['roles_public'] = array (
    '4' => 'Оптовый покупатель',
    '5' => 'Архитектор/Дизайнер',
);

// array key is sundomain name in admin side
// array values is real subdomain name
$enum['subdomains'] = array(
    'mebel'      => DOMAIN_1,
    'kuhni'      => DOMAIN_2,
    'svet'       => DOMAIN_3,
    'aksessuari' => DOMAIN_4
);

$enum['subjects'] = array(
    '0' => '',
    '5' => 'Написать письмо директору',
    '1' => 'Сделать заказ',
    '2' => 'Узнать о состоянии заказа',
    '3' => 'Вопрос',
    '4' => 'Предложение',
    '6' => 'Другое...',
);
$enum['subjects_en'] = array(
    '0' => 'New contact form notification from under-style.ru',
    '1' => 'New contact form notification from under-style.ru',
    '2' => 'New contact form notification from under-style.ru',
    '3' => 'New contact form notification from under-style.ru',
    '4' => 'New contact form notification from under-style.ru',
    '5' => 'New contact form notification from under-style.ru',
    '6' => 'New contact form notification from under-style.ru'
);