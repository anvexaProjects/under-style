<?php


// number options
define('MAX_PAGES', '10');
define('SEARCH_LIMIT', '10');
define('PAGE_LIMIT', '6');
define('SEARCH_CHARS', '180');

define('LMT_CATALOG_PER_PAGE', '20');
define('LMT_CATALOG_ITEMS_FILTERED', '20');
define('LMT_BLOCK_NEW', '2');
define('LMT_BLOCK_DISCOUNT', '1');
define('LMT_INDEX_PROD_BLOKS', '2');
define('LMT_INDEX_NEWS', '3');
define('LMT_NEWS', '6');

define('MAX_VALUE', '999999999');
define('MAX_LEVEL_SIMPLE_TREE', '0');
define('ALLOW_DRAG_SIMPLE_TREE', false);
define('VIEW_LAST_LEVEL_SIMPLE_TREE', false);

// admin limiters
define('USERS_LIMIT', '30'); // number of users per page in admin panel
define('COMMENTS_LIMIT', '30'); // number of comments per page in admin panel
define('ORDERS_ADMIN_LIMIT', '10'); // number of orers per page for admin panel
define('ADMIN_MAX_PAGES', '20'); // number of max pages to display in paging control
define('ADMIN_DB_DIC_PAGESIZE', '40'); // number of items per page for db dictionaries
define('IMAGES_LIMIT', '30'); // number of images per page in admin panel

// user limiters
define('LMT_INDEX_PROJ', '6');

define('TITLE', 'under-style.ru');

define('IS_DEBUG_INIT', '0');  // show debug information
define('ENABLE_CACHING', false);
define('ENABLE_COMPRESSION', false);
define('APP_VERSION', '14.01.2014'); //


define('MAX_UPLOAD_FILE_SIZE', '50'); // in megabytes
define('EMAIL_ADDRESS', 'info@deinstallatieingenieur.nl');

define('COOKIE_TIME', 60*60*24*30); // 30 days
define('CHECK_SPAM2', '445DD124-EBB6-4ad2-802C-0C5BEFF64A95');


define('ADD_NEW_ALGORITHM_RESORTING', '1');

define('WATERMARK_PATH', '/images/site/watermark_%s.png');
define('WATERMARK_ENABLE', true);

define('UNISENDER_KEY', '57dx4pfimufjs8ez5fiwtyipurgdifbyxeby86xe');
?>