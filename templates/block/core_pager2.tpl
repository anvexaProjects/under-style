{if $aPaging.totalPages>1}
{strip}
<div id="pages">
    {foreach from=$aPaging.urls key=sUrl item=iPage name=f}
    {if $sUrl}<a href="{$sUrl}">{$iPage}</a>{else}<span>{$iPage}</span>{/if}{if !$smarty.foreach.f.last}&bull;{/if}
    {/foreach}
</div>
{/strip}
{/if}
