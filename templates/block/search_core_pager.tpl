{if $aPaging.totalPages>1}
{if $aPaging.prevUrl}<a href="{$aPaging.prevUrl|replace:'search&':''}" class="pager_link">&larr;</a>&nbsp;{/if}&nbsp;

{foreach from=$aPaging.urls key=sUrl item=iPage}
{if $sUrl}<a href="{$sUrl|replace:'search&':''}" class="pager_link">{$iPage}</a>{else}<strong>{$iPage}</strong>{/if} &nbsp;
{/foreach}

{if $aPaging.nextUrl} <a href="{$aPaging.nextUrl|replace:'search&':''}" class="pager_link">&rarr;</a>&nbsp;{/if}
{/if}