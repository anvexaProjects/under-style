{if $aPaging.totalPages>1}
<ul class="pager">
{foreach from=$aPaging.urls key=sUrl item=iPage}
	<li>
		{if $sUrl}
		{if $smarty.get.path eq $smarty.get.global.lan|cat:'/publisher/news'}
		<a href="{$smarty.get.global.root|subdomain:$smarty.get.path}/{$smarty.get.path}/{if $smarty.get.y}{$smarty.get.y}/{/if}page={$iPage}/">{$iPage}</a>
		{elseif $smarty.get.path eq $smarty.get.global.lan|cat:'/books' && $smarty.get.query}
		<a href="{$smarty.get.global.root|subdomain:$smarty.get.path}/{$smarty.get.path}/page={$iPage}/query={$smarty.get.query}/">{$iPage}</a>
		{else}
		<a href="{$sUrl}">{$iPage}</a>
		{/if}
		{else}
		<span>{$iPage}</span>
		{/if}
	</li>
{/foreach}
</ul>
{/if}