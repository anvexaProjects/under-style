<div class="photo"><a href="{$smarty.get.global.root}/{$prop.p}/"><span><!-- --></span>
{strip}
	{if $big_img_path neq ''}<a href="{$smarty.get.global.root}{$big_img_path}" class="lightbox" {if $prop.title neq ''}  title="{$prop.title}"{/if} rel="prettyPhoto[g]">{/if}
	<img src="{$smarty.get.global.root}{$img_path}" 
		{if $prop.width neq ''}  width="{$prop.width}" {/if}
		{if $prop.height neq ''} height="{$prop.height}" {/if} 
		{if $prop.align neq ''}  align="{$prop.align}" {/if}
		{if $prop.class neq ''}  class="{$prop.class}" {/if}
		{if $prop.id neq ''}  id="{$prop.id}" {/if}  
		{if $prop.alt neq ''}  alt="{$prop.alt}"{/if} /> 
	{if $big_img_path neq ''}</a>{/if}
{/strip}
</a></div>