{if $aPaging.totalPages>1}
    <nav style="text-align: center;">

            {*{$smarty.const.S_YOU_SEEN_AT_PAGE}*}

        <ul class="pagination">
            <li>
                {if $aPaging.prevUrl}
                    <a aria-label="Previous" href="{$aPaging.prevUrl}" class="cur">
                        <span aria-hidden="true">&laquo; {$smarty.const.S_PREVIOUS_PAGE}</span>
                    </a>
                {/if}
            </li>
            {foreach from=$aPaging.urls key=sUrl item=iPage}

                {if $sUrl}
                    <li><a href="{$sUrl}">{$iPage}</a></li>
                {else}
                    <li class="active"><a href="">{$iPage}</a></li>
                {/if}

            {/foreach}
            {if $smarty.get.global.rstruct.1.control eq 'subdomen'}

                <li>
                <a href="/{$smarty.get.path}/page=all/">
                    Все сразу
                </a>
                </li>
            {/if}
            <li>
                {if $aPaging.nextUrl}
                    <a aria-label="Next" href="{$aPaging.nextUrl}">
                        <span aria-hidden="true"> {$smarty.const.S_NEXT_PAGE} &raquo;</span>
                    </a>
                {/if}
            </li>
        </ul>
    </nav>
{/if}