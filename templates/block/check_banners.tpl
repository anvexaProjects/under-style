{if !$items.0.is_hidden}
    {include_php
        file="content/lister.inc.php"
        tpl_name = "master/header_banners.tpl"
        plevel = 0
        cond = "as1.control='header_banner'"
        fields = "as1.img1, as1.sprop7, as1.sprop1"
        disable_caching = true
    }
{/if}