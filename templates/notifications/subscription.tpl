Hello {$data.fname} {$data.lname}.<br /><br />

Thank you for the subscribing on www.wero-dienstverlening.nl.<br /><br />

Please follow the URL below to activate your account.<br />
<a href="http://{$smarty.const.SITE_ADR}{$smarty.const.SITE_REL}/pub/subscription_handler.php?u={$data.hash}">http://{$smarty.const.SITE_ADR}{$smarty.const.SITE_REL}/pub/subscription_handler.php?u={$data.hash}</a><br>
