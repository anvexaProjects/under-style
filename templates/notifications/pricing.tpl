<p>Новое сообшение об уточнении цены от {$smarty.const.TITLE}</p>
<b>{$smarty.const.S_PROD_NAME}:</b> {$smarty.post.prod_name}<br>
<b>{$smarty.const.S_LINK}: </b><a href="{$smarty.post.link}">{$smarty.post.link}</a><br />
<b>{$smarty.const.S_NAME}</b> {$smarty.post.name}<br>
<b>{$smarty.const.S_PHONE}:</b> {$smarty.post.phone}<br>
<b>{$smarty.const.S_EMAIL}:</b> <a href="mailto:{$smarty.post.email}">{$smarty.post.email}</a><br>
{$smarty.post.message|nl2br}