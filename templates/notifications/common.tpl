<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>{$smarty.const.TITLE}</title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>

<table border="0">
<tr><td><p>{$content}</p></td></tr>
<tr><td>
<br>
<p>
<hr>
&copy; {$smarty.now|date_format:"%Y"}&nbsp;{$smarty.const.TITLE}&nbsp;&mdash; all rights reserved
<br />Sent {$smarty.now|date_format:"%b %d, %Y  %a %H:%M:%S %z"}
</p>
</td></tr>
</table>

</body>
</html>