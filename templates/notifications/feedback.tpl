<p>{$smarty.const.LBL_CONTACT_NOTE_1}<br />{$smarty.const.LBL_CONTACT_NOTE_2}<br /><br /></p>


<p>{$smarty.const.S_NEW_MASSAGE}  {$smarty.const.TITLE}</p>
<b>{$smarty.const.S_NAME}</b> {$smarty.post.name}<br />
<b>{$smarty.const.S_PHONE}:</b> {$smarty.post.phone}<br />
<b>{$smarty.const.S_SUBJECT}:</b> {$enum.subjects[$smarty.post.subject]}<br />
<b>{$smarty.const.S_TIME}:</b> {$smarty.post.time}<br>
<b>{$smarty.const.S_EMAIL}:</b> <a href="mailto:{$smarty.post.email}">{$smarty.post.email}</a><br />
{$smarty.post.message|nl2br}