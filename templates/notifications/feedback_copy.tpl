<p>You have sent an email to {$smarty.const.TITLE}</p>
<b>{$smarty.const.S_NAME}:</b> {$smarty.post.name}<br>
{if $smarty.post.lname}<b>{$smarty.const.S_LNAME}:</b> {$smarty.post.lname}<br>{/if}
{if $smarty.post.mname}<b>{$smarty.const.S_MNAME}:</b> {$smarty.post.mname}<br>{/if}
{if $smarty.post.street}<b>{$smarty.const.S_LNAME}:</b> {$smarty.post.street}<br>{/if}
{if $smarty.post.nr}<b>{$smarty.const.S_N}:</b> {$smarty.post.nr}<br>{/if}
{if $smarty.post.postcode}<b>{$smarty.const.S_POSTCODE}:</b> {$smarty.post.postcode}<br>{/if}
{if $smarty.post.address}<b>{$smarty.const.S_ADDRESS}:</b> {$smarty.post.address}<br>{/if}
{if $smarty.post.phone}<b>{$smarty.const.S_PHONE}:</b> {$smarty.post.phone}<br>{/if}
{if $smarty.post.email}<b>{$smarty.const.S_EMAIL}:</b> <a href="mailto:{$smarty.post.email}">{$smarty.post.email}</a><br>{/if}
{$smarty.post.message|nl2br}