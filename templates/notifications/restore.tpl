Ваш пароль для сайта {$smarty.const.TITLE} был изменен<br /><br />
Новый пароль: {$pass}<br /><br />
Пройдите по ссылке, что бы новый пароль вступил в силу:
<a href="http://{$smarty.const.SITE_ADR}{$smarty.const.SITE_REL}/pub/login.php?location=salers&u={$user.id}&h={$pass_hash}&recovery=2">http://{$smarty.const.SITE_ADR}{$smarty.const.SITE_REL}/pub/login.php?location=salers&u={$user.id}&h={$pass_hash}&recovery=2</a>
