<div id="registration">
    <form {$form_data.attributes}>
        {$form_data.javascript}
        {$form_data.hidden}
        {include file="ablock/error_block.tpl"}
        <table>
            <col width="170">
            <tr>
                <td>{$smarty.const.S_INTRODUCTION}&nbsp;<span style="color: red;">*</span></td>
                <td>{$form_data.name.html}</td>
            </tr>
            <tr>
                <td>{$smarty.const.S_PHONE}&nbsp;<span style="color: red;">*</span></td>
                <td>{$form_data.phone.html}</td>
            </tr>
            <tr>
                <td>{$smarty.const.S_SUBJECT}</td>
                <td>{$form_data.subject.html}</td>
            </tr>
            <tr>
                <td>{$smarty.const.S_TIME}</td>
                <td>{$form_data.time.html}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="{$smarty.const.S_REQUEST_CALL}" style="background: #000; border: 0; padding: 5px; color: #fff;" class="submit_button" style="margin:0;"></td>
            </tr>
        </table>
        <div style="display: none">
            {$form_data.field_1.html}{$form_data.field_2.html}
        </div>
    </form>
</div>