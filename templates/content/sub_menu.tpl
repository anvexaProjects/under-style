<div class="submenu">
	<ul>
		{foreach from=$items item=i name=f}
		<li {if $smarty.foreach.f.last} class="last"{/if}>
			<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>
		</li>
		{/foreach}
	</ul>
</div>