<div id="registration">

    <form {$form_data.attributes}>
        {$form_data.javascript}
        {$form_data.hidden}

        {include file="ablock/error_block.tpl"}

        {if $smarty.get.s && $smarty.get.s == 1}
            <script>
                alert("{$smarty.const.LBL_SENT_SUCCESS}");
                window.location.href = "/about/contacts/feedback/";
            </script>
        {/if}



        {*{$smarty.const.S_INTRODUCTION}&nbsp;<span style="color: red;">*</span>*}
        {*{$form_data.name.html}*}

        {*{$smarty.const.S_PHONE}*}
        {*{$form_data.phone.html}*}

        {*{$smarty.const.S_EMAIL}&nbsp;<span style="color: red;">*</span>*}
        {*{$smarty.const.S_YOU_MESSAGE}&nbsp;<span style="color: red;">*</span>*}

        {*{$form_data.message.html}*}
        <div class="col-md-6 col-md-offset-2">
            <div class="input-group">
                <input id="name" class="form-control" name="name" type="text" placeholder="{$smarty.const.S_INTRODUCTION}">
            </div>

            <div class="input-group">
                <input id="phone" class="form-control" name="phone" type="text" placeholder="{$smarty.const.S_PHONE}">
            </div>

            <div class="input-group">
                <input id="email" class="form-control" name="email" type="text" placeholder="{$smarty.const.S_EMAIL}">
            </div>

            <div class="input-group">
                <textarea rows="3" id="message" class="form-control" name="message" placeholder="{$smarty.const.S_YOU_MESSAGE}"></textarea>
            </div>

            <br>

            <button class="btn btn-primary btn-block">Отправить</button>

        </div>

        <div style="display: none">
            {$form_data.field_1.html}{$form_data.field_2.html}
        </div>

    </form>
</div>