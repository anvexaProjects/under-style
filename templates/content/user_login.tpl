<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>DiamondCMS - {$smarty.const.S_LOGIN_PAGE}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Keywords" content="" />
	<meta name="Description" content="" />
	<link rel="stylesheet" type="text/css" href="{$smarty.get.global.root}/templates/css/site/main.css" />
	<script src='{$smarty.get.global.root}/templates/js/jquery/jquery-1.9.1.js' type='text/javascript'></script>
	<script type="text/javascript">
		{literal}
		$(document).ready(function(){
		{/literal}
		{if $smarty.post.submit_restore or $smarty.get.recovery eq 1}
			showRestoreForm();
		{elseif $smarty.get.recovery eq 2 and !$smarty.post}
			$('#login_form').hide();
			$('#restore_form').hide();
			$('#restore_message').show();
		{/if}
		{literal}
		});
		function showRestoreForm(reverse) {
			if (reverse) {
				$('#login_form').show();
				$('#restore_form').hide();
				$('#restore_message').hide();
			} else {
				$('#login_form').hide();
				$('#restore_form').show();
				$('#restore_message').hide();
			}
		}
		{/literal}
	</script>
 </head>
<body class="auth">
<div class="block_auth">

  <div class="text_auth">
  <form name="" id="auth_form" method="post" action="" style="margin: 0; padding: 0;">
  <table id="login_form">
    <tr>
      <td colspan="2"><h2 class="auth">{$smarty.const.S_AUTH_L}</h2></td>
    </tr>
    <tr>
      <td>{$smarty.const.S_LOGIN}</td>
      <td><input style="width: 250px; height: 20px;" type="text" id="login" name="login" /></td>
    </tr>
    <tr>
      <td>{$smarty.const.S_PASSWORD}</td>
      <td><input style="width: 250px; height: 20px;" type="password" id="pass" name="pass" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input class="enter" value="{$smarty.const.S_ENTER}" name="submit_login" type="submit" /><a style="float: right; color: #000;" href="javascript:showRestoreForm()">{$smarty.const.S_FORGOT}</a>
      </td>
    </tr>
    {if $error_login}
      <tr>
          <td></td>
          <td align="left" colspan="2"><div class="err" style="color: red;">{$error_login}</div></td>
      </tr>
   {/if}
    <tr>
  </table>
	</form>
	<form name="" id="auth_form" method="post" action="" style="margin: 0; padding: 0;">
	<table id="restore_form" style="display: none;">
    <tr>
      <td colspan="2"><h2 class="auth">{$smarty.const.S_PASSWORD_RECOVERY}</h2></td>
    </tr>
	<tr>
      <td>{$smarty.const.S_LOGIN}</td>
      <td><input style="width: 250px; height: 20px;" type="text" id="login_restore" name="login_restore" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input class="enter" value="{$smarty.const.S_DO_RECOVERY}" name="submit_restore" type="submit" /><a style="float: right; color: #000;" href="javascript:showRestoreForm(true)">{$smarty.const.S_LOGIN_FORM}</a>
      </td>
    </tr>
    {if $error_restore}
      <tr>
          <td></td>
          <td align="left" colspan="2"><div class="err" style="color: red;">{$error_restore}</div></td>
      </tr>
   {/if}
    {if $smarty.get.recovery eq 1}
      <tr>
          <td></td>
          <td align="left" colspan="2">{$smarty.const.LBL_RESTORE_PASS_STARTED}</td>
      </tr>
   {/if}
	</table>
	</form>
  </table>
	<table id="restore_message" style="display: none;">
    <tr>
      <td><h2 class="auth">{$smarty.const.S_PASSWORD_RECOVERY}</h2></td>
    </tr>
      <tr>
          <td>
			  {if $restored}{$smarty.const.LBL_RESTORE_PASSED}{else}{$smarty.const.LBL_RESTORE_FAILED}{/if}
			  <br /><br /><a style="float: right; color: #000;" href="javascript:showRestoreForm(true)">{$smarty.const.S_LOGIN_FORM}</a>
		  </td>
      </tr>
	</table>
         {* src="{$smarty.get.global.root}images/site/button_login.png" *}
  </div>
  <div style="clear: both;"></div>
    <div class="auth_info">
    <span style="color: #7E7E7E; font-size: 11px;">{$smarty.const.S_ENTER_COND}</span> <a style="font-size: 11px; color: #fff;" href="{$smarty.get.global.root}/reg/">{$smarty.const.S_REG}</a>
	
  </div>          
</div>
</body>
</html>


