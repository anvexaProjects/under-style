{*<?*}
{php}

function rus2translit($string) {

    $converter = array(

        'а' => 'a',   'б' => 'b',   'в' => 'v',

        'г' => 'g',   'д' => 'd',   'е' => 'e',

        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

        'и' => 'i',   'й' => 'y',   'к' => 'k',

        'л' => 'l',   'м' => 'm',   'н' => 'n',

        'о' => 'o',   'п' => 'p',   'р' => 'r',

        'с' => 's',   'т' => 't',   'у' => 'u',

        'ф' => 'f',   'х' => 'h',   'ц' => 'c',

        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',

        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',



        'А' => 'A',   'Б' => 'B',   'В' => 'V',

        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

        'И' => 'I',   'Й' => 'Y',   'К' => 'K',

        'Л' => 'L',   'М' => 'M',   'Н' => 'N',

        'О' => 'O',   'П' => 'P',   'Р' => 'R',

        'С' => 'S',   'Т' => 'T',   'У' => 'U',

        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',

        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

    );

    return strtr($string, $converter);

}

function str2url($str) {

    // переводим в транслит

    $str = rus2translit($str);

    // в нижний регистр

    $str = strtolower($str);

    // заменям все ненужное нам на "-"

    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);

    // удаляем начальные и конечные '-'

    $str = trim($str, "-");

    return $str;

}

/*
$oDb = &Database::get();
$rows = $oDb->getRows('SELECT  art_struct.sprop13 as name , art_struct.parent_id
FROM    art_struct
WHERE   art_struct.control = "prod" GROUP BY art_struct.sprop13');


var_dump($rows);


*/

/*
$oDb = &Database::get();
$rows = $oDb->getRows('SELECT * FROM collection_psevdonims');

$values = array();
foreach($rows as $row)
{
	$oDb->update('collection_psevdonims', array('psevdonim'=>str2url($row['name'])), 'psevdonim_id = '.$row['psevdonim_id']);
}

die("test");

*/


$oDb = &Database::get();

//var_dump($_GET);
//die(test);
$subdomen_id = $_GET['global']['rstruct'][1]['struct_id'];
$subdomen_psevd = $_GET['global']['rstruct'][1]['psevdonim'];

$sSql = "SELECT a1.struct_id, a1.sprop3, a1.sprop4, a1.sprop13 as sprop13, a2.psevdonim as cat_psevdonim, as1.psevdonim as proizv_psevdonim, as1.sprop3 as proizv_name
                    FROM art_struct AS a1
                    INNER JOIN art_struct AS as1 ON (a1.parent_id = as1.struct_id AND as1.is_deleted = 0)
                    INNER JOIN art_struct AS as2 ON (as1.parent_id = as2.struct_id AND as2.is_deleted = 0 AND as2.struct_id=$subdomen_id)
                    INNER JOIN category_psevdonims AS a2 ON (a2.name = a1.sprop4)
                    WHERE a1.control = 'prod' GROUP BY a1.struct_id";


$prods = $oDb->getRows($sSql);


$coll_psevds = $oDb->getRows('SELECT * FROM collection_psevdonims');

$coll_psevds_norm = array();

foreach($coll_psevds as $item)
{

    $coll_psevds_norm[$item['name']] = $item;
    //unset($coll_psevds_norm[$item['name']]['name']);
}


$struct = array();

foreach($prods as $prod)
{
    if(!isset($struct[$prod['cat_psevdonim']]))
    {
        $struct[$prod['cat_psevdonim']] = array('name'=>$prod['sprop4'], 'children'=>array());
    }

    if(!isset($struct[$prod['cat_psevdonim']]['children'][$prod['proizv_psevdonim']]))
    {
        $struct[$prod['cat_psevdonim']]['children'][$prod['proizv_psevdonim']] = array('name'=>$prod['proizv_name'], children=>array());
    }

    $part = $prod['sprop13'];



   // var_dump(!isset($struct[$prod['cat_psevdonim']]['children'][$prod['proizv_psevdonim']]['children'][$coll_psevds_norm[$part]]));
   // die('test');

        if(isset($coll_psevds_norm[$part]) && !isset($struct[$prod['cat_psevdonim']]['children'][$prod['proizv_psevdonim']]['children'][$coll_psevds_norm[$part]]))
        {


            $struct[$prod['cat_psevdonim']]['children'][$prod['proizv_psevdonim']]['children'][$coll_psevds_norm[$part]['psevdonim']] = array('name'=>$coll_psevds_norm[$part]['name'], 'children'=>array());
        }


}
if (isset($_GET['property'])) {
    return;
}
/*foreach($prods as $prod)
{
    $struct[$prod['cat_psevdonim']] = array('name'=>$prod['sprop4'], 'children'=>array());
}*/

/*
//var_dump($struct);
//die();

$collections = $oDb->getRows('SELECT * FROM art_struct WHERE parent_id='.$subdomen_id.' and control="collection"');

$ids = array();

foreach($collections as $collection)
{
     $ids[] = $collection['struct_id'];
}


$rows = $oDb->getRows('SELECT * FROM art_struct INNER JOIN category_psevdonims ON (art_struct.sprop4 = category_psevdonims.name) WHERE control="prod" AND is_deleted = 0 AND parent_id IN ('.implode(',', $ids).') GROUP BY sprop4');

$this->assign('rows', $rows);*/
echo  '<ul class="nav">';
$vendors = $oDb->getRows("SELECT a1.struct_id, a1.psevdonim, a1.sprop3 FROM art_struct as a1 JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' GROUP BY a1.psevdonim  ORDER BY a1.sprop3 ASC");
if (!preg_match('#/(sale|offers|archives)/#', $_SERVER['REQUEST_URI'])) {
$vendors = $oDb->getRows("SELECT a1.struct_id, a1.psevdonim, a1.sprop3 FROM art_struct as a1 JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' GROUP BY a1.psevdonim  ORDER BY a1.sprop3 ASC");
$uri = explode('/', $_SERVER['REQUEST_URI']);


    //echo  '<li class="cur">Каталог товаров</li>';

    if ($subdomen_psevd == 'sanitary')
    echo '<li class="cur">Каталог сантехники</li>';
    elseif ($subdomen_psevd == 'plitka')
    echo '<li class="cur">Каталог плитки</li>';
    elseif ($subdomen_psevd == 'mebel')
    echo '<li class="cur">Каталог мебели</li>';
    elseif ($subdomen_psevd == 'svet')
    echo '<li class="cur">Каталог света</li>';
    else
    echo '<li class="head">Каталог товаров</li>';

$first = true;
echo  '<li class="h">Производители</li>';
foreach($vendors as $vendor) {
    $link = '/'.$subdomen_psevd.'/'.$vendor['psevdonim'].'/';
    echo '<li class="';
    $exp_link = explode('/', $link);
    if(strpos($_SERVER['REQUEST_URI'], $link) === 0|| ($exp_link[1] == $uri[1] && mb_strtolower($exp_link[2]) == mb_strtolower($uri[2]))) {
        echo 'active ';
    }
    if ($first) {
        $first = false;
        echo 'first ';
    }
    echo 'vendor"><a href="'.$link.'">'.$vendor['sprop3'].'</a>';
    if (strpos($_SERVER['REQUEST_URI'], $link) === 0 || ($exp_link[1] == $uri[1] && mb_strtolower($exp_link[2]) == mb_strtolower($uri[2]))) {
    $_GET['vstruct_id'] = $vendor['struct_id'];
{/php}{*?>*}
{*<p class="h filter">{$smarty.const.S_TO_LOCATIONS}</p>
<div class="catSelect last filter locations">
{assign var = 'struct_id' value=$smarty.get.vstruct_id}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_location.tpl"
cond="as1.parent_id='$struct_id' AND as1.is_deleted = 0"
field="sprop6"}
</div>*}
{*<?*}
{php}

        echo '<ul class="nav">';


// echo "<!-- ";
// echo "SELECT `t1`.`psevdonim_id`, `t1`.`name`, `t1`.`psevdonim` FROM `collection_psevdonims` `t1` JOIN `art_struct` `t2` ON (`t2`.`sprop13` = `t1`.`name` AND `t2`.`is_deleted` = 0)  WHERE  `t1`.`parent_id` = '".$vendor['struct_id']."' AND `t2`.`sprop1` != '1'  GROUP BY `t1`.`name` ORDER BY `t1`.`name` ASC";
// echo " -->";



        $colList = $oDb->getRows("SELECT `t1`.`psevdonim_id`, `t1`.`name`, `t1`.`psevdonim` FROM `collection_psevdonims` `t1` JOIN `art_struct` `t2` ON (`t2`.`sprop13` = `t1`.`name` AND `t2`.`is_deleted` = 0)  WHERE  `t2`.`parent_id` = '".$vendor['struct_id']."' AND `t2`.`sprop1` != '1'  GROUP BY `t1`.`name` ORDER BY `t1`.`name` ASC");
        if (!empty($colList)) {
            echo '<li class="h ">Коллекции</li>';
        }
        foreach($colList as $collitem) {
            $link2 = $link.$collitem['psevdonim'].'/';
            echo '<li ';
            if($_SERVER['REQUEST_URI'] == $link2) {
                $class = 'class="active" ';
                echo $class;
            }else {
                $class = '';
            }
            echo '><a href="'.$link2.'">'.$collitem['name'].'</a></li>';
        }

// echo "<!-- ";
// echo "SELECT DISTINCT a.sprop4, c.psevdonim FROM `art_struct` as a INNER JOIN  `category_psevdonims` as c ON (a.sprop4=c.name AND a.is_deleted = 0) WHERE a.control='prod' AND a.parent_id = '".$vendor['struct_id']."' ORDER BY a.sprop4 ASC";
// echo " -->";


        $groupList = $oDb->getRows("SELECT DISTINCT a.sprop4, c.psevdonim FROM `art_struct` as a INNER JOIN  `category_psevdonims` as c ON (a.sprop4=c.name AND a.is_deleted = 0) WHERE a.control='prod' AND a.parent_id = '".$vendor['struct_id']."' AND `a`.`sprop1` != '1'  ORDER BY a.sprop4 ASC");
        if (!empty($groupList)) {
            echo '<li class="h">Товарные группы</li>';
        }
        foreach($groupList as $groupitem) {
			if ($groupitem['psevdonim']) {
				$link3 = $link.'collection/'.$groupitem['psevdonim'].'/';
				echo '<li ';
				if($_SERVER['REQUEST_URI'] == $link3) {
					$class = 'class="active" ';
					echo $class;
				}else {
					$class = '';
				}
				echo '><a href="'.$link3.'">'.$groupitem['sprop4'].'</a></li>';
			}
        }
        /*$placeList = $oDb->getRows("SELECT DISTINCT a.sprop6, p.psevdonim FROM `art_struct` as a INNER JOIN  `place_psevdonims` as p ON (a.sprop6=p.name) WHERE a.control='prod' AND a.parent_id = '".$vendor['struct_id']."' ORDER BY a.sprop6 ASC");
        if (!empty($placeList)) {
            echo '<li class="h">Расположение</li>';
        }
        foreach($placeList as $placeitem) {
            $link4 = $link.'place/'.$placeitem['psevdonim'].'/';
            echo '<li ';
            if($_SERVER['REQUEST_URI'] == $link4) {
                $class = 'class="active" ';
                echo $class;
            }else {
                $class = '';
            }
            echo '><a href="'.$link4.'">'.$placeitem['sprop6'].'</a></li>';
        }*/
        echo  '</ul>';
    }
    echo '</li>';
}



$placeList = $oDb->getRows("SELECT DISTINCT a.sprop6, c.psevdonim FROM `art_struct` as a1, `art_struct` as a INNER JOIN  `place_psevdonims` as c ON (a.sprop6=c.name  AND a.is_deleted = 0) WHERE a.control='prod' AND a1.parent_id = '$subdomen_id'  AND a1.is_deleted = 0 AND a.parent_id = a1.struct_id ORDER BY a.sprop6 ASC");
if (!empty($placeList)) {
    echo '<li class="h">Расположение</li>';
}
foreach($placeList as $placeitem) {
    $link4 = '/'.$subdomen_psevd.'/place/'.$placeitem['psevdonim'].'/';
    echo '<li ';
    if($_SERVER['REQUEST_URI'] == $link4) {
        $class = 'class="active" ';
        echo $class;
    }else {
        $class = '';
    }
    echo '><a href="'.$link4.'">'.$placeitem['sprop6'].'</a></li>';
}
$GLOBALS['hide_sub_menu']=true;

}else {
    /* echo  '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendors[0]['psevdonim'].'/#cat">КАТАЛОГ ТОВАРОВ</a></li>'; */
    /*if ($subdomen_psevd == 'sanitary')
    echo '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог сантехники</a></li>';
    elseif ($subdomen_psevd == 'plitka')
    echo '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог плитки</a></li>';
    elseif ($subdomen_psevd == 'mebel')
    echo '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог мебели</a></li>';
    elseif ($subdomen_psevd == 'svet')
    echo '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог света</a></li>';
    else
    echo '<li class="head"><a href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог товаров</a></li>';*/
	echo '<li class="head"><a href="/'.$subdomen_psevd.'/">';
	switch ($subdomen_psevd) {
		case 'sanitary': echo 'Каталог сантехники'; break;
		case 'plitka': echo 'Каталог плитки'; break;
		case 'mebel': echo 'Каталог мебели'; break;
		case 'svet': echo 'Каталог света'; break;
		default: echo 'Каталог товаров'; break;
	}
	echo '</a></li>';
}
echo  '</ul>';
//include($_SERVER['DOCUMENT_ROOT'].'/templates/content/static_menu_catalog_sale.tpl');

{/php}{*?>*}
{include file="content/static_menu_catalog_spec.tpl"}
{include file="content/static_menu_catalog_sale.tpl"}
{include file="content/static_menu_catalog_archives.tpl"}
