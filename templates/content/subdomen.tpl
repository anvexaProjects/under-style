{include file="content/picking.tpl"}

<div class="row">

    <div class="col-lg-12">
        <h1>
            {$smarty.get.global.struct.1.name}
            {if $smarty.get.property eq 'available'} | {$smarty.const.S_IS_AVAILABLE}
            {elseif $smarty.get.property eq 'hold'} | {$smarty.const.S_ON_HOLD}
            {elseif $smarty.get.property eq 'offers'} | {$smarty.const.S_SPECIAL_OFERT}
            {elseif $smarty.get.property eq 'sale'} | {$smarty.const.S_SALE}
            {elseif $smarty.get.property eq 'catalog'} | {$smarty.const.S_CATALOG_PRODUCTS}
            {elseif $smarty.get.property eq 'archives'} | {$smarty.const.S_ARCHIVES}
            {/if}
        </h1>

        {include file="content/catalog_crumbs.tpl" }

        {php}
            if($_SERVER['REQUEST_URI'] != '/viewed/' && $_SERVER['REQUEST_URI'] != '/saved_items/') {
        {/php}


        {*{include file="content/static_menu_catalog.tpl"}*}
        {*{include_php file = "content/lister.inc.php" tpl_name="content/sub_menu_catalog.tpl" plevel = 0 cond = "as1.struct_id = '`$smarty.get.global.struct.1.struct_id`'" disable_caching = true}*}

        {php}
            } else {
        {/php}

            <div class="rSide saved_viewed">

                {php} } {/php}

            {include file="content/subdomen_catalog.tpl" }

            {*{include_php file="content/lister.inc.php" tpl_name="content/subdomen_catalog.tpl" allow_paging=true page_size=6}*}
        </div>


        <div class="col-md-12">
            {include_php file="content/lister.inc.php"
            tpl_name="content/destinations.tpl" plevel=0
            cond="as1.control='subdomen' AND as1.level=1"}
        </div>
    </div>
</div>