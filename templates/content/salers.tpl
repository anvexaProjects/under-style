{if $smarty.get.global.struct.1.spath eq 'wholesale_buyers'}
{include_php file="content/check_access.inc.php location='salers'}
{elseif $smarty.get.global.struct.1.spath eq 'architects_designers'}
{include_php file="content/check_access.inc.php location='des_arc'}
{/if}
<div id="cont">
		<div class="title clearfix">
					<div class="title clearfix">
			<h1>{$smarty.get.global.struct.1.name}</h1>
		<div class="path" style="">
        	{include_php
        		file="content/lister.inc.php"
        		tpl_name="content/breadcrumbs.tpl"
        		plevel=0
        		cond="a1.path = '$path'"
        	}
        </div>
		</div>
		<div class="mainCont clearfix">
			<div class="lSide">
				{include_php
                    file="content/lister.inc.php"
                    tpl_name="content/prices.tpl"
                    plevel=1
                    parent=$smarty.get.global.struct.1.path
                }
			</div>
			<div class="rSide">
				{include_php
                    file="content/imglister.inc.php"
                    tpl_name="content/pricelists.tpl"
                    plevel=1
                    parent=$smarty.get.global.struct.1.path
                    disable_caching = true
                }
                <div id = "no_prices">
                    {$smarty.const.S_NO_PRICES_INCEPTION}
                    <span id="no_prices_value"></span>
                    {$smarty.const.S_NO_PRICES_END}
                </div>
			</div>
		</div>

		{include_php file="content/lister.inc.php"
	tpl_name="content/destinations.tpl" plevel=0
	cond="as1.control='subdomen' AND as1.level=1"}

	</div>