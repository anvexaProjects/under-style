{php}

    $oDb = &Database::get();
    $sql = "
        SELECT struct_id as id, psevdonim as title
        FROM art_struct
        WHERE control = 'subdomen'
    ";
    $subdomens = $oDb->getRows($sql);

    $subdomensList = array();

    foreach($subdomens as $item) {
    switch ($item['title'])
    {
    case 'mebel':
    $title = 'Мебель';
    break;
    case 'svet':
    $title = 'Свет';
    break;
    case 'sanitary':
    $title = 'Сантехника';
    break;
    default:
    $title = 'Плитка';
    }
    $subdomensList[$item['id']] = $title;
    }

    //$sql = "
    //    SELECT t1.sprop4 as title, t2.psevdonim as name, t3.parent_id
    //    FROM art_struct AS t1
    //    JOIN category_psevdonims AS t2 ON t1.sprop4 = t2.name
    //    JOIN art_struct AS t3 ON t1.parent_id = t3.struct_id
    //    WHERE t1.sprop4 <> ''
    //    AND t3.parent_id > 0
    //    AND t1.is_deleted = '0'
    //    AND t1.is_hidden = '0'
    //    GROUP BY t1.sprop4, t3.parent_id
    //    ORDER BY t2.psevdonim
    //";
    //$groups = $oDb->getRows($sql);

    $sql = "
        SELECT
        a1.parent_id AS subdomen,
        cp.psevdonim AS group_name, a2.sprop4 AS group_title,
        a1.psevdonim AS vendor_name, a1.sprop3 AS vendor_title
        FROM art_struct AS a1
        JOIN art_struct AS a2
        ON a2.parent_id = a1.struct_id
        LEFT JOIN category_psevdonims AS cp
        ON cp.name = a2.sprop4
        WHERE a1.control = 'collection'
        AND a1.is_deleted = '0'
        AND a1.is_hidden = '0'
        AND a2.sprop1 != '1'
        AND a2.sprop4 != ''
        GROUP BY vendor_name, group_name
        ORDER BY group_name ASC
    ";
    $vendors = $oDb->getRows($sql);

    //$sql = "
    //    SELECT
    //    t1.psevdonim AS col_name, t1.name AS col_title,
    //    t2.psevdonim AS vendor_name,
    //    cp.psevdonim AS group_name,
    //    cp.name AS group_title
    //    FROM collection_psevdonims as t1
    //    JOIN art_struct AS t2
    //    ON (t2.sprop13 = t1.name AND t2.is_deleted = 0)
    //    LEFT JOIN category_psevdonims AS cp
    //    ON cp.name = t2.sprop4
    //    WHERE t2.sprop4 != ''
    //    GROUP BY col_title, vendor_name, group_name
    //    ORDER BY t2.sprop4 ASC
    //";

    $sql = "
        SELECT
        t1.psevdonim AS col_name, t1.name AS col_title,
        t2.psevdonim AS vendor_name, t2.sprop3 AS vendor_title,
        t2.sprop4 AS group_title
        FROM collection_psevdonims as t1
        JOIN art_struct as t2
        ON (t2.sprop13 = t1.name AND t2.is_deleted = 0)
        WHERE t2.sprop4 != ''
        GROUP BY vendor_name, group_title
        ORDER BY group_title ASC
    ";
    $colls = $oDb->getRows($sql);

    $result = array();

    foreach($colls as $col) {
        foreach($vendors as $vendor) {
            $result['subdomen'][$vendor['subdomen']]['title'] = $subdomensList[$vendor['subdomen']];
            $result['subdomen'][$vendor['subdomen']]['bind'][$vendor['group_name']] = $vendor['group_name'];
            $result['subdomen'][$vendor['subdomen']]['bind'][$vendor['vendor_name']] = $vendor['vendor_name'];
            $result['subdomen'][$vendor['subdomen']]['bind'][$col['col_name']] = $col['col_name'];

            $result['group'][$vendor['group_name']]['title'] = $vendor['group_title'];
            $result['group'][$vendor['group_name']]['bind'][$vendor['subdomen']] = $vendor['subdomen'];
            $result['group'][$vendor['group_name']]['bind'][$vendor['vendor_name']] = $vendor['vendor_name'];
            $result['group'][$vendor['group_name']]['bind'][$col['col_name']] = $col['col_name'];

            $result['vendor'][$vendor['vendor_name']]['title'] = $vendor['vendor_title'];
            $result['vendor'][$vendor['vendor_name']]['bind'][$vendor['subdomen']] = $vendor['subdomen'];
            $result['vendor'][$vendor['vendor_name']]['bind'][$vendor['group_name']] = $vendor['group_name'];
            $result['vendor'][$vendor['vendor_name']]['bind'][$col['col_name']] = $col['col_name'];

            $result['col'][$col['col_name']]['title'] = $col['col_title'];

            if($vendor['group_title'] == $col['group_title']) {
            $result['col'][$col['col_name']]['bind'][$vendor['group_name']] = $vendor['group_name'];

            if($vendor['vendor_name'] == $col['vendor_name']) {
            $result['col'][$col['col_name']]['bind'][$vendor['subdomen']] = $vendor['subdomen'];
        }
    }

    $result['col'][$col['col_name']]['bind'][$col['vendor_name']] = $col['vendor_name'];

    }
    }

    ksort($result['vendor']);


    //echo '<pre>'; print_r($_GET); echo '</pre>';
    //echo '<pre>'; print_r($result['vendor']); echo '</pre>';
    //die();


{/php}

<nav id="panel-taxonomy-1" class="navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9" style="padding-right: 50px;">

                <div id="filters">
                    <div class="row">

                        <form action="/picking_filter/" method="get" onsubmit="return check_form(this);">

                            <div class="row">
                                <div class="col-md-3">

                                    <div class="form-group">
                                        <select name="category_filter" id="category" class="form-control"
                                                style="background:none;">
                                            <option value="">Выбрать категорию</option>
                                            {php}

                                                foreach($result['subdomen'] as $key => $item) {
                                                $hasSelected = @$_GET['category_filter'] == $key ? 'selected' : '';
                                                $className = implode(' ', $item['bind']);
                                                echo "<option value='$key' class=' $className' $hasSelected>{$item['title']}</option>";
                                                }
                                            {/php}
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                </div>
                                <div class="col-md-3">


                                    <div class="form-group">
                                        <select name="vendor_filter" id="vendor" class="form-control">
                                            <option value="">Выбрать бренд</option>
                                            {php}
                                                foreach($result['vendor'] as $key => $item) {
                                                $hasSelected = @$_GET['vendor_filter'] == $key ? 'selected' : '';
                                                $className = implode(' ', $item['bind']);
                                                echo "<option value='$key' class=' $className' $hasSelected>{$item['title']}</option>";
                                                }
                                            {/php}
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <div class="form-group">
                                        <select name="col_filter" id="col" class="form-control">
                                            <option value="">Выбрать коллекцию</option>
                                            {php}
                                                foreach($result['col'] as $key => $item) {
                                                $hasSelected = @$_GET['col_filter'] == $key ? 'selected' : '';
                                                $className = implode(' ', $item['bind']);
                                                echo "<option value='$key' class=' $className' $hasSelected>{$item['title']}</option>";
                                                }
                                            {/php}
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                </div>
                                <div class="col-md-3">

                                    <div class="form-group">
                                        <select name="group_filter" id="group" class="form-control">
                                            <option value="">Товарная группа</option>
                                            {php}
                                                foreach($result['group'] as $key => $item) {
                                                $hasSelected = @$_GET['group_filter'] == $key ? 'selected' : '';
                                                $className = implode(' ', $item['bind']);
                                                echo "<option value='$key' class=' $className' $hasSelected>{$item['title']}</option>";
                                                }
                                            {/php}
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>


                                </div>
                                <div class="col-md-3">

                                    <button class="btn btn-primary btn-block">Применить</button>

                                    <button type="button" class="btn btn-default btn-block" onclick="resetFilter();">✕ Очистить</button>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>


            </div>
            <div class="col-md-3">
                <div class="row">
                    <div id="search-navigation">

                        <form method=get action='{$smarty.get.global.root}/search/'>

                            <div class="input-group">
                                <input placeholder="{$smarty.get.query|default:$smarty.const.S_SEARCH}" type='text'
                                       name='query' id='searchtext' title='{$smarty.const.S_SEARCH}'
                                       class="form-control"/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>


                        <p>
                            <a href='{$smarty.get.global.root}/saved_items/'>
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                {$smarty.const.S_FAVORITES}
                            </a>
                        </p>

                        <p>
                            <a href='{$smarty.get.global.root}/viewed/'>
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                {$smarty.const.S_VIEWED_PRODUCTS}
                            </a>
                        </p>
                        <!-- /.input-group -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>