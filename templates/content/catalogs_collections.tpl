<div class="b">
	{foreach from=$items item=i}
        {foreach from = $i.files item = f}
                <a href="{$smarty.get.global.root}/upload/collections/{$f.file}" target="_blank">
                    {include_php
                    file="master/img.inc.php"
                    path="/upload/collections"
                    img=$f.image
                    tpl_name="block/img.tpl"
                    alt=$i.name
                    width="62"
                    height="78"
                    no_img = "/images/site/no_image_collection_main.jpg"}
                </a>
        {/foreach}
	{/foreach}
</div>
