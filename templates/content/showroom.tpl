<div id="container">
    <h1>{$smarty.get.global.struct.1.name}</h1>

    {include_php file="content/lister.inc.php" tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}

    <div class="showroom_carousel">
        <div class="showroomImgsWrapper clearfix">
            <div id="showroomImgs">
                {foreach from=$items.0.images item=i name=f}
                    {*{$i|@print_r}*}
                    <a class="fancybox-thumb" rel="fancybox-thumb"
                       href="{$smarty.get.global.root}/upload/designers_fullscreen/{$i.image_name}"
                       id="picture_{$smarty.foreach.f.index}">
                        <div class="zoom"><!--  --></div>
                        <img src="{$smarty.get.global.root}/upload/designers_big/{$i.image_name}" width="950" alt="{$i.image_name}" title="{$i.image_name}"/>
                    </a>
                {/foreach}
            </div>
            <a class="prev" id="showroom_prev" href="#"><i class="fa fa-chevron-circle-left"></i></a>
            <a class="next" id="showroom_next" href="#"><i class="fa fa-chevron-circle-right"></i></a>
        </div>

        <div id="showroomThumbs">
            {foreach from=$items.0.images item=i name=j}
                <img src="{$smarty.get.global.root}/upload/designers/{$i.image_name}" width="116" height="80" alt="{$i.image_name}" title="{$i.image_name}"/>
            {/foreach}
        </div>
    </div>
    {$items.0.content|content}

    {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}
    {include_php file="content/lister.inc.php" tpl_name="content/destinations.tpl" plevel=0 cond="as1.control='subdomen' AND as1.level=1"}
</div>
<div id="showroom_dots" style="display: none;">
    {foreach from=$items.0.images item=i name=f}
        <div class="i_{$smarty.foreach.f.index}">
            <div data-interaction="hover" style="width: 100%; height: 100%"
                 class="ipicture ipicture_tmp__{$smarty.foreach.f.index}">
                <div class="ip_slide_tmp_" style="width: 100%; height: 100%">
                    {foreach from=$i.dots item=d}
                        <div class="ip_tooltip_tmp_ ip_img32_tmp_" style="top: {$d.y_pct}%; left: {$d.x_pct}%;"
                             data-button="moreblack" data-tooltipbg="bgblack" data-round="roundBgW"
                             data-animationtype="ltr-slide">
                            <p><a href="{$d.link}">{$d.description}</a></p>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    {/foreach}
</div>