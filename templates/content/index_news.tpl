<div id="news" class="well">
    <h4>{$smarty.const.S_LAST_NEWS}</h4>

    <div class="row">
        <div class="col-lg-12">
            <ul class="list-unstyled">

                {foreach from=$items item=i name=n}
                    {if $smarty.foreach.n.iteration < 3}
                        <li>
                        {if $smarty.foreach.n.iteration eq 2}
                            {*<p>{$i.name|strip_tags}... <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/"><em>подробнее</em></a></p>*}
                            <p>{$i.content|content|strip_tags|truncate:90} <a class="link" href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/"><em>подробнее</em></a></p>
                        {else}
                            <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name|strip_tags}</a>
                        {/if}
                        </li>
                    {/if}
                {/foreach}

                {*<li>*}
                    {*<a href="/news/Anons_kollekcii_stocco_v_salone_Andegraund/">Анонс коллекций итальянской фабрики Stocco в салоне*}
                        {*Андерграунд</a>*}
                {*</li>*}
                {*<li>*}
                    {*<p>Анонс коллекций итальянской фабрики Stocco в... <a*}
                                {*href="/news/SICIS_NOVOSTI_IZ_CERSAIE_2015_POLNAJa_VERSIJa_ChAST_2/"><em>подробнее</em></a></p>*}
                {*</li>*}
            </ul>
            <a href="{$smarty.get.global.root}/news/"><em>{$smarty.const.S_READ_ALL_NEWS}</em></a>
        </div>
    </div>
    <!-- /.row -->
</div>

{*<div class="newsBlockInner">*}
{*<a class="newsBlockInner_link" href="{$smarty.get.global.root}/news/">{$smarty.const.S_LAST_NEWS}</a> <a href="/rss/news" border="0"><img href="/images/rss.png" align="absmiddle"/></a>*}
{*{foreach from=$items item=i name=n}*}
{*{if $smarty.foreach.n.iteration < 3}*}
{*<div class="newsEl clearfix{if $smarty.foreach.n.iteration eq 2} last{/if}">*}
{*<div class="n_dt"><span>{$i.created|date_format:"%d/%m"}</span></div>*}
{*<div class="n_text">*}
{*<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name|strip_tags}</a>*}
{*<p>{$i.content|content|strip_tags|truncate:90}</p>*}
{*</div>*}
{*</div>*}
{*{/if}*}
{*{/foreach}*}
{*<a href="{$smarty.get.global.root}/news/" class="more">{$smarty.const.S_READ_ALL_NEWS}</a>*}
{*</div>*}