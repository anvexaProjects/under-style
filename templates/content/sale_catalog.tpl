{*<pre>*}
{*{$smarty.get.global|@print_r}*}
{*</pre>*}



<div id="groups" class="row">
    {*{assign var="struct_id" value=$smarty.get.global.struct.1.struct_id}*}
    {assign var="psevdonim" value=$smarty.get.global.struct.2.path}
    {*{$smarty.get.global.struct.2.path|@print_r}*}
    {php}

        $oDb = &Database::get();

        $psevdonim = $this->get_template_vars('psevdonim');

        $data = $oDb->getRows("
            SELECT DISTINCT t1.sprop4  AS group_title, cp.psevdonim AS group_name
            FROM art_struct AS t1
            LEFT JOIN category_psevdonims AS cp ON cp.name = t1.sprop4
            WHERE t1.parent_id IN (
                SELECT t2.struct_id
                FROM art_struct AS t1
                LEFT JOIN art_struct AS t2 ON t2.parent_id = t1.struct_id AND t2.is_deleted = 0
                WHERE t1.struct_id IN (
                    SELECT t1.struct_id
                    FROM art_struct AS t1
                    WHERE t1.psevdonim = '$psevdonim'
                )
            )
            AND t1.sprop4 != ''
            AND t1.sprop13 != ''
            AND t1.is_deleted = '0'
            AND t1.is_hidden = '0'
            AND t1.sprop10 = '1'
            AND t1.sprop1 != '1'
        ");

        if(!empty($data)) {

        foreach ($data as $item) {
        $name = $item['group_name'];
        $text = $item['group_title'];
        echo "
        <div class='col-md-3'>
            <article>
                <h4>
                    <a href='$name/'>
                        <span>$text</span>
                    </a>
                </h4>
            </article>
        </div>
        ";
        }

        }

    {/php}
</div>
