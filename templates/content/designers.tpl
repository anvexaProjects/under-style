<div id="cont">
    <div class="title clearfix">
        <h1>{$smarty.get.global.struct.1.name}</h1>

        <div class="path" style="">{include_php file="content/lister.inc.php"
            tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}
        </div>
    </div>
    <div class="mainCont clearfix">

        <div class="sortBlock">
            {$smarty.const.S_SORT}&nbsp;&nbsp;&nbsp; {if $smarty.get.sort}
            <a
                    href="{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/">
                {else}
                <span>{/if}{$smarty.const.S_ALL}{if $smarty.get.sort}

            </a>
            {else}
            </span>
            {/if} &nbsp;&nbsp; {if !$smarty.get.sort}
            <a
                    href="{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?sort=1">
                {else}
                <span>{/if}{$smarty.const.S_BY_ALPHABET}{if !$smarty.get.sort}

            </a>
            {else}
            </span>
            {/if}
        </div>

        {*<div id="designers{if $smarty.foreach.f.iteration%5 eq 0} last{/if}">*}
        <div id="products">
            {foreach from=$items item=i name=f}
                <div class="col-md-3 post-box">
                    <div class="thumbnail box-shadow">

                        {include_php
                        file     = "master/img.inc.php"
                        path     = "/upload/article"
                        img      = $i.img1
                        tpl_name = "block/img.tpl"
                        alt      = $i.name
                        no_img   = "/images/site/empty_designers.phg"
                        }

                        <div class="caption">
                            <h3>
                                <a href="{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$i.path}/">{$i.name}</a>
                            </h3>

                            <p>
                                <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$smarty.const.S_PORTFOLIO}</a>
                            </p>

                            {$smarty.const.S_PROJECTS_ON_SITE}
                            {include_php
                            file     = "content/lister.inc.php"
                            tpl_name = "block/projects_counter.tpl"
                            plevel   = 1
                            parent   = $i.path
                            fields   = "as1.struct_id"
                            }
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>

    </div>
    {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}
</div>
