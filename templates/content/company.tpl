<div id="cont">
    <div class="title clearfix">
        <h1>{$smarty.get.global.struct.1.name}</h1>

        <div class="path" style="">
            {include_php file="content/lister.inc.php"
            tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}
        </div>
    </div>
    <div class="mainCont clearfix">
        {if $items.0.img1}
            <div style="float:left; margin-right:10px; margin-bottom:10px">
                {include_php
                file="master/img.inc.php"
                path="/upload/article_big"
                img=$items.0.img1
                tpl_name="block/img.tpl"
                alt=$i.name
                width="370"
                height="210"
                }
            </div>
        {/if}
        {if $items.0.img2}
            <p>{include_php file="master/img.inc.php" path="/upload/article"
                img=$items.0.img2 tpl_name="block/img.tpl" alt=$i.name}</p>
            <br/>
        {/if}
        {$items.0.content|content}
        <p>
            <a href="{$smarty.get.global.root}/partnership/catalogs/#{$items.0.name}">{$smarty.const.S_MANUFACTURER_COLLECTIONS}</a>
        </p>
        <br/>
        <br/>
        {$items.0.content2|content}
    </div>
    {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}
    {include_php file="content/lister.inc.php"
    tpl_name="content/destinations.tpl" plevel=0
    cond="as1.control='subdomen' AND as1.level=1"}

</div>
