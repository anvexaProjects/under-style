{assign var=brand value=$smarty.get.brand}
    <div id="cont">
		<div class="title clearfix">
			<h1>{$brand}</h1>
			<div class="path" style="">
        	{include_php file="content/lister.inc.php" tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}
        </div>
		</div>
		<div class="mainCont clearfix">
			<div class="lSide">
				{include file="content/sub_menu_catalog.tpl"}
			</div>
			<div class="rSide">
				{include_php
					file="content/lister.inc.php"
					tpl_name="content/brand_descr.tpl"
					plevel=0
					cond="as1.control='company' AND a1.name='$brand'"
				}

				<div id="products" class="productsBlock clearfix">
					{include_php
	file="content/imglister.inc.php"
	tpl_name="content/prod_item_wrapper.tpl" plevel=0
	cond="as1.control='prod' AND as1.sprop3='$brand'"}
				</div>
			</div>
		</div>
    {*<DIV id="share_block">
		{include file="block/share_block.tpl"}
	</DIV>*}
		{include_php file="content/lister.inc.php" tpl_name="content/destinations.tpl" plevel=0 cond="as1.control='subdomen' AND as1.level=1"}

	</div>