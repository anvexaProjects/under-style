<div id="cont">
	<div class="title clearfix">
		<h1>{$smarty.get.global.struct.1.name}</h1>
		<div class="path" style="">{include_php file="content/lister.inc.php"
			tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}</div>
	</div>
	<div class="mainCont clearfix">



		<div class="designer">
			{if $items.0.img1}
			<div class="img">
				<img
					src="{$smarty.get.global.root}/upload/article_big/{$items.0.img1}"
					width="210" height="140" alt="" />
			</div>
			{/if}
			<h3>{$items.0.name}</h3>
			{$items.0.content|content}
			<div class="clear">
				<!--  -->
			</div>
			<p>{$items.0.content2|content}</p>
		</div>

		<div class="designPrj clearfix">{include_php
			file="content/imglister.inc.php"
			tpl_name="content/designer_projects.tpl" plevel=1
			parent=$items.0.path}</div>

	</div>
	{*<DIV id="share_block">
		{include file="block/share_block.tpl"}
	</DIV>*}
</div>
