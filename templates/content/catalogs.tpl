<div id="cont">
	<div class="title clearfix">
		<h1>{$smarty.get.global.struct.1.name}</h1>
		<div class="path" style="">{include_php file="content/lister.inc.php"
			tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}</div>
	</div>
	<div class="mainCont clearfix">
			<div class="letters">
				{foreach from=$vendors item=letter name=f}
				<a class="btn btn-default btn-lg"  href="javascript:;">{$letter.0|upper|truncate:1:"":true}</a>
				{/foreach}
			</div>
            <br>
			{foreach from=$vendors item=v name=f}
			{foreach from=$v item=i name=n}
			<div class="catEl clearfix" l="{$i|truncate:1:"":true}">
				<a name="{$i}" href="{$smarty.get.global.root}/manufacturer/?brand={$i}">
                    {assign var = "escaped_i" value=$i|escape}
					{include_php
					file="content/lister.inc.php"
                    tpl_name="content/catalogs_brand.tpl"
					plevel=0 cond="as1.control='company' AND a1.name='$escaped_i'"
                    }
				</a>
				{include_php
				file="content/filelister.inc.php"
				tpl_name="content/catalogs_collections.tpl" plevel=0
				cond="as1.control='collection' AND as1.sprop3='$escaped_i'"}</div>
			{/foreach}
			{/foreach}

	</div>
	{*<DIV id="share_block">
		{include file="block/share_block.tpl"}
	</DIV>*}
</div>