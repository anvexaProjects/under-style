<div id="content">

    <h1>{$smarty.get.global.struct.1.name}</h1>
    {include_php file="content/lister.inc.php" tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}


    {*<div id="designers{if $smarty.foreach.f.iteration%5 eq 0} last{/if}">*}
    <div id="products" class="row">
        {foreach from=$items item=i name=f}
            <div class="col-md-3 post-box">
                <div class="thumbnail box-shadow">
                    {include_php
                    file     = "master/img.inc.php"
                    path     = "/upload/designers"
                    img      = $i.images.0.image_name
                    tpl_name = "block/img.tpl"
                    alt      = $i.name
                    width    = "165"
                    height   = "102"
                    no_img   = "/images/site/empty_designer_progect.phg"
                    }
                    <div class="caption">
                            <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/" class="disItem">
                                {$i.name}
                            </a>

                    </div>
                </div>
            </div>
        {/foreach}
    </div>

    {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}
</div>
