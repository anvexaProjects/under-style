<script type="text/javascript">
{literal}
$(document).ready(function(){
	$(function () {
		$('#feed_all').on('click', function () {
			$('.feed').prop('checked', this.checked);
		});
	});
})
{/literal}
</script>
<div id="cont">
	<div class="title clearfix">
		<h1>{$smarty.get.global.struct.1.name}</h1>
		<div class="path" style="">{include_php file="content/lister.inc.php"
			tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}</div>
	</div>
	<div class="mainCont clearfix">
		<div class="subscribeForm">
			{if $smarty.get.s eq 1}<h3>{$smarty.const.S_THANKS_FOR_SUBSCRIPTION}</h3>{/if}
			<form {$form_data.attributes}>
				{$form_data.javascript}
				{$form_data.hidden}
				{include file="ablock/error_block.tpl"}
				<table>
					<tbody><tr>
						<td>{$smarty.const.S_ENTER_NAME}</td>
						<td><div>{$form_data.name.html}</div></td>
					</tr>
					<tr>
						<td>{$smarty.const.S_ENTER_EMAIL}</td>
						<td><div>{$form_data.email.html}</div></td>
					</tr>
				</tbody></table>
				<div class="hr"><!--  --></div>
				<h4>{$smarty.const.S_CHOOSE_NEWS}</h4>
				{foreach from=$form_data.list item=i}
				<p>{$i.html}</p>
				{/foreach}
				<input type="checkbox" name="" value="" id="feed_all"><label>{$smarty.const.S_ALL2}</label>
				<!--label class="other"><input type="checkbox" name="" value=""></label><input type="text" id="" value="Другое" class="inp"-->
				<div class="hr"><!--  --></div>
				<h4>{$smarty.const.S_HOW_DO_YOU_KNOW}</h4>
				{foreach from=$form_data.source item=i}
				<p>{$i.html}</p>
				{/foreach}
				<label class="other">{$smarty.const.S_OTHER} &nbsp;&nbsp;</label><input type="text" id="" value="" name="source_other" class="inp">
				<div class="hr"><!--  --></div>
				<input type="submit" value="{$smarty.const.S_SUBSCRIBE}" class="btn">
			</form>
		</div>
	</div>

    {include_php file="content/lister.inc.php"
    tpl_name="content/destinations.tpl" plevel=0
    cond="as1.control='subdomen' AND as1.level=1"}
</div>