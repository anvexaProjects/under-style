<div id="categories" class="container-fluid">
    <div class="row">

        {if $smarty.get.global.struct.1.path neq 'index'}
            <h2 style="text-align: center;">
                {if $smarty.get.global.struct.1.control neq 'subdomen'}
                    {$smarty.const.S_DESTINATIONS}
                {else}
                    {$smarty.const.S_OTHER_DESTINATIONS}
                {/if}
            </h2>
            <br>
            <br>
        {else}

        {/if}

        {foreach from=$items item=i name=foo}
            {*{if $smarty.get.global.struct.1.path neq $i.path}*}
            {*{$smarty.foreach.foo.total}*}
            {*<div class="col-md-3 {if ($smarty.foreach.foo.iteration == 2) && ($smarty.get.global.struct.1.path neq 'index')}col-md-offset-2{/if}">*}
            <div class="col-md-3">
                <div class="row">
                    <div class="thumbnail wrap">
                        {*{include_php file="master/img.inc.php" path="/upload/article" img=$i.img1 tpl_name="block/img.tpl" alt=$i.name width="342" height="166"}*}

                        <div class="hi-icon-effect-5 hi-icon-effect-5a" style="margin: auto;">
                            <a title="{$i.name}" href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/" class="hi-icon hi-icon-{$i.path}"></a>

                            <div class="thumbnail-border">
                                <a class="caption" href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {*{/if}*}
        {/foreach}
    </div>
</div>


{*{if $smarty.get.global.struct.1.path neq 'index'}*}

{*<h2>*}
{*{if $smarty.get.global.struct.1.control neq 'subdomen'}*}
{*{$smarty.const.S_DESTINATIONS}*}
{*{else}*}
{*{$smarty.const.S_OTHER_DESTINATIONS}*}
{*{/if}*}
{*</h2>*}

{*{else}*}

{*<div class="rubBlock floatl clearfix">*}
{*{/if}*}
{*{foreach from=$items item=i name=f}*}
{*{if $smarty.get.global.struct.1.path neq $i.path}*}
{*<div class="rubEl{if $smarty.foreach.f.iteration%2 eq 0} r_right{if $smarty.foreach.f.last} last{/if}{/if}">*}
{*<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/?gate=1">*}
{*{include_php file="master/img.inc.php" path="/upload/article" img=$i.img1 tpl_name="block/img.tpl" alt=$i.name width="342" height="166"}*}
{*<span><font class="i">{$i.name}</font></span>*}
{*</a>*}
{*</div>*}
{*{/if}*}
{*{/foreach}*}
{*</div>*}
