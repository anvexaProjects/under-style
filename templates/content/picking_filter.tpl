{include file="content/picking.tpl"}

{*<h2>{$smarty.const.S_RESULT}</h2>*}
{*<br/><br/>*}
{*{if $category_filter}*}
    {*<p><strong>{$smarty.const.S_CAT}</strong> {$category_filter}</p>*}
{*{/if}*}
{*{if $vendor_filter}*}
    {*<p><strong>{$smarty.const.S_MANUFACTURER}</strong> {$vendor_filter}</p>*}
{*{/if}*}
{*{if $col_filter}*}
    {*<p><strong>{$smarty.const.S_COLLECTION}</strong> {$col_filter}</p>*}
{*{/if}*}
{*{if $smarty.post.group_filter}*}
    {*<p><strong>{$smarty.const.S_GROUP}</strong> {$smarty.post.group_filter}</p>*}
{*{/if}*}
{*{if $smarty.post.place_filter}*}
    {*<p><strong>{$smarty.const.S_PLACE}</strong> {$smarty.post.place_filter}</p>*}
{*{/if}*}
{*<br/><br/>*}

{*<div class="col-md-12" >*}
<div>
    {include file="block/core_pager.tpl"}
</div>
<br/><br/>
    {if $items}

    <div id="products" class="row">
        {foreach from=$items item=i name=f}
            {assign var=vendor value=$i.sprop3|escape}
            {assign var=parent value=$i.parent_id}
            {*{$i|@print_r}*}

            {if  strripos($i.name, 'SEO ')=== false }
                <div class="col-md-3 post-box"
                     factory="{$i.sprop3|replace:' ':'_'}"
                     place="{$i.sprop6|replace:' ':'_'}"
                     rub="{$i.sprop4|replace:' ':'_'}"
                     collection="{$i.sprop13|replace:' ':'_'}"
                        >

                    {if $i.sprop7}
                        <span class="label label-default">{$smarty.const.S_AVAILABLE}</span>
                    {/if}
                    {if $i.sprop8}
                        <span class="label label-primary">{$smarty.const.S_NOT_AVAILABLE}</span>
                    {/if}
                    {if $i.sprop9}
                        <p class="label label-warning">
                            {$smarty.const.S_SPEC_OFFER}<br>
                            {assign var="timer" value=$i.end_time}
                        </p>
                    {/if}
                    {if $i.sprop2}
                        {*<div class="flag_new">*}
                        {*<img src="{$smarty.get.global.root}/images/site/knopka.png"/>*}
                        {*</div>*}
                        <span class="label label-danger">Новый!</span>
                    {/if}

                    <div class="thumbnail box-shadow">

                        <img class="item-image" src="/upload/article/{$i.image_name}" alt="{$i.title}" title="{$i.title}">



                        <div class="caption">
                            <h3>
                                <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>
                            </h3>
                            {if $i.sprop3}
                                <p>
                                    {$smarty.const.S_MANUFACTURER}
                                    <span class="b">{$i.sprop3}</span>
                                </p>
                            {/if}
                            {if $i.sprop5}
                                <p>
                                    {$smarty.const.S_COUNTRY}
                                    <span class="b">{$i.sprop5}</span>
                                </p>
                            {/if}
                            {if $i.sprop13}
                                <p>
                                    {$smarty.const.S_COLLECTION}
                                    <span class="b">{$i.sprop13}</span>
                                </p>
                            {/if}
                            {if $items.0.created}
                                <p>{$items.0.created|date_format:"%m%y"}</p>
                            {/if}
                            <p class="price_prod{if $i.sprop9} cancel{/if}">{$i.sprop11}</p>
                            {if $i.sprop9}
                                <div class="price_prod"><span>{$i.sprop12}</span></div>
                                <div class="counter">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>
                            {/if}
                        </div>
                    </div>


                    {*<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/" class="prodItem">*}
                    {*<div class="img">*}
                    {*<img src="/upload/article/{$i.image_name}">*}
                    {*</div>*}
                    {*{if $i.sprop7}*}
                    {*<div class="flag_av">{$smarty.const.S_AVAILABLE}</div>*}
                    {*{/if}*}
                    {*{if $i.sprop8}*}
                    {*<div class="flag">{$smarty.const.S_NOT_AVAILABLE}</div>*}
                    {*{/if}*}
                    {*{if $i.sprop9}*}
                    {*<div class="flag_sp">{$smarty.const.S_SPEC_OFFER}</div>*}
                    {*{assign var="timer" value=$i.end_time}*}
                    {*{/if}*}
                    {*{if $i.sprop2}*}
                    {*<div class="flag_new"><img src="{$smarty.get.global.root}/images/site/knopka.png"/></div>*}
                    {*{/if}*}
                    {*<div class="h3">{$i.name}</div>*}
                    {*{if $i.sprop3}*}
                    {*<p>*}
                    {*{$smarty.const.S_MANUFACTURER}*}
                    {*<span class="b">{$i.sprop3}</span>*}
                    {*</p>*}
                    {*{/if}*}
                    {*{if $i.sprop5}*}
                    {*<p>*}
                    {*{$smarty.const.S_COUNTRY}*}
                    {*<span class="b">{$i.sprop5}</span>*}
                    {*</p>*}
                    {*{/if}*}
                    {*{if $i.sprop13}*}
                    {*<p>*}
                    {*{$smarty.const.S_COLLECTION}*}
                    {*<span class="b">{$i.sprop13}</span>*}
                    {*</p>*}
                    {*{/if}*}
                    {*{if $items.0.created}*}
                    {*<p>{$items.0.created|date_format:"%m%y"}</p>*}
                    {*{/if}*}
                    {*<p class="price_prod{if $i.sprop9} cancel{/if}">{$i.sprop11}</p>*}
                    {*{if $i.sprop9}*}
                    {*<div class="price_prod"><span>{$i.sprop12}</span></div>*}
                    {*<div class="counter">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>*}
                    {*{/if}*}
                    {*</a>*}


                </div>
            {/if}
            {if $smarty.get.global.struct.1.control eq 'manufacturer' &&
            $smarty.foreach.f.iteration %4 eq 0}
                <div class="clear"><!--  --></div>
            {/if}
        {/foreach}
    </div>
    {else}
        <p><strong>{$smarty.const.S_NO_RESULT}</strong></p>
        <br stle="clear: both">
    {/if}


<div style="margin-bottom: 15px;">
    <div class="paging clearfix">
        <div class="pages floatl" id="paging_block"></div>
        <div class="pages_nav floatr" id="nav_pager"></div>
    </div>
</div>

<div class="col-md-12">
    {include file="block/core_pager.tpl"}
</div>