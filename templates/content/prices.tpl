<div class="submenu">
    {if $smarty.get.global.struct.1.control eq 'wholesale_buyers'}
	<ul>
		<li><a href="" style="cursor: default;" onclick="return false;">{$smarty.const.S_PRICE}</a>
                <ul>
                    {foreach from=$items item=i name=f}
                        <li>
                            <a href="" class="price">{$i.name}</a>
                        </li>
                    {/foreach}
                </ul>
		</li>
        {else}
            {include_php file = "content/lister.inc.php"
                tpl_name = "content/price_switcher.tpl"
                plevel = 0
                cond = "as1.control='wholesale_buyers'"
                fields = "a1.name, a1.path, as1.sprop1"
            }
        {/if}

	</ul>
    <li class="last">
        <a href="{$smarty.get.global.root}/admin/exit.php">
            {$smarty.const.S_EXIT}
        </a>
    </li>
</div>