{assign var=struct_id value=$smarty.get.global.struct.1.struct_id}
{assign var=path  value=$smarty.get.global.struct.1.path}
{assign var= "available" value=$items.0.sprop1}
{assign var= "catalog"   value=$items.0.sprop2}
{assign var= "hold"      value=$items.0.sprop3}
{assign var= "offers"    value=$items.0.sprop4}
{assign var= "sale"      value=$items.0.sprop5}
{assign var= "all"       value=$items.0.sprop6}
{assign var= "archives"  value=$items.0.sprop7}
{php}
if (isset($GLOBALS['hide_sub_menu'])) {
    return;
}
{/php}
<div>
<ul  class="nav">
{if $available}
<li {if $smarty.get.property eq 'available' && !$smarty.get.gate}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=available">
{$smarty.const.S_IS_AVAILABLE}
</a>
{if $smarty.get.property eq 'available' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $catalog || $hold}
<li {if $smarty.get.property eq 'catalog' && !$smarty.get.gate}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=catalog">
{$smarty.const.S_CATALOG_PRODUCTS}
</a>
{if $smarty.get.property eq 'catalog' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $hold999}
<li {if $smarty.get.property eq 'hold' && !$smarty.get.gate}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=hold">
{$smarty.const.S_ON_HOLD}
</a>
{if $smarty.get.property eq 'hold' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $offers}
<li {if $smarty.get.property eq 'offers' && !$smarty.get.gate}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=offers">
{$smarty.const.S_SPECIAL_OFERT}
</a>
{if $smarty.get.property eq 'offers' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $sale}
<li {if $smarty.get.property eq 'sale' && !$smarty.get.gate}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=sale">
{$smarty.const.S_SALE}
</a>
{if $smarty.get.property eq 'sale' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $all && !$hold}
<li {if !$smarty.get.property && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod'}class="cur"{else}class="head"{/if}>
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/">
{$smarty.const.S_ALL_PRODUCTS}
</a>
{if !$smarty.get.property && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
{if $archives || $hold}
<li class="last{if $smarty.get.property eq 'archives' && !$smarty.get.gate} cur{else} head{/if}">
<a href="{if $smarty.get.global.struct.1.control eq 'prod'}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.3.path}/{$smarty.get.global.struct.3.path}{else}{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}{/if}/?property=archives">
{$smarty.const.S_ARCHIVES}
</a>
{if $smarty.get.property eq 'archives' && !$smarty.get.gate && $smarty.get.global.struct.1.control neq 'prod' && $smarty.get.global.struct.1.control neq 'viewed' && $smarty.get.global.struct.1.control neq 'saved_items'}
{include file = "content/sub_menu_filters.tpl"}
{/if}
</li>
{/if}
</ul>
</div>