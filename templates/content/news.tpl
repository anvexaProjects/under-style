<div id="news" class="row content">
    <div class="col-lg-12">
        <h2>{$smarty.get.global.struct.1.name}</h2>
        {*<a href="/rss/news" style="margin-left: 10px" title="RSS-лента"><img src="/images/rss.png" height="20" /></a>*}
        {include_php file="content/lister.inc.php" tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}

        {*<form class="form-inline">*}
        {*<div class="form-group">*}
        {*<label for="exampleInputName2">Name</label>*}
        {*<input type="text" id="subscribe_name" placeholder="Имя" class="form-control"/>*}
        {*</div>*}
        {*<div class="form-group">*}
        {*<label for="exampleInputEmail2">Email</label>*}
        {*<input type="email" id="subscribe_email" placeholder="Email" class="form-control"/>*}
        {*</div>*}
        {*<a title="Подписаться на новости" style="cursor: pointer; "><img src="/images/ok.png"*}
        {*id="subscribe_button"/></a>*}
        {*<button type="submit" class="btn btn-default">Send invitation</button>*}
        {*</form>*}

        {include file="block/core_pager.tpl"}

        <div class="col-lg-10">
            {foreach from=$items item=i}
                <div class="row thumbnail-new">
                    {if $i.img1}
                    <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">
                        {/if}
                        {include_php file="master/img.inc.php"
                        path="/upload/article"
                        img=$i.img1
                        tpl_name="block/img.tpl"
                        alt=$i.name
                        title=$i.name
                        no_img="/images/site/empty_news.png"
                        }
                        {if $i.img1}</a>{/if}

                    <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>

                    <p>
                        <span class="glyphicon glyphicon-time"></span> {$i.created|date_format:"%d.%m.%Y"}
                    </p>
                    <hr>
                    <p>{$i.description}</p>
                </div>
            {/foreach}

        </div>

        {include file="block/core_pager.tpl"}

        {if $smarty.get.global.struct.1.control eq 'actions' && $smarty.get.global.struct.1.path neq 'about/useful_information'}
            <p style="margin-top: 15px">
                <a href="{$smarty.get.global.root}/{include_php file = "content/lister.inc.php" tpl_name = "block/link_path.tpl" plevel = 0 cond = "as1.control='old_actions'" fields = "a1.path"}/">{$smarty.const.S_ACTIONS_ARCHIVE}</a>
            </p>
        {/if}

        {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}
        {include_php file="content/lister.inc.php"
        tpl_name="content/destinations.tpl" plevel=0
        cond="as1.control='subdomen' AND as1.level=1"}
    </div>
</div>
