{assign var=parent_id value=$smarty.get.global.struct.1.parent_id}
{assign var=id value=$smarty.get.global.struct.1.struct_id}
{include_php file="content/viwed.inc.php" id="$id"}

{include file="content/picking.tpl"}

<div id="content">

    <h1 class="page-header">{$smarty.get.global.struct.1.name}</h1>

    {include_php
    file="content/lister.inc.php"
    tpl_name="content/breadcrumbs.tpl"
    plevel=0
    cond="a1.path = '$path'"
    }


    {*<div class="col-md-3">*}
    {*{include file="content/static_menu_catalog.tpl"}*}
    {*{include file="content/sub_menu_catalog.tpl"}*}
    {*</div>*}


    {*<h2 class="page-header">{$items.0.name}</h2>*}

    <div class="row">

        <div class="col-md-8">
            {*<img class="img-responsive" src="http://placehold.it/750x500" alt="">*}
            {if $items.0.images.0.image_name}
                <a href="{$smarty.get.global.root}/upload/article_big/{$items.0.images.0.image_name}"
                   rel="prettyPhoto[{$items.0.struct_id}]"
                   class="lightbox" title="{$items.0.image_desc}">
                    <img class="img-responsive" src="{$smarty.get.global.root}/upload/article_big/{$items.0.images.0.image_name}" alt="{$items.0.name}" title="{$items.0.name}"/>
                </a>
            {else}
                Нет фото
            {/if}
        </div>

        <div class="col-md-4">
            {*<h3>Project Description</h3>*}
            {*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>*}
            {*<h3>Project Details</h3>*}
            {*<ul>*}
                {*<li>Lorem Ipsum</li>*}
                {*<li>Dolor Sit Amet</li>*}
                {*<li>Consectetur</li>*}
                {*<li>Adipiscing Elit</li>*}
            {*</ul>*}
            {include file="block/descr_feat.tpl"}


            {php}
                // -----------------MESSAGES --------------------
                include ('../pub/email_msgs.php');
            {/php}


            {if $items.0.sprop7}
                <img  src="{$smarty.get.global.root}/images/site/available.png"/>
            {/if}
            {if $items.0.sprop8}
                <img  src="{$smarty.get.global.root}/images/site/not_available.png"/>
            {/if}
            {if $items.0.sprop9}
                <img style=" margin-left:110px" src="{$smarty.get.global.root}/images/site/spec.png"/>
                {assign var="timer" value=$items.0.end_time}
                <div class="counter_prod">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>
            {/if}

            <br/><h5>{$smarty.const.S_YOU_CAN}</h5>


            {if !isset($smarty.session.saved_items) || !in_array($id, $smarty.session.saved_items)}
                <a id="save_item" item="{$id}" href="">{$smarty.const.S_ADD_FAVORITE}</a>
                <a href="javascript:void();" style="cursor:default">{$smarty.const.S_FAVORITE_ADDED}</a>
            {else}
                <a href="javascript:void();" style="cursor:default">{$smarty.const.S_FAVORITE_ADDED}</a>
            {/if}

            <div id="soc_links" style="display: block">
                <span class='st_facebook' displayText='Facebook'></span>
                <span class='st_twitter' displayText='Tweet'></span>
                {*<span class='st_googleplus_large' displayText='Google +'></span>
                <span class='st_livejournal_large' displayText='LiveJournal'></span>
                <span class='st_odnoklassniki_large' displayText='Odnoklassniki'></span>*}
                <span class='st_vkontakte' displayText='Vkontakte'></span>
                {*<span class='st_mail_ru_large' displayText='mail.ru'></span>*}
            </div>


        </div>

    </div>



    <div class="row">
        <br>

        {foreach from=$items.0.images item=j name=n}
            {if !$smarty.foreach.n.first}
                <div class="col-sm-2 col-xs-6">
                <a href="{$smarty.get.global.root}/upload/article_big/{$j.image_name}" rel="prettyPhoto[{$items.0.struct_id}]" class="lightbox" {if $smarty.foreach.n.iteration+1 %5 eq 0} class="last"{/if}>
                    <img class="img-responsive portfolio-item" src="{$smarty.get.global.root}/upload/article/{$j.image_name}" alt=""/>
                </a>
                </div>
            {/if}
        {/foreach}

    </div>


    {* hide product price *}

    {* if !$items.0.sprop10}
                        <div class="price_prod{if $items.0.sprop9} cancel{/if}">{$items.0.sprop11}</div>
          {if $items.0.sprop9}
                            <div class="price_prod"><span>{$items.0.sprop12}</span></div>
                        {/if }
                    {/if *}





    {include_php file="content/taglister.inc.php" tpl_name="content/keywords.tpl" article_id=$items.0.struct_id }


    {include_php
    file="content/imglister.inc.php"
    tpl_name="content/prod_item_wrapper.tpl"
    plevel=0
    cond="as1.parent_id='$parent_id' AND as1.struct_id !='$id'"
    }

    {include_php file="content/lister.inc.php"
    tpl_name="content/destinations.tpl" plevel=0
    cond="as1.control='subdomen' AND as1.level=1"}

</div>

