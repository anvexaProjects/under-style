<section id="stock">
    <div class="row">
        <div class="col-lg-12">
            <header>
                <h2>{$smarty.const.S_ACTUEL_ACTIONS}</h2>
                <hr class="border-dark">
            </header>
        </div>
    </div>
    <div class="row">

        {foreach from=$items item=i name=f}
            {if $smarty.foreach.f.iteration < 4}
                <div class="col-md-4 portfolio-item">
                    <div class="thumbnail">
                        <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">
                            {*<img class="img-responsive" src="http://placehold.it/700x400" alt="">*}

                            {if $i.img2}
                                {include_php
                                file="master/img.inc.php"
                                path="/upload/article_mid"
                                img=$i.img2
                                tpl_name="block/img.tpl"
                                alt=$i.name
                                title=$i.name
                                width="285"
                                height="102"
                                no_img="/images/site/empty_image.png"
                                }
                            {else}
                                {include_php
                                file="master/img.inc.php"
                                path="/upload/article_mid"
                                img=$i.img1
                                tpl_name="block/img.tpl"
                                alt=$i.name
                                title=$i.name
                                width="285"
                                height="102"
                                no_img="/images/site/empty_image.png"
                                }
                            {/if}
                            {if $i.end_time > 0 && ($i.end_time|strtotime > $smarty.now|strtotime)}
                            <div class="action-time">
                                <span class="header">Акция</span>
                                <p>{$i.created|date_format:"%d.%m.%y"} - {$i.end_time|date_format:"%d.%m.%y"}</p>
                            </div>
                            {/if}
                        </a>

                        <h3>
                            <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name|strip_tags}</a>
                        </h3>

                        <p>{$i.description|strip_tags|nl2br|truncate:160}</p>
                    </div>
                </div>
            {/if}
        {/foreach}
    </div>
</section>

{*<div class="actionsBlock">*}
{*<span class="actionsBlock_actuel_actions">{$smarty.const.S_ACTUEL_ACTIONS}</span>*}
{*<div class="actions clearfix">*}
{*{foreach from=$items item=i name=f}*}
{*{if $smarty.foreach.f.iteration < 4}*}
{*<div class="actionEl{if $smarty.foreach.f.iteration eq 3} last{/if}">*}
{*<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">*}
{**}
{*<div class="img">*}
{*<div class="brd"><!--  --></div>*}
{*{if $i.img2}*}
{*{include_php*}
{*file="master/img.inc.php"*}
{*path="/upload/article_mid"*}
{*img=$i.img2*}
{*tpl_name="block/img.tpl"*}
{*alt=$i.name width="285"*}
{*height="102"*}
{*no_img="/images/site/empty_image.png"*}
{*}*}
{*{else}*}
{*{include_php*}
{*file="master/img.inc.php"*}
{*path="/upload/article_mid"*}
{*img=$i.img1*}
{*tpl_name="block/img.tpl"*}
{*alt=$i.name width="285"*}
{*height="102"*}
{*no_img="/images/site/empty_image.png"*}
{*}*}
{*{/if}*}
{*</div>*}
{*{$i.name|strip_tags}</a>*}
{*<p>{$i.description|strip_tags|nl2br|truncate:160}</p>*}
{*<div class="cor"><!--  --></div>*}
{*</div>*}
{*{/if}*}
{*{/foreach}*}
{*</div>*}
{*</div>*}