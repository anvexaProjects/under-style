{if $smarty.get.global.struct.1.spath eq 'wholesale_buyers'}
	{assign var='location' value='salers'}
{elseif $smarty.get.global.struct.1.spath eq 'architects_designers'}
	{assign var='location' value='des_arc'}
{/if}
{foreach from=$items item=i name=f}
{foreach from=$i.images item=im name=j}
{assign var="image_title" value=$im.image_title}
<div class="catElp clearfix" price="{$i.name}">
	{include_php file="content/lister.inc.php" tpl_name="content/catalogs_brand.tpl" plevel=0 cond="a1.name='$image_title'" fields="as1.img2"}
	<div class="price_block">
    	<a href="{$smarty.get.global.root}/pub/download.php?id={$im.image_id}&location={$location}">
        	<img src="{$smarty.get.global.root}/images/site/pdf.png"></a>
		<a class="price_link" href="{$smarty.get.global.root}/pub/download.php?id={$im.image_id}&location={$location}">{$im.image_desc}</a>
	</div>
</div>
{/foreach}
{/foreach}