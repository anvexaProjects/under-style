{foreach from=$items item=i name=n}
<li {if $smarty.get.global.struct.1.path eq $i.path}class="cur"{/if}>
    {if $smarty.get.global.struct.1.path neq $i.path}
    <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">
        {$i.name}
    </a>
    {else}
        <a href="/" onclick="return false" style="cursor: default" class="bold">
            {$i.name}
        </a>
    {/if}
</li>
{/foreach}