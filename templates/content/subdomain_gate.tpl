{include file="content/picking.tpl"}

<div id="item-catalog" class="row content">
    <div class="col-md-12">
        <h2>{$smarty.get.global.struct.1.name}</h2>

        {include_php
        file="content/lister.inc.php"
        tpl_name="content/breadcrumbs.tpl"
        plevel=0
        cond="a1.path = '$path'"
        }

        <div class="row">
            <div class="col-md-3">
                {*include_php file = "content/lister.inc.php" tpl_name="content/sub_menu_catalog.tpl" plevel = 0 cond = "as1.struct_id = '`$smarty.get.global.struct.1.struct_id`'" disable_caching = true*}
                <div class="list-group">
                    {*<?*}
                    {php}
                        $subdomen_id = $_GET['global']['rstruct'][1]['struct_id'];
                        $subdomen_psevd = $_GET['global']['rstruct'][1]['psevdonim'];
                        $oDb = &Database::get();
                        //                 $vendor = $oDb->getRows("SELECT `psevdonim` FROM `art_struct` WHERE `control` = 'collection' AND `is_deleted` = '0' AND `is_hidden` = '0' AND `parent_id`='$subdomen_id' ORDER BY `sprop3` ASC LIMIT 1");
                        $vendor = $oDb->getRows("SELECT a1.psevdonim as `psevdonim` FROM art_struct as a1 JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' and a2.sprop4 != 'Без категории' GROUP BY a1.psevdonim  ORDER BY a1.sprop3 ASC LIMIT 1");
                        if (!empty($vendor)) {
                        if ($subdomen_psevd == 'sanitary')
                        echo '
                        <a class="list-group-item" href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог
                            сантехники</a>
                        ';
                        elseif ($subdomen_psevd == 'plitka')
                        echo '
                        <a class="list-group-item" href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог
                            плитки</a>
                        ';
                        elseif ($subdomen_psevd == 'mebel')
                        echo '
                        <a class="list-group-item" href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог
                            мебели</a>
                        ';
                        elseif ($subdomen_psevd == 'svet')
                        echo '
                        <a class="list-group-item" href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог
                            света</a>
                        ';
                        else
                        echo '
                        <a class="list-group-item" href="/'.$subdomen_psevd.'/'.$vendor[0]['psevdonim'].'/#cat">Каталог
                            товаров</a>
                        ';
                        }else {
                        echo '
                        <a class="list-group-item">Каталог товаров</a>
                        ';
                        }
                        $vendor = $oDb->getRows("SELECT a1.struct_id, a1.psevdonim, a1.sprop3 FROM art_struct as a1 LEFT JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop9 = '1' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' and a2.sprop4 != 'Без категории' GROUP BY a1.psevdonim ORDER BY a1.sprop3 ASC LIMIT 1");

                        //if (!empty($vendor)) {
                        //echo '<a class="list-group-item" href="/'.$subdomen_psevd.'/offers/'.$vendor[0]['psevdonim'].'/#cat">СПЕЦПРЕДЛОЖЕНИЯ</a>';
                        //}else {
                        //echo '<a class="list-group-item" href="/'.$subdomen_psevd.'/offers/">СПЕЦПРЕДЛОЖЕНИЯ</a>';
                        //}

                        $vendor = $oDb->getRows("SELECT a1.psevdonim FROM art_struct as a1 LEFT JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop10 = '1' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' GROUP BY a1.psevdonim ORDER BY a1.sprop3 ASC");
                        if (!empty($vendor)) {
                        echo '
                        <a class="list-group-item"
                           href="/'.$subdomen_psevd.'/sale/'.$vendor[0]['psevdonim'].'/#cat">РАСПРОДАЖА</a>
                        ';
                        }else {
                        echo '
                        <a class="list-group-item">СПЕЦПРЕДЛОРАСПРОДАЖАЖЕНИЯ</a>
                        ';
                        }
                        $vendor = $oDb->getRows("SELECT a1.psevdonim FROM art_struct as a1 LEFT JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop1 = '1' and a1.parent_id='$subdomen_id' GROUP BY a1.psevdonim ORDER BY a1.sprop3 ASC");
                        if (!empty($vendor)) {
                        echo '
                        <a class="list-group-item"
                           href="/'.$subdomen_psevd.'/archives/'.$vendor[0]['psevdonim'].'/#cat">АРХИВ</a>
                        ';
                        }else {
                        echo '
                        <a class="list-group-item">АРХИВ</a>
                        ';
                        }
                    {/php}
                    {*?>*}
                </div>
            </div>
            <div class="col-md-9">

                <div class="salers_carousel">
                    <div id="salers" class="clearfix">
                        {foreach from=$items.0.images item=i}
                            <img src="{$smarty.get.global.root}/upload/article_big/{$i.image_name}" width="762"
                                 height="450"
                                 alt=""/>
                        {/foreach}
                    </div>
                    <a class="prev" id="salers_prev" href="#"><span>prev</span></a>
                    <a class="next" id="salers_next" href="#"><span>next</span></a>
                </div>


            </div>
        </div>
        <div class="post-rows post-text-fix post-img-fix">
            {$items.0.content|content}
            {*include file="block/share_block.tpl"*}
        </div>
    </div>

    {*<div class="mainCont clearfix">*}
    {*<div class="lSide">*}

    {*</div>*}
    {*<div class="rSide">*}
    {*<div class="clear"><!--  --></div>*}

    {*</div>*}
    {*</div>*}

    {include_php file="content/lister.inc.php"
    tpl_name="content/destinations.tpl" plevel=0
    cond="as1.control='subdomen' AND as1.level=1"}

</div>
</div>