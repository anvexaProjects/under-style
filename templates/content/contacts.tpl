{assign var=path  value=$smarty.get.global.struct.1.path}

<div class="row content post-text-fix">
    <div class="col-md-12">
        <h1>{$smarty.get.global.struct.1.name}</h1>

        {include_php file="content/lister.inc.php" tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}

        <div class="row">
            <div class="col-md-3">
                {include file="content/sub_menu_contacts.tpl"}
            </div>
            <div class="col-md-9 ">
                {if $path eq 'about/contacts'}


                    {$items.0.content|content}

                    <div style="display:none;visibility:hidden">
                        <div itemscope itemtype="http://schema.org/Organization">
                            <span itemprop="name">Салон-магазин "АНДЕГРАУНД"</span>
                            <span itemprop="description">Салон «Андеграунд» — не просто магазин сантехники или точка по продаже элитной мебели. Это творческая мастерская, специалисты которой всегда готовы предложить готовое решение любой задачи, связанной с организацией пространства ванной комнаты — например, подобрать эксклюзивную немецкую сантехнику, элитную испанскую и итальянскую керамическую плитку или предметы декора.</span>
                            Контакты:
                            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                Адрес:
                                <span itemprop="streetAddress">ул. 1905 года, 25</span>
                                <span itemprop="postalCode">123022</span>
                                <span itemprop="addressLocality">Москва</span>,
                            </div>
                            Телефон:<span itemprop="telephone">+7 (495) 984 32 60</span>,
                            Телефон:<span itemprop="telephone">+7 (499) 253 69 32</span>,
                            Телефон:<span itemprop="telephone">+7 (499) 253 53 30</span>,
                            Электронная почта: <span itemprop="email">info@under-style.ru</span>
                        </div>
                    </div>

                    <div id="map" style="width: 665px; height: 340px;">
                        {include file="block/yandex_map.tpl"}
                    </div>
                {/if}

                {if $path neq 'about/contacts'}
                    {$items.0.content|content}
                    {if $path eq 'about/contacts/feedback'}
                        {include_php file = "content/feedback.inc.php"}
                    {/if}
                {/if}
            </div>
        </div>


        {*<div id="share_block">*}
        {*{include file="block/share_block.tpl"}*}
        {*</div>*}
        {include_php file="content/lister.inc.php"
        tpl_name="content/destinations.tpl" plevel=0
        cond="as1.control='subdomen' AND as1.level=1"}
    </div>
</div>