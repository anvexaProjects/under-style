{if $items.0.img2}
    <div class="img" style="width: 150px">
        {include_php
        file="master/img.inc.php"
        path="/upload/article"
        img=$items.0.img2
        tpl_name="block/img.tpl"
        alt=$items.0.name
        no_img="/images/site/empty_image_brand.png"
        }
    </div>
{else}
    <div class="img" style="width: 150px">
        {$items.0.name}
    </div>
{/if}
