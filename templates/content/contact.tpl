<div id="body_sec">
	{include_php file="content/lister.inc.php" tpl_name="content/article_menu.tpl" plevel=1 parent=$smarty.get.global.rstruct.1.path}
	<div class="secondCont">
		<div class="cont">
			<h1>{$default.item.name}</h1>
			{if $smarty.get.s eq 1}{$smarty.const.LBL_SENT_SUCCESS}{/if}
			{include file="ablock/error_block.tpl"}
			<form {$form_data.attributes}>
			{$form_data.javascript}
			{$form_data.hidden}
			<div style="display: none;">{$form_data.field_1.html}{$form_data.field_2.html}</div>
			<table>
				<tr><td>{$smarty.const.S_NAME}:</td><td>{$form_data.name.html}</td></tr>
				<tr><td>{$smarty.const.S_EMAIL}:</td><td>{$form_data.email.html}</td></tr>
			</table>
			{$form_data.message.html}
			<input type="submit" value="{$smarty.const.S_SEND} &gt;" class="btn2"/>
			</form>
		</div>
		<div class="otherInfo">
			{include_php file="master/img.inc.php" path="/upload/article" img=$items.0.img1 big_path="/upload/article_big" render=false}
			<div>{$items.0.description|nl2br}</div>
		</div>
	</div>
	<div class="clear"><!-- --></div>
</div>