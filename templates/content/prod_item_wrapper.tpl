{if $items}

    {if $smarty.get.global.struct.1.control eq 'prod'}
        <h3 class="page-header">{$smarty.const.S_SIMILAR_PRODUCTS}</h3>
    {/if}

    {*{$items|@print_r}*}

    {*{php}*}
    {*die();*}
    {*{/php}*}
    <div id="products" class="row">
        {foreach from=$items item=i name=f}

            {assign var=vendor value=$i.sprop3|escape}
            {assign var=parent value=$i.parent_id}

            {if $i.is_hidden eq 0}
                <div class="col-md-3 post-box">

                    {if $i.sprop7}
                        <span class="label label-default">{$smarty.const.S_AVAILABLE}</span>
                    {/if}
                    {if $i.sprop8}
                        <span class="label label-primary">{$smarty.const.S_NOT_AVAILABLE}</span>
                    {/if}
                    {if $i.sprop9}
                        <p class="label label-warning">
                            {$smarty.const.S_SPEC_OFFER}<br>
                            {assign var="timer" value=$i.end_time}
                        </p>
                    {/if}
                    {if $i.sprop2}
                        {*<p class="label label-danger">*}
                        {*<img src="{$smarty.get.global.root}/images/site/knopka.png"/>*}
                        {*</p>*}
                        <span class="label label-danger">Новый!</span>
                    {/if}

                    <div class="thumbnail box-shadow"
                         factory="{$i.sprop3|replace:' ':'_'}"
                         place="{$i.sprop6|replace:' ':'_'}"
                         rub="{$i.sprop4|replace:' ':'_'}"
                         collection="{$i.sprop13|replace:' ':'_'}"
                            >

                        {include_php
                        class="item-image"
                        file="master/img.inc.php"
                        path="/upload/article"
                        img=$i.images.0.image_name
                        tpl_name="block/img.tpl"
                        alt=$i.name
                        title=$i.name
                        }


                        <div class="caption">

                            <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>

                            {if $i.sprop3}
                                <p>
                                    {$smarty.const.S_MANUFACTURER}
                                    <b>{$i.sprop3}</b>
                                </p>
                            {/if}
                            {if $i.sprop5}
                                <p>
                                    {$smarty.const.S_COUNTRY}
                                    <b>{$i.sprop5}</b>
                                </p>
                            {/if}
                            {if $i.sprop13}
                                <p>
                                    {$smarty.const.S_COLLECTION}
                                    <b>{$i.sprop13}</b>
                                </p>
                            {/if}
                            {if $items.0.created}
                                <p>{$items.0.created|date_format:"%m%y"}</p>
                            {/if}

                            <p class="price_prod{if $i.sprop9} cancel{/if}">{$i.sprop11}</p>

                            {if $i.sprop9}
                                <div class="price_prod"><span>{$i.sprop12}</span></div>
                                <div class="counter">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>
                            {/if}

                        </div>
                    </div>
                </div>
            {/if}

            {*{if $smarty.get.global.struct.1.control eq 'manufacturer' &&*}
            {*$smarty.foreach.f.iteration %4 eq 0}*}
            {*{/if}*}
        {/foreach}
    </div>
    {if $smarty.get.global.struct.1.control eq 'prod'}

    {/if}

{/if}