{if $items}

{if $smarty.get.global.struct.1.control eq 'prod'}
<div id="otherProducts" class="productsBlock clearfix scroll-pane" style="height: 330px">
	<h5>{$smarty.const.S_SIMILAR_PRODUCTS}</h5>
{/if}
{foreach from=$items item=i name=f}
    {if $i.sprop2 eq '1'}
{assign var=vendor value=$i.sprop3|escape}
{assign var=parent value=$i.parent_id}
<div class="prodItemWrapper vis"
factory="{$i.sprop3|replace:' ':'_'}"
place="{$i.sprop6|replace:' ':'_'}"
rub="{$i.sprop4|replace:' ':'_'}"
collection="{$i.sprop13|replace:' ':'_'}"
>

	<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/" class="prodItem">
		<div class="img">
			{include_php file="master/img.inc.php"
	path="/upload/article" img=$i.images.0.image_name tpl_name="block/img.tpl"
	alt=$i.name width="165" height="102"}
		</div>
        {if $i.sprop7}
            <div class="flag_av">{$smarty.const.S_AVAILABLE}</div>
        {/if}
        {if $i.sprop8}
            <div class="flag">{$smarty.const.S_NOT_AVAILABLE}</div>
        {/if}
        {if $i.sprop9}
            <div class="flag_sp">{$smarty.const.S_SPEC_OFFER}</div>
        {assign var="timer" value=$i.end_time}
        {/if}
        {if $i.sprop2}
            <div class="flag_new"><img src = "{$smarty.get.global.root}/images/site/knopka.png" /></div>
        {/if}
		<h3>{$i.name}</h3>
        {if $i.sprop3}
            <p>
                {$smarty.const.S_MANUFACTURER}
                <b>{$i.sprop3}</b>
            </p>
        {/if}
        {if $i.sprop5}
		<p>
			{$smarty.const.S_COUNTRY}
			<b>{$i.sprop5}</b>
		</p>
        {/if}
        {if $i.sprop13}
		<p>
			{$smarty.const.S_COLLECTION}
			<b>{$i.sprop13}</b>
		</p>
        {/if}
        {if $items.0.created}
            <p>{$items.0.created|date_format:"%m%y"}</p>
        {/if}
        <p class="price_prod{if $i.sprop9} cancel{/if}">{$i.sprop11}</p>
        {if $i.sprop9}
        <div class="price_prod"><span>{$i.sprop12}</span></div>
        <div class="counter">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>
        {/if}
	</a>
</div>
{if $smarty.get.global.struct.1.control eq 'manufacturer' &&
	$smarty.foreach.f.iteration %4 eq 0}
    <div class="clear"><!--  --></div>
{/if}
    {/if}
{/foreach}
{if $smarty.get.global.struct.1.control eq 'prod'}
</div>
{/if}

{/if}