<div id="registration">
<form {$form_data.attributes}>
	{$form_data.javascript}
	{$form_data.hidden}
  {include file="ablock/error_block.tpl"}
  {if $smarty.get.s && $smarty.get.s == 1}<span class="err">{$smarty.const.S_REG_SUCCESS}</span>{/if}
	<table>
		<col width="170">
      <tr>
				<td >{$smarty.const.S_U_EMAIL}&nbsp;<span style="color: red;">*</span></td>
				<td>{$form_data.email.html}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_NAME}&nbsp;<span style="color: red;">*</span></td>
				<td>{$form_data.name.html}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_PASSWORD}<span style="color: red;">*</span></td>
				<td>{$form_data.pass.html}</td>
			</tr>
      <tr>
				<td>Подтвердить:<span style="color: red;">*</span></td>
				<td>{$form_data.conf_pass.html}</td>
			</tr>
      <tr>
				<td>Роль:</td>
				<td>{$form_data.role.html}</td>
			</tr>
	  <tr>
      <td>&nbsp;</td>
      <td><input type="submit" value="Регистрация" style="background: #000; border: 0; padding: 5px; color: #fff;" class="submit_button" style="margin:0;"></td>
    </tr>
		</table>
	<div style="display: none">
		{$form_data.field_1.html}{$form_data.field_2.html}
	</div>


</form>
</div>