<div>
    {include file="block/core_pager.tpl"}
</div>

<div id="products" class="row">
{if $smarty.get.global.struct.1.control eq 'prod'}
<div id="otherProducts" class="productsBlock clearfix scroll-pane" >
    <h5>{$smarty.const.S_SIMILAR_PRODUCTS}</h5>
    {/if}


    {*<div class="col-sm-6 col-md-4">*}
    {*<div class="thumbnail">*}
    {*<img data-src="holder.js/100%x200" alt="100%x200" src="" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">*}

    {*<div class="caption"><h3>Thumbnail label</h3>*}

    {*<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>*}

    {*<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p></div>*}
    {*</div>*}
    {*</div>*}
    {*{$items|@print_r}*}


    {foreach from=$items item=i name=f}
        {assign var=vendor value=$i.sprop3|escape}
        {assign var=parent value=$i.parent_id}

        {if  strripos($i.name, 'SEO ')=== false }
            <div class="col-md-3 post-box">

                {if $i.sprop7}
                    <span class="label label-default">{$smarty.const.S_AVAILABLE}</span>
                {/if}
                {if $i.sprop8}
                    <span class="label label-primary">{$smarty.const.S_NOT_AVAILABLE}</span>
                {/if}
                {if $i.sprop9}
                    <p class="label label-warning">
                        {$smarty.const.S_SPEC_OFFER}<br>
                        {assign var="timer" value=$i.end_time}
                    </p>
                {/if}
                {if $i.sprop2}
                    <span class="label label-danger">Новый!</span>
                {/if}

                <div class="thumbnail box-shadow"
                     factory="{$i.sprop3|replace:' ':'_'}"
                     place="{$i.sprop6|replace:' ':'_'}"
                     rub="{$i.sprop4|replace:' ':'_'}"
                     collection="{$i.sprop13|replace:' ':'_'}"
                        >

                    {*{$i|@print_r}*}

                    <img class="item-image" src="/upload/article/{$i.image_name}" alt="{$i.title}" title="{$i.title}">

                    <div class="caption">


                        <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a>

                        {if $i.sprop3}
                            <p>
                                {$smarty.const.S_MANUFACTURER}
                                <b>{$i.sprop3}</b>
                            </p>
                        {/if}

                        {if $i.sprop5}
                            <p>
                                {$smarty.const.S_COUNTRY}
                                <b>{$i.sprop5}</b>
                            </p>
                        {/if}

                        {if $i.sprop13}
                            <p>
                                {$smarty.const.S_COLLECTION}
                                <b>{$i.sprop13}</b>
                            </p>
                        {/if}

                        {if $items.0.created}
                            <p>{$items.0.created|date_format:"%m%y"}</p>
                        {/if}

                        <p class="price_prod{if $i.sprop9} cancel{/if}">{$i.sprop11}</p>

                        {if $i.sprop9}
                            <div class="price_prod"><span>{$i.sprop12}</span></div>
                            <div class="counter">{include_php file="content/timer.inc.php" time=$timer} {$smarty.const.S_DAYS}</div>
                        {/if}

                    </div>
                </div>
            </div>
        {/if}

    {/foreach}

    {if $smarty.get.global.struct.1.control eq 'prod'}
</div>
{/if}
</div>

{if $bottom_text eq '1'}
    <div class="post-box col-md-12">
        {$seotext}
    </div>
{/if}


<div>
    {include file="block/core_pager.tpl"}
</div>