 <div id="cont">
	<div class="title clearfix">
		<div class="title clearfix" >
		    <h1>{$smarty.get.global.struct.1.name}</h1>
            <div style="clear: both;"></div>
            <div class="path" style="float: left !important;" >

                {foreach from=$items item=item}
                {if $item.control neq 'subdomen'}
                    <span style="padding-bottom: 5px; display: block"><a style="font-weight: bold;" href="{$smarty.get.global.root|subdomain:$item.path}/{$item.path}/">{$item.name}</a></span>
                {/if}

                    {if $item.spath eq 'actueel'}
                    {include_php
                    file="content/lister.inc.php"
                    tpl_name = "content/menu_subdomains.tpl"
                    plevel = 0
                    cond = "as1.control='subdomen'"
                    }
                    {else}
                    {foreach from = $item.items item = i}
                        <span style="padding-bottom: 5px; margin-left: 20px; display: block"><a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/">{$i.name}</a></span>
                    {/foreach}
                    {/if}
                {/foreach}

            </div>
        </div>
    </div>
</div>
 <script>
     $("span span").find('span:contains("Treesse")').hide();
</script>