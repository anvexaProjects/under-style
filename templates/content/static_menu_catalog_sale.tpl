{*<?*}
{php}

$oDb = &Database::get();
$subdomen_id = $_GET['global']['rstruct'][1]['struct_id'];
$subdomen_psevd = $_GET['global']['rstruct'][1]['psevdonim'];
$vendors = $oDb->getRows("SELECT a1.struct_id, a1.psevdonim, a1.sprop3 FROM art_struct as a1 LEFT JOIN art_struct as a2 ON a2.parent_id=a1.struct_id WHERE a1.control = 'collection' AND a1.is_deleted = '0' AND a2.is_deleted = '0' AND a1.is_hidden = '0' AND a2.sprop10 = '1' AND a2.sprop1 != '1' and a1.parent_id='$subdomen_id' GROUP BY a1.psevdonim  ORDER BY a1.sprop3 ASC");
$uri = explode('/', $_SERVER['REQUEST_URI']);

echo  '<ul class="nav">';
if (strpos($_SERVER['REQUEST_URI'], 'sale/')) {
echo  '<li class="cur">РАСПРОДАЖА</li>';
echo  '<li class="h vendors">Производители</li>';
foreach($vendors as $vendor) {
    $link = '/'.$subdomen_psevd.'/sale/'.$vendor['psevdonim'].'/';
    echo '<li class="';
    $exp_link = explode('/', $link);
    if(strpos($_SERVER['REQUEST_URI'], $link) === 0 || ($exp_link[1] == $uri[1] && mb_strtolower($exp_link[3]) == mb_strtolower($uri[3]))) {
        echo 'active ';
    }
    echo 'vendor"><a href="'.$link.'">'.$vendor['sprop3'].'</a>';
    if (strpos($_SERVER['REQUEST_URI'], $link) === 0 || ($exp_link[1] == $uri[1] && mb_strtolower($exp_link[3]) == mb_strtolower($uri[3]))) {
        echo '<ul class="nav">';
        $colList = $oDb->getRows("SELECT c.name, c.psevdonim FROM `collection_psevdonims` as c JOIN `art_struct` as a ON (c.name=a.sprop13  AND a.is_deleted = 0) WHERE c.parent_id = '".$vendor['struct_id']."' AND a.sprop10 = '1'AND a.sprop1 != '1'  GROUP BY c.name ORDER BY c.name ASC");
        if (!empty($colList)) {
            echo '<li class="h ">Коллекции</li>';
        }
        foreach($colList as $collitem) {
            $link2 = $link.$collitem['psevdonim'].'/';
            echo '<li ';
            if($_SERVER['REQUEST_URI'] == $link2) {
                $class = 'class="active" ';
                echo $class;
            }else {
                $class = '';
            }
            echo '><a href="'.$link2.'">'.$collitem['name'].'</a></li>';
        }
        $groupList = $oDb->getRows("SELECT DISTINCT a.sprop4, c.psevdonim FROM `art_struct` as a INNER JOIN  `category_psevdonims` as c ON (a.sprop4=c.name  AND a.is_deleted = 0) WHERE a.control='prod' AND a.parent_id = '".$vendor['struct_id']."' AND a.sprop10 = '1' AND a.sprop1 != '1' ORDER BY a.sprop4 ASC");
        if (!empty($groupList)) {
            echo '<li class="h">Товарные группы</li>';
        }
        foreach($groupList as $groupitem) {
            $link3 = $link.'collection/'.$groupitem['psevdonim'].'/';
            echo '<li ';
            if($_SERVER['REQUEST_URI'] == $link3) {
                $class = 'class="active" ';
                echo $class;
            }else {
                $class = '';
            }
            echo '><a href="'.$link3.'">'.$groupitem['sprop4'].'</a></li>';
        }
        /*$placeList = $oDb->getRows("SELECT DISTINCT a.sprop6, p.psevdonim FROM `art_struct` as a INNER JOIN  `place_psevdonims` as p ON (a.sprop6=p.name) WHERE a.control='prod' AND a.parent_id = '".$vendor['struct_id']."' AND a.sprop10 = '1' AND a.sprop1 != '1' ORDER BY a.sprop6 ASC");
        if (!empty($placeList)) {
            echo '<li class="h">Расположение</li>';
        }
        foreach($placeList as $placeitem) {
            $link4 = $link.'place/'.$placeitem['psevdonim'].'/';
            echo '<li ';
            if($_SERVER['REQUEST_URI'] == $link4) {
                $class = 'class="active" ';
                echo $class;
            }else {
                $class = '';
            }
            echo '><a href="'.$link4.'">'.$placeitem['sprop6'].'</a></li>';
        }*/
        echo  '</ul>';
    }
    echo '</li>';
}
	    
// echo "<!--" ."SELECT DISTINCT a.sprop6, c.psevdonim FROM `art_struct` as a1, `art_struct` as a INNER JOIN  `place_psevdonims` as c ON (a.sprop6=c.name  AND a.is_deleted = 0) WHERE a.control='prod' AND a1.parent_id = '$subdomen_id'  AND a1.is_deleted = 0 AND a.parent_id = a1.struct_id AND a.sprop10 = '1' AND a.sprop1 != '1' ORDER BY a.sprop6 ASC". "-->";
	    
$placeList = $oDb->getRows("SELECT DISTINCT a.sprop6, c.psevdonim FROM `art_struct` as a1, `art_struct` as a INNER JOIN  `place_psevdonims` as c ON (a.sprop6=c.name  AND a.is_deleted = 0) WHERE a.control='prod' AND a1.parent_id = '$subdomen_id'  AND a1.is_deleted = 0 AND a.parent_id = a1.struct_id AND a.sprop10 = '1' AND a.sprop1 != '1' ORDER BY a.sprop6 ASC");
if (!empty($placeList)) {
    echo '<li class="h">Расположение</li>';
}
foreach($placeList as $placeitem) {
    $link4 = '/'.$subdomen_psevd.'/sale/place/'.$placeitem['psevdonim'].'/';
    echo '<li ';
    if($_SERVER['REQUEST_URI'] == $link4) {
        $class = 'class="active" ';
        echo $class;
    }else {
        $class = '';
    }
    echo '><a href="'.$link4.'">'.$placeitem['sprop6'].'</a></li>';
}
}else {
    echo  '<li class="head"><a href="/'.$subdomen_psevd.'/sale/'.$vendors[0]['psevdonim'].'/#cat">РАСПРОДАЖА</a></li>';
}
echo  '</ul>';

{/php}
