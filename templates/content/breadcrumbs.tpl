{assign var=brand value=$smarty.get.brand}
<ol class="breadcrumb">
{foreach from=$smarty.get.global.rstruct item=i key=k name=f}
	<li>
	{if $smarty.foreach.f.first}
    <a href="{$smarty.get.global.root}/">{$smarty.const.S_MAIN}</a>
	{elseif $smarty.foreach.f.last}
    	<span>{if $smarty.get.global.struct.1.control eq 'manufacturer'}{$brand}{else}{$i.name}{/if}</span>
	{elseif $i.parent_id == 1}
    	<span>{if $smarty.get.global.struct.1.control eq 'manufacturer'}{$brand}{else}{$i.name}{/if}</span>
    {else}
    <a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path|lower}/">{$i.name}</a>
    {/if}
	</li>
{/foreach}
</ol>

{*<ol class="breadcrumb">*}
	{*<li><a href="#">Home</a></li>*}
	{*<li><a href="#">Library</a></li>*}
	{*<li class="active">Data</li>*}
{*</ol>*}