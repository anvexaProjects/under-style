{assign var = 'grand_parent' value = $smarty.get.global.struct.3.struct_id}
{assign var = 'grand_parent_path' value = $smarty.get.global.struct.3.path}
{assign var = 'struct_id' value=$smarty.get.global.struct.1.struct_id}
<p class="h filter" style="border-top:0">{$smarty.const.S_MANUFACTURERS}</p>
<div class="catSelect filter factory">
{if $smarty.get.global.struct.1.control eq 'subdomen'}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_vendors.tpl"
cond="as1.control = 'prod' AND as1.parent_id IN (SELECT struct_id FROM art_struct WHERE parent_id = `$struct_id`) AND as1.is_deleted = 0"
field="sprop3"}
{else}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_vendors.tpl"
cond="as1.parent_id='$grand_parent' AND as1.is_deleted = 0"
field="sprop3"}
{/if}
</div>
<p class="h filter">Товарная группа</p>
<div class="catSelect filter rub">
{if $smarty.get.global.struct.1.control eq 'subdomen'}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_type.tpl"
field="sprop4"
cond = "as1.parent_id IN (SELECT struct_id FROM art_struct WHERE parent_id = '$struct_id') AND as1.is_deleted = 0"
}
{else}
{include_php
file="content/lister.inc.php"
tpl_name="block/rub_menu.tpl"
plevel=1
parent=$grand_parent_path
fields="as1.struct_id"
}
{/if}
</div>
<p class="h filters">{$smarty.const.S_COLLECTIONS}</p>
<div class="catSelect collections" id="collection">
{if $smarty.get.global.struct.1.control eq 'subdomen'}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_collections.tpl"
cond="as1.control = 'prod' AND as1.parent_id IN (SELECT struct_id FROM art_struct WHERE parent_id = `$struct_id`) AND as1.is_deleted = 0"
field="sprop13"}
{else}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_collections.tpl"
cond="as1.parent_id='$grand_parent' AND as1.is_deleted = 0"
field="sprop13"}
{/if}
</div>
<p class="h filter">{$smarty.const.S_TO_LOCATIONS}</p>
<div class="catSelect last filter locations">
{if $smarty.get.global.struct.1.control eq 'subdomen'}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_location.tpl"
cond="as1.control = 'prod' AND as1.parent_id IN (SELECT struct_id FROM art_struct WHERE parent_id = `$struct_id`) AND as1.is_deleted = 0"
field="sprop6"}
{else}
{include_php
file="content/property_selector.inc.php"
tpl_name="content/sub_menu_catalog_location.tpl"
cond="as1.parent_id='$grand_parent' AND as1.is_deleted = 0"
field="sprop6"}
{/if}
</div>