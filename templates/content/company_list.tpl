<div id="cont">


    <h1>{$smarty.get.global.struct.1.name}</h1>
    <div class="path" style="">{include_php file="content/lister.inc.php"
        tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}
    </div>


    <div>

        <div class="letters btn-group" role="group" aria-label="Basic example">
            {foreach from=$vendors item=letter name=f key = key}
                <a type="button" class="btn btn-default btn-lg">{$key}</a>
            {/foreach}
        </div>

        <br>
        <br>

        {foreach from=$vendors item=letter name=l key= key}
            <div class="cmpEl clearfix" l="{$key}" {if $smarty.foreach.l.iteration eq 1}style="min-height: 80px"{/if}>
                <strong class="letter">
                    {$key}
                </strong>
                {*<div class="l">*}
                <ul>
                    {foreach from=$letter item=vendor name=v}
                    {assign var =vendor_escape value=$vendor|htmlentities}
                    <li>{include_php file="content/lister.inc.php" tpl_name="content/manufacturer_link.tpl" plevel=0 cond="a1.name='$vendor_escape' AND as1.control = 'company'" fields="a1.path, a1.name, as1.control"}</li>
                    {if $smarty.foreach.v.iteration%5 eq 0}
                </ul>
                <ul>
                    {/if}
                    {/foreach}
                </ul>
                {*</div>*}
            </div>
        {/foreach}

    </div>
    {*<DIV id="share_block">
        {include file="block/share_block.tpl"}
    </DIV>*}

    {include_php file="content/lister.inc.php" tpl_name="content/destinations.tpl" plevel=0 cond="as1.control='subdomen' AND as1.level=1"}

</div>
