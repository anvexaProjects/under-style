{if $smarty.get.property eq 'available'}
{foreach from=$items item=i}
{include_php
	file="content/imglister.inc.php"
	tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
    page_size = 1000
	cond="as1.control='prod' AND as1.sprop7='1' AND as1.sprop1 != '1'"}
{/foreach}
{elseif $smarty.get.property eq 'hold'}
{foreach from=$items item=i}
{include_php
	file="content/imglister.inc.php"
    tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
    page_size = 1000
    cond="as1.control='prod' AND as1.sprop8='1' AND as1.sprop1 != '1'"}
{/foreach}
{elseif $smarty.get.property eq 'offers'}
{foreach from=$items item=i}
{include_php
	file="content/imglister.inc.php"
	tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
    page_size = 1000
	cond="as1.control='prod' AND as1.sprop9='1' AND as1.sprop1 != '1'"}
{/foreach}
{elseif $smarty.get.property eq 'sale'}
{foreach from=$items item=i}
{include_php
	file="content/imglister.inc.php"
	tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
    page_size = 1000
	cond="as1.control='prod' AND as1.sprop10='1' AND as1.sprop1 != '1'"}
{/foreach}
{elseif $smarty.get.property eq 'archives'}
    {foreach from=$items item=i}
        {include_php
        file="content/imglister.inc.php"
        tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
        page_size = 1000
        cond="as1.control='prod' AND as1.sprop1='1'"}
    {/foreach}
{elseif $smarty.get.property eq 'catalog'}
    {foreach from=$items item=i}
        {include_php
        file="content/imglister.inc.php"
        tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
        page_size = 1000
        cond="as1.control='prod' AND as1.sprop14='1'"}
    {/foreach}
{elseif $smarty.get.type eq 'new'}
    {foreach from=$items item=i}
        {include_php
        file="content/imglister.inc.php"
        tpl_name="content/prod_item_wrapper_new.tpl" plevel=1 parent=$i.path
        page_size = 1000
        cond="as1.control='prod' AND as1.sprop1 != '1'"}
    {/foreach}
{else}
{foreach from=$items item=i}
{include_php
	file="content/imglister.inc.php"
	tpl_name="content/prod_item_wrapper.tpl" plevel=1 parent=$i.path
    page_size = 1000
	cond="as1.control='prod' AND as1.sprop1 != '1'"}
{/foreach}
{/if}