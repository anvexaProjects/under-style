{assign var=struct_id value=$smarty.get.global.struct.1.struct_id}
{assign var=path  value=$smarty.get.global.struct.1.path}

<div class="list-group">


    <a class="list-group-item {if $path eq 'about/contacts'}active{/if}" href="/about/contacts/">Адрес и схема
        проезда</a>

    <a class="list-group-item {if $path eq 'about/contacts/contacts'}active{/if}" href="/about/contacts/contacts/">О
        компании</a>

    <!--
		<li {if $path eq 'about/contacts/staff'}class="cur"{/if}>
			<a href="/about/contacts/staff/">Сотрудники</a>
		</li>
		-->

    <a class="list-group-item {if $path eq 'about/contacts/details'}active{/if}" href="/about/contacts/details/">Реквизиты
        компании</a>

    <a class="list-group-item {if $path eq 'about/contacts/vacancy'}active{/if}" href="/about/contacts/vacancy/">Вакансии</a>

    <a class="list-group-item {if $path eq 'about/contacts/feedback'}active{/if}" href="/about/contacts/feedback/"
       style="color: #F00">Письмо директору</a>

</div>
