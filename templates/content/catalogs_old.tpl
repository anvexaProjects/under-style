<div id="cont">
	<div class="title clearfix">
		<h1>{$smarty.get.global.struct.1.name}</h1>
		<div class="path" style="">{include_php file="content/lister.inc.php"
			tpl_name="content/breadcrumbs.tpl" plevel=0 cond="a1.path = '$path'"}</div>
	</div>
	<div class="mainCont clearfix">
			<div class="letters">
				{foreach from=$vendors item=letter name=f}
				<a href="">{$letter.0|upper|truncate:1:"":true}</a>
				{/foreach}
			</div>
            <div class="catalog_tabs">1
                <a id = "all" href = "{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?all=1" {if $smarty.get.all}class = "bold" onclick="return false" {/if}>
                    {$smarty.const.S_ALL2}
                </a>
                <a id = "furniture" href = "{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?furniture=1"{if $smarty.get.furniture}class = "bold" onclick="return false"{/if}>
                    {$smarty.const.S_FURNITURE}
                </a>
                <a id = "till" href = "{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?till=1" {if $smarty.get.till}class = "bold" onclick="return false"{/if}>
                    {$smarty.const.S_TILL}
                </a>
                <a id = "sanitary" href = "{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?sanitary=1"{if $smarty.get.sanitary}class = "bold" onclick="return false"{/if}>
                    {$smarty.const.S_SANITARY}
                </a>
                <a id = "light" href = "{$smarty.get.global.root|subdomain:$smarty.get.global.struct.1.path}/{$smarty.get.global.struct.1.path}/?light=1"{if $smarty.get.light}class = "bold" onclick="return false"{/if}>
                    {$smarty.const.S_LIGHT}
                </a>
            </div>
			{foreach from=$vendors item=v name=f}
			{foreach from=$v item=i name=n}
			<div class="catEl clearfix" l="{$i|truncate:1:"":true}">
				<a name="{$i}" href="{$smarty.get.global.root}/manufacturer/?brand={$i}">
                    {assign var = "escaped_i" value=$i|escape}
					{include_php
					file="content/lister.inc.php" tpl_name="content/catalogs_brand.tpl"
					plevel=0 cond="as1.control='company' AND a1.name='$escaped_i'"}
				</a>
				{include_php
				file="content/filelister.inc.php"
				tpl_name="content/catalogs_collections.tpl" plevel=0
				cond="as1.control='collection' AND as1.sprop3='$escaped_i'"}</div>
			{/foreach}
			{/foreach}

	</div>
	{*<DIV id="share_block">
		{include file="block/share_block.tpl"}
	</DIV>*}
</div>