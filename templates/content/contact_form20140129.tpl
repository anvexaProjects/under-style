<div id="registration" style="text-align: left; margin-left: 40px;">

<form {$form_data.attributes}>
    {$form_data.javascript}
    {$form_data.hidden}
    {include file="ablock/error_block.tpl"}
    {if $smarty.get.s && $smarty.get.s == 1}
        <span class="err">{$smarty.const.LBL_SENT_SUCCESS}</span>
    {/if}
    <table>
        <col width="250">
        <tr>
            <td>{$smarty.const.S_INTRODUCTION}&nbsp;<span style="color: red;">*</span></td>
            <td>{$form_data.name.html}</td>
        </tr>
        <tr>
            <td>{$smarty.const.S_PHONE}</td>
            <td>{$form_data.phone.html}</td>
        </tr>
        <tr>
            <td>{$smarty.const.S_EMAIL}&nbsp;<span style="color: red;">*</span></td>
            <td>{$form_data.email.html}</td>
        </tr>
        <tr>
            <td>{$smarty.const.S_SUBJECT}&nbsp;<span style="color: red;">*</span></td>
            <td>{$form_data.subject.html}</td>
        </tr>
        <tr>
            <td>{$smarty.const.S_YOU_MESSAGE}&nbsp;<span style="color: red;">*</span></td>
            <td>{$form_data.message.html}</td>
        </tr>
        <tr>
            <td>{$smarty.const.S_TIME}</td>
            <td>{$form_data.time.html}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="{$smarty.const.S_SUBMIT}" style="background: #000; border: 0; padding: 5px; color: #fff;" class="submit_button" style="margin:0;"></td>
        </tr>
    </table>
    <div style="display: none">
        {$form_data.field_1.html}{$form_data.field_2.html}
    </div>
</form>
</div>