{include_php file="master/slider.inc.php"}
{include file="content/picking.tpl"}


<nav id="menu-middle" class="navbar" style="margin-top: 60px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                {include_php file="content/lister.inc.php" tpl_name="content/destinations.tpl" plevel=0 cond="as1.control='subdomen' AND as1.level=1"}
            </div>
            <div class="col-md-3">
                <div class="row">
                    {include_php file="content/lister.inc.php" tpl_name="content/index_news.tpl" plevel=0  cond="as1.control IN ('new', 'company')" ord="as1.created" sequence='down'}
                </div>
            </div>
        </div>
    </div>
</nav>


{include_php file="content/lister.inc.php" tpl_name="content/index_action.tpl" plevel=1 parent = "about/actions" cond="as1.control='action'" ord="as1.created" sequence='down'}

<br>
<div id="blog" class="row content">
    <!-- Blog Post Content Column -->
    <div class="col-lg-10">

        <!-- Blog Post -->
        <!-- Title -->
        {*<header>*}
            {*<h2>*}
                {*Новые статьи*}
            {*</h2>*}
            {*<hr class="border-dark">*}
        {*</header>*}
        {*<br>*}
        {*<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste*}
        {*ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus,*}
        {*voluptatibus.</p>*}
        <div class="post-rows post-text-fix">

            {*{$items.0|print_r}*}

            {if $items.0.content}
                {$items.0.content|content}
            {/if}

        </div>
    </div>
</div>

{*<section id="gallery">*}
{*<div class="row">*}
{*<div class="col-lg-12">*}
{*<header>*}
{*<h2>*}
{*Галерея*}
{*</h2>*}
{*<hr class="border-dark">*}
{*</header>*}
{*</div>*}
{*</div>*}
{*<div class="row">*}
{*<div class="col-md-12">*}
{*<img class="img-responsive" src="http://placehold.it/750x500" alt="">*}
{*</div>*}
{*</div>*}
{*<br>*}
{*<br>*}

{*<div class="row">*}
{*<div class="col-sm-3 col-xs-6">*}
{*<a href="#">*}
{*<img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">*}
{*</a>*}
{*</div>*}

{*<div class="col-sm-3 col-xs-6">*}
{*<a href="#">*}
{*<img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">*}
{*</a>*}
{*</div>*}

{*<div class="col-sm-3 col-xs-6">*}
{*<a href="#">*}
{*<img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">*}
{*</a>*}
{*</div>*}

{*<div class="col-sm-3 col-xs-6">*}
{*<a href="#">*}
{*<img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">*}
{*</a>*}
{*</div>*}

{*</div>*}
{*</section>*}