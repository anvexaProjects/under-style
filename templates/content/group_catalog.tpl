{include_php
file            = "content/innerlister.inc.php"
tpl_name        = "content/seo.tpl" plevel=$smarty.get.global.level
parent          = $smarty.get.global.struct.1.path
cond            ="as1.control='collection'"
disable_caching = true
}


{*<p>*}
{*<b></b>*}
{*<span>Товаров в категории {$smarty.get.global.struct.1.name}</span>*}
{*</p>*}


{*{|@print_r}*}


<div id="groups" class="row">
    {*{assign var="struct_id" value=$smarty.get.global.struct.1.struct_id}*}
    {assign var="struct_id" value=$smarty.get.global.struct.1.struct_id}
    {php}

        $oDb = &Database::get();

        function getsGroups($oDb) {
        return $oDb->getRows(
        "
        SELECT DISTINCT a.struct_id, a.sprop4, c.psevdonim, c.psevdonim_id
        FROM art_struct as a
        INNER JOIN `category_psevdonims` as c
        ON (a.sprop4 = c.name AND a.is_deleted = 0)
        WHERE a.control = 'prod'
        AND a.sprop1 != '1'
        AND a.sprop4 != ''
        GROUP BY a.sprop4
        ");
        }

        function getTreeLevelsById($oDb, $struct_id) {
        return $oDb->getRow("
        SELECT t1.struct_id AS lev1, t2.struct_id as lev2, t3.struct_id as lev3
        FROM art_struct AS t1
        LEFT JOIN art_struct AS t2 ON t2.struct_id = t1.parent_id
        LEFT JOIN art_struct AS t3 ON t3.struct_id = t2.parent_id
        WHERE t1.struct_id = '$struct_id';
        ");
        }

        function updateGroupSubdomen($oDb, $data) {
        $sql = 'UPDATE category_psevdonims SET subdomen = CASE psevdonim_id ';

        $sqlList = array_map(function ($item) {
        $template = "WHEN '%s' THEN '%s'";
        return sprintf($template, $item['psevdonim_id'], $item['subdomen']);
        }, $data);

        $sql .= implode(" ", $sqlList).' END';
        //print_r($sql);
        $oDb->query($sql);
        }

        function collectGroupSubdomens($oDb, $data) {
        foreach($data as &$item1) {
        $levels = getTreeLevelsById($oDb, $item1['struct_id']);
        $item1['subdomen'] = $levels['lev3'];

        }

        return $data;
        }

        //$groups = getsGroups($oDb);
        //$groups = collectGroupSubdomens($oDb, $groups);
        //updateGroupSubdomen($oDb, $groups);


        //print_r($this->get_template_vars('struct_id'));
        //die();


        $struct_id = $this->get_template_vars('struct_id');


        //$data = $oDb->getRows("
        //    SELECT DISTINCT a.sprop4, c.psevdonim
        //    FROM art_struct as a
        //    INNER JOIN `category_psevdonims` as c
        //    ON (a.sprop4 = c.name AND a.is_deleted = 0)
        //    WHERE a.control = 'prod'
        //    AND a.sprop1 != '1'
        //    AND a.sprop4 != ''
        //    AND c.subdomen = $struct_id
        //    ORDER BY a.sprop4 ASC
        //");


        switch($struct_id) {
        case 54:
        $where_not_in = " AND t2.psevdonim NOT IN ('stoly-i-stoliki') ";
        break;
        case 55:
        $where_not_in = " AND t2.psevdonim NOT IN ('keramicheskaya-plitka', 'nastennye-bra', 'podvesnye-bra') ";
        break;
        default:
        $where_not_in = '';
        }

        $data = $oDb->getRows("
        SELECT t1.sprop4 as title, t2.psevdonim as name
        FROM art_struct AS t1
        JOIN category_psevdonims AS t2 ON t1.sprop4 = t2.name
        JOIN art_struct AS t3 ON t1.parent_id = t3.struct_id
        WHERE t1.sprop4 <> ''
        AND t3.parent_id = $struct_id
        $where_not_in
        AND t1.is_deleted = '0'
        AND t1.is_hidden = '0'
        GROUP BY t1.sprop4, t3.parent_id
        ORDER BY t2.psevdonim
        ");

        if(!empty($data)) {

        foreach ($data as $item) {
        $href = $item['name'];
        $text = $item['title'];
        echo "
        <div class='col-md-3'>
            <article>
                <h4>
                    <a href='$href/'>
                        <span>$text</span>
                    </a>
                </h4>
            </article>
        </div>
        ";
        }

        }

    {/php}
</div>
