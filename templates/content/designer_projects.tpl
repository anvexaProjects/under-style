{foreach from=$items item=i name=f}
{if $smarty.foreach.f.iteration eq 1}<h2>{$smarty.const.S_PROJECTS}</h2>{/if}
<a href="{$smarty.get.global.root|subdomain:$i.path}/{$i.path}/" class="disItem">
	<div class="img">
        {include_php
            file     = "master/img.inc.php"
            path     = "/upload/article"
            img      = $i.images.0.image_name
            tpl_name = "block/img.tpl"
            alt      = $i.name
            width    = "165"
            height   = "102"
            no_img   = "/images/site/empty_designer_progect.phg"
        }
	</div>
	<h3>{$i.name}</h3>
</a>
{/foreach}