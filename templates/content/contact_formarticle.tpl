{assign var=path  value=$smarty.get.global.struct.1.path}

<div id="registration">

    <h3 style="text-align: center;">Заявка на сотрудничество</h3>

    <form {$form_data.attributes}>
        {$form_data.javascript}
        {$form_data.hidden}

        {include file="ablock/error_block.tpl"}

        {if $smarty.get.s && $smarty.get.s == 1}
            <script>
                alert("{$smarty.const.LBL_SENT_SUCCESS}");
            </script>
        {/if}
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="input-group">
                    <input id="name" class="form-control" name="name" type="text" placeholder="{$smarty.const.S_INTRODUCTION}">
                </div>

                <div class="input-group">
                    <input id="phone" class="form-control" name="phone" type="text" placeholder="{$smarty.const.S_PHONE}">
                </div>

                <div class="input-group">
                    <input id="email" class="form-control" name="email" type="text" placeholder="{$smarty.const.S_EMAIL}">
                </div>

                <div class="input-group">
                    <textarea rows="3" id="message" class="form-control" name="message" placeholder="{$smarty.const.S_YOU_MESSAGE}"></textarea>
                </div>

                <br>

                <button class="btn btn-primary btn-block">Отправить</button>

            </div>

            <div style="display: none">
                {$form_data.field_1.html}{$form_data.field_2.html}
            </div>
        </div>
    </form>
</div>

