<link rel="stylesheet" href="../lib/elfinder-1.2/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" href="../lib/elfinder-1.2/css/elfinder.css" type="text/css" media="screen" title="no title" charset="utf-8">
<script src="../lib/elfinder-1.2/js/elfinder.min.js" type="text/javascript" charset="utf-8"></script>
<script language="javascript" type="text/javascript" src="../lib/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
{if $smarty.const.ALLOW_ADVANCED_MODE eq '1'}
{literal}
		tinyMCE.init({
            mode : "specific_textareas",
			editor_selector : "tiny_mce",
			theme : "advanced",
			height: "300",
			width: "99%",
			language : "{/literal}{$smarty.const.CMS_LANG}{literal}",
			plugins : "youtubeIframe,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,typograf,heading",
		
			theme_advanced_buttons1 : "fullscreen,preview,visualaid,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,sub,sup,|,cleanup,removeformat,attribs,|,image,youtubeIframe,media,typograf,code,help",
			theme_advanced_buttons2 : "h1,h2,h3,|,bold,italic,underline,strikethrough,|,styleselect,styleprops,fontsizeselect,forecolor,backcolor,|,link,unlink,anchor,|,hr,charmap,iespell,nonbreaking,",
			theme_advanced_buttons3 : "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,|,tablecontrols,",
				
		
		
			
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			file_browser_callback: function(field_name, url, type, win) {
            aFieldName = field_name, aWin = win;
  
                      if($('#elfinder').length == 0) {
                         $('body').append($('<div/>').attr('id', 'elfinder'));
                         $('#elfinder').elfinder({
                             url : '{/literal}{$smarty.const.SITE_REL}{literal}/lib/elfinder-1.2/connectors/php/connector.php',
                             dialog : { width: 900, modal: true, title: 'Files', zIndex: 400001 }, // open in dialog window
                             editorCallback: function(url) {
                                 aWin.document.forms[0].elements[aFieldName].value = url;
                             },
                             closeOnEditorCallback: true
                         });
                     } else {
                         $('#elfinder').elfinder('open');
                     }
                 },
			theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp,hr",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,
			convert_urls : true,
			relative_urls : false,
            forced_root_block: '',
			extended_valid_elements : "iframe[src|width|height|name|align|frameborder|allowfullscreen|id|scrolling]",
			content_css : "{/literal}{$smarty.const.SITE_REL}/templates/css/content.css?v={$smarty.const.APP_VERSION}{literal}"
		});
{/literal}
{else}
{literal}
		tinyMCE.init({
            mode : "specific_textareas",
			editor_selector : "tiny_mce",
			theme : "advanced",
			height: "300",
			width: "99%",
			language : "{/literal}{if $smarty.const.CMS_LANG eq 'ru' or $smarty.const.CMS_LANG eq 'de'}en{else}{$smarty.const.CMS_LANG}{/if}{literal}",
			plugins : "youtubeIframe,style,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable,xhtmlxtras",
		
		
			theme_advanced_buttons1 : "fullscreen,styleselect,undo,redo,pastetext,pasteword,|,bold,italic,underline,strikethrough,sub,sup,|,link,unlink,anchor,bullist,numlist,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,|,removeformat,image,youtubeIframe,code",
			theme_advanced_buttons2 : "",
	        theme_advanced_buttons3 : "",
		
		
			
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			file_browser_callback: function(field_name, url, type, win) {
            aFieldName = field_name, aWin = win;
  
                      if($('#elfinder').length == 0) {
                         $('body').append($('<div/>').attr('id', 'elfinder'));
                         $('#elfinder').elfinder({
                             url : '{/literal}{$smarty.const.SITE_REL}{literal}/lib/elfinder-1.2/connectors/php/connector.php',
                             dialog : { width: 900, modal: true, title: 'Files', zIndex: 400001 }, // open in dialog window
                             editorCallback: function(url) {
                                 aWin.document.forms[0].elements[aFieldName].value = url;
                             },
                             closeOnEditorCallback: true
                         });
                     } else {
                         $('#elfinder').elfinder('open');
                     }
                 },
			theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp,hr",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,
			convert_urls : true,
			relative_urls : false,
            forced_root_block: '',
			extended_valid_elements : "iframe[src|width|height|name|align|frameborder|allowfullscreen|id|scrolling]",
			content_css : "{/literal}{$smarty.const.SITE_REL}/templates/css/content.css?v={$smarty.const.APP_VERSION}{literal}"
		});

{/literal}
{/if}


</script>