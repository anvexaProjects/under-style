{assign var="cls" value="pager"}
<div class="pages">
{foreach item="page" from=$pages name="pages"}
	{strip}
		{if $active_page != 1 and $smarty.foreach.pages.first == "true"}
		<a class={$cls} href="{$init_url}page={math equation='x-1' x=$active_page}" ><<</a>&nbsp;
		{/if}
	{/strip}
	{strip}
		{if $active_page == $page}
			<b class={$cls} style="text-decoration: none;">{$page}&nbsp;</b>
		{else}
			<a class={$cls} href="{$init_url}page={$page}" >{$page}</a>&nbsp;
		{/if}
	{/strip}
	{strip}
		{if $active_page != $page and $smarty.foreach.pages.last == "true"}
			<a class={$cls} href="{$init_url}page={math equation='x+1' x=$active_page}" >>></a>
		{/if}
	{/strip}
{/foreach}
</div>