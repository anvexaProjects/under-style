{if !empty($form_data.errors) || !empty($error_msg)}
<div style="margin: 0px 17px 5px 0px;">
{strip}
	{foreach item="error_msg" from=$error_msg name="error_msg"} 
		<span class="err">{$error_msg}<br></span>
	{/foreach}

	{foreach item="e" from=$form_data.errors}
		<span class="err">{$e}</span><br>
		{assign var="is_err_occured" value="1"}
	{/foreach}
{/strip}
</div>
{/if}

