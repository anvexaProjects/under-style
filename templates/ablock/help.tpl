<div style="margin-right: -20px">
    <div style="float: right; font-size:10px; text-decoration:underline">
        <img align="absmiddle" src="{$path}images/icons/help.gif" alt="help" onClick="return ExpandHelp(this)">
        {*<span onClick="return ExpandHelp(this)">help</span>*}
    </div>
    
    <div style="border: 1px solid black; width: 300px; height: 315px; position:absolute; margin: 2px 40px 2px 2px; right: 10px; z-index:10; font-size: 11px" class="Hidden" id="help" onClick="return HideHelp(this)">
            <div style="color:#FFFFFF; background-color:#aaaaaa; text-align:center; border-bottom: 1px solid black; ">click to close</div>
            <div style="background-color:#ffffff; padding: 5px">
            <table>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/icons/add.gif" alt="add category"></td>
                    <td>&nbsp;-&nbsp;добавить раздел</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/icons/add2.gif" alt="add article"></td>
                    <td>&nbsp;-&nbsp;добавить публикацию</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/icons/edt.gif" alt="edit"></td>
                    <td>&nbsp;-&nbsp;редактировать публикацию</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/icons/delete.gif" alt="delete"></td>
                    <td>&nbsp;-&nbsp;удалить публикацию</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i1.gif" alt="category"></td>
                    <td>&nbsp;-&nbsp;раздел</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i2.gif" alt="content article"></td>
                    <td>&nbsp;-&nbsp;новость/публикация</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i15.gif" alt="special article"></td>
                    <td>&nbsp;-&nbsp;специальная статья</td>
                </tr>
                <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i8.gif" alt=""></td>
                    <td>&nbsp;-&nbsp;альбом</td>
                </tr>
			          <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i9.gif" alt=""></td>
                    <td>&nbsp;-&nbsp;фото</td>
                </tr>
 			          <tr>
                    <td><img align="absmiddle" src="{$path}images/tree/i10.gif" alt=""></td>
                    <td>&nbsp;-&nbsp;баннер</td>
                </tr>
            </table>
			<br>
		    <table>
                <tr>
                    <td valign="top"  class="st">фото&nbsp;*</td>
                    <td>&nbsp;-&nbsp;фото, отображаемое в выборках</td>
                </tr>
                <tr>
                    <td valign="top" class="st"><strong>статья</strong></td>
                    <td>&nbsp;-&nbsp;суперновость либо фото, которое является изображением для альбома</td>
                </tr>
			</table>
            </div>
    </div>
    <div class="clr"><!-- --></div>
</div>
