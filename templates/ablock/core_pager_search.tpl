{if $aPaging.totalPages>1}
<div class="pages floatl">
    <p class="floatl">{$smarty.const.S_SEARCH_TEXT}:</p>
{if $aPaging.firstUrl}<a href="{$aPaging.firstUrl}" class="pager_link">&lt;&lt;</a> &nbsp;{/if}

{foreach from=$aPaging.urls key=sUrl item=iPage}
{if $sUrl}<a href="{$sUrl}" class="pager_link">{$iPage}</a>{else}<a href="{$sUrl}" class="cur">{$iPage}</a>{/if} &nbsp;
{/foreach}

{if $aPaging.lastUrl}  <a href="{$aPaging.lastUrl}" class="pager_link">&gt;&gt;</a>{/if}
</div>
{/if}