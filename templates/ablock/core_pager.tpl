{if $aPaging.totalPages>1}
<div class="pages">
{if $aPaging.firstUrl}<a href="{$aPaging.firstUrl}" class="pager_link">&lt;&lt;</a> &nbsp;{/if}
{if $aPaging.prevUrl}<a href="{$aPaging.prevUrl}" class="pager_link">&lt;</a>&nbsp;{/if}&nbsp;

{foreach from=$aPaging.urls key=sUrl item=iPage}
{if $sUrl}<a href="{$sUrl}" class="pager_link">{$iPage}</a>{else}{$iPage}{/if} &nbsp;
{/foreach}

{if $aPaging.nextUrl} <a href="{$aPaging.nextUrl}" class="pager_link">&gt;</a>&nbsp;{/if}
{if $aPaging.lastUrl}  <a href="{$aPaging.lastUrl}" class="pager_link">&gt;&gt;</a>{/if}
</div>
{/if}