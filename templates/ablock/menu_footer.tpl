{foreach from=$amenu item=item name=menu}
{if '#'==$item.path}
  {if $item.child.0.path eq $script_file}
    <span>{$item.title}</span>
  {else}
    <a href="{$item.child.0.path}" title="{$item.title}">{$item.title}</a>
  {/if}
{else}
  {if $item.path eq $script_file}
    <span>{$item.title}</span>
  {else}
    <a href="{$item.path}" title="{$item.title}">{$item.title}</a>
  {/if}
{/if}

{if !$smarty.foreach.menu.last} | {/if} 
{/foreach}
