{strip}
	{assign var="edt" value="edt.gif"}

	{* delete icon  *}
	{if ($item.control neq 'article' and $item.control neq 'prod_lev_1' and $item.control neq 'prod_lev_2' and $item.control neq 'prod_lev_3'
		 and $item.control neq 'prod_lev_4') or $item.path eq '404'}
		{assign var="del" value="sp.gif"}{else}
	{assign var="del" value="delete.gif"}{/if}
	   
	   
	{* add icon  $item.alias *} 
	{if ($item.control neq 'article' and $item.control neq 'prod_lev_1' and $item.control neq 'prod_lev_2' and $item.control neq 'prod_lev_3') or $item.path eq '404'}
		{assign var="add" value="sp.gif"}
	{else}
		{assign var="add" value="add2.gif"}{/if}


	{* add cat icon *} 
	{* if $item.type eq '2' or $item.type eq '15' or $item.type eq '6' or $item.type eq '8' or $item.type eq '9' or $item.type eq '10' 
		or ($item.level > 1 and $item.parentid neq '566')
		or $item.alias eq 'gallery' or $item.alias eq 'forum' or $item.alias eq 'banner'}
	  {assign var="add_cat" value="sp.gif"}
	{else}
	  {assign var="add_cat" value="add.gif"}
	{/if *}
	 {assign var="add_cat" value="sp.gif"}

	{* set child type  *}
	{if $item.path eq 'shop'}{assign var="child_type" value="prod_lev_1"}
	{elseif $item.control eq 'prod_lev_1'}{assign var="child_type" value="prod_lev_2"}
	{elseif $item.control eq 'prod_lev_2'}{assign var="child_type" value="prod_lev_3"}
	{elseif $item.control eq 'prod_lev_3'}{assign var="child_type" value="prod_lev_4"}
	{else}{assign var="child_type" value="article"}{/if}

	<div class="row">
	{if $item.control eq 'prod_lev_2'}
		<a href="articles.php?cat_id={$item.id}">
			<img align="absmiddle" src="{$path}images/icons/go.gif">
		</a>&nbsp;&nbsp;
	{else}
		<img align="absmiddle" src="{$path}images/icons/sp.gif" >&nbsp;&nbsp;
	{/if}
	
	
	{if $edt neq 'sp.gif'}
		{if $item.control eq 'prod_lev_3'}
			<a href="set_edit.php?id={$item.raw_id}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$edt}">
			</a>
		{elseif $item.control eq 'prod_lev_4'}
			<a href="product_edit.php?id={$item.raw_id}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$edt}">
			</a>
		{else}
			<a href="art_edit.php?id={$item.raw_id}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$edt}" >            
			</a>
		{/if}&nbsp;&nbsp;
	{else}
		<img align="absmiddle" src="{$path}images/icons/sp.gif" >&nbsp;&nbsp;
	{/if}
	
	{if $del neq 'sp.gif'}
    {if $item.control eq 'prod_lev_3'}
		<a href="set_del.php?id={$item.raw_id}&bur={$bur}">
			<img align="absmiddle" src="{$path}images/icons/{$del}" onClick="return DelIt(this);">
		</a>
    {elseif $item.control eq 'prod_lev_4'}
    <a href="product_del.php?id={$item.raw_id}&bur={$bur}">
			<img align="absmiddle" src="{$path}images/icons/{$del}" onClick="return DelIt(this);">
		</a>
    {else}
		<a href="art_del.php?id={$item.raw_id}&bur={$bur}">
			<img align="absmiddle" src="{$path}images/icons/{$del}" onClick="return DelIt(this);">
		</a>
		{/if}&nbsp;&nbsp;
	{else}
		<img align="absmiddle" src="{$path}images/icons/sp.gif" >&nbsp;&nbsp;
	{/if}
		
	
	{if $add neq 'sp.gif'}			
		{if $item.control eq 'prod_lev_2'}
			<a href="set_edit.php?parent_id={$item.raw_id}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$add}">
			</a>
		{elseif $item.control eq 'prod_lev_3'}
			<a href="product_edit.php?parent_id={$item.raw_id}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$add}">
			</a>
		{else}
			<a href="art_edit.php?pid={$item.raw_id}&control={$child_type}&bur={$bur}">
				<img align="absmiddle" src="{$path}images/icons/{$add}">
			</a>
		{/if}&nbsp;&nbsp;
	{else}
		<img align="absmiddle" src="{$path}images/icons/sp.gif" >&nbsp;&nbsp;
	{/if}

	{if $add_cat neq 'sp.gif'}				
		<a href="art_edit.php?pid={$item.raw_id}&control=article&bur={$bur}">
			<img align="absmiddle" src="{$path}images/icons/{$add_cat}">
		</a>&nbsp;&nbsp;
	{else}
		<img align="absmiddle" src="{$path}images/icons/sp.gif" >&nbsp;&nbsp;
	{/if}


	</div>    
{/strip}			
			