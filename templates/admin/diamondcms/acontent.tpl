{if $page eq 'index'}
    {include_php file='admin/articles.inc.php'}
{elseif $page eq 'art_edit'}
    {include_php file='admin/art_edit.inc.php'}
{elseif $page eq 'users'}
    {include_php file="admin/user_list.inc.php"}
{elseif $page eq 'user_edit'}
    {include_php file="admin/user_edit.inc.php"}
{elseif $page eq 'robots'}
    {include_php file="admin/robots.inc.php"}
{elseif $page eq 'config'}
    {include_php file="admin/config.inc.php"}
{elseif $page eq 'slider'}
    {include_php file="admin/slider.inc.php"}
{elseif $page eq 'dicts'}
    {include_php file="admin/dicts.inc.php"}
{elseif $page eq 'feedbacks'}
    {include_php file="admin/feedbacks.inc.php"}
{elseif $page eq 'reqs'}
    {include_php file="admin/reqs.inc.php"}
{elseif $page eq 'reqs_edit'}
    {include_php file="admin/reqs_edit.inc.php"}
{elseif $page eq 'orders'}
    {include_php file="admin/order_list.inc.php"}
{elseif $page eq 'order_edit'}
    {include_php file="admin/order_edit.inc.php"}
{elseif $page eq 'export_import'}
    {include_php file="admin/export_import.inc.php"}
{elseif $page eq 'resources'}
    {include_php file="admin/resources.inc.php"}
{elseif $page eq 'subscription'}
    {if $smarty.get.a eq 'manage_contacts'}
        {include_php file="admin/subscription/manage_contacts.inc.php"}
    {elseif $smarty.get.a eq 'manage_contacts_groups'}
        {include_php file="admin/subscription/manage_contacts_groups.inc.php"}
    {elseif $smarty.get.a eq 'contacts_import'}
        {include_php file="admin/subscription/contacts_import.inc.php"}
    {elseif $smarty.get.a eq 'statistics'}
        {include_php file="admin/subscription/statistics.inc.php"}
    {else}
        {include_php file="admin/subscription/new_email.inc.php"}{/if}

{elseif $page eq 'db_admin'}
    {include file="admin/`$smarty.const.CMS`/db_admin.tpl"}
{else}{include file='ablock/404.tpl'}
{/if}
