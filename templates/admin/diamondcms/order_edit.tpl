<div class="path">&nbsp;</div>
<div class="tab_block shiftTop_m30">
    <ul>
        <li id="#tab-status" class="here">{$smarty.const.S_ORDER}</li>
    </ul>
</div>
<div id="editContent" class="block shiftTop_30">
	<div id="tab-status" class="contentBlock">
    <form {$form_data.attributes}>
    {$form_data.hidden}
    {$form_data.status.html}&nbsp;
    <input type="submit" class="inp save" value="&nbsp;&nbsp;{$smarty.const.S_SAVE}&nbsp;&nbsp;"/>
    <input type="button" onclick="window.location='orders.php'" class="inp undo" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;"/>
    <br /><br /><br />
    </form>
    <table class="edit_tbl">
        <tr class="grey">
    		<td width="30%" class="black">{$smarty.const.S_NAME}</td>
    		<td width="20%" class="black">{$smarty.const.S_PRICE}</td>
    		<td width="20%" class="black">{$smarty.const.S_QUANTITY}</td>		
         	<td width="30%" class="black">{$smarty.const.S_AMOUNT}</td>
        </tr>
        {foreach item="item" name="item" from=$order.items}     
        <tr{cycle values='"", class="grey"'}>
            <td>
              <strong>{$item.product}</strong>
            </td>
            <td>{$item.price|string_format:"%.2f"}&nbsp;&euro;</td>
            <td>{$item.num}</td>
            <td>{$item.item_price|string_format:"%.2f"}&nbsp;&euro;</td>
        </tr>
        {/foreach}
         <tr >
            <td colspan="2">&nbsp;</td>
            <td><strong>{$smarty.const.S_SUBTOTAL}:</strong></td>
            <td>{$order.price|string_format:"%.2f"}&nbsp;&euro;</td>
        </tr>
         <tr >
            <td colspan="2">&nbsp;</td>
            <td><strong>{$smarty.const.S_REGULAR_DISCOUNT}:</strong></td>
            <td>{$order.items.0.regular_discount}%</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>{$smarty.const.S_TOTAL2}:</strong></td>
            <td><strong>{$order.total|string_format:"%.2f"}</strong>&nbsp;&euro;</td>
        </tr>
    </table>
    <br>    
  
	</div>
	<div id="tab-address" class="contentBlock">
        <table style="width: 100%" class="edit_tbl">
            <tr>
                <td width="20%"><strong>{$smarty.const.S_NAME}:</strong></td>
                <td width="1%">&nbsp;</td>
                <td width="79%">{$order.items.0.oname}</td>
            </tr>
			<tr>
				<td>{$smarty.const.S_BIRTH_DATE}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.birthday|date_format:"%d.%m.%Y"}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_EMAIL}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.email}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_SEX}:</td>
				<td>&nbsp;</td>
				<td>{$enum.sex[$order.items.0.sex]}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_FIRST_NAME}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.name}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_LAST_NAME}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.lname}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_MIDDLE_NAME}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.mname}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_STREET} / {$smarty.const.S_HOUSE_N}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.street} / {$order.items.0.house}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_PLZ} / {$smarty.const.S_ORT}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.plz} / {$order.items.0.ort}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_COUNTRY}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.country}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_PHONE}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.phone}</td>
			</tr>
			<tr>
				<td>{$smarty.const.S_FAX}:</td>
				<td>&nbsp;</td>
				<td>{$order.items.0.fax}</td>
			</tr>   
        </table>
 	</div>
</div>