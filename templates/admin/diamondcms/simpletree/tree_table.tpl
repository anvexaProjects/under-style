<table cellspacing="0" width="95%" style="border-bottom-width: 0px; margin-right: -20px; padding-right: 0px; width: 95%">
    <tr>
	    <td>
	    {if $smarty.get.global.struct.1.level neq 0}
	    <a href="javascript:loadTableView('{$smarty.get.global.struct.1.parent_id}')"><img src="{$smarty.const.SITE_REL}/images/tree/dir_up7.gif" alt="{$smarty.const.S_UP}" title="{$smarty.const.S_UP_LEVEL}" /></a>
	    {/if}
	    </td>
	    <td>
			{foreach from=$struct item=i name=struct}
			{if !$smarty.foreach.struct.last}
			<a href="javascript:loadTableView('{$i.id}')">{$i.name}</a> /
			{else}
			<b>{$i.name}</b>
			{/if}
			{/foreach}
		</td>
		<td width="250" nowrap="nowrap" align="right">
            {if $saler_red || $des_red}
                {if $i.struct_id eq '67' && $saler_red}
                    {include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i mode="top"}
                {elseif $i.struct_id eq '68' && $des_red}
                    {include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i mode="top"}
                {/if}
            {else}
			{include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i mode="top"}
            {/if}
		</td>
	</tr>
{foreach from=$tree item=i name=tree}
    <tr id="row_{$i.id}">
        <td width="16">
            {if $i.subitem_count > 0}<img src="../images/tree/fl.gif">{else}<img src="../images/tree/doc.gif">{/if}
        </td>

        <td {if $i.is_hidden eq 1}style="color: #999999"{/if}>
            &nbsp;
            {if $i.subitem_count>0}
                <a href="javascript:loadTableView('{$i.id}')">

                    {/if}

                    {$i.name}

                    {*{$i.name|truncate:$smarty.const.TRUNCATE_ARTICLES_NAMES_IN_SIMPLE_TREE:"...":true}*}

                    {*{$smarty.const.TRUNCATE_ARTICLES_NAMES_IN_SIMPLE_TREE|@print_r}*}

                    {if $i.subitem_count>0}
                </a>
            {/if}
        </td>

		<td width="190" nowrap="nowrap" align="right">
			{if !$smarty.foreach.tree.first && $i.level > 1}
				<a href="javascript:changeOrder('{$i.id}', 'up')"><img class="updown" src="../images/icons/up.gif"></a>
			{else}
				<img src="../images/icons/sp.gif" alt="" />
			{/if}
			{if !$smarty.foreach.tree.last && $i.level > 1}
				<a href="javascript:changeOrder('{$i.id}', 'down')"><img class="updown" src="../images/icons/down.gif"></a>
			{else}
				<img src="../images/icons/sp.gif" alt="" />
			{/if}
			<img src="../images/icons/sp.gif" alt="" />
            {if $saler_red || $des_red}
                {if ($i.control eq 'pricelist' && $i.parent_id eq 67) && $saler_red}
                    {include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i}
                {elseif ($i.control eq 'pricelist' && $i.parent_id eq 68) && $des_red}
                    {include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i}
                {/if}
            {else}
                {include file="admin/`$smarty.const.CMS`/simpletree/tree_icons.tpl" i=$i}
            {/if}
		</td>
    </tr>
{/foreach}
</table>