<ul class="simpleTree">
    <li class="root" id='0'>
        <ul>
        {foreach from=$tree item=i}
            <li id="{$i.ord}_{$i.id}" class="doc"><span class="text" id="{$i.id}">{$i.name}</span>
            {if $i.subitem_count > 0}
                <ul class="ajax">
                    <li id="{$i.ord}">{ldelim}url:simpletree_async.php?id={$i.id}&action=list&tree_id={$smarty.get.tree_id}{rdelim}</li>
                </ul>
            {/if}
            </li>
        {/foreach}
        </ul>
    </li>
</ul>