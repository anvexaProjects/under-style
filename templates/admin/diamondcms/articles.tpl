<script language="javascript">
    tid = "{$smarty.get.bid}";
    asyncUrl = "simpletree_async.php";
</script>
<script language="javascript" type="text/javascript" src="../templates/js/simpletree/jquery.getUrlParam.js"></script>
<script language="javascript" type="text/javascript" src="../templates/js/simpletree/jquery.simple.tree.js?asyncUrl=simpletree_async.php&drag={$smarty.const.ALLOW_DRAG_SIMPLE_TREE}" id="js"></script>
<script language="javascript" type="text/javascript" src="../templates/js/simpletree/simpletree.js"></script>
<link rel="stylesheet" type="text/css" href="../templates/css/simpletree.css">
<h1>{$smarty.const.S_SITEMAP}</h1>
<table width="99%" cellspacing="0">
    <tr>
        <td width="30%" id="tree"></td>
        <td width="70%" id="data"></td>
        <td width="30%"></td>
    </tr>
</table>

<script language="javascript">loadTableView("{if $smarty.get.bid}{$smarty.get.bid}{else}1{/if}");</script>