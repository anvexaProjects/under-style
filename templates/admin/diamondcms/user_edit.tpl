<h1>{if $smarty.request.id}{$smarty.const.S_USER_EDIT}{else}{$smarty.const.S_USER_ADD}{/if}</h1>
<div class="tab_block">
    <ul>
        <li id="#tab-main" class="here">{$smarty.const.S_PROPERTIES}</li>
    </ul>
</div>
<div id="editContent2" class="block">
    {if $smarty.get.s|default:0 eq 1}<b>{$smarty.const.FLB_OPERATION_OK}</b>{/if}
	<div style="margin: 0px 17px 5px 17px">
	{include file="ablock/error_block.tpl"}
	{$form_data.javascript}
	</div>
    <form {$form_data.attributes} enctype="multipart/form-data">
    <div id="tab-main">
        <fieldset>
            <legend>{$smarty.const.S_USER_INFO}</legend>
            <div>
			{if $mode eq 'edit'}<p><em>{$smarty.const.LBL_BLANK_PASSWORD_NO_EDIT}</em></p>{/if}            
                <table class="fieldset">
                    <tr>
                        <td class="colLbl" align="right"><label for="login">{$smarty.const.S_LOGIN}</label>&nbsp;<b><font color="#FF0000">*</font></b></td>
                        <td class="colInp">{$form_data.login.html}</td>                    
                        <td class="colLbl" align="right"><label for="is_locked">{$smarty.const.S_IS_LOCKED}</label></td>
                        <td class="colInp">{$form_data.is_locked.html}</td>                                     
                    </tr>
                    <tr>
                        <td class="colLbl" align="right"><label for="pass1">{$smarty.const.S_PASSWORD}</label>{if $mode eq 'add'}&nbsp;<b><font color="#FF0000">*</font></b>{/if}</td>
                        <td class="colInp">{$form_data.pass1.html}</td>   
                        <td class="colLbl" align="right"><label for="name">{$smarty.const.S_USERNAME}</label></td>
                        <td class="colInp">{$form_data.name.html}</td>                                     
                    </tr>
                    <tr>
                        <td class="colLbl" align="right"><label for="pass2">{$smarty.const.S_CONFIRM_PASSWORD}</label>{if $mode eq 'add'}&nbsp;<b><font color="#FF0000">*</font></b>{/if}</td>
                        <td class="colInp">{$form_data.pass2.html}</td> 
                        <td class="colLbl" align="right"><label for="email">{$smarty.const.S_EMAIL}</label></td>
                        <td class="colInp">{$form_data.email.html}</td>                              
                    </tr>
                    <tr>
                    	<td class="colLbl" align="right"><label for="role">{$smarty.const.S_ROLE}</label></td>    
                        <td class="colInp">{$form_data.role.html}</td>                 
                </table>
            </div>
        </fieldset>
</div>
        {$form_data.hidden}
        <div class="edit_action_btn">
        	 <input class="inp save" type="submit" value="{$smarty.const.S_SAVE_CLOSE}" name="save_close"/>
             <input class="inp save" type="submit" value="{$smarty.const.S_SAVE}" name="save"/>
             <input type="button" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;" class="inp undo" onClick="window.location='{$smarty.get.bur|default:'users.php'}'">
        </div>
    </form>
</div>
