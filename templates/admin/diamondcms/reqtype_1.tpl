<h1>{$smarty.const.S_REQUEST}</h1>
<div class="tab_block">
    <ul>
        <li id="#tab-main" class="here">{$smarty.const.S_PROPERTIES}</li>
    </ul>
</div>
<div id="editContent2" class="block">
    <div id="tab-main">
        <fieldset>
            <div>
                <table class="fieldset">
                    <tr>
                        <td align="right"><b>{$smarty.const.S_TYPE}:&nbsp;</b></td>
                        <td>{$smarty.const.S_ORDER_TYPE_1}</td>
                        <td align="right"><b>{$smarty.const.S_PRODUCT}:&nbsp;</b></td>
                        <td>{$item.product}</td>
                        <td align="right"><b>{$smarty.const.S_AANTAL_STICKERS}:&nbsp;</b></td>
                        <td>{$item.aantal_stickers}</td>
                    </tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_AANTAL_KLEUREN}:&nbsp;</b></td>
                        <td>{$item.aantal_kleuren}</td>
                        <td align="right"><b>{$smarty.const.S_KLEUR_OVERIGE}:&nbsp;</b></td>
                        <td>{$item.kleur_overige}</td>
                        <td align="right"><b>{$smarty.const.S_BREEDTE}:&nbsp;</b></td>
                        <td>{$item.breedte}</td>
                    </tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_HOOGTE}:&nbsp;</b></td>
                        <td>{$item.hoogte}</td>
                        <td align="right"><b>{$smarty.const.S_AFLOPEND_IN_DE_BREEDTE}:&nbsp;</b></td>
                        <td>{$item.aflopend_breedte}</td>
                        <td align="right"><b>{$smarty.const.S_AFLOPEND_IN_DE_HOOGTE}:&nbsp;</b></td>
                        <td>{$item.aflopend_hoogte}</td>
                    </tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_MATERIAAL}:&nbsp;</b></td>
                        <td>{$item.materiaal}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_BEDRIJFSNAAM}:&nbsp;</b></td>
                        <td>{$item.bedrijfsnaam}</td>
                        <td align="right"><b>{$smarty.const.S_AANHEF}:&nbsp;</b></td>
                        <td>{$item.aanhef}</td>
                        <td align="right"><b>{$smarty.const.S_VOORNAAM}:&nbsp;</b></td>
                        <td>{$item.voornaam}</td>
                    </tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_TUSSENVOEGSEL}:&nbsp;</b></td>
                        <td>{$item.tussenvoegsel}</td>
                        <td align="right"><b>{$smarty.const.S_ACHTERNAAM}:&nbsp;</b></td>
                        <td>{$item.achternaam}</td>
                        <td align="right"><b>{$smarty.const.S_ADRES}:&nbsp;</b></td>
                        <td>{$item.adres}</td>
                    </tr>
                    <tr>
                        <td align="right" nowrap><b>{$smarty.const.S_HUISNUMMER_TOEVOEGING}:&nbsp;</b></td>
                        <td>{$item.huisnummer_toevoeging}</td>
                        <td align="right"><b>{$smarty.const.S_POSTCODE}:&nbsp;</b></td>
                        <td>{$item.postcode}</td>
                        <td align="right"><b>{$smarty.const.S_PLAATS}:&nbsp;</b></td>
                        <td>{$item.plaats}</td>
                    </tr> 
                    <tr>
                        <td align="right"><b>{$smarty.const.S_LAND}:&nbsp;</b></td>
                        <td>{$item.land}</td>
                        <td align="right"><b>{$smarty.const.S_TELEFOONNUMMER}:&nbsp;</b></td>
                        <td>{$item.telefoonnummer}</td>
                        <td align="right"><b>{$smarty.const.S_FAXNUMMER}:&nbsp;</b></td>
                        <td>{$item.faxnummer}</td>
                    </tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_EMAIL}:&nbsp;</b></td>
                        <td><a href="mailto:{$item.email}">{$item.email}</a></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>                        
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td align="right"><b>{$smarty.const.S_CREATED}:&nbsp;</b></td>
                        <td>{$item.created|date_format:"%d.%m.%Y"}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>                        
                </table>
            </div>
        </fieldset>
    </div>
    <div class="edit_action_btn">
         <input type="button" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;" class="inp undo" onClick="window.location='{$smarty.get.bur|default:'reqs.php'}'">
    </div>
</div>
