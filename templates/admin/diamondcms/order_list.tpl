<div id="editContent" class="block">
    <form name="" method="post" action="">
        <table class="edit_tbl">
            <tr class="grey">
                <td nowrap><a href="{$aSorting.order_id.url}">{$smarty.const.S_ORDER}&nbsp;#</a>&nbsp;<img src="../images/icons/s_{$aSorting.order_id.img}"></td>
        		<td nowrap><a href="{$aSorting.ordered.url}" class="tbl_header">{$smarty.const.S_DATE}</a>&nbsp;<img src="../images/icons/s_{$aSorting.ordered.img}"></td>
        		<td nowrap><a href="{$aSorting.status.url}" class="tbl_header">{$smarty.const.S_STATUS}</a>&nbsp;<img src="../images/icons/s_{$aSorting.status.img}"></td>
        		<td nowrap><a href="{$aSorting.total.url}" class="tbl_header">{$smarty.const.S_TOTAL}</a>&nbsp;<img src="../images/icons/s_{$aSorting.total.img}"></td>
                <td nowrap><a href="{$aSorting.customer.url}" class="tbl_header">{$smarty.const.S_CUSTOMER}</a>&nbsp;<img src="../images/icons/s_{$aSorting.customer.img}"></td>
                <td nowrap><a href="{$aSorting.email.url}" class="tbl_header">{$smarty.const.S_EMAIL}</a>&nbsp;<img src="../images/icons/s_{$aSorting.email.img}"></td>
	       		<td nowrap>&nbsp;</td>
            </tr>
            <tr class="grey">
        		<td valign="top" align="left" class="tblEm">{filter data=$aFilter.order_id class="htmlInput" style="width: 20px"}</td>
                <td valign="top" align="left" class="tblEm" nowrap>
                    {$smarty.const.S_FROM}&nbsp;{filter data=$aFilter.ordered_from class="htmlInput" style="width: 50px"}
                    {$smarty.const.S_TO}&nbsp;{filter data=$aFilter.ordered_to class="htmlInput" style="width: 50px"}
            	</td>
				<td valign="top" align="left" class="tblEm">{filter data=$aFilter.status class="htmlInput" style="width: 50px"}</td>
        		<td valign="top" align="left" class="tblEm" nowrap>
                    {$smarty.const.S_FROM}&nbsp;{filter data=$aFilter.total_from class="htmlInput" style="width: 50px"}
                    {$smarty.const.S_TO}&nbsp;{filter data=$aFilter.total_to class="htmlInput" style="width: 50px"}
            	</td>           
                <td valign="top" align="left" class="tblEm">{filter data=$aFilter.customer class="htmlInput" style="width: 50px"}</td>
                <td valign="top" align="left" class="tblEm">{filter data=$aFilter.email class="htmlInput" style="width: 50px"}</td>
        		<td valign="top" align="left" width="1%" nowrap  class="tblEm" style="vertical-align: top">
        			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</td>
            </tr>
            {foreach item="item" name="item" from=$items}
            <tr{cycle values=', class="grey"'}>
                <td><a href="order_edit.php?id={$item.order_id}" >{$item.order_id}</a></td>
                <td>{$item.ordered|date_format:"%d.%m.%Y %H:%M"}</td>
                <td>{$status[$item.status]}</td>
                <td>{$item.total|string_format:"%.2f"}&nbsp;&euro;</td>
                <td>{$item.customer}</td>
                <td><a href="mailto:{$item.email}">{$item.email}</a></td>
                <td>
                    <a href="order_edit.php?id={$item.order_id}" ><img src="../images/icons/edt.gif" alt=""></a>
                </td>
            </tr>
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="10"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</div>