{include file="admin/"|cat:$smarty.const.CMS|cat:"/subscription/menu.tpl"}
{assign var="subsrc" value='src="'|cat:$smarty.const.SITE_REL|cat:"/"}
<div id="editContent" class="block">
    <form name="" method="post" action="">
        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
            <tr> 
        		<th><a href="{$aSorting.subject.url}">{$smarty.const.S_SUBJECT}</a>&nbsp;<img src="../images/icons/s_{$aSorting.subject.img}"></th>
                <th><a href="{$aSorting.contacts_num.url}">{$smarty.const.S_CONTACTS_NUM}</a>&nbsp;<img src="../images/icons/s_{$aSorting.contacts_num.img}"></th>
                <th><a href="{$aSorting.is_test.url}">{$smarty.const.S_IS_TEST_EMAIL}</a>&nbsp;<img src="../images/icons/s_{$aSorting.is_test.img}"></th>
                <th><a href="{$aSorting.created.url}">{$smarty.const.S_CREATED}</a>&nbsp;<img src="../images/icons/s_{$aSorting.created.img}"></th>
				<th>&nbsp;</th>
            </tr>
            <tr>
        		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.subject class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">&nbsp;</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.is_test class="htmlInput"}</th>
				<th>&nbsp;</th>
        		<th valign="top" align="left" width="1%" nowrap  class="tblEm">
	       			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</th>
            </tr>       
            {foreach item="item" name="item" from=$items key=k}     
            <tr {cycle values=', class="grey"'} id="view__{$item.id}">
                <td>{$item.subject}</a></td>
                <td>{$item.contacts_num}</a></td>
                <td>{$bool[$item.is_test]}</a></td>
                <td>{$item.created|date_format:"%d.%m.%Y %H:%M"}</td>
                <td nowrap>
					<img src="../images/icons/stats.gif" class="inline_img" alt="" onclick="toggleStat('s_{$item.id}')"/>
					<a href="../pub/subscription_iframe.php?m={$item.id}&&TB_iframe=true&height=600&width=800&modal=true" class="thickbox"><img src="../images/icons/go.gif" alt="" /></a>
                    {*<img src="../images/icons/delete.gif" class="inline_img" onclick="inlineDelete('{$item.id}', 'stats')" alt="{$smarty.const.S_DELETE}" />
					<div id="mail_{$item.id}" style="display: none;">{$item.template|replace:'src="':$subsrc}</div>*}
                </td>
            </tr>
            <tr class="s_{$item.id} hidden"><td colspan="2">&nbsp;</td><td><b>{$smarty.const.S_EMAIL_COUNTER}</b></td><td><b>{$smarty.const.S_EMAIL_COUNTER_PERCENT}</b></td><td>&nbsp;</td></tr>
            <tr class="s_{$item.id} hidden"><td colspan="2">&nbsp;</td><td>{$smarty.const.S_EMAILS_SENT}:</td><td>{$item.sent_items} ({$item.sent_items*100/$item.contacts_num|@round}%)</td><td>&nbsp;</td></tr>
            <tr class="s_{$item.id} hidden"><td colspan="2">&nbsp;</td><td>{$smarty.const.S_EMAILS_OPENED}:</td><td>{$item.viewed_items} ({$item.viewed_items*100/$item.contacts_num|@round}%)</td><td>&nbsp;</td></tr>
            <tr class="s_{$item.id} hidden"><td colspan="2">&nbsp;</td><td>{$smarty.const.S_EMAILS_OPENED_IN_BROWSER}:</td><td>{$item.viewed_in_browser_items} ({$item.viewed_in_browser_items*100/$item.contacts_num|@round}%)</td><td>&nbsp;</td></tr>
            {* <tr class="s_{$item.id} hidden"><td colspan="2">&nbsp;</td><td>{$smarty.const.S_EMAILS_TRANS_OPENED_IN_BROWSER}:</td><td>{$item.viewed_translated_items} ({$item.viewed_translated_items*100/$item.contacts_num|@round}%)</td><td>&nbsp;</td></tr> *}
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="6"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</div>