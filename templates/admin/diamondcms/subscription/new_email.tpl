{include file="admin/"|cat:$smarty.const.CMS|cat:"/subscription/menu.tpl"}
{literal}
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$("#newmail_preview").click(function(){
		var html = ('<a href="#" onclick="{window.parent.tb_remove(); return false;}"><img src="/images/lightbox/close.gif" align="right" style="padding: 5px;" /></a>' + tinyMCE.get('newemail_text').getContent()).
            replace(new RegExp('src="', 'g'), 'src="' + root + '/').
            replace(new RegExp("src='", 'g'), "src='" + root + "/");
		$("#current_tpl").empty().html(html);
		$("#newmail_preview_link").trigger("click");
	});
});

function escapeHTML (str)
{
   var div = document.createElement('div');
   var text = document.createTextNode(str);
   div.appendChild(text);
   return div.innerHTML;
};

tinyMCE.init({
	plugins : "table,safari,style,layer,table,advhr,advimage,advlink,contextmenu,paste,directionality,noneditable,nonbreaking,xhtmlxtras,template,pagebreak",

    height: "500",
    width: "99%",        
    mode : "specific_textareas",
    elements : "tiny_mce",
    theme : "advanced",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",

    theme_advanced_buttons1 : "styleprops,bold,italic,underline,|,bullist,numlist,outdent,indent,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,image,hr,|,removeformat,cleanup,attribs,code",    
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    file_browser_callback : "ajaxfilemanager",
	convert_urls : false,
//	relative_urls: false,
	external_link_list_url: '{/literal}{$smarty.const.SITE_REL}{literal}/templates/js/subscription_links.js',
	document_base_url: '{/literal}{$smarty.const.SITE_REL}/{literal}'
});
    
function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "{/literal}{$smarty.const.SITE_REL}{literal}/lib/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
        break;
        case "media":
        break;
        case "flash": 
        break;
        case "file":
        break;
        default:
        return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "{/literal}{$smarty.const.SITE_REL}{literal}/lib/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 440,
        inline : "yes",
        close_previous : "no"
    },
    {
        window : win,
        input : field_name,
    });
}    
</script>
{/literal}
<div id="editContent2" class="block">
	<fieldset>
        <legend>{$smarty.const.S_SUBSCRIPTION_TEMPLATES}</legend>
		<table class="grid">
			{foreach from=$tpls item=i key=k}
			<tr id="view__{$i.id}">
				<td><a href="subscription.php?id={$i.id}">{if $i.is_default}<strong>{/if}{$i.subject}</a>{if $i.is_default}</strong>{/if}</td>
				<td width="18"><img src="../images/icons/delete.gif" class="inline_img" onclick="inlineDelete('{$i.id}', 'tpl')" alt="{$smarty.const.S_DELETE}" /></td>
			<tr>
			{/foreach}
		</table>
	</fieldset>
    
	<form method="post" enctype="multipart/form-data">
	<fieldset>
        <legend>{$smarty.const.S_SUBSCRIPTION_NEW}</legend>
        
        {include file="ablock/error_block.tpl"}
        {if $smarty.get.s}<p><strong>{$smarty.const.FLB_OPERATION_OK}</strong></p>{/if}
		<p>{$smarty.const.S_SUBJECT}: <input type="text" name="newmail_subj" style="width:80%;" value="{if !isset($smarty.post.newmail_subj)}{$ctpl.subject}{else}{$smarty.post.newmail_subj}{/if}" /></p>
		<p>{$smarty.const.S_NEW_EMAIL}</p>
		<textarea rows="30" name="newemail_text" class="tiny_mce" id="newemail_text">{$smarty.post.newemail_text|default:$ctpl.template}</textarea>
		{*<br /><p>{$smarty.const.S_NEW_TRANSLATED_EMAIL}</p>
		<textarea rows="30" name="newemail_trans_text" class="tiny_mce" id="newemail_trans_text">{$smarty.post.newemail_trans_text|default:$ctpl.template_translated}</textarea>
		*}
		<p>
			{$smarty.const.S_GROUP}:&nbsp;
			<select name="group" style="width: 150px;">
				{foreach from=$groups item=i key=k}
				<option value="{$k}"{if $smarty.post.group eq $k} selected="selected"{/if}>{$i}</option>
				{/foreach}
			</select>		
			<input type="submit" class="inp send" value="{$smarty.const.S_SEND}" name="newmail_send" />
			<input type="submit" class="inp send" value="{$smarty.const.S_SEND_TEST}" name="newmail_send_test" />
			<input type="button" class="inp prev" value="{$smarty.const.S_PREVIEW}" name="newmail_preview" id="newmail_preview"  />
			<input type="submit" class="inp save" value="{$smarty.const.S_SAVE}" name="newmail_save" />
            <input type="submit" class="inp save" value="{$smarty.const.S_SAVE_AS_NEW}" name="newmail_save_as_new" />
            <input type="submit" class="inp dflt" value="{$smarty.const.S_SAVE_AS_DEFAULT}" name="newmail_set_default" />
            
            <input type="hidden" value="{$id}" name="id" />
		</p>
    </fieldset>
	</form>
</div>
<a href="#TB_inline?height=800&amp;width=940&amp;inlineId=current_tpl&modal=true" class="thickbox" id="newmail_preview_link" style="display:none"></a>
<div id="current_tpl" style="display:none"></div>