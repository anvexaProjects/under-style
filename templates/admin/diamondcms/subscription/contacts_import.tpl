{include file="admin/"|cat:$smarty.const.CMS|cat:"/subscription/menu.tpl"}
<div id="editContent2" class="block">
	
	<form method="post" enctype="multipart/form-data">
	<fieldset>
        <legend>{$smarty.const.S_CONTACTS_IMPORT} &mdash; {$smarty.const.S_TEXT_OPTION}</legend>
        
        {if $smarty.post.import_text}{include file="ablock/error_block.tpl"}{/if}
        {if $smarty.get.s eq 'it'}<p><strong>{$smarty.const.FLB_OPERATION_OK}</strong></p>{/if}
        
		<p>
			{$smarty.const.S_TEXT_OPTION_TITLE} <input type="text" value="{if !isset($smarty.post.import_separator)},{else}{$smarty.post.import_separator}{/if}" name="import_separator" style="width:50px;" />
			&nbsp;&nbsp;&nbsp;{$smarty.const.S_GROUP}
			<select name="group" style="width:150px;">
				{foreach from=$groups item=i key=k}
				<option value="{$k}"{if $smarty.post.group eq $k} selected="selected"{/if}>{$i}</option>
				{/foreach}
			</select>
		</p>
		<textarea name="import_emails">{$smarty.post.import_emails}</textarea>
		<p><input type="submit" class="inp save" value="{$smarty.const.S_SUBMIT}" name="import_text" /></p>
        
    </fieldset>
	</form>
    
	<form method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>{$smarty.const.S_CONTACTS_IMPORT} &mdash; {$smarty.const.S_UPLOAD_CSV_OPTION}</legend>
        
		{if $smarty.post.import_csv}{include file="ablock/error_block.tpl"}{/if}
        {if $smarty.get.s eq 'if'}<p><strong>{$smarty.const.FLB_OPERATION_OK}</strong></p>{/if}
        
		<p>{$smarty.const.S_UPLOAD_CSV_TITLE}</p>
		<input type="file" name="import_file" id="file" style="width:300px;" />
        <p><input type="submit" class="inp save" value="{$smarty.const.S_SUBMIT}" name="import_csv" /></p>
	</fieldset>
	</form>
    
    <fieldset>
        <legend>{$smarty.const.S_CONTACTS_EXPORT}</legend>
        
        <p><a href="subscription_export.php">{$smarty.const.S_CONTACTS_CSV_EXPORT}</a></p>
	</fieldset>
    
</form>
</div>