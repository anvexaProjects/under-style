{include file="admin/"|cat:$smarty.const.CMS|cat:"/subscription/menu.tpl"}
<div id="editContent" class="block">
<fieldset>
        <legend>{$smarty.const.S_CONTACT_GROUP_ADD}</legend>
		{include file="ablock/error_block.tpl"}
        {if $smarty.get.s}<p><strong>{$smarty.const.FLB_OPERATION_OK}</strong></p>{/if}

		{strip}
		<form method="post" action="">
		<table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
		<colgroup>
			<col width="85%" />
			<col width="10%" />
			<col width="5%" />
		</colgroup>	
            <tr> 
        		<th><label for="form_fname">{$smarty.const.S_NAME}</label></th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			<tr>
				<td><input type="text" name="name" id="form_name" value="{$smarty.post.name}" style="width: 98%;" /></td>
				<td style="vertical-align: bottom; padding-bottom: 8px;" colspan="2"><input type="submit" class="inp save" name="group_add" value="{$smarty.const.S_SUBMIT}" /></td>
			</tr>
		</table>
		</form>
		{/strip}
</fieldset>
<br>
<fieldset>
 <legend>{$smarty.const.S_CONTACT_GROUP_LIST}</legend>

    <form name="" method="post" action="">
        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
			<colgroup>
				<col width="75%" />
				<col width="20%" />
				<col width="5%" />
			</colgroup>
            <tr> 
        		<th><a href="{$aSorting.name.url}">{$smarty.const.S_NAME}</a>&nbsp;<img src="../images/icons/s_{$aSorting.name.img}"></th>
                <th><a href="{$aSorting.created.url}">{$smarty.const.S_CREATED}</a>&nbsp;<img src="../images/icons/s_{$aSorting.created.img}"></th>
	       		<th>&nbsp;</th>
            </tr>
            <tr>
        		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.name class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">&nbsp;</th>
        		<th valign="top" align="left" width="1%" nowrap  class="tblEm">
	       			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</th>
            </tr>       
            {foreach item="item" name="item" from=$items key=k}     
            <tr {cycle values=', class="grey"'} id="view__{$item.id}">
                <td{if $item.is_default} style="font-weight: bold;"{/if}>{$item.name}</td>
                <td>{$item.created|date_format:"%d.%m.%Y %H:%M"}</td>
                <td nowrap>
                    <img src="../images/icons/edt.gif" class="inline_img" onclick="inlineEdit('{$item.id}', 'contact_group')" alt="{$smarty.const.S_EDIT}" />
                    {if !$item.is_default}<img src="../images/icons/delete.gif" class="inline_img" onclick="inlineDelete('{$item.id}', 'contact_group', '{$smarty.const.FLB_CONFIRM}')" alt="{$smarty.const.S_DELETE}" />{/if}
                </td>
            </tr>
            <tr {cycle values=', class="grey"'} id="edit__{$item.id}" style="display: none;">
                <td><input type="text" name="name" value="{$item.name}" /></td>
                <td>&nbsp;</td>
                <td nowrap>
                    <input type="hidden" name="id" value="{$item.id}" />
                    <img src="../images/icons/apply.gif" class="inline_img" onclick="inlineApply('{$item.id}', 'contact_group')" alt="{$smarty.const.S_SAVE}" />
                    <img src="../images/icons/cancel2.gif" class="inline_img" onclick="inlineInitState('{$item.id}', 'contact_group')" alt="{$smarty.const.S_CANCEL}" />
                </td>
            </tr>
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="6"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</fieldset>	
</div>