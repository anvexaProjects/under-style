{include file="admin/"|cat:$smarty.const.CMS|cat:"/subscription/menu.tpl"}
<div id="editContent" class="block">
<fieldset>
        <legend>{$smarty.const.S_CONTACT_ADD}</legend>
		{include file="ablock/error_block.tpl"}
        {if $smarty.get.s}<p><strong>{$smarty.const.FLB_OPERATION_OK}</strong></p>{/if}

		{strip}
		<form method="post" action="">
	
	        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
			<colgroup>
				<col width="23%" />
				<col width="16%" />
				<col width="16%" />
				<col width="7%" />
				<col width="10%" />
				<col width="12%" />
				<col width="11%" />
				<col width="5%" />
			</colgroup>
            <tr> 
                <th><label for="form_email">{$smarty.const.S_EMAIL}</label></th>
        		<th><label for="form_fname">{$smarty.const.S_FIRST_NAME}</label></th>
                <th><label for="form_lname">{$smarty.const.S_LAST_NAME}</label></th>
                <th><label for="form_is_active">{$smarty.const.S_IS_ACTIVE}</label></th>
                <th><label for="form_is_test">{$smarty.const.S_IS_TEST_USER}</label></th>
				<th><label for="form_group">{$smarty.const.S_GROUP}</label></th>
                <th>&nbsp;</th>
	       		<th>&nbsp;</th>
            </tr>
            <tr>
				<td><input type="text" name="email" id="form_email" value="{$smarty.post.email}" style="width: 98%;" /></td>
				<td><input type="text" name="fname" id="form_fname" value="{$smarty.post.fname}" style="width: 98%;" /></td>
				<td><input type="text" name="lname" id="form_lname" value="{$smarty.post.lname}" style="width: 98%;" /></td>

				<td width="90">
						<select name="is_active">
                        {foreach from=$bool item=i key=k name=f}
                        <option value="{$k}"{if $smarty.post.is_active eq $k OR (!isset($smarty.post.is_active) AND $smarty.foreach.f.last)} selected="selected"{/if}>{$i}</option>
                        {/foreach}
                    </select>
				</td>
				<td width="90">
						<select name="is_test">
                        {foreach from=$bool item=i key=k}
                        <option value="{$k}"{if $smarty.post.is_test eq $k} selected="selected"{/if}>{$i}</option>
                        {/foreach}
                    </select>
				</td>
				<td width="90">
						<select name="group">
                        {foreach from=$groups item=i key=k}
                        <option value="{$k}"{if $smarty.post.group eq $k} selected="selected"{/if}>{$i}</option>
                        {/foreach}
                    </select>
				</td>				
				<td style="vertical-align: bottom; padding-bottom: 8px;" colspan="2"><input type="submit" class="inp save" name="contact_add" value="{$smarty.const.S_SUBMIT}" /></td>
			</tr>
		</table>
		</form>
		{/strip}
</fieldset>
<br/>
<fieldset>
	 <legend>{$smarty.const.S_CONTACT_LIST}</legend>

    <form name="" method="post" action="">
        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
			<colgroup>
				<col width="23%" />
				<col width="16%" />
				<col width="16%" />
				<col width="7%" />
				<col width="10%" />
				<col width="12%" />
				<col width="11%" />
				<col width="5%" />
			</colgroup>
            <tr> 
                <th><a href="{$aSorting.email.url}">{$smarty.const.S_EMAIL}</a>&nbsp;<img src="../images/icons/s_{$aSorting.email.img}"></th>
        		<th><a href="{$aSorting.fname.url}">{$smarty.const.S_FIRST_NAME}</a>&nbsp;<img src="../images/icons/s_{$aSorting.fname.img}"></th>
                <th><a href="{$aSorting.lname.url}">{$smarty.const.S_LAST_NAME}</a>&nbsp;<img src="../images/icons/s_{$aSorting.lname.img}"></th>
                <th><a href="{$aSorting.is_active.url}">{$smarty.const.S_IS_ACTIVE}</a>&nbsp;<img src="../images/icons/s_{$aSorting.is_active.img}"></th>
                <th><a href="{$aSorting.is_test.url}">{$smarty.const.S_IS_TEST_USER}</a>&nbsp;<img src="../images/icons/s_{$aSorting.is_test.img}"></th>
				<th><a href="{$aSorting.group_name.url}">{$smarty.const.S_GROUP}</a>&nbsp;<img src="../images/icons/s_{$aSorting.group_name.img}"></th>
                <th><a href="{$aSorting.created.url}">{$smarty.const.S_CREATED}</a>&nbsp;<img src="../images/icons/s_{$aSorting.created.img}"></th>
	       		<th>&nbsp;</th>
            </tr>
            <tr>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.email class="htmlInput"}</th>
        		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.fname class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.lname class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.is_active class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.is_test class="htmlInput"}</th>
				<th valign="top" align="left" class="tblEm">{filter data=$aFilter.group_name class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">&nbsp;</th>
        		<th valign="top" align="left" width="1%" nowrap  class="tblEm">
	       			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</th>
            </tr>       
            {foreach item="item" name="item" from=$items key=k}     
            <tr {cycle values=', class="grey"'} id="view__{$item.group_map_id}">
                <td{if $item.is_active eq 0} class="dis"{/if}>{$item.email}</a></td>
                <td{if $item.is_active eq 0} class="dis"{/if}>{$item.fname}</a></td>
                <td{if $item.is_active eq 0} class="dis"{/if}>{$item.lname}</a></td>
                <td{if $item.is_active eq 0} class="dis"{/if}>{$bool[$item.is_active]}</a></td>
                <td{if $item.is_active eq 0} class="dis"{/if}>{$bool[$item.is_test]}</a></td>
				<td{if $item.is_active eq 0} class="dis"{/if}>{$item.group_name}</a></td>
                <td{if $item.is_active eq 0} class="dis"{/if} nowrap>{$item.created|date_format:"%d.%m.%Y %H:%M"}</td>
                <td nowrap>
                    <img src="../images/icons/edt.gif" class="inline_img" onclick="inlineEdit('{$item.group_map_id}', 'contact')" alt="{$smarty.const.S_EDIT}" />
                    <img src="../images/icons/delete.gif" class="inline_img" onclick="inlineDelete('{$item.group_map_id}', 'contact')" alt="{$smarty.const.S_DELETE}" />
                </td>
            </tr>
            <tr {cycle values=', class="grey"'} id="edit__{$item.group_map_id}" style="display: none;">
                <td><input type="text" name="email" value="{$item.email}" /></td>
                <td><input type="text" name="fname" value="{$item.fname}" /></td>
                <td><input type="text" name="lname" value="{$item.lname}" /></td>
                <td>
                    <select name="is_active">
                        {foreach from=$bool item=i2 key=k2}
                        <option value="{$k2}"{if $k2 eq $item.is_active} selected="selected"{/if}>{$i2}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="is_test">
                        {foreach from=$bool item=i3 key=k3}
                        <option value="{$k3}"{if $k3 eq $item.is_test} selected="selected"{/if}>{$i3}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="group">
                        {foreach from=$groups item=i4 key=k4}
                        <option value="{$k4}"{if $k4 eq $item.group_id} selected="selected"{/if}>{$i4}</option>
                        {/foreach}
                    </select>
                </td>				
                <td>&nbsp;<input type="hidden" name="group_map_id" value="{$item.group_map_id}" /></td>
                <td nowrap>
                    <input type="hidden" name="id" value="{$item.id}" />
                    <img src="../images/icons/apply.gif" class="inline_img" onclick="inlineApply('{$item.group_map_id}', 'contact')" alt="{$smarty.const.S_SAVE}" />
                    <img src="../images/icons/cancel2.gif" class="inline_img" onclick="inlineInitState('{$item.group_map_id}', 'contact')" alt="{$smarty.const.S_CANCEL}" />
                </td>
            </tr>
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="8"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</fieldset>	
</div>