<script language="javascript" type="text/javascript" src="../lib/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript" src="../templates/js/jquery/jquery.thickbox-compressed.js"></script>
<link rel="stylesheet" href="../templates/css/jquery.thickbox.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">root = '{$smarty.const.SITE_REL}';</script>

<h1>{$smarty.const.S_SUBSCRIPTION}</h1>
<div class="tab_block">
    <ul>
        <li{if $smarty.get.a neq 'manage_contacts' && $smarty.get.a neq 'manage_contacts_groups' && $smarty.get.a neq 'contacts_import' && $smarty.get.a neq 'statistics'} class="here"{/if}><a href="subscription.php" title="{$smarty.const.S_SUBSCRIPTION_NEW}">{$smarty.const.S_SUBSCRIPTION_NEW}</a></li>
		<li{if $smarty.get.a eq 'manage_contacts_groups'} class="here"{/if}><a href="subscription.php?a=manage_contacts_groups" title="{$smarty.const.S_SUBSCRIPTION_MANAGE_CONTACTS_GROUPS}">{$smarty.const.S_SUBSCRIPTION_MANAGE_CONTACTS_GROUPS}</a></li>
        <li{if $smarty.get.a eq 'manage_contacts'} class="here"{/if}><a href="subscription.php?a=manage_contacts" title="{$smarty.const.S_SUBSCRIPTION_MANAGE_CONTACTS}">{$smarty.const.S_SUBSCRIPTION_MANAGE_CONTACTS}</a></li>
        <li{if $smarty.get.a eq 'contacts_import'} class="here"{/if}><a href="subscription.php?a=contacts_import" title="{$smarty.const.S_SUBSCRIPTION_CONTACTS_IMPORT_EXPORT}">{$smarty.const.S_SUBSCRIPTION_CONTACTS_IMPORT_EXPORT}</a></li>
        <li{if $smarty.get.a eq 'statistics'} class="here"{/if}><a href="subscription.php?a=statistics" title="{$smarty.const.S_SUBSCRIPTION_STATISTICS}">{$smarty.const.S_SUBSCRIPTION_STATISTICS}</a></li>
    </ul>
</div>