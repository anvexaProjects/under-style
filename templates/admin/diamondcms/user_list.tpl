{literal}
<script type="text/javascript">
//<![CDATA[
    function DelIt(caller) 
    {
        if (!window.confirm('{/literal}{$smarty.const.CF_DELETE_USER}{literal}')) return false;
    }
//]]>
</script>
{/literal}

<h1>{$smarty.const.S_USERS}</h1>
<div id="editContent" class="block">
    <form name="" method="post" action="">
        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
            <tr> 
				<th width="5">&nbsp;</th>
        		<th width="30%"><a href="{$aSorting.login.url}">{$smarty.const.S_LOGIN}</a>&nbsp;<img src="../images/icons/s_{$aSorting.login.img}"></th>
        		<th width="20%"><a href="{$aSorting.role.url}" class="tbl_header">{$smarty.const.S_ROLES}</a>&nbsp;<img src="../images/icons/s_{$aSorting.role.img}"></th>
        		<th width="30%"><a href="{$aSorting.full_name.url}" class="tbl_header">{$smarty.const.S_NAME}</a>&nbsp;<img src="../images/icons/s_{$aSorting.full_name.img}"></th>
                <th width="20%"><a href="{$aSorting.logged.url}" class="tbl_header">{$smarty.const.S_LAST_LOGGED}</a>&nbsp;<img src="../images/icons/s_{$aSorting.logged.img}"></th>
	       		<th>&nbsp;</th>
            </tr>
            <tr>
				<th>&nbsp;</td>
	       		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.login class="htmlInput"}</th>
	       		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.role class="htmlInput"}</th>
        		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.full_name class="htmlInput"}</th>
                <th>&nbsp;</th>
        		<th valign="top" align="left" width="1%" nowrap  class="tblEm">
	       			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</th>
            </tr>       
            {foreach item="item" name="item" from=$items}     
            <tr {cycle values=', class="grey"'}>
				<td>&nbsp;</td>
                <td{if $item.is_locked eq 1} class="dis"{/if}><a href="user_edit.php?id={$item.id}&page={$smarty.get.page}" >{$item.login}</a></td>
                <td>
                    {if $item.role eq $smarty.const.ADMIN}{$roles.1}
                    {elseif $item.role eq $smarty.const.SALER}{$roles.4}
                    {elseif $item.role eq $smarty.const.DESIGNER}{$roles.5}
                    {elseif $item.role eq $smarty.const.SALERS_REDACTOR}{$roles.6}
                    {elseif $item.role eq $smarty.const.DESIGNERS_REDACTOR}{$roles.7}
                    {/if}
                </td>
                <td{if $item.is_locked eq 1} class="dis"{/if}>{$item.name}</td>
                <td{if $item.is_locked eq 1} class="dis"{/if}>{if $item.logged neq '0000-00-00 00:00:00'}{$item.logged|date_format:"%d.%m.%Y %H:%M"}{/if}</td>
                <td nowrap>
                    <a href="user_edit.php?id={$item.id}&page={$smarty.get.page}" ><img src="../images/icons/edt.gif" alt="edit"></a>
                    <a href="user_del.php?id={$item.id}&page={$smarty.get.page}" onClick="return DelIt(this);"><img src="../images/icons/delete.gif" alt="delete"></a>
                </td>
            </tr>
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="6"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</div>