{include_php file="admin/check_access.inc.php"}<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DiamondCMS - {$smarty.const.S_ADMIN_CONSOLE}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="max-age=31536000" />
	<meta http-equiv="Expires" content="Mon, 19 Apr 2060 20:00:00 GMT" />
	<link href="../templates/css/{$smarty.const.CMS}/main.css?v={$smarty.const.APP_VERSION}" rel="stylesheet" type="text/css" />
	<link href="../templates/css/shared.css?v={$smarty.const.APP_VERSION}" rel="stylesheet" type="text/css" />
	<link href="../templates/css/ui.datepicker.css?v={$smarty.const.APP_VERSION}" rel="stylesheet" type="text/css" />
	<link href="../templates/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="../templates/css/jquery.ipicture.css?v={$smarty.const.APP_VERSION}" rel="stylesheet" type="text/css" />

	<script src="../templates/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../templates/js/jquery/jquery.corner.js" type="text/javascript"></script>
	<script src="../templates/js/jquery/jquery.ui.datepicker.js" type="text/javascript"></script>
	<script src="../templates/js/jquery/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
	<script src="../templates/js/jquery/jquery.ipicture.min.js" type="text/javascript"></script>



	{if $smarty.const.CMS_LANG eq 'ru'}<script src="../templates/js/jquery/jquery.ui.datepicker-{$smarty.const.CMS_LANG}.js" type="text/javascript"></script>{/if}
	<script src="../templates/js/common.js" type="text/javascript"></script>
</head>
<body>
    <div id="minWidth">
        <div id="min-width"
            <div id="header">
                <table>
                    <tr>
                        <td width="22%"><div class="left_top"><!--  --></div><div class="logo"><a href="index.php"><img src="../images/admin/logo.jpg" width="201" height="48" alt="DiamondCMS" /></a></div></td>
                        <td width="78%"><div class="cenerheader"><div class="rrheader"><img src="../images/admin/top.jpg" width="728" height="102" border="0" alt="" /></div></div></td>
                        <td><div class="rightheader"><div class="right_end"><!--  --></div></div></td>
                    </tr>
                </table>
            </div>
            <div id="center">
                <table>
                    <tr>
                        <td width="22%">
                            <div id="lSide">{include_php file='admin/menu.inc.php' tpl_name="admin/`$smarty.const.CMS`/main_menu.tpl"}</div>
                        </td>
                        <td width="78%">
                            <table>
                                <tr>
                                    <td width="100%">
                                        <div id="mainContent">
                                        {include file="admin/`$smarty.const.CMS`/acontent.tpl"}
                                        {include_php file="fx/sql_debug.inc.php"}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td><div class="rightheader2"><!--  --></div></td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="footer">
            <table>
                <tr>
                    <td width="22%"><div class="copy">2008&nbsp;&copy;&nbsp;<a href="http://diamondcms.info" target="_blank">diamondcms.info</a></div><div class="left_top"><!--  --></div><div class="rasporka"><!--  --></div></td>
                    <td width="78%">
                        <div class="copy">{include_php file='admin/menu.inc.php' tpl_name='ablock/menu_footer.tpl'}</div>
                        <div class="bluefooter"><div class="right_end"><!--  --></div></div>
                    </td>
                    <td style="vertical-align:bottom"><div class="greenfooter"><!--  --></div></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
{include_php file="master/close_db.inc.php"}