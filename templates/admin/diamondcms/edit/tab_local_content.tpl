{include file="ablock/tiny_mce_config.js.tpl"}
{foreach from=$langs item=i key=k name=langs}
<div id="tab-local_content_{$k}" class="contentBlock">

<fieldset>
<legend>{$smarty.const.S_CONTENT}</legend>
<div style="margin: 5px" class="tinyMCE">
    <div class="clr"><!-- --></div>
    {$form_data.$k.content.html}
    <div class="clr"><!-- --></div>
</div>
</fieldset>

</div>		
{/foreach}