{literal}
<script language="JavaScript">
    $(function(){
        $("#created").datepicker({dateFormat: "dd.mm.yy"});
    });
</script>
{/literal}
<div id="tab_prop" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_PROPERTIES}&nbsp;</legend>
        <table class="fieldset">
            <tr>
                <td class="colLbl" align="right">{$smarty.const.S_ALIAS}&nbsp;*</td>
                <td class="colInp">{$form_data.alias.html}</td>
                <td class="colLbl" align="right"><label for="created">{$smarty.const.S_CREATION_DATE}</label></td>
                <td class="colInp" nowrap="nowrap">{$form_data.created.html}</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="ord">{$smarty.const.S_WEIGHT}</label></td>
                <td class="colInp">{$form_data.ord.html}</td>
                <td class="colLbl" align="right"><label for="is_hidden">{$smarty.const.S_HIDE_FROM_SELECTION}</label></td>
                <td class="colInp">{$form_data.is_hidden.html}</td>
            </tr>
			
            <tr>
                <td class="colLbl" align="right"><label for="ord">Выводить в футере</label></td>
                <td class="colInp">{$form_data.is_footer.html}</td>
                <td class="colLbl" align="right"></td>
                <td class="colInp"></td>
            </tr>
        </table>
    </fieldset>         
</div>
