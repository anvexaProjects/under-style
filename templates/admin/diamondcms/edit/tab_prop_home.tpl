{literal}
<script language="JavaScript">
    $(function(){
        $("#created").datepicker({dateFormat: "dd.mm.yy"});
    });
</script>
{/literal}
<div id="tab_prop_home" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_PROPERTIES}&nbsp;</legend>
        <table class="fieldset">
            <tr>
                <td class="colLbl" align="right">{$smarty.const.S_ALIAS}&nbsp;*</td>
                <td class="colInp">{$form_data.alias.html}</td>
                <td class="colLbl" align="right"><label for="created">{$smarty.const.S_CREATION_DATE}</label></td>
                <td class="colInp" nowrap="nowrap">{$form_data.created.html}</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="ord">{$smarty.const.S_WEIGHT}</label></td>
                <td class="colInp">{$form_data.ord.html}</td>
                <td class="colLbl" align="right"><label for="is_hidden">{$smarty.const.S_HIDE_FROM_SELECTION}</label></td>
                <td class="colInp">{$form_data.is_hidden.html}</td>
            </tr>
            <tr>
				<td class="colLbl" align="right"><label for="sprop7">{$smarty.const.S_TWITTER}</label></td>
                <td class="colInp">{$form_data.sprop7.html}</td>
                <td class="colLbl" align="right"><label for="sprop8">{$smarty.const.S_FACEBOOK}</label></td>
                <td class="colInp">{$form_data.sprop8.html}</td>
            </tr>
            <tr>
				<td class="colLbl" align="right"><label for="sprop9">{$smarty.const.S_VKONTACTE}</label></td>
                <td class="colInp">{$form_data.sprop9.html}</td>
                <td class="colLbl" align="right"><label for="sprop10">{$smarty.const.S_ODNOCLASSNIKI}</label></td>
                <td class="colInp">{$form_data.sprop10.html}</td>
            </tr>
        </table>
    </fieldset>
</div>
