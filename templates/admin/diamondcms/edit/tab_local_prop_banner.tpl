{foreach from=$langs item=i key=k name=langs}
<div id="tab_local_prop_banner_{$k}" class="contentBlock">
<fieldset>
    <legend>{$smarty.const.S_PROPERTIES}</legend>
    <table class="fieldset">
        <tr>
            <td class="colLbl" align="right">{$smarty.const.S_NAME}&nbsp;*</td>
            <td class="colEdt">{$form_data.$k.name.html}</td>
            <td class="colLbl" align="right">{$smarty.const.S_LINK}</td>
            <td class="colEdt">{$form_data.sprop1.html}</td>
        </tr>
    </table>
</fieldset> 

</div>
{/foreach}