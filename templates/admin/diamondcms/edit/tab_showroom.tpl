<div id="tab_showroom" class="contentBlock">
<script type="text/javascript">
{literal}
$(document).ready(function() {
	{/literal}
	{if $smarty.get.iid}
	$('#tabs li:last').trigger('click');
	{else}
	$('#tabs li:last').hide();
	{/if}
	{literal}
	parent = $('.showroom');
	initAddDot();
	initRemoveDot();
	//initCoordinates();
	initIpicture();


	function initAddDot() {
		$('.add_dot', $(parent)).click(function(){
			$(this).parent().append($('#tpl').html());
			initRemoveDot();
			showMarker();
			return false;
		});
	}

	function initRemoveDot() {
		$('.remove_dot', $(parent)).click(function(){
			$(this).parent().remove();
			showMarker();
			return false;
		});
	}

	function initCoordinates(marker) {
		var img = $('.showroom_image');
		
		var marker_position = $(marker).position();
		var parent_position = $(marker).parent().position();

		var x = Math.round(marker_position.left - parent_position.left - 20);
		var y = Math.round(marker_position.top - parent_position.top - 40);
		$('.status:last', $(parent)).filter(':visible').html(x +', '+ y);
		$('.x:last', $(parent)).val(x);
		$('.y:last', $(parent)).val(y);
		$('.x_pct:last', $(parent)).val(Math.round(x*100/$(img).width()));
		$('.y_pct:last', $(parent)).val(Math.round(y*100/$(img).height()));
	}

	function showMarker() {
		var offset = $('.showroom_image', $(parent)).offset();
		var img = $('.showroom_image');
		$('#marker').show().offset({left: offset.left + $(img).width()/2, top: offset.top + $(img).height()/2});
		$('#marker').draggable({
			containment: "#ipicture",
			drag: function(){
				initCoordinates(this);
			}
		});
	}

	function hideMarker() {
		$('#marker').hide();
	}

	function initIpicture() {
		$('#ipicture').iPicture();
	}

});
{/literal}
</script>
<fieldset>
<legend>{$smarty.const.S_SHOWROOM}</legend>
{$showroom_image.image_name}<br />
<table class="showroom" style="width: 100%">
	<tr>
		<td style="width: 850px">
			<div id="ipicture" data-interaction="hover">
				<div class="ip_slide">
					<img src="{$smarty.const.SITE_REL}{$showroom_dir}/{$showroom_image.image_name}" class="showroom_image ip_tooltipImg" width="850px" />
					{foreach from=$showroom_dots item=d name=f}
					<div id="dot_{$d.id}" class="ip_tooltip ip_img32" style="top: {$d.y_pct}%; left: {$d.x_pct}%;" data-button="moreblack" data-tooltipbg="bgblack" data-round="roundBgW" data-animationtype="ltr-slide">
						<p><a href="{$d.link}">{$d.description}</a></p>
					</div>
					{/foreach}
				</div>
			</div>
		</td>
		<td>
			<a href="javascript:;" class="add_dot">Добавить точку</a><br />
			{foreach from=$showroom_dots item=i}
			<div style="padding: 8px 0; border-bottom: 1px solid #CCC" class="dot">
				<input type="hidden" name="showroom_x[]" class="x" value="{$i.x}">
				<input type="hidden" name="showroom_y[]" class="y" value="{$i.y}">
				<input type="hidden" name="showroom_x_pct[]" class="x_pct" value="{$i.x_pct}">
				<input type="hidden" name="showroom_y_pct[]" class="y_pct" value="{$i.y_pct}">
				Описание:<br /><input type="text" name="showroom_description[]" value="{$i.description}" style="width: 265px;">
				Ссылка:<br /><input type="text" name="showroom_link[]" value="{$i.link}" style="width: 265px;">
				Координаты: &nbsp;<span class="status">{$i.x}, {$i.y}</span>
				<a href="#" class="remove_dot" style="float: right;" id="delete_dot_{$i.id}">Удалить точку</a>
			</div>
			{/foreach}
		</td>
	</tr>
</table>
<input type="hidden" name="showroom_image_id" value="{$smarty.request.iid}" />
<div style="display: none;" id="tpl">
	<div style="padding: 8px 0; border-bottom: 1px solid #CCC" class="dot">
		<input type="hidden" name="showroom_x[]" class="x">
		<input type="hidden" name="showroom_y[]" class="y">
		<input type="hidden" name="showroom_x_pct[]" class="x_pct">
		<input type="hidden" name="showroom_y_pct[]" class="y_pct">
		Описание:<br /><input type="text" name="showroom_description[]" style="width: 265px;">
		Ссылка:<br /><input type="text" name="showroom_link[]" style="width: 265px;">
		Координаты: &nbsp;<span class="status">0, 0</span>
		<a href="javascript:;" class="remove_dot" style="float: right;">Удалить точку</a>
	</div>
</div>
<img src="../images/icons/ipicture_button_red.png" id="marker" style="display: none; position: absolute; z-index: 10;" />
</fieldset>     
</div>
