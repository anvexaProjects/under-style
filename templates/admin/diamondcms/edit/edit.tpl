<h1>{if $smarty.request.id}{$smarty.const.S_ART_EDIT}{else}{$smarty.const.S_ART_ADD}{/if}</h1>

{include file="admin/`$smarty.const.CMS`/edit/js_common.tpl"}
{include_php file="admin/path_navigator.inc.php" tpl_name="admin/`$smarty.const.CMS`/edit/path_navigator.tpl"}

<div class="tab_block">
    {if $smarty.const.ALLOW_SINGLE_TAB eq '1'}
        <ul class="tabs" id="tabs">
            <li class="here">{$smarty.const.S_PROPERTIES}</li>
        </ul>
    {else}
        {*{$tabs|@print_r}*}
        <ul class="tabs" id="tabs">
            {foreach from=$tabs item=tab key=k name=cfg}
                {if !$tab.local}
                    <li id="#{$tab.id}"{if $smarty.foreach.cfg.first} class="here"{/if}>{$tab.name}</li>
                {else}
                    {foreach from=$langs item=lang key=l}
                        <li id="#{$tab.id}_{$l}">{$tab.name}{if sizeof($langs) > 1}&nbsp;-&nbsp;{$l}{/if}</li>
                    {/foreach}
                {/if}
            {/foreach}
            {*<li id="#tab_file_upload" class="here">Общие свойства</li>*}
        </ul>
    {/if}
</div>
<div id="editContent2" class="block">

    {if $smarty.get.s|default:0 eq 1}<b>{$smarty.const.FLB_OPERATION_OK}</b>{/if}
    {include file="ablock/error_block.tpl"}

    <form {$form_data.attributes}>
        <input type="hidden" name="MAX_FILE_SIZE" value="100000000"/>
        {$form_data.javascript}
        {$form_data.hidden}

        {foreach from=$tabs item=tab key=k}
            {include file="admin/`$smarty.const.CMS`/edit/`$tab.id`.tpl"}
        {/foreach}

        {*{include file="admin/`$smarty.const.CMS`/edit/tab_file_upload.tpl"}*}

        <div id="bottomBtn">
            <input class="inp save" type="submit" value="{$smarty.const.S_SAVE_CLOSE}" name="save_close"/>
            <input class="inp save" type="submit" value="{$smarty.const.S_SAVE}" name="save"/>
            <input type="button" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;" class="inp undo" onClick="window.location='{$smarty.request.bur|default:$bur}'">
        </div>
        <input type="hidden" value="{$smarty.request.bur}" name="bur">
        <input type="hidden" value="{$smarty.request.id}" name="id">
        <input type="hidden" value="{$smarty.request.pid}" name="pid">

    </form>

</div>