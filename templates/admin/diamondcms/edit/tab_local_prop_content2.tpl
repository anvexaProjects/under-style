{include file="ablock/tiny_mce_config.js.tpl"}
{foreach from=$langs item=i key=k name=langs}
<div id="tab_local_prop_content2_{$k}" class="contentBlock">
<fieldset>
    <legend>{$smarty.const.S_PROPERTIES}</legend>
    <table class="fieldset">
        <tr>
            <td class="colLbl" align="right">{$smarty.const.S_NAME}&nbsp;*</td>
            <td class="colEdt">{$form_data.$k.name.html}</td>
            <td class="colLbl">&nbsp;</td>
            <td class="colEdt">&nbsp;</td>
        </tr>
    </table>
</fieldset> 

<fieldset>
<legend>{$smarty.const.S_META_TAGS}</legend>
    <table class="fieldset">
        <tr>
            <td class="colLbl" align="right" valign="top">{$smarty.const.S_PAGE_TITLE}</td>
            <td class="colEdt">{$form_data.$k.title.html}</td>
            <td class="colLbl"align="right">&nbsp;</td>
            <td class="colEdt">&nbsp;</td>
        </tr>
        <tr>
            <td class="colLbl" align="right" valign="top">{$smarty.const.S_DESCRIPTION2}</td>
            <td class="colEdt" valign="top">{$form_data.$k.meta_text.html}</td>
            <td class="colLbl" align="right" valign="top">{$smarty.const.S_KEYWORDS}</td>
            <td class="colEdt" valign="top">{$form_data.$k.meta.html}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
{if $smarty.request.control eq 'contacts'}
    <h3>{$smarty.const.S_CONTACT_PAGE_CONTENT}</h3>
{elseif $smarty.request.control eq 'company' ||
    $smarty.request.control eq 'designer'
}
    <h3>{$smarty.const.S_BRIEF}</h3>
{/if}
<legend>{$smarty.const.S_CONTENT}</legend>

<div style="margin: 5px" class="tinyMCE">
    <div class="clr"><!-- --></div>
    {$form_data.$k.content.html}
    <div class="clr"><!-- --></div>
</div>
{if $smarty.request.control eq 'contacts'}
    <h3>{$smarty.const.S_FOOTER_CONTENT}</h3>
{elseif $smarty.request.control eq 'company'}
    <h3>{$smarty.const.S_COMPANY_INFORMATION}</h3>
{elseif $smarty.request.control eq 'designer'}
    <h3>{$smarty.const.S_DESIGNER_INFORMATION}</h3>
{/if}
<div style="margin: 5px" class="tinyMCE">
    <div class="clr"><!-- --></div>
    {$form_data.$k.content2.html}
    <div class="clr"><!-- --></div>
</div>
</fieldset>
</div>
{/foreach}