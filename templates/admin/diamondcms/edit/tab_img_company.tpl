<div id="tab_img_company" class="contentBlock">
	<fieldset>
		<legend>{$smarty.const.S_IMAGES}</legend>
		<table>
			{section name=number loop=$upl|@sizeof} {assign var=i
			value=$smarty.section.number.iteration} {assign var=img
			value=img`$smarty.section.number.iteration`}
			<tr>
				<td width="300px">
                    {if $smarty.section.number.iteration == 1}
                        <label for="{$img}">{$smarty.const.S_IMG_COMPANY}</label>
                    {elseif $smarty.section.number.iteration == 2}
                        <label for="{$img}">{$smarty.const.S_IMG_LOGO}</label>
                    {/if}
					{$form_data.$img.html}
				</td>
				<td align="left">
					{if $default.$img neq ''}
					<label for="delete_{$i}">
						{$form_data.delete.$i.html} {$smarty.const.S_DELETE}&nbsp;
						<a href="{$smarty.const.SITE_REL}{$upl.1.0.dir}/{$default.$img}">{$default.$img}</a>
					</label>
					{/if}
				</td>
			</tr>
			{/section}
		</table>
	</fieldset>
</div>
