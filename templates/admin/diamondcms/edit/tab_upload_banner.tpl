<div id="tab_upload_banner" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_IMAGES}</legend>
        <div style="margin-left: 15px; margin-bottom: 15px;">
            {$smarty.const.S_BANNER_COMMENT}
        </div>
        <table>
            {section name=number loop=$upl|@sizeof}
                {assign var=i value=$smarty.section.number.iteration}
                {assign var=img value=img`$smarty.section.number.iteration`}
                <tr>
                    <td width="50px" style="vertical-align: middle">
                        <label for="{$img}">{$smarty.const.S_BANNER}</label>
                    </td>
                    <td>
                        {$form_data.$img.html}
                    </td>
                    <td align="left">
                        {if $default.$img neq ''}
                            <label for="delete_{$i}">
                                {$form_data.delete.$i.html}
                                {$smarty.const.S_DELETE}&nbsp;<a href="{$smarty.const.SITE_REL}{$upl.$i.0.dir}/{$default.$img}">{$default.$img}</a>
                            </label>
                        {/if}
                    </td>
                    <td style="text-align: right">
                        <label for="sprop7">
                            {$smarty.const.S_SHOW_FLASH}
                        </label>
                    </td>
                    <td style="text-align: left; width: 30px">
                        {$form_data.sprop7.html}
                    </td>
                </tr>
            {/section}
        </table>

    </fieldset>
</div>
