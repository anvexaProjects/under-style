{foreach from=$langs item=i key=k name=langs}
<div id="tab_local_prop_{$k}" class="contentBlock">
<fieldset>
    <legend>{$smarty.const.S_PROPERTIES}</legend>
    <table class="fieldset">
        <tr>
            <td class="colLbl" align="right">{$smarty.const.S_NAME}&nbsp;*</td>
            <td class="colEdt">{$form_data.$k.name.html}</td>
            <td class="colLbl">&nbsp;</td>
            <td class="colEdt">&nbsp;</td>
        </tr>
    </table>
</fieldset> 

<fieldset>
<legend><label for="description">{$smarty.const.S_VIDEO_CODE}&nbsp;</label></legend>
    <div style="margin: 5px">{$form_data.video_code.html}</div>
</fieldset>
    
</div>
{/foreach}