<div id="tab_multiupload_prices" class="contentBlock">
{assign var="hash" value=$smarty.now}
<fieldset>
<legend>{$smarty.const.S_PRICE}</legend>
<link href="../lib/3rdparty/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../lib/3rdparty/uploadify/scripts/swfobject.js"></script>
<script type="text/javascript" src="../lib/3rdparty/uploadify/scripts/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="../templates/js/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="../templates/js/uploadify_ajax.js"></script>
<script type="text/javascript">
{literal}
asyncImagerUrl = '../admin/uploadify_async.php';
art_id = '{/literal}{$default.struct_id}{literal}';
control = '{/literal}{$default.control}{literal}';
$(document).ready(function() {
    initImagesTable();
    $("#uploadify").uploadify({
    	{/literal}
        'uploader'       : '../lib/3rdparty/uploadify/scripts/uploadify.swf',
        'script'         : '../admin/uploadify.php',
        'cancelImg'      : '../images/spacer.gif',
        //'folder'         : '{$enum.upl[$default.control].0.dir}',
        'queueID'        : 'fileQueue',
        'auto'           : true,
        'multi'          : true,
        'queueSizeLimit' : 999,
        'scriptData'     : {ldelim}'id': '{$default.struct_id}', 'control': '{$default.control}', 'image_hash': '{$hash}'{rdelim},
        'onAllComplete'  : initImagesTable
        {literal}
    });
  

		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}
		$("input .tit").autocomplete({
				source: function( request, response ) {
					$.getJSON( "ajax_autocomplete_factory.php", {
						term: extractLast( request.term )
					}, response );
				},
			minLength: 2,
			select: function( event, ui ) {
							}
		});
}); 

{/literal}
</script>
<div style="padding: 10px;">
	<div id="fileQueue"></div>
	<input type="file" name="uploadify" id="uploadify" />
    <div style="padding: 5px 0; font-style: italic">{$smarty.const.S_MULTIUPLOAD_LABEL}</div>
	<br />
	<table id="images_list">
	</table>
	<div id="delete_images_link"><a href="javascript:deleteImages();">{$smarty.const.S_DELETE}</a></div>
</div>
</fieldset>
<input type="hidden" name="image_hash" value="{$hash}" />
</div>
