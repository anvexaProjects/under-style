<div id="tab_subomain_category" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_CATEGORIES}</legend>
        <table class="fieldset">
            <tr>
                    <td class="colLbl" align="right"><label for="sprop1">{$smarty.const.S_IS_AVAILABLE}</label></td>
                    <td class="colInp">{$form_data.sprop1.html}</td>
                    <td class="colLbl" align="right">&nbsp;</td>
                    <td class="colInp">&nbsp;</td>
                </tr>
                <tr>
                    <td class="colLbl" align="right"><label for="sprop2">{$smarty.const.S_CATALOG_PRODUCTS}</label></td>
                    <td class="colInp">{$form_data.sprop2.html}</td>
                    <td class="colLbl" align="right">&nbsp;</td>
                    <td class="colInp">&nbsp;</td>
                </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop3">{$smarty.const.S_ON_HOLD}</label></td>
                <td class="colInp">{$form_data.sprop3.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop4">{$smarty.const.S_SPECIAL_OFERT}</label></td>
                <td class="colInp">{$form_data.sprop4.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop5">{$smarty.const.S_SALE}</label></td>
                <td class="colInp">{$form_data.sprop5.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop6">{$smarty.const.S_ALL_PRODUCTS}</label></td>
                <td class="colInp">{$form_data.sprop6.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop7">{$smarty.const.S_ARCHIVES}</label></td>
                <td class="colInp">{$form_data.sprop7.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>
        </table>
    </fieldset>
</div>