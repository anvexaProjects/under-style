{if $smarty.const.ALLOW_SINGLE_TAB neq '1'}
{literal}
	<script src="../templates/js/jquery/jquery-tabs-pack.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("div.contentBlock").hide();
		var div_id = $("#tabs li.here").attr("id");
		$(div_id).show();
		
		$("#tabs li").click(function(){
			$("div.contentBlock").hide();
			$("#tabs li").removeClass("here");
			$(this).addClass("here");
			$(this.id).show();
		})
{/literal}
		{if $smarty.request.control eq 'company'}
{literal}
			$("#name_ru").bind("keyup", function() {
				$.post( "/admin/ajax_checkname.php", { name: $(this).val() }, function( data ) {
					if (data.error) 
						$("#name_ru").css("border", "3px solid #FF0000");
					else
						$("#name_ru").css("border", "");
				}, "json");
			});
{/literal}
		{/if}
{literal}
		
	});
	</script>
{/literal}
{/if}