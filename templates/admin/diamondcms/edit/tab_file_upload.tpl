<div id="tab_file_upload" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_FILE}</legend>
        {assign var = "size" value=$default.collections}
        <table style="width: 100%" id = "file_form">
            {if $size eq 0}
                <tr>
                    <td colspan="2">
                        <label for="file_0">{$smarty.const.S_COLLECTION_FILE}</label>
                        {$form_data.file.0.html}
                    </td>
                    <td colspan="2">
                        <label for="image_0">{$smarty.const.S_IMAGE}</label>
                        {$form_data.image.0.html}
                    </td>
                    <td align="left">
                    </td>
                </tr>
            {/if}
            {section name=number loop=$size}
                {assign var=i value=$smarty.section.number.index}
                    <tr>
                        <td style="width: 100px">
                            <a href="{$smarty.const.SITE_REL}/upload/collections/{$default.file.$i}">{$default.file.$i}</a>
                        </td>

                        <td align="left" style="display: none">
                            {if $default.file.$i neq ''}
                                {$form_data.delete.$i.html}
                                <label for="delete_{$i}">{$smarty.const.S_DELETE}</label>&nbsp;
                                {$form_data.id.$i.html}
                                {$form_data.file_path.$i.html}
                                {$form_data.image_path.$i.html}
                            {/if}
                        </td>
                        <td style="display: none">
                            <label for="{$file}">{$smarty.const.S_COLLECTION_FILE}</label>
                            {$form_data.file.$i.html}
                        </td>
                        <td style="display: none">
                            <label for="{$image}">{$smarty.const.S_IMAGE2}</label>
                            {$form_data.image.$i.html}
                        </td>
                        <td>
                            <input type="button" name = "edit" class = "edit" value="{$smarty.const.S_EDIT}" style = "width: 100px">
                        </td>
                    </tr>
            {/section}
            <tr></tr>
        </table>
        <input type = "button" name = "add" id = "add" VALUE="{$smarty.const.S_ADD}" style="width: 70px; margin-left: 10px; margin-top: 10px">
    </fieldset>
</div>
<script>
    {literal}
    element = "<tr>"+
            "<td colspan = '2'>{/literal}{$smarty.const.S_COLLECTION_FILE}{literal}<input type='file' name = 'file[]'></td>"+
            "<td colspan = '2'>{/literal}{$smarty.const.S_IMAGE}{literal}<input type='file' name = 'image[]'></td><td></td>";
    $("#add").click(function(){
        $("#file_form").append(element);
    })
    $(".edit").click(function(){
       el = $(this).parent().parent();
       el.find("td").show()
       $(this).hide();
    })
    {/literal}
</script>