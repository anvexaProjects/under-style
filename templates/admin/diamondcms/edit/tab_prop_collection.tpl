{literal}
<script language="JavaScript">
    $(function(){
        $("#created").datepicker({dateFormat: "dd.mm.yy"});
    });
        $(document).ready(function() {
            function split( val ) {
                return val.split( /,\s*/ );
            }
            function extractLast( term ) {
                return split( term ).pop();
            }
            $( "#sprop3" ).autocomplete({
                source: function( request, response ) {
                    $.getJSON( "ajax_autocomplete_factory.php", {
                        term: extractLast( request.term )
                    }, response );
                },
                minLength: 2,
                select: function( event, ui ) {
                }
            });
    });
</script>
{/literal}
<div id="tab_prop_collection" class="contentBlock">
    <fieldset>
        <legend>{$smarty.const.S_COLLECTION_PROPERTIES}&nbsp;</legend>
        <table class="fieldset">
             <tr>
                <td class="colLbl" align="right"><label for="sprop3">{$smarty.const.S_VENDOR}</label></td>
                 <div class="ui-widget"><td class="colInp">{$form_data.sprop3.html}</td></div>
                <td class="colLbl" align="right"><label for="sprop5">{$smarty.const.S_COUNTRY}</label></td>
                <td class="colInp" nowrap="nowrap">{$form_data.sprop5.html}</td>
            </tr>

            <tr>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">Для добавления производителя нажмите <a href="{$smarty.const.SITE_REL}/admin/art_edit.php?parent_id=47ru&control=company" target='_blank'>здесь</a></td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>
            </tr>

            <tr>
                <td class="colLbl" align="right"><label for="sprop6">{$smarty.const.S_LOCATION}</label></td>
                <td class="colInp">{$form_data.sprop6.html}</td>
                <td class="colLbl" align="right">&nbsp;</td>
                <td class="colInp">&nbsp;</td>   
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop7">{$smarty.const.S_IS_AVAILABLE}</label></td>
                <td class="colInp">{$form_data.sprop7.html}</td>            
                <td class="colLbl" align="right"><label for="sprop8">{$smarty.const.S_ON_HOLD}</label></td>
                <td class="colInp" nowrap="nowrap">{$form_data.sprop8.html}</td>
            </tr>
            <tr>
                <td class="colLbl" align="right"><label for="sprop9">{$smarty.const.S_SPECIAL_OFERT}</label></td>
                <td class="colInp">{$form_data.sprop9.html}</td>
                <td class="colLbl" align="right"><label for="sprop10">{$smarty.const.S_SALE}</label></td>
                <td class="colInp">{$form_data.sprop10.html}</td>   
            </tr>
        </table>
    </fieldset>         
</div>