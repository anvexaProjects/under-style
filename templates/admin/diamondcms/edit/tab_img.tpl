<div id="tab_img" class="contentBlock">
<fieldset>
<legend>{$smarty.const.S_IMAGES}</legend>
<table>
{if $smarty.request.control eq 'subdomen'}
    {assign var='upl' value=1}
{/if}
{section name=number loop=$upl|@sizeof}
{assign var=i value=$smarty.section.number.iteration}
{assign var=img value=img`$smarty.section.number.iteration`}
    <tr>
        <td width="300px">
            {if $smarty.section.number.iteration eq 2 && $smarty.request.control eq 'action'}
                <label for="{$img}">{$smarty.const.S_IMG_ON_MAIN}</label>
            {elseif $smarty.request.control eq 'subdomen'}
                <label for="{$img}">{$smarty.const.S_IMG_ON_FOOTER}</label>
            {elseif $smarty.request.control eq 'collection' && $smarty.section.number.iteration eq 1}
                <label for="{$img}">{$smarty.const.S_COLLECTION_IMAGE}</label>
            {elseif $smarty.request.control eq 'collection' && $smarty.section.number.iteration eq 2}
                <label for="{$img}">{$smarty.const.S_COLLECTION_FILE}</label>
            {else}
                <label for="{$img}">{$smarty.const.S_IMG_ON_PAGE}</label>
            {/if}
            {$form_data.$img.html}
        </td>
        <td align="left">
            {if $default.$img neq ''}
                <label for="delete_{$i}">
                    {$form_data.delete.$i.html}
                    {$smarty.const.S_DELETE}&nbsp;<a href="{$smarty.const.SITE_REL}{$upl.$i.0.dir}/{$default.$img}">{$default.$img}</a>
                </label>
            {/if}
        </td>
    </tr>
{/section}
</table>
</fieldset>     
</div>
