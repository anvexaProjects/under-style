{foreach from=$langs item=i key=k name=langs}
<div id="tab_local_prop_{$k}" class="contentBlock">
<fieldset>
    <legend>{$smarty.const.S_PROPERTIES}</legend>
    <table class="fieldset">
        <tr>
            <td class="colLbl" align="right">{$smarty.const.S_NAME}&nbsp;*</td>
            <td class="colEdt">{$form_data.$k.name.html}</td>
            <td class="colLbl">&nbsp;</td>
            <td class="colEdt">&nbsp;</td>
        </tr>
    </table>
</fieldset> 
{if $smarty.get.control neq 'header_banners'}
    <fieldset>
        <legend>{$smarty.const.S_META_TAGS}</legend>
        <table class="fieldset">
            <tr>
                <td class="colLbl" align="right" valign="top">{$smarty.const.S_PAGE_TITLE}</td>
                <td class="colEdt">{$form_data.$k.title.html}</td>
                <td class="colLbl"align="right">&nbsp;</td>
                <td class="colEdt">&nbsp;</td>
            </tr>
            <tr>
                <td class="colLbl" align="right" valign="top">{$smarty.const.S_DESCRIPTION2}</td>
                <td class="colEdt" valign="top">{$form_data.$k.meta_text.html}</td>
                <td class="colLbl" align="right" valign="top">{$smarty.const.S_KEYWORDS}</td>
                <td class="colEdt" valign="top">{$form_data.$k.meta.html}</td>
            </tr>
        </table>
    </fieldset>
{/if}
</div>
{/foreach}