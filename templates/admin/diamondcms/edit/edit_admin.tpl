<h1>{if $smarty.request.id}{$smarty.const.S_ART_EDIT}{else}{$smarty.const.S_ART_ADD}{/if}</h1>
<script src="../templates/js/jquery/jquery-tabs-pack.js" type="text/javascript"></script>
<script src="../templates/js/jquery/jquery.validate.js" type="text/javascript"></script>
{include file="ablock/tiny_mce_config.js.tpl"}
{literal}
<script type="text/javascript">
$(document).ready(function() {

    showHideDivs();
    $($('div.tab_block ul li.here').attr("id")).show();

    $('div.tab_block ul li').click(function()
    {
        showHideDivs();
        $($(this).attr("id")).show();

        $('div.tab_block ul li').removeClass("here");
        $(this).addClass("here");
    });

    function showHideDivs(action)
    {
        $('div.tab_block ul li').each(function(){
            switch(action) {
                default:
                    $($(this).attr('id')).hide();
                    break;
                case(0):
                    $($(this).attr('id')).hide();
                    break;
                case(1):
                    $($(this).attr('id')).show();
                    break;
            }
        });
    }

    // datepicker for #created
    $("#created").datepicker({dateFormat: "dd.mm.yy"});

    // validation
    $("#item_edit").validate({
		rules: {
			mode: "mode_format",
			ch_mode: "mode_format",
			is_deleted: "bool",
			is_protected: "bool",
			{/literal}
			{foreach from=$langs item=i key=k name=langs}
			"{$k}[lang]": "required"{if !$smarty.foreach.langs.last},{/if}
			{/foreach}
			{literal}
    	},
    	messages: {
    		{/literal}
			{foreach from=$langs item=i key=k name=langs}
			"{$k}[lang]": "The field is required"{if !$smarty.foreach.langs.last},{/if}
			{/foreach}
			{literal}
    	}
    });

	$.validator.addMethod("mode_format", function(value) {
		if (value.length != 3 ) return false;
		var spl = value.split('');
		for (var i = 0; i < spl.length; i ++)
		{
			if (spl[i] != 0 && spl[i] != 1) return false;
		}
		return true;
	}, "Incorrect format. Correct e.g. 011");

	$.validator.addMethod("bool", function(value) {
		if (value != 0 && value != 1) return false;
		return true;
	}, "The value can be 0 or 1");

});

</script>

<style type="text/css">
#item_edit label.error {color: #FF0000;}
#item_edit input.error {border: 1px solid #FF0000;}
</style>
{/literal}
{include_php file="admin/path_navigator.inc.php" tpl_name="admin/`$smarty.const.CMS`/edit/path_navigator.tpl"}
<div class="tab_block">
    <ul class="tabs" id="tabs">
        <li class="here" id="#tab-struct">Struct props</li>
        {foreach from=$langs item=i key=k name=langs}
        <li id="#tab-content_{$k}">Local props {if sizeof($langs) > 1}&nbsp;-&nbsp;{$k}{/if}</li>
        <li id="#tab-content_content_{$k}">Content {if sizeof($langs) > 1}&nbsp;-&nbsp;{$k}{/if}</li>
        {/foreach}

    </ul>
</div>

<div id="editContent2" class="block">

    {include file="ablock/error_block.tpl"}

    <form {$form_data.attributes} enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
    {$form_data.javascript}
    {$form_data.hidden}

	<div id="tab-struct" class="contentBlock">
	    <fieldset>

	        <table class="fieldset">
	        <tr>
				<td class="colLbl" align="right">Alias</td>
                <td class="colEdt">{$form_data.alias.html}</td>
                <td class="colLbl" align="right">Ord</td>
                <td class="colEdt">{$form_data.ord.html}</td>
    		</tr>
	        <tr>
				<td class="colLbl" align="right">Control</td>
                <td class="colEdt"><input type="text" name="control" value="{if $smarty.post.control}{$smarty.post.control}{else}{if $default.control neq 'admin'}{$default.control}{else}article{/if}{/if}" /></td>
				<td class="colLbl" align="right">Mode (add/edit/delete)</td>
                <td class="colEdt"><input type="text" name="mode" value="{$smarty.post.mode|default:$default.mode|default:'011'}" /></td>
			</tr>
			<tr>
                <td class="colLbl" align="right">Child Control</td>
                <td class="colEdt"><input type="text" name="ch_control" value="{$smarty.post.ch_control|default:$default.ch_control|default:'article'}" /></td>
				<td class="colLbl" align="right">Child Mode (add/edit/delete)</td>
                <td class="colEdt"><input type="text" name="ch_mode" value="{$smarty.post.ch_mode|default:$default.ch_mode|default:'011'}" /></td>
    		</tr>
	        </table>
	    </fieldset>

	    <fieldset>
	        <table class="fieldset">
	        <tr>
				<td class="colLbl" align="right">Img1</td>
                <td class="colEdt"><input type="text" name="img1" value="{$smarty.post.img1|default:$default.img1}" /></td>
				<td class="colLbl" align="right">Img2</td>
                <td class="colEdt"><input type="text" name="img2" value="{$smarty.post.img2|default:$default.img2}" /></td>
    		</tr>
	        <tr>
                <td class="colLbl" align="right">Img3</td>
                <td class="colEdt"><input type="text" name="img3" value="{$smarty.post.img3|default:$default.img3}" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
    		</tr>
	        <tr>
				<td class="colLbl" align="right">Prop1</td>
                <td class="colEdt"><input type="text" name="sprop1" value="{$smarty.post.sprop1|default:$default.sprop1}" /></td>
				<td class="colLbl" align="right">Prop2</td>
                <td class="colEdt"><input type="text" name="sprop2" value="{$smarty.post.sprop2|default:$default.sprop2}" /></td>
    		</tr>
            <tr>
				<td class="colLbl" align="right">Prop3</td>
                <td class="colEdt"><input type="text" name="sprop3" value="{$smarty.post.sprop3|default:$default.sprop3}" /></td>
				<td class="colLbl" align="right">Prop4</td>
                <td class="colEdt"><input type="text" name="sprop4" value="{$smarty.post.sprop4|default:$default.sprop4}" /></td>
            </tr>
            <tr>
				<td class="colLbl" align="right">Prop5</td>
                <td class="colEdt"><input type="text" name="sprop5" value="{$smarty.post.sprop5|default:$default.sprop5}" /></td>
				<td class="colLbl" align="right">Prop6</td>
                <td class="colEdt"><input type="text" name="sprop6" value="{$smarty.post.sprop6|default:$default.sprop6}" /></td>
            </tr>

	        </table>
	    </fieldset>

	    <fieldset>
	        <table class="fieldset">
	        <tr>
				<td class="colLbl" align="right">Created</td>
                <td class="colEdt">{$form_data.created.html}</td>
				<td class="colLbl" align="right">Is hidden</td>
                <td class="colEdt">{$form_data.is_hidden.html}</td>
    		</tr>
	        <tr>
				<td class="colLbl" align="right">Is protected</td>
                <td class="colEdt"><input type="text" name="is_protected" value="{$smarty.post.is_protected|default:$default.is_protected}" /></td>
				<td class="colLbl" align="right">Is deleted</td>
                <td class="colEdt"><input type="text" name="is_deleted" value="{$smarty.post.is_deleted|default:$default.is_deleted}" /></td>
    		</tr>
	        </table>
	    </fieldset>
	</div>

	{foreach from=$langs item=i key=k name=langs}
	<div id="tab-content_{$k}" class="contentBlock">
	    <fieldset>
	        <table class="fieldset">
	        <tr>
				<td class="colLbl" align="right">Path</td>
                <td class="colEdt"><input type="text" name="{$k}[path]" value="{$smarty.post.$k.path|default:$default.$k.path}" /></td>
				<td class="colLbl" align="right">Lang</td>
                <td class="colEdt"><input type="text" name="{$k}[lang]" value="{$smarty.post.$k.lang|default:$k}" /></td>
    		</tr>
	        <tr>
				<td class="colLbl" align="right">Name</td>
                <td class="colEdt">{$form_data.$k.name.html}</td>
	            <td class="colLbl">&nbsp;</td>
	            <td class="colEdt">&nbsp;</td>
    		</tr>
	        </table>
	    </fieldset>

	    <fieldset>
	        <table class="fieldset">
	        <tr>
				<td class="colLbl" align="right">Prop1</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop1]" value="{$smarty.post.cprop1|default:$default.$k.cprop4}" /></td>
				<td class="colLbl" align="right">Prop2</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop2]" value="{$smarty.post.cprop2|default:$default.$k.cprop2}" /></td>

    		</tr>
	        <tr>
				<td class="colLbl" align="right">Prop3</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop3]" value="{$smarty.post.cprop3|default:$default.$k.cprop3}" /></td>
				<td class="colLbl" align="right">Prop4</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop4]" value="{$smarty.post.cprop4|default:$default.$k.cprop4}" /></td>
    		</tr>
	        <tr>
				<td class="colLbl" align="right">Prop5</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop5]" value="{$smarty.post.cprop5|default:$default.$k.cprop5}" /></td>
				<td class="colLbl" align="right">Prop6</td>
                <td class="colEdt"><input type="text" name="{$k}[cprop6]" value="{$smarty.post.cprop6|default:$default.$k.cprop6}" /></td>
    		</tr>
	        </table>
	    </fieldset>

	    <fieldset>
	        <table class="fieldset">
	        <tr>
	            <td class="colLbl" align="right" valign="top">SEO title</td>
	            <td class="colEdt">{$form_data.$k.title.html}</td>
	            <td class="colLbl"align="right">&nbsp;</td>
	            <td class="colEdt">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="colLbl" align="right" valign="top">SEO meta text</td>
	            <td class="colEdt" valign="top">{$form_data.$k.meta_text.html}</td>
	            <td class="colLbl" align="right" valign="top">SEO keywords</td>
	            <td class="colEdt" valign="top">{$form_data.$k.meta.html}</td>
	        </tr>
	        </table>
	    </fieldset>
	</div>

	<div id="tab-content_content_{$k}" class="contentBlock">
	    <fieldset>
		<div style="margin: 5px" class="tinyMCE">
			Description<br />
		    <div class="clr"><!-- --></div>
		    {$form_data.$k.description.html}
		    <div class="clr"><!-- --></div>
		    <br />

			Content<br />
		    <div class="clr"><!-- --></div>
		    {$form_data.$k.content.html}
		    <div class="clr"><!-- --></div>
		    <br />

			Content 2<br />
		    <div class="clr"><!-- --></div>
		    {$form_data.$k.content2.html}
		    <div class="clr"><!-- --></div>
		</div>
	    </fieldset>
	</div>
	{/foreach}
    {*include file="admin/`$smarty.const.CMS`/edit/tab_prop.tpl"}
    {include file="admin/`$smarty.const.CMS`/edit/tab_local_prop.tpl"}
    {include file="admin/`$smarty.const.CMS`/edit/tab_local_content.tpl"*}

    <div id="bottomBtn">
		<input class="inp save" type="submit" value="{$smarty.const.S_SAVE_CLOSE}" name="save_close"/>
        <input class="inp save" type="submit" value="{$smarty.const.S_SAVE}" name="save"/>
        <input type="button" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;" class="inp undo" onClick="window.location='{$smarty.request.bur|default:$bur}'">
    </div>
    <input type="hidden" value="{$smarty.request.bur}" name="bur">
    <input type="hidden" value="{$smarty.request.id}" name="id">
    <input type="hidden" value="{$smarty.request.pid}" name="pid">

    </form>

</div>