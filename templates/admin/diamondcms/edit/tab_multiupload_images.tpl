<tr class="nodrop nodrag">
    <td><strong>{$smarty.const.S_NAME}</strong></td>
    {if $smarty.request.control eq 'home'}
        <td><strong>{$smarty.const.S_LINK}</strong></td>
    {else}
        <td><strong>{$smarty.const.S_DESCRIPTION}</strong></td>
    {/if}
    {if $smarty.get.control eq 'pricelist'}
    <td><strong>{$smarty.const.S_VENDOR}</strong></td>
    {/if}
    <td width="30px;">&nbsp;</td>
</tr>
{foreach from=$items item=i}
<tr id="{$i.image_id}">
    <td>
        <input type="checkbox" class="check delete_image" />&nbsp;
        <a href="{$smarty.const.SITE_REL}{$path}/{$i.image_name}">{$i.image_name}</a>
    </td>
    <td id="iedit_{$i.image_id}">
        <span>{$i.image_desc}</span>
        <input type="text" value="{$i.image_desc}" style="display: none;" />
    </td>
    {if $smarty.get.control eq 'pricelist'}
    <td id="tiedit_{$i.image_id}">
        <span>{$i.image_title}</span>
        <input class="tit" type="text" value="{$i.image_title}" style="display: none;" />
    </td>
    {/if}
    <td class="iedit_{$i.image_id}" nowrap>
        <a href="javascript:toggleInlineEdit('{$i.image_id}', true);" class="iedit" class="block"><img src="../images/icons/edt.gif" alt="edit" border="0"></a>
        <a href="javascript:updateItem({$i.image_id});" class="inline" style="display: none;"><img src="../images/icons/apply.gif" alt="apply" border="0"></a>
        <a href="javascript:toggleInlineEdit('{$i.image_id}', false);" class="inline" style="display: none;"><img src="../images/icons/cancel2.gif" alt="cancel" border="0"></a>
		{if $smarty.request.control eq 'designer_project'}
		<a href="art_edit.php?control={$smarty.request.control}&id={$smarty.request.art_id}{$smarty.const.CMS_LANG}&iid={$i.image_id}" onclick="javascript: return confirm('Вы переходите на страницу добавления точек на изображение. Все несохраненные будут потеряны. Переходим?')"><img src="../images/icons/go.gif" alt="apply" border="0"></a>
		{/if}
    </td>
</tr>
{/foreach}