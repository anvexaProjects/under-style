<script language="javascript">
{literal}
$(function(){
    var p = $("#inliner");
    var ajax_url = '../admin/ajax_handler.php';
    $(".ico_inline", p).click(function(){
        var tr = $(this).parents("tr").get(0);
        if ($(this).hasClass("ico_edit")) {
            reset_form()
            $("span.i_form", tr).show();
            $("span.i_view", tr).hide();
        }
        if ($(this).hasClass("ico_cancel")) {
            reset_form()
        }
        if ($(this).hasClass("ico_delete")) {
            $.post(
                ajax_url, c_serialize(tr) + '&action=delete',
                function(rs) {
                    if (rs == 1) $(tr).remove();
                }
            );
        }
        if ($(this).hasClass("ico_ok")) {
            $.post(
                ajax_url, c_serialize(tr) + '&action=update',
                function(rs) {
                    if (rs == 1) {
                        var val = '';
                        $(".val_inline", tr).each(function(){
                            $("span.i_view", $(this).parents("td:eq(0)")).text($(this).val());
                            reset_form();
                        });
                    }
                }
            );
        }
        if ($(this).hasClass("ico_add")) {
            $.post(
                ajax_url, c_serialize(tr) + '&action=add',
                function(rs) {
                    if (rs == 1) window.location.reload();
                }
            );
        }

    });

    $("#dict_list").change(function(){
        window.location.href = 'dicts.php?ent=' + $(this).val();
    });
});

function c_serialize(el) {
    var s = [];
    $("input, select, textarea", $(el)).each(function(i){
        s[i] = $(this).attr("name") + '=' + $(this).val();
    });
    return s.join('&');
}

function reset_form() {
    $("span.i_form").hide();
    $("span.i_view").show();
}

</script>
<style type="text/css">
    .i_form {display: none;}
    #editContent .edit_tbl td span {font-weight: normal;}
</style>
{/literal}

<h1>{$smarty.const.S_DICTS}{if $ent} ({$ents.$ent}){/if}</h1>
<div id="editContent" class="block">
    {$smarty.const.S_SELECT_DICT}: 
    <select id="dict_list">
        {foreach from=$ents item=i key=k}
        <option value="{$k}"{if $k eq $smarty.get.ent} selected="selected"{/if}>{$i}</option>
        {/foreach}
    </select>
    <br /><br />
    <table class="edit_tbl" id="inliner">
        <tr>
            <th>{$smarty.const.S_VALUE}</th>
            <th>&nbsp;</th>
        </tr>
        {foreach from=$items item=i}
        <tr>
            <td>
                <span class="i_view">{$i.value_nl}</span>
                <span class="i_form">
                    <input type="text" class="val_inline" name="value_nl" value="{$i.value_nl}" />
                </span>
            </td>
            <td width="1%" nowrap>
                <span class="i_view">
                    <a href="javascript:;"><img src="../images/icons/edt.gif" alt="edit" class="ico_inline ico_edit"></a>
                    <a href="javascript:;"><img src="../images/icons/delete.gif" alt="delete" class="ico_inline ico_delete"></a>
                </span>
                <span class="i_form">
                    <a href="javascript:;"><img src="../images/icons/apply.gif" alt="ok" class="ico_inline ico_ok"></a>
                    <a href="javascript:;"><img src="../images/icons/cancel2.gif" alt="cancel" class="ico_inline ico_cancel"></a>
                </span>
                <input type="hidden" name="id" value="{$i.id}" />
                <input type="hidden" name="entity_id" value="{$i.entity_id}" />
            </td>
        </tr>
        {/foreach}
        <tr>
            <td>
                <input type="text" class="val_inline" name="value_nl" value="" />
            </td>
            <td width="1%" nowrap>
                <a href="javascript:;"><img src="../images/icons/add.gif" alt="ok" class="ico_inline ico_add"></a>
                <input type="hidden" name="entity_id" value="{$ent}" />
                <input type="hidden" name="entity" value="{$ents.$ent}" />
            </td>
        </tr>
    </table>
</div>