<h1>{$smarty.const.S_ROBOTS_EDIT}</h1>
<div class="tab_block">
    <ul>
        <li id="#tab-main" class="here">{$smarty.const.S_ROBOTS_E}</li>
    </ul>
</div>
<div id="editContent2" class="block">
    {if $smarty.get.s|default:0 eq 1}<b>{$smarty.const.FLB_OPERATION_OK}</b>{/if}
	<div style="margin: 0px 17px 5px 17px">
	{include file="ablock/error_block.tpl"}
	</div>
    <form action="" enctype="multipart/form-data" method="post">
    <div id="tab-main">
        <fieldset>
            <legend>{$smarty.const.S_ROBOTS_E}</legend>
            <div>
                <table class="fieldset">
                    <tr>
                        <TEXTAREA NAME="robots" style="height: 300px">{$robots} </TEXTAREA>                                     
                    </tr>
                
                </table>
            </div>
        </fieldset>
</div>
        <div class="edit_action_btn">
        	 <input class="inp save" type="submit" value="{$smarty.const.S_SAVE_CLOSE}" name="save_close"/>
             <input class="inp save" type="submit" value="{$smarty.const.S_SAVE}" name="save"/>
             <input type="button" value="&nbsp;&nbsp;{$smarty.const.S_CANCEL}&nbsp;&nbsp;" class="inp undo" onClick="window.location='{$smarty.get.bur|default:'index.php'}'">
        </div>
    </form>
</div>
