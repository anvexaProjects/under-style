<div class="mainMenuFrame">
<div id="mainMenu">
	<ul class="lMenu">
        <li  class="here" ><a href="http://under-style.ru/pub/updater.php" target="_blank">Обновить каталог</a></li>
		{assign var="current_params" value="&"|@explode:$smarty.server.QUERY_STRING}
	    {foreach from=$amenu item=item name=menu}
        {if $smarty.session.role eq $smarty.const.SALERS_REDACTOR || $smarty.session.role eq $smarty.const.DESIGNERS_REDACTOR}
            {if $item.title eq 'Карта сайта' || $item.title eq 'Выход'}
                <li {if $item.path eq $script_file} class="here" {/if}>
                    {if $item.path eq $script_file}
                        <span title="{$item.title}">{$item.title}</span>
                    {else}
                        <a href="{$item.path}" title="{$item.title}">{$item.title}</a>
                    {/if}
                    {if !empty($item.child)}
                        <ul>
                            {foreach from=$item.child item=child name=submenu}

                                {assign var="script_query" value=$child.path|@parse_url:$smarty.const.PHP_URL_QUERY}
                                {assign var="script_params" value="&"|@explode:$script_query}
                                {assign var="diff_params" value=$current_params|@array_diff:$script_params}

                                {if $child.path|@parse_url:$smarty.const.PHP_URL_PATH eq $script_file && !($diff_params)}
                                    <li class="here"><span title="{$child.title}">{$child.title}</span></li>
                                {else}
                                    <li><a href="{$child.path}" title="{$child.title}">{$child.title}</a></li>
                                {/if}
                            {/foreach}
                        </ul>
                    {/if}
                </li>
            {/if}
        {else}
            <li {if $item.path eq $script_file} class="here" {/if}>
                {if $item.path eq $script_file}
                    <span title="{$item.title}">{$item.title}</span>
                {else}
                    <a href="{$item.path}" title="{$item.title}">{$item.title}</a>
                {/if}
                {if !empty($item.child)}
                    <ul>
                        {foreach from=$item.child item=child name=submenu}

                            {assign var="script_query" value=$child.path|@parse_url:$smarty.const.PHP_URL_QUERY}
                            {assign var="script_params" value="&"|@explode:$script_query}
                            {assign var="diff_params" value=$current_params|@array_diff:$script_params}

                            {if $child.path|@parse_url:$smarty.const.PHP_URL_PATH eq $script_file && !($diff_params)}
                                <li class="here"><span title="{$child.title}">{$child.title}</span></li>
                            {else}
                                <li><a href="{$child.path}" title="{$child.title}">{$child.title}</a></li>
                            {/if}
                        {/foreach}
                    </ul>
                {/if}
            </li>
        {/if}
	    {/foreach}
	    <li class="last">&nbsp;</li>
	</ul>
</div>
</div>




	