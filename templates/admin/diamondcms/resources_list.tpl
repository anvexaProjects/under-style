<h1>{$smarty.const.S_RESOURCES}</h1>
<h2>{if $mode eq 'strings'}{$smarty.const.S_CONFIG_STRINGS}{elseif $mode eq 'labels'}{$smarty.const.S_CONFIG_LABELS}{/if}</h2>
{literal}
<script type="text/javascript">/*<![CDATA[*/function editConfigItem(c){var a=$("#"+c+"_content").attr("value");var b=$("td ."+c+"_txt:eq(0)").text();if((a===undefined)||(a=="")){alert("{/literal}{$smarty.const.FLB_REQUIRED_VALUE}{literal}");return}if(a==b){toggleEditControls(c,false);return}if(!is_validchars_key(c)){alert("{/literal}{$smarty.const.FLB_BAD_RESOURCEKEY}{literal}");return}$.get("../admin/resources_ajax.php",{operation:"edit",id:c,m:"{/literal}{$mode}{literal}",l:"{/literal}{$lang}{literal}",content:a},function(f){if(!f.result){alert("{/literal}{$smarty.const.FLB_OPERATION_NO_OK}{literal}\n"+f.reply)}else{var e=$("."+c+"_txt:eq(0)").parent();var d=e.css("background-color");e.css("background-color","#33CC66");$("."+c+"_txt:eq(0)").text(a);toggleEditControls(c,false);setTimeout(function(){e.css("background-color",d)},300)}},"json")}function is_validchars_key(b){if(b==""){return false}var a=/[^A-Za-z0-9_]/;if(a.test(b)){return false}else{a=/^[A-Za-z]/;if(!a.test(b)){return false}return true}}function deleteConfigItem(c){if(window.confirm("{/literal}{$smarty.const.CF_DELETE_RESOURCE}{literal}")==true){var b=$("tr td."+c+"_txt").parent();var a=b.css("background-color");b.css("background-color","#CC4B4B");$.get("../admin/resources_ajax.php",{operation:"delete",id:c,m:"{/literal}{$mode}{literal}",l:"{/literal}{$lang}{literal}"},function(d){if(!d.result){alert("{/literal}{$smarty.const.FLB_OPERATION_NO_OK}{literal}\n"+d.reply);b.css("background-color",a)}else{b.remove()}},"json")}}function add_new_config_item(){var a=$("input:text[name='key']").val();if(!is_validchars_key(a)){alert("{/literal}{$smarty.const.FLB_BAD_RESOURCEKEY}{literal}");return}else{var b=$("input:text[name='val']").val();if(b==""){alert("{/literal}{$smarty.const.FLB_REQUIRED_VALUE}{literal}");return}else{$.get("../admin/resources_ajax.php",{operation:"addnew",id:a,m:"{/literal}{$mode}{literal}",l:"{/literal}{$lang}{literal}",content:b},function(f){if(!f.result){alert("{/literal}{$smarty.const.FLB_OPERATION_NO_OK}{literal}\n"+f.reply)}else{a=a.toUpperCase();clean_form_to_add_new_item();if(f.operation=="add"){var e=($("tr #cloneable + tr").hasClass("grey"));$("tr #cloneable").after(generate_added_row(a,b,e))}else{if(f.operation=="update"){var d=$("tr td."+a+"_txt").parent();var c=d.css("background-color");d.css("background-color","#33CC66");$("tr td."+a+"_txt:eq(0)").text(b);$("#"+a+"_content").attr("value",b);setTimeout(function(){d.css("background-color",c)},300)}}}},"json")}}}function generate_added_row(e,c,d){var a=$("#cloneable").clone();a.removeAttr("id");if(!d){a.addClass("grey")}var b=a.children("td");b.eq(0).text(e);b.eq(1).text(c).addClass(e+"_txt");b.eq(2).addClass(e+"_inp");b.eq(2).children("input").attr("id",e+"_content");b.eq(2).children("input").val(c);b.eq(3).addClass(e+"_txt");b.eq(3).children("a:eq(0)").attr("href","javascript:toggleEditControls('"+e+"', true);");b.eq(3).children("a:eq(1)").attr("href","javascript:deleteConfigItem('"+e+"', true);");b.eq(4).addClass(e+"_inp");b.eq(4).children("a:eq(0)").attr("href","javascript:editConfigItem('"+e+"', true);");b.eq(4).children("a:eq(1)").attr("href","javascript:toggleEditControls('"+e+"', false);");a.show();return a}function clean_form_to_add_new_item(){$("input:text[name='key']").val("");$("input:text[name='val']").val("")}function toggleEditControls(b,a){if(true==a){$("."+b+"_txt").hide();$("."+b+"_inp").show();$(".addnewbtn").hide()}else{$("."+b+"_txt").show();$("."+b+"_inp").hide();$(".addnewbtn").show()}};/*]]>*/</script>
{/literal}
<div id="editContent" class="block">
<table class="edit_tbl res_editor">
<tr class="grey">
<th width="30%">{$smarty.const.S_LIST}</th>
<th>{$smarty.const.S_DESCRIPTION}</th>
<th width="8%">&nbsp;</th>
</tr>
<tr id="addnewrow" class="grey">
<td class="colInp"><input name="key" type="text"></td>
<td class="colInp"><input name="val" type="text"></td>
<td align="right" class="addnewbtn" colspan="3">
<a href="javascript:add_new_config_item()"><img src="../images/icons/add.gif" alt="add new resource" border=0></a>
</td>
</tr>
<tr id="cloneable" style="display:none">
<td></td>
<td></td>
<td style="display:none"><input type="text" value="" /></td>
{strip}
<td align="right">
<a href=""><img src="../images/icons/edt.gif" alt="edit" border=0></a>
<a href=""><img src="../images/icons/delete.gif" alt="delete" border=0></a>
</td>
{/strip}
<td style="display:none" nowrap="nowrap" align="right">
<a href=""><img src="../images/icons/apply.gif" alt="apply" border=0></a>
<a href=""><img src="../images/icons/cancel2.gif" alt="cancel" border=0></a>
</td>
</tr>
{foreach item=i name=items key=k from=$items}
<tr{cycle values=', class="grey"'}>
<td>{$k}</td>
<td class="{$k}_txt">{$i.resVal|escape:'html'}</td>
<td class="{$k}_inp" style="display:none"><input type="text" value="{$i.resVal|escape:'html'}" id="{$k}_content" /></td>
{strip}
<td class="{$k}_txt" align="right">
<a href="javascript:toggleEditControls('{$k}',true)"><img src="../images/icons/edt.gif" alt="edit" border=0></a>
{if $i.canDelete eq '1'}
<a href="javascript:deleteConfigItem('{$k}')"><img src="../images/icons/delete.gif" alt="delete" border=0></a>
{/if}
</td>
{/strip}
<td class="{$k}_inp" style="display:none" nowrap="nowrap" align="right">
<a href="javascript:editConfigItem('{$k}')"><img src="../images/icons/apply.gif" alt="apply" border=0></a>
<a href="javascript:toggleEditControls('{$k}',false)"><img src="../images/icons/cancel2.gif" alt="cancel" border=0></a>
</td>
</tr>
{/foreach}
</table>
</div>