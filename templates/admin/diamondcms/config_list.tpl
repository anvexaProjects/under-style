<h1>{$smarty.const.S_CONFIG}</h1>
{literal}
<script type="text/javascript">
function editConfigItem(id) {
	var content = $('#' + id + '_content').val();
	var comment = $('#' + id + '_comment').val();
	if (content === undefined) {
		alert('{/literal}{$smarty.const.FLB_ADD_CONFIG}{literal}');
		return false;
	}
	if (comment === undefined) comment = '';
	$.get(
		'../admin/config.php',
		{id: id, m: '{/literal}{$mode}{literal}', l: '{/literal}{$lang}{literal}', content: content, comment: comment},
		function(){
			$('.' + id + '_txt:eq(0)').text(content);
			$('.' + id + '_txt:eq(1)').text(comment);
			toggleEditControls(id, false);
		}	
	);
}

function toggleEditControls(id, hide_txt) {
	if (true == hide_txt) {
		$('.' + id + '_txt').hide();
		$('.' + id + '_inp').show();
	} else {
		$('.' + id + '_txt').show();
		$('.' + id + '_inp').hide();
		
	}
	
}
</script>
{/literal}
<div id="editContent" class="block">
    <table  class="edit_tbl">
        <tr class="grey">
            <th width="30%">{$smarty.const.S_LIST}</th>
            <th>{$smarty.const.S_DESCRIPTION}</th>
            <th width="30%">{$smarty.const.S_COMMENTS}</th>
            <th width="8%">&nbsp;</th>
        </tr>
        {foreach item=i name=items from=$items key=k}     
        <tr{cycle values=', class="grey"'}>
            <td>{$k}</td>
            <td class="{$k}_txt">{$i.content}</td>
            <td class="{$k}_txt">{$i.attr.comment}</td>
            <td class="{$k}_inp" style="display: none;"><input type="text" value="{$i.content}" id="{$k}_content" /></td>
            <td class="{$k}_inp" style="display: none;"><input type="text" value="{$i.attr.comment}" id="{$k}_comment" /></td>
            <td class="{$k}_txt" align="right">
            	<a href="javascript:toggleEditControls('{$k}', true);"><img src="../images/icons/edt.gif" alt="edit" border=0></a>
            </td>
            <td class="{$k}_inp"  style="display: none;" nowrap="nowrap"  align="right">
            	<a href="javascript:editConfigItem('{$k}');"><img src="../images/icons/apply.gif" alt="apply" border=0></a>
            	<a href="javascript:toggleEditControls('{$k}', false);"><img src="../images/icons/cancel2.gif" alt="cancel" border=0></a>
            </td>
            
            {*<td><a href="javascript:editConfigItem('{$k}');"><img src="../images/icons/edt.gif" alt="edit" border=0></a></td>*}
        </tr>
        {/foreach}
    </table>
</div>