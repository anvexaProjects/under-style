<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>DiamondCMS - {$smarty.const.S_LOGIN_PAGE}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Keywords" content="" />
	<meta name="Description" content="" />
	<link rel="stylesheet" type="text/css" href="../templates/css/{$smarty.const.CMS}/main.css" />
 
</head>
<body>
<div id="minWidth">
    <div id="min-width">
        <div id="center"  align="center">
            <div style="width: 380px; margin-top: 150px">
                <div id="mainContent">
                    <div id="editContent2" class="block" style="padding-left: 0px">
                        <form name="" method="post" action="">
                            <img src="../images/admin/logo.gif" width="201" height="48" alt="" style="margin: 20px 0px 10px 55px" />
                            <table style="width: 100%; margin-left: 28px;">
                                <tr>
                                    <td align="right" style="width: 5%"><label for="login">{$smarty.const.S_LOGIN}:</label></td>
                                    <td style="width: 68%"><input type="text" id="login" name="login" /></td>
                                    <td style="width: 30%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right"><label for="pass">{$smarty.const.S_PASSWORD}:</label></td>
                                    <td><input type="password" id="pass" name="pass" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                {if $error_login}
                                <tr>
                                    <td></td>
                                    <td align="left" colspan="2"><div class="err">{$error_login}</div></td>
                                </tr>
                                {/if}
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">
                                        <div class="edit_action_btn">
                                            <input type="submit" value="{$smarty.const.S_ENTER}" class="inp save" name="submit_login" />
                                            <!-- input type="reset" value="Отменить" class="inp undo" / -->
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>