<h1>{$smarty.const.S_FEEDBACKS}</h1>
<div id="editContent" class="block">
    <form name="" method="post" action="">
        <table cellspacing="0" style="border-bottom-width: 0px; padding-right: 0px;" class="edit_tbl">
            <tr> 
        		<th><a href="{$aSorting.name.url}" class="tbl_header">{$smarty.const.S_NAME}</a>&nbsp;<img src="../images/icons/s_{$aSorting.name.img}"></th>
                <th><a href="{$aSorting.email.url}" class="tbl_header">{$smarty.const.S_EMAIL}</a>&nbsp;<img src="../images/icons/s_{$aSorting.email.img}"></th>
                <th width="40%"><a href="{$aSorting.message.url}" class="tbl_header">{$smarty.const.S_MESSAGE}</a>&nbsp;<img src="../images/icons/s_{$aSorting.message.img}"></th>
                <th><a href="{$aSorting.is_subs.url}" class="tbl_header">{$smarty.const.S_IS_SUBS}</a>&nbsp;<img src="../images/icons/s_{$aSorting.is_subs.img}"></th>
                <th><a href="{$aSorting.created.url}" class="tbl_header">{$smarty.const.S_CREATED}</a>&nbsp;<img src="../images/icons/s_{$aSorting.created.img}"></th>
	       		<th>&nbsp;</th>
            </tr>
            <tr>
        		<th valign="top" align="left" class="tblEm">{filter data=$aFilter.name class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.email class="htmlInput"}</th>
                <th>&nbsp;</th>
                <th valign="top" align="left" class="tblEm">{filter data=$aFilter.is_subs class="htmlInput"}</th>
                <th valign="top" align="left" class="tblEm" nowrap>
                    {filter data=$aFilter.created_from class="htmlInput" style="width: 70px" id="from"}
                    {filter data=$aFilter.created_to class="htmlInput" style="width: 70px" id="to"}
                </th>
        		<th valign="top" align="left" width="1%" nowrap  class="tblEm">
	       			<input type="image" name="act[{$aFilter._submit}]" alt="Show" value="Show"  src="../images/icons/search.gif" style="border: 0px"/>&nbsp;&nbsp;
        			<input type="image" name="act[{$aFilter._reset}]" alt="Reset" value="Reset" src="../images/icons/refresh2.gif"  style="border: 0px" />
        		</th>
            </tr>       
            {foreach item="item" name="item" from=$items}     
            <tr {cycle values=', class="grey"'}>
                <td>{$item.name}</td>
                <td><a href="mailto:{$item.email}">{$item.email}</a></td>
                <td>{$item.message|@strip_tags|nl2br}</td>
                <td>{if $item.is_subs}Y{else}N{/if}</td>
                <td>{$item.created|date_format:"%d.%m.%Y %H:%M"}</td>
                <td nowrap>
                    <a href="feedback_del.php?id={$item.id}&page={$smarty.get.page}"><img src="../images/icons/delete.gif" alt="delete"></a>
                </td>
            </tr>
            {/foreach}
            {if $aPaging.totalPages>1}
            <tr><td colspan="6"><div align="center">{include file="ablock/core_pager.tpl"}</div></td></tr>
            {/if} 
        </table>
    </form>
</div>