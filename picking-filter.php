<?

include_once 'core/settings.inc';

$template = sprintf("mysql:host=localhost;dbname=%s;", MAINDB);
$dbh = new PDO($template, DBUSER, DBPASSWD);

$sth = $dbh->prepare("
    SELECT t1.sprop13
    FROM art_struct AS t1
    WHERE parent_id IN (
        SELECT t2.struct_id
        FROM art_struct AS t1
        LEFT JOIN art_struct AS t2 ON t2.parent_id = t1.struct_id
        WHERE t1.struct_id = 55
    ) AND t1.sprop13 != ''
    GROUP BY t1.sprop13
");
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_COLUMN, 0);


?>
